// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

extension NSManagedObject {

    static func deleteCache(cacheName: String) {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: cacheName)
    }

    static func sortDescriptor(forStringKey key: String) -> NSSortDescriptor {
        return NSSortDescriptor(key: key, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
    }

    static func fetch<T>(_ request: NSFetchRequest<T>) -> [T] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(request)
        } catch {
            return [T]()
        }
    }

    static func entityDescription(forEntityName name: String) -> NSEntityDescription? {
        let context = DBHelper.instance.getManagedObjectContext()
        return NSEntityDescription.entity(forEntityName: name, in: context)
    }

    static func deleteAll(entityName: String) {
        let context = DBHelper.instance.getManagedObjectContext()
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

        do {
            try context.execute(deleteRequest)
            DBHelper.instance.saveContext()
        } catch {
            print ("There was an error")
        }
    }
}

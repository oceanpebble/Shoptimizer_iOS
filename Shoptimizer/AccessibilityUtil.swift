// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class AccessibilityUtil {
    static var identifierForDoneItems: String {
        return "DONE"
    }

    static var identifierForLowItems: String {
        return "LOW"
    }

    static var identifierForOutItems: String {
        return "OUT"
    }

    static var identifierForStockedItems: String {
        return "STOCKED"
    }

    static var identifierForSelectedCategory: String {
        return "SELECTED"
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

public class SuppliesItemCategory: NSManagedObject {

    // MARK: - Constants

    static let CACHE_NAME = "suppliesItemCategories"
    static let ENTITY_NAME = "SuppliesItemCategory"
    static let COL_NAME = "name"
    static let REF_SUPPLITES_ITEMS_FOR_CATEGORY = "suppliesItemsForCategory"

    // MARK: - Create

    static func insert(name: String?) -> SuppliesItemCategory {
        let suppliesItemCategory = SuppliesItemCategory(entity: entityDescription()!, insertInto: DBHelper.instance.getManagedObjectContext())
        suppliesItemCategory.name = name
        DBHelper.instance.saveContext()
        return suppliesItemCategory
    }

    // MARK: - Read

    static func categories() -> [SuppliesItemCategory] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(SuppliesItemCategory.fetchRequest())
        } catch {
            return [SuppliesItemCategory]()
        }
    }

    static func categories(for suppliesItem: SuppliesItem?) -> [SuppliesItemCategory] {
        guard let suppliesItem = suppliesItem else { return [SuppliesItemCategory]()}
        
        let fetchRequest: NSFetchRequest<SuppliesItemCategory> = SuppliesItemCategory.fetchRequest()

        let predicate = NSPredicate(format: "%K contains %@", SuppliesItemCategory.REF_SUPPLITES_ITEMS_FOR_CATEGORY, suppliesItem)
        fetchRequest.predicate = predicate

        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
        } catch {
            return [SuppliesItemCategory]()
        }
    }

    static func categories(for searchString: String) -> [SuppliesItemCategory] {
        let request = standardFetchRequest()
        request.predicate = predicate(for: searchString)
        return fetch(request)
    }

    static func categoriesWithDelegate(_ delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<SuppliesItemCategory> {

    NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: SuppliesItemCategory.CACHE_NAME)
        let fetchRequest: NSFetchRequest<SuppliesItemCategory> = SuppliesItemCategory.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: SuppliesItemCategory.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptionName = NSSortDescriptor(key: SuppliesItemCategory.COL_NAME, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptionName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath: nil, cacheName: SuppliesItemCategory.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching supplies item categories.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController
    }

    static func existsCategory(name: String) -> Bool {
        let request = standardFetchRequest()
        request.predicate = NSPredicate(format: "%K = %@", COL_NAME, name)
        return fetch(request).count > 0
    }

    // MARK: - Delete

    static func deleteAll() {
        deleteAll(entityName: ENTITY_NAME)
    }

    // MARK: - Util

    public static func predicate(for searchString: String) -> NSPredicate? {
        if searchString.count >= 2 {
            return NSPredicate(format: "%K contains[cd] %@", COL_NAME, searchString)
        } else {
            return nil
        }
    }

    private static func standardFetchRequest() -> NSFetchRequest<SuppliesItemCategory> {
        let fetchRequest: NSFetchRequest<SuppliesItemCategory> = SuppliesItemCategory.fetchRequest()
        fetchRequest.entity = entityDescription()
        return fetchRequest
    }

    private static func entityDescription() -> NSEntityDescription? {
        return entityDescription(forEntityName: SuppliesItemCategory.ENTITY_NAME)
    }
}

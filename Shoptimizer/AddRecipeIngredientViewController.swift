// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class AddRecipeIngredientViewController : UIViewController, AddEntityViewDelegate {
    func setTitle() {
        
    }
    

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    @IBOutlet fileprivate weak var ingredientNameTextField: UITextField!
    @IBOutlet fileprivate weak var ingredientQuantityTextField: UITextField!
    @IBOutlet fileprivate weak var ingredientUnitPicker: UIPickerView!
    @IBOutlet weak var includeInListSwitch: UISwitch!
    @IBOutlet weak var includeInSearchSwitch: UISwitch!
    @IBOutlet fileprivate weak var saveButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!

    var fetchedIngredient: RecipeIngredient?
    var recipeForIngredient: Recipe?
    var editingMode: EditingMode

    // *********************************************************
    //
    // MARK: - UIViewController
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextFieldDelegates()
        setTextFieldContents()
        setUnitPickerValue()
        setSwitchValues()
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit Ingredient", comment: "")
        }
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        addDoneButtonToKeyboard(forTextFields: [ingredientNameTextField, ingredientQuantityTextField], forTextViews: [], targeting: self, using: #selector(doneButtonAction))
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews, otherwise the scrollview will be positioned too low on the screen
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let specificSender = sender as? UIBarButtonItem, specificSender != saveButton {
            return true;
        }

        guard let newIngredientName = ingredientNameTextField.text?.trimmingCharacters(in: .whitespaces), newIngredientName.isNotEmpty else {
            let enterNameString = NSLocalizedString("Please enter a name.", comment: "")
            GeneralHelper.presentInformationDialogWithText(enterNameString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false;
        }

        if (self.editingMode == .create || newIngredientName != self.fetchedIngredient?.name)
            && RecipeIngredient.isIngredientAvailable(withName: newIngredientName, inRecipe: self.recipeForIngredient!) {
            let ingredientAlreadyExistsString = NSLocalizedString("An ingredient with this name already exists for this recipe.", comment: "")
            GeneralHelper.presentInformationDialogWithText(ingredientAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false;
        }

        guard let quantity = GeneralHelper.ensureValidDouble(fromString: self.ingredientQuantityTextField.text, allowZero: true) else {
            let quantityInvalidString = NSLocalizedString("Please enter a number >= 0.0 for the amount.", comment: "")
            GeneralHelper.presentInformationDialogWithText(quantityInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        let unit = UnitPickerHelper.getUnitPickerData()[self.ingredientUnitPicker.selectedRow(inComponent: 0)]
        let includeInList = NSNumber(value: self.includeInListSwitch.isOn)
        let includeInSearch = NSNumber(value: self.includeInSearchSwitch.isOn)

        if self.editingMode == .edit {
            self.fetchedIngredient?.name = newIngredientName
            self.fetchedIngredient?.quantity = quantity
            self.fetchedIngredient?.unit = unit
            self.fetchedIngredient?.includeInList = includeInList
            self.fetchedIngredient?.includeInSearch = includeInSearch
            DBHelper.instance.saveContext()
        } else {
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: newIngredientName, quantity: quantity, andUnit: unit, andIncludeInList: includeInList, andIncludeInSearch: includeInSearch, forRecipe: self.recipeForIngredient!, intoContext: DBHelper.instance.getManagedObjectContext())
            DBHelper.instance.saveContext()
        }
        
        return true
    }

    // *********************************************************
    //
    // MARK: - View Controller Life Cycle
    //
    // *********************************************************

    func setTextFieldDelegates() {
        self.ingredientNameTextField.delegate = UITextFieldHelper.instance
        self.ingredientQuantityTextField.delegate = UITextFieldHelper.instance
    }

    func setTextFieldContents() {
        self.ingredientNameTextField.text = self.fetchedIngredient?.name

        if let quantity = self.fetchedIngredient?.quantity,
            let convertedQuantity = GeneralHelper.decimalFormatter().string(from: quantity) {
            self.ingredientQuantityTextField.text = String(convertedQuantity)
        }
    }

    func setTextViewDelegates() {
        // no text views in this view
    }

    func setTextViewContents() {
        // no text views in this view
    }

    fileprivate func setUnitPickerValue() {
        self.ingredientUnitPicker.dataSource = self;
        self.ingredientUnitPicker.delegate = self;

        if let ingredient = self.fetchedIngredient {
            let unit = UnitPickerHelper.translate(unit: ingredient.unit!)
            self.ingredientUnitPicker.selectRow(UnitPickerHelper.getIndexOfUnit(unit), inComponent:0, animated:false)
        }
    }

    fileprivate func setSwitchValues() {
        if let includeInList = self.fetchedIngredient?.includeInList {
            self.includeInListSwitch.isOn = includeInList.boolValue
        } else {
            self.includeInListSwitch.isOn = false
        }

        if let includeInSearch = self.fetchedIngredient?.includeInSearch {
            self.includeInSearchSwitch.isOn = includeInSearch.boolValue
        } else {
            self.includeInSearchSwitch.isOn = false
        }
    }

    /* Unfortunetly, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let info = (aNotification as NSNotification).userInfo,
           let value = info[UIResponder.keyboardFrameBeginUserInfoKey] {
            var kbHeight = (value as AnyObject).cgRectValue.size.height
            if kbHeight == 0.0 {
                kbHeight = (info[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
            }
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbHeight, right: 0.0)
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            var aRect = scrollView.frame;
            aRect.size.height -= kbHeight;

            if let activeTextField = UITextFieldHelper.instance.activeField,
                !aRect.contains(activeTextField.frame.origin) {
                scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                return
            }

            if let activeTextView = UITextViewHelper.instance.activeView {
                let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
                let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
                if !aRect.contains(transformedLowerLeftOfActiveTextView) {
                    let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                    let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height - 50) // 50 = height of done button toolbar
                    scrollView.scrollRectToVisible(rect, animated: true)
                }
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func doneButtonAction() {
        UITextFieldHelper.instance.activeField?.resignFirstResponder()
        UITextViewHelper.instance.activeView?.resignFirstResponder()
        keyboardWillBeHidden(Notification(name: Notification.Name(rawValue: "foo"))) // name is irrelevant since notification is not used
    }
}

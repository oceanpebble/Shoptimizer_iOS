// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class RecipeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    @IBOutlet weak var portionsLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var workingTimeLabel: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!

    var recipe : Recipe!

    let cellTitles = [NSLocalizedString("Ingredients", comment: ""), NSLocalizedString("Preparation", comment: ""), NSLocalizedString("Categories", comment: "")]

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = self.recipe.name
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        portionsLabel.text = self.recipe.portions?.stringValue
    }

    override func viewWillAppear(_ animated: Bool) {
        workingTimeLabel.text = GeneralHelper.convertTimeDurationInt(recipe.workingTimeInMinutes())
        totalTimeLabel.text = GeneralHelper.convertTimeDurationInt(recipe.totalTimeInMinutes())
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sender = sender as? UITableViewCell else { return }

        if sender.textLabel?.text == NSLocalizedString("Ingredients", comment: "") {
            let targetController = segue.destination as? RecipeIngredientsTableViewController
            targetController?.recipe = self.recipe
        } else if sender.textLabel?.text == NSLocalizedString("Preparation", comment: "") {
            let targetController = segue.destination as? RecipePreparationTableViewController
            targetController?.recipe = self.recipe
        } else if sender.textLabel?.text == NSLocalizedString("Categories", comment: "") {
            let targetController = segue.destination as? RecipeCategoriesTableViewController
            targetController?.recipe = self.recipe
        }

        return
    }

    // *********************************************************
    //
    // MARK: - UITableViewDataSource
    //
    // *********************************************************    


    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Details", comment: "")
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellTitles.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeDetailsCell", for: indexPath)
        cell.textLabel?.text = cellTitles[indexPath.row]

        return cell
    }

    // *********************************************************
    //
    // MARK: - UITableViewDelegate
    //
    // *********************************************************

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let text = tableView.cellForRow(at: indexPath)?.textLabel?.text,
            let cell = tableView.cellForRow(at: indexPath) else { return }

        switch text {
        case NSLocalizedString("Ingredients", comment: ""): self.performSegue(withIdentifier: "showRecipeIngredientsSegue", sender: cell)
        case NSLocalizedString("Preparation", comment: ""): self.performSegue(withIdentifier: "showRecipePreparationSegue", sender: cell)
        case NSLocalizedString("Categories", comment: ""): self.performSegue(withIdentifier: "showRecipeCategoriesSegue", sender: cell)
        default: break
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}

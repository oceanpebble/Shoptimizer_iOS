// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class ShoppingListItemRepository {
    
    func updateItem(_ item: ShoppingListItem, done: Bool) {
        item.setValue(NSNumber(value: done), forKey: ShoppingListItemEntity.COL_DONE)
    }
    
    func deleteItem(_ item: ShoppingListItem) {
        DBHelper.instance.getManagedObjectContext().delete(item)
        DBHelper.instance.saveContext()
    }
    
    func nameFor(_ item: ShoppingListItem) -> String {
        return item.itemName!
    }
    
    func shopNameFor(_ item: ShoppingListItem) -> String? {
        return item.shopName
    }
    
    func amountQuantityFor(_ item: ShoppingListItem) -> NSNumber? {
        return item.itemAmountQuantity
    }
    
    func amountUnitFor(_ item: ShoppingListItem) -> String? {
        return item.itemAmountUnit
    }
    
    func doneFor(_ item: ShoppingListItem) -> Bool {
        return item.done?.boolValue ?? false
    }
    
    func brandNameFor(_ item: ShoppingListItem) -> String? {
        return item.brandName
    }
    
    func priceFor(_ item: ShoppingListItem) -> NSNumber? {
        return item.itemPrice
    }
    
    func currencyFor(_ item: ShoppingListItem) -> String? {
        return item.itemCurrency
    }
    
    func notesFor(_ item: ShoppingListItem) -> String? {
        return item.notes
    }
    
    func listFor(_ item: ShoppingListItem) -> ShoppingList {
        return item.listForItem!
    }
    
    func undoneItemsNotOnAggregatedListWithName(_ name: String?, andShop shop: String?) -> [ShoppingListItem] {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListItemEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        var predicate = NSPredicate()
        if let name = name, let shop = shop {
            predicate = NSPredicate(format:"%K = %@ and %K = %@ and %K = %@ and %K != %@", ShoppingListItemEntity.COL_ITEM_NAME, name, ShoppingListItemEntity.COL_DONE, NSNumber(booleanLiteral: false), ShoppingListItemEntity.COL_SHOP_NAME, shop, ShoppingListItemEntity.REF_LISTFORITEM, AggregatedListHelper.instance.getAggregatedList())
        } else if let name = name {
            predicate = NSPredicate(format:"%K = %@ and %K = %@ and %K != %@", ShoppingListItemEntity.COL_ITEM_NAME, name, ShoppingListItemEntity.COL_DONE, NSNumber(booleanLiteral: false), ShoppingListItemEntity.REF_LISTFORITEM, AggregatedListHelper.instance.getAggregatedList())
        }

        fetchRequest.predicate = predicate

        let sortDescriptorItemName = NSSortDescriptor(key: ShoppingListItemEntity.COL_ITEM_NAME, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorItemName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath: nil, cacheName: ShoppingListItemEntity.CACHE_NAME)

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController.fetchedObjects ?? []
    }
    
    func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency:String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList) -> ShoppingListItem {
        let listItemEntity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        let listItem = ShoppingListItem(entity: listItemEntity!, insertInto: DBHelper.instance.getManagedObjectContext())
        listItem.itemName = itemName
        listItem.itemAmountQuantity = itemAmountQuantity
        listItem.itemAmountUnit = itemAmountUnit
        listItem.shopName = shopName
        listItem.done = NSNumber(value: doneStatus as Bool)
        listItem.brandName = brandName
        listItem.itemPrice = itemPrice
        listItem.itemCurrency = itemCurrency
        listItem.listForItem = listForItem
        listItem.notes = notes
        DBHelper.instance.saveContext()

        return listItem;
    }
    
    func updateShoppingListItem(_ item: ShoppingListItem, itemName: String, shopName: String, amountQuantity: NSNumber, amountUnit: String, brandName: String, price: NSNumber, currency: String?, notes: String?) {
        item.itemName = itemName
        item.itemAmountQuantity = amountQuantity
        item.itemAmountUnit = amountUnit
        item.shopName = shopName
        item.brandName = brandName
        item.itemPrice = price
        item.itemCurrency = currency
        item.notes = notes
        DBHelper.instance.saveContext()
    }
}

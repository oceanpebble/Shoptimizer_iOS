// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class SuppliesItemRepository {
    
    func allItems() -> [SuppliesItem] {
        return SuppliesItem.getAll()
    }
    
    func updateItem(_ item: SuppliesItem, curAmountQuantity: Double) {
        item.setValue(NSNumber(value: curAmountQuantity), forKey: SuppliesItem.COL_CUR_AMOUNT_QUANTITY)
    }
}

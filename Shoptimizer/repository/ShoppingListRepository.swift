// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class ShoppingListRepository {
    
    func itemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        return list.itemsForList?.allObjects as? [ShoppingListItem] ?? []
    }
    
    func doneItemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        return itemsFor(list).filter({$0.done?.boolValue == true})
    }    
    
    func isListIncludedInAggregatedList(_ list: ShoppingList?) -> Bool {
        return list?.include_in_aggregate?.boolValue ?? false
    }
    
    func nameFor(_ list: ShoppingList) -> String {
        return list.name!
    }
    
    func numberOfItemsOn(_ list: ShoppingList) -> Int {
        return list.totalItemsCount()
    }
    
    func numberOfDoneItemsOn(_ list: ShoppingList) -> Int {
        return list.doneItemsCount()
    }
    
    func dominantCurrencyOf(_ list: ShoppingList) -> String? {
        return list.dominantCurrency()
    }
    
    func dominantCurrency(on list: ShoppingList, in shop: String) -> String? {
        let currencies = itemsFor(list)
            .filter({StringUtil.similarStrings(s1: $0.shopName, s2: shop)})
            .filter({$0.itemCurrency != nil})
            .map({$0.itemCurrency!})

        var currenciesDict = [String:Int]()
        for currency in currencies {
            if currenciesDict[currency] == nil {
                currenciesDict[currency] = 1
            } else {
                currenciesDict[currency]! += 1
            }
        }

        let sorted = currenciesDict.sorted(by: {$0.value > $1.value})
        let topValue = sorted.first?.value
        let topValues = sorted.filter({$0.value == topValue})
        let sortedTopValues = topValues.sorted(by: <)
        return sortedTopValues.first?.key
    }
    
    func totalPriceOfAllItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        return list.getTotalPriceOfAllItems(forCurrency: currency)
    }
    
    func totalPriceOfAllItems(forCurrency currency: String, inShop shop: String, on list: ShoppingList) -> Double {
        return itemsFor(list)
            .filter({StringUtil.similarStrings(s1: $0.shopName, s2: shop)})
            .filter({$0.itemCurrency == currency})
            .map({$0.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})
    }
    
    func totalPriceOfDoneItems(forCurrency currency: String, on list: ShoppingList) -> Double {        
        return list.getTotalPriceOfDoneItems(forCurrency: currency)
    }
    
    func totalPriceOfDoneItems(forCurrency currency: String, inShop shop: String, on list: ShoppingList) -> Double {
        return itemsFor(list)
            .filter({$0.done!.boolValue == true})
            .filter({$0.shopName == shop})
            .filter({$0.itemCurrency == currency})
            .map({$0.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})
    }
    
    func deleteShoppingList(_ list: ShoppingList) {
        DBHelper.instance.getManagedObjectContext().delete(list)
        DBHelper.instance.saveContext()
    }
    
    func isListItemAvailable(withName name: String, andShop shop: String, inList list: ShoppingList) -> Bool {
        return itemsFor(list)
            .filter({StringUtil.similarStrings(s1: $0.itemName, s2: name)})
            .filter({StringUtil.similarStrings(s1: $0.shopName, s2: shop)})
            .count > 0
    }
}

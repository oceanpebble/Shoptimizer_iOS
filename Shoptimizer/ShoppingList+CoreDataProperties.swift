// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension ShoppingList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingList> {
        return NSFetchRequest<ShoppingList>(entityName: "ShoppingList");
    }

    @NSManaged public var include_in_aggregate: NSNumber?
    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var itemsForList: NSSet?

}

// MARK: Generated accessors for itemsForList
extension ShoppingList {

    @objc(addItemsForListObject:)
    @NSManaged public func addToItemsForList(_ value: ShoppingListItem)

    @objc(removeItemsForListObject:)
    @NSManaged public func removeFromItemsForList(_ value: ShoppingListItem)

    @objc(addItemsForList:)
    @NSManaged public func addToItemsForList(_ values: NSSet)

    @objc(removeItemsForList:)
    @NSManaged public func removeFromItemsForList(_ values: NSSet)

}

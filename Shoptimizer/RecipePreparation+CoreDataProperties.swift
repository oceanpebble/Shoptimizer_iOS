// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension RecipePreparation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecipePreparation> {
        return NSFetchRequest<RecipePreparation>(entityName: "RecipePreparation")
    }

    @NSManaged public var title: String?
    @NSManaged public var preparation: String?
    @NSManaged public var number: NSNumber?
    @NSManaged public var recipeForPreparation: Recipe?
    @NSManaged public var timeWait: NSNumber?
    @NSManaged public var timePrepare: NSNumber?
    @NSManaged public var summary: String?
}

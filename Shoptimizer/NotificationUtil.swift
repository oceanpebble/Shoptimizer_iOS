// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit
import UserNotifications

// MARK: - Notification Categories

enum NotificationCategoryID : String {
    case recipeTask

    static let ids : [NotificationCategoryID] = [.recipeTask]
}

// MARK: - Notification Actions

enum NotificationActionID : String {
    var title : String {
        switch self {
        case .markTaskAsDone: return NSLocalizedString("Done", comment: "")
        case .showTasks: return NSLocalizedString("Show tasks", comment: "")
        case .showRecipeForTask: return NSLocalizedString("Show recipe", comment: "")
        }
    }
    case markTaskAsDone
    case showTasks
    case showRecipeForTask

    static let ids : [NotificationActionID] = [.markTaskAsDone, .showTasks, .showRecipeForTask]
}

// MARK: - Notification Controller

class NotificationController : NSObject, UNUserNotificationCenterDelegate {

    private let userInfoRecipeID = "RecipeID"
    private let userInfoTaskID = "TaskID"
    private let viewControllerRecipe = "RecipeViewController"

    static var nextID : Int {
        let userDefaults = UserDefaults.standard
        let idFromDefaults = userDefaults.integer(forKey: Constants.USER_DEFAULTS_NOTIFICATION_ID_COUNTER)
        let newId = idFromDefaults + 1
        userDefaults.set(newId, forKey: Constants.USER_DEFAULTS_NOTIFICATION_ID_COUNTER)
        userDefaults.synchronize()
        return newId
    }

    // MARK: - Singleton initialization

    public override init() {
        super.init()
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().setNotificationCategories(prepareCategories() as! Set<UNNotificationCategory>)
    }

    class var sharedInstance : NotificationController {
        struct Static {
            static let instance : NotificationController = NotificationController()
        }
        return Static.instance
    }

    // MARK: - Categories
    
    private func prepareCategories() -> Set<NSObject> {
        var preparedCategories = Set<UNNotificationCategory>()
        
        // Category 'Recipe Task'
        let actionMarkTaskAsDone = UNNotificationAction(identifier: NSLocalizedString(NotificationActionID.markTaskAsDone.rawValue, comment: ""), title: NotificationActionID.markTaskAsDone.title, options: UNNotificationActionOptions.authenticationRequired)
        let actionViewTask = UNNotificationAction(identifier: NSLocalizedString(NotificationActionID.showTasks.rawValue, comment: ""), title: NotificationActionID.showTasks.title, options: UNNotificationActionOptions.foreground)
        let actionViewTaskInRecipe = UNNotificationAction(identifier: NSLocalizedString(NotificationActionID.showRecipeForTask.rawValue, comment: ""), title: NotificationActionID.showRecipeForTask.title, options: UNNotificationActionOptions.foreground)
        let actionsForCategoryTask = [actionMarkTaskAsDone, actionViewTask, actionViewTaskInRecipe]
        let categoryRecipeTask = UNNotificationCategory(identifier: NotificationCategoryID.recipeTask.rawValue, actions: actionsForCategoryTask, intentIdentifiers: [], options: .customDismissAction)
        preparedCategories.insert(categoryRecipeTask)
        
        return preparedCategories
    }

    // MARK: - Request Authorization


    func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound], completionHandler:  { (granted, error) in })
    }

    // MARK: - Prepare Notifications

    func prepareNotification(forCategory categoryID: NotificationCategoryID, withTitle title: String, andBody body: String, toAppearAt date: DateComponents, taskId: Int, recipeId: Int) -> (UNNotificationContent, UNCalendarNotificationTrigger) {

        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = title
        notificationContent.body = body
        notificationContent.categoryIdentifier = categoryID.rawValue
        notificationContent.userInfo[userInfoTaskID] = taskId
        notificationContent.userInfo[userInfoRecipeID] = recipeId
        notificationContent.sound = UNNotificationSound.default
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)

        return (notificationContent, trigger)
    }

    // MARK: - Schedule Notifications

    func scheduleNotification(with details: (UNNotificationContent, UNCalendarNotificationTrigger)) {
        scheduleNotification(withContent: details.0, andTrigger: details.1)
    }

    func scheduleNotification(withContent content: UNNotificationContent, andTrigger trigger: UNNotificationTrigger) {
        let request = UNNotificationRequest(identifier: String(NotificationController.nextID), content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) {(error: Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }

    // MARK: - Unschedule Notifications

    func unscheduleNotification(withId id: Int) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [String(id)])
    }

    func unscheduleAllTasks() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests();
    }

    // MARK: - UNUserNotificationDelegate

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        switch response.actionIdentifier {
        case NotificationActionID.markTaskAsDone.rawValue:
            let taskId = response.notification.request.content.userInfo[userInfoTaskID] as! Int
            Task.markTaskAsDone(withId: taskId)
        case NotificationActionID.showTasks.rawValue:
            let topViewController = GeneralHelper.getTopViewController() as! UITabBarController
            topViewController.selectedIndex = Constants.TAB_BAR_INDEX_TASKS
        case NotificationActionID.showRecipeForTask.rawValue:
            let recipeId = response.notification.request.content.userInfo[userInfoRecipeID] as! Int
            let topViewController = GeneralHelper.getTopViewController() as! UITabBarController
            topViewController.selectedIndex = Constants.TAB_BAR_INDEX_RECIPES
            if let recipe = Recipe.recipe(withId: recipeId) {
                let tabs = topViewController.children
                let recipesTab = tabs[Constants.TAB_BAR_INDEX_RECIPES]
                let recipesTVC = recipesTab.children[0] as! UITableViewController
                let recipesNC = recipesTVC.navigationController!
                let recipeVC = topViewController.storyboard!.instantiateViewController(withIdentifier: viewControllerRecipe) as! RecipeViewController
                recipeVC.recipe = recipe
                recipesNC.pushViewController(recipeVC, animated: true)
            }
        default:
            break
        }

        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.list, .banner])
    }

    // MARK: - Handle Notification Actions

    func handleAction(withIdentifier identifier: String, forResponse response: UNNotificationResponse) {
        switch response.actionIdentifier {
        case NotificationActionID.markTaskAsDone.rawValue:
            markTaskAsDone(withId: Int(response.notification.request.identifier)!)
        case NotificationActionID.showTasks.rawValue:
            viewTasks()
        case NotificationActionID.showRecipeForTask.rawValue:
            let recipeId = response.notification.request.content.userInfo[userInfoRecipeID] as! Int
            viewTaskInRecipe(withId: recipeId)
        default:
            break
        }
    }

    private func markTaskAsDone(withId id: Int) {
        Task.markTaskAsDone(withId: id)
    }

    private func viewTasks() {
        let topViewController = GeneralHelper.getTopViewController() as! UITabBarController
        topViewController.selectedIndex = Constants.TAB_BAR_INDEX_TASKS
    }

    private func viewTaskInRecipe(withId id: Int) {
        let topViewController = GeneralHelper.getTopViewController() as! UITabBarController
        topViewController.selectedIndex = Constants.TAB_BAR_INDEX_RECIPES
        if let recipe = Recipe.recipe(withId: id) {
            let tabs = topViewController.children
            let recipesTab = tabs[Constants.TAB_BAR_INDEX_RECIPES]
            let recipesTVC = recipesTab.children[0] as! UITableViewController
            let recipesNC = recipesTVC.navigationController!
            let recipeVC = topViewController.storyboard!.instantiateViewController(withIdentifier: viewControllerRecipe) as! RecipeViewController
            recipeVC.recipe = recipe
            recipesNC.pushViewController(recipeVC, animated: true)
        }
    }
}

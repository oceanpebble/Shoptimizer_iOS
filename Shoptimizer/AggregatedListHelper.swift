// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class AggregatedListHelper {
    static let instance = AggregatedListHelper()
    fileprivate var aggregatedList: ShoppingList?

    func getAggregatedList() -> ShoppingList {
        if self.aggregatedList != nil {
            return self.aggregatedList!
        }

        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingList> = ShoppingList.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptor = NSSortDescriptor(key:ShoppingListEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors

        let predicate = NSPredicate(format:"%K == %@", ShoppingListEntity.COL_NAME, self.nameOfAggregatedList())
        fetchRequest.predicate = predicate

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:nil, cacheName:ShoppingListEntity.CACHE_NAME)

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController.fetchedObjects![0]
    }

    func aggregatedListForLanguage(_ language: String) -> ShoppingList {
        if self.aggregatedList != nil {
            return self.aggregatedList!
        }

        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingList> = ShoppingList.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptor = NSSortDescriptor(key:ShoppingListEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors

        let predicate = NSPredicate(format:"%K == %@", ShoppingListEntity.COL_NAME, self.nameOfAggregatedListForLanguage(language))
        fetchRequest.predicate = predicate

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:nil, cacheName:ShoppingListEntity.CACHE_NAME)

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController.fetchedObjects![0]
    }

    func addItem(_ listItem: ShoppingListItem) {
        guard !listItem.done!.boolValue else { return }
        let itemName = listItem.itemName!
        let shopName = listItem.shopName
        let amountQuantity = listItem.itemAmountQuantity
        let amountUnit = listItem.itemAmountUnit
        let brandName = listItem.brandName
        let itemPrice = listItem.itemPrice
        let itemCurrency = listItem.itemCurrency
        self.addItem(itemName, forShop: shopName, amountQuantity: amountQuantity, andAmountUnit: amountUnit, andBrand: brandName, forPrice: itemPrice, atCurrency: itemCurrency)
    }

    func addItem(_ itemName: String, forShop shopName: String?, amountQuantity: NSNumber?, andAmountUnit amountUnit: String?, andBrand brandName: String?, forPrice price: NSNumber?, atCurrency currency: String?) {
        let itemOnAggregatedList = self.findMatchForItem(itemName, shop:shopName, andUnit:amountUnit, andCurrency: currency)
        if itemOnAggregatedList == nil {
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: itemName, quantity:amountQuantity, unit:amountUnit, doneStatus:false, brand: brandName, forPrice: price, atCurrency: currency, inShop:shopName, forList:self.getAggregatedList(), intoContext:DBHelper.instance.getManagedObjectContext())
        } else {
            if let amountQuantityOfItemOnAggregatedList = itemOnAggregatedList!.itemAmountQuantity {
                let amountUnitOfItemOnAggregatedList = itemOnAggregatedList!.itemAmountUnit
                let amountQuantityMapped = UnitPickerHelper.convertValue(amountQuantity, fromBaseUnit:amountUnit, toTargetUnit:amountUnitOfItemOnAggregatedList)
                let newQuantity = amountQuantityOfItemOnAggregatedList.doubleValue + amountQuantityMapped.doubleValue
                itemOnAggregatedList!.itemAmountQuantity = NSNumber(value: newQuantity)
            }

            if let priceOfItemOnAggregatedList = itemOnAggregatedList!.itemPrice {
                if let price = price, itemOnAggregatedList!.itemCurrency == currency {
                    let newPrice = priceOfItemOnAggregatedList.doubleValue + price.doubleValue
                    itemOnAggregatedList!.itemPrice = NSNumber(value: newPrice)
                }
            } else {
                itemOnAggregatedList!.itemPrice = price
                itemOnAggregatedList!.itemCurrency = currency
            }
        }
        DBHelper.instance.saveContext()
    }

    func deleteItem(_ listItem: ShoppingListItem) {
        let itemName = listItem.itemName!
        let shopName = listItem.shopName
        let amountQuantity = listItem.itemAmountQuantity
        let amountUnit = listItem.itemAmountUnit
        let brand = listItem.brandName
        let itemPrice = listItem.itemPrice
        let itemCurrency = listItem.itemCurrency
        self.deleteItem(itemName, forShop:shopName, amountQuantity:amountQuantity, andAmountUnit:amountUnit, fromBrand: brand, withPrice: itemPrice, andCurrency: itemCurrency)
    }

    func deleteItem(_ itemName: String, forShop shopName: String?, amountQuantity: NSNumber?, andAmountUnit amountUnit: String?, fromBrand brand: String?, withPrice price: NSNumber?, andCurrency currency: String?) {
        if let itemOnAggregatedList = self.findMatchForItem(itemName, shop:shopName, andUnit:amountUnit, andCurrency: currency) {
            DBHelper.instance.getManagedObjectContext().delete(itemOnAggregatedList)
            DBHelper.instance.saveContext()
        }
    }

    func subtractItem(_ listItem: ShoppingListItem) {
        let itemName = listItem.itemName!
        let shopName = listItem.shopName
        let amountQuantity = listItem.itemAmountQuantity
        let amountUnit = listItem.itemAmountUnit
        let brand = listItem.brandName
        let itemPrice = listItem.itemPrice
        let itemCurrency = listItem.itemCurrency
        self.subtractItem(itemName, forShop: shopName, amountQuantity: amountQuantity, andAmountUnit: amountUnit, fromBrand: brand, withPrice: itemPrice, andCurrency: itemCurrency)
    }

    func subtractItem(_ itemName: String, forShop shopName: String?, amountQuantity: NSNumber?, andAmountUnit amountUnit: String?, fromBrand brand: String?, withPrice price: NSNumber?, andCurrency currency: String?) {
        let itemOnAggregatedList = self.findMatchForItem(itemName, shop: shopName, andUnit: amountUnit, andCurrency: currency)
        if itemOnAggregatedList != nil {
            let amountUnitOfItemOnAggregatedList = itemOnAggregatedList!.itemAmountUnit
            let amountQuantityOfItemOnAggregatedList = itemOnAggregatedList!.itemAmountQuantity ?? 0.0
            let amountQuantityMapped = UnitPickerHelper.convertValue(amountQuantity, fromBaseUnit: amountUnit, toTargetUnit: amountUnitOfItemOnAggregatedList)
            let newQuantity = amountQuantityOfItemOnAggregatedList.doubleValue - amountQuantityMapped.doubleValue

            var newPrice = 0.0
            if let priceOfItemOnAggregatedList = itemOnAggregatedList?.itemPrice,
                let currencyOfItemOnAggregatedList = itemOnAggregatedList?.itemCurrency,
                let price = price,
                currencyOfItemOnAggregatedList == currency {
                newPrice = priceOfItemOnAggregatedList.doubleValue - price.doubleValue
            } else if let priceOfItemOnAggregatedList = itemOnAggregatedList?.itemPrice {
                newPrice = priceOfItemOnAggregatedList.doubleValue
            }

            var numberOfListThatHaveTheItem = 0
            for list in ShoppingList.shoppingLists().filter({($0.include_in_aggregate!.boolValue)}) {
                for item in list.itemsForList! {
                    let listItem = item as! ShoppingListItem
                    if listItem.itemName == itemName
                        && listItem.shopName == shopName
                        && listItem.itemAmountUnit == amountUnit
                        && listItem.itemCurrency == currency {
                        numberOfListThatHaveTheItem += 1
                    }
                }
            }

            if numberOfListThatHaveTheItem <= 1 &&
                newQuantity <= Constants.COMPARISON_TOLERANCE && newPrice <= Constants.COMPARISON_TOLERANCE {
                DBHelper.instance.getManagedObjectContext().delete(itemOnAggregatedList!)
            } else {
                itemOnAggregatedList!.itemAmountQuantity = NSNumber(value: newQuantity)
                itemOnAggregatedList!.itemPrice = NSNumber(value: newPrice)
            }

            DBHelper.instance.saveContext()
        }
    }

    func findMatchForItem(_ itemName: String, shop shopName: String?, andUnit itemAmountUnit: String?, andCurrency currency: String?) -> ShoppingListItem? {
        let controllerWithItems = self.itemsOnAggregatedList()
        guard let fetchedItems = controllerWithItems.fetchedObjects else { return nil }
        for fetchedItem in fetchedItems {
            let fetchedItemName = fetchedItem.itemName!
            let fetchedItemShopName = fetchedItem.shopName
            let fetchedItemAmountUnit = fetchedItem.itemAmountUnit
            let fetchedItemCurrency = fetchedItem.itemCurrency
            if StringUtil.similarStrings(s1: fetchedItemName, s2: itemName)
                && StringUtil.similarStrings(s1: fetchedItemShopName, s2: shopName)
                && (StringUtil.similarStrings(s1: fetchedItemAmountUnit, s2: itemAmountUnit) || UnitPickerHelper.baseUnit(fetchedItemAmountUnit, isConvertibleToTargetUnit:itemAmountUnit))
                && StringUtil.similarStrings(s1: fetchedItemCurrency, s2: currency) {
                return fetchedItem
            }
        }
        return nil
    }

    func itemsOnAggregatedList() -> NSFetchedResultsController<ShoppingListItem> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListItemEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let predicate = NSPredicate(format:"%K == %@", ShoppingListItemEntity.REF_LISTFORITEM, self.getAggregatedList())
        fetchRequest.predicate = predicate

        let sortDescriptorShopName = NSSortDescriptor(key:ShoppingListItemEntity.COL_SHOP_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptorDoneStatus = NSSortDescriptor(key:ShoppingListItemEntity.COL_DONE, ascending:true)
        let sortDescriptorItemName = NSSortDescriptor(key:ShoppingListItemEntity.COL_ITEM_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorShopName, sortDescriptorDoneStatus, sortDescriptorItemName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:ShoppingListItemEntity.COL_SHOP_NAME, cacheName:ShoppingListItemEntity.CACHE_NAME)

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController
    }

    func nameOfAggregatedList() -> String {
        let language = GeneralHelper.getPreferredLanguage()
        return self.nameOfAggregatedListForLanguage(language)
    }
    
    func nameOfAggregatedListForLanguage(_ language: String) -> String {
        if language == "de" {
            return "Aggregierte Liste";
        }
        
        return "Aggregated List";
    }

    func deleteItemsFromAggregatedListForList(_ list: ShoppingList) {
        guard let listItems = list.itemsForList else { return }

        for listItem in listItems {
            self.subtractItem(listItem as! ShoppingListItem)
        }
    }
    
    func updateAggregatedList() {
        if let allItemsOnAggregatedList = AggregatedListHelper.instance.itemsOnAggregatedList().fetchedObjects {
            for item in allItemsOnAggregatedList {
                AggregatedListHelper.instance.subtractItem(item)
            }
        }

        for item in ShoppingListItem.undoneItemsForListsContainedInAggregatedList() {
            AggregatedListHelper.instance.addItem(item)
        }
    }
    
    func isAggregatedList(_ listName: String) -> Bool {
        return listName == AggregatedListHelper.instance.nameOfAggregatedList()
    }
    
    func isAggregatedList(_ list: ShoppingList) -> Bool {
        return isAggregatedList(list.name ?? "")
    }
}

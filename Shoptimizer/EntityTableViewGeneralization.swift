// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

protocol EntityTableViewDelegate {
    associatedtype EntityType: NSManagedObject

    var fetchedResultsController: NSFetchedResultsController<EntityType>! { get set }

    var reusableCellIdentifier: String { get }
    
    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath)
}

/*
 Protocol extension for default implementations of certain methods.
*/
extension EntityTableViewDelegate {

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    func etdNumberOfSections(in tableView: UITableView) -> Int{
        if let count = self.fetchedResultsController?.sections?.count {
            return count
        }
        
        return 1
    }

    func etdTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reusableCellIdentifier, for: indexPath)
        self.configure(cell: cell, atIndexPath: indexPath)
        return cell
    }

    func etdTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.fetchedResultsController?.sections?[section].numberOfObjects {
            return count
        }

        return 0
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func etdControllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>, inTableView tableView: UITableView) {
        tableView.beginUpdates()
    }

    func etdController(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType, inTableView tableView: UITableView) {
        switch(type) {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return;
        }
    }

    func etdController(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?, inTableView tableView: UITableView) {
        switch(type) {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with:.fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with:.fade)
        case .move:
            tableView.deleteRows(at: [indexPath!], with:.fade)
            tableView.insertRows(at: [newIndexPath!], with:.fade)
        case .update:
            self.configure(cell: tableView.cellForRow(at: indexPath!), atIndexPath:indexPath!)
        }
    }

    func etdControllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>, inTableView tableView: UITableView) {
        tableView.endUpdates()
        tableView.reloadData()
    }
}

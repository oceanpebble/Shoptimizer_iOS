// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

protocol AddEntityViewDelegate: UITextFieldDelegate {

    var editingMode: EditingMode { get set }

    func setupView()
    func setTextFieldDelegates()
    func setTextFieldContents()
    func setTextViewDelegates()
    func setTextViewContents()
    func setTitle()
}

/*
 Protocol extension for default implementations of certain methods.
 */
extension AddEntityViewDelegate {

    func setupView() {
        setTextFieldDelegates()
        setTextFieldContents()
        setTextViewDelegates()
        setTextViewContents()
        setTitle()
    }

    func setupScrollView(_ scrollView: UIScrollView) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
    }

    func addDoneButtonToKeyboard(forTextFields fields: [UITextField], forTextViews views: [UITextView], targeting target: UIViewController, using selector: Selector) {
        let doneToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: UIBarButtonItem.Style.done, target: target, action: selector)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        for field in fields {
            field.inputAccessoryView = doneToolbar
        }
        
        for view in views {
            view.inputAccessoryView = doneToolbar
        }
    }

    func registerForKeyboardNotifications(shownSelector: Selector, hiddenSelector: Selector) {
        NotificationCenter.default.addObserver(self, selector: shownSelector, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: hiddenSelector, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    func unregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
}

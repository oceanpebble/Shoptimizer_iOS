// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

enum EditingMode {
    case create
    case edit
}

enum RecipeOperations : Int {
    case create_LIST
    case subtract_RECIPE
    case filter_BY_SUPPLIES
}

enum ShoptimizerVersions : String {
    case v_1_0_0 = "1.0.0"
    case v_1_1_0 = "1.1.0"
    case v_2_0_0 = "2.0.0"
    case v_2_1_0 = "2.1.0"
    case v_2_2_0 = "2.2.0"
    case v_2_3_0 = "2.3.0"
    case v_2_3_1 = "2.3.1"
    case v_3_0_0 = "3.0.0"
    case v_3_1_0 = "3.1.0"
    case v_3_1_1 = "3.1.1"
    case v_3_2_0 = "3.2.0"
}

enum Currencies: String {
    case eur = "EUR"
    case usd = "USD"
    case gbp = "GBP"
}

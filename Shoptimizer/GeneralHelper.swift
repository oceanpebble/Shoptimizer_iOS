// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class GeneralHelper {
    static func getPreferredLanguage() -> String {
        let preferredLanguages = Locale.preferredLanguages
        var selectedLanguage = ""
        for preferredLanguage in preferredLanguages {
            let components = preferredLanguage.components(separatedBy: "-")
            let tempLanguage = components[0]
            if tempLanguage == "en" || tempLanguage == "de" {
                selectedLanguage = tempLanguage
                break;
            }
        }

        if selectedLanguage.isEmpty {
            selectedLanguage = "en"
        }
        
        return selectedLanguage;
    }

    static func presentInformationDialogWithText(_ infoText: String, andTitle title: String) {
        let dialog = UIAlertController(title: title, message:infoText, preferredStyle:.alert)
        let okayAction = UIAlertAction(title:"OK", style:.default, handler:nil)
        dialog.addAction(okayAction)
        getTopViewController().present(dialog, animated:false, completion:nil)
    }

    static func nameAlreadyExists(_ name: String, inFetchedResultsController controller: NSFetchedResultsController<NSManagedObject>, forKey key: String) -> Bool {
        let objects = controller.fetchedObjects!

        for object in objects {
            let objectName = object.value(forKey: key) as! String
            if objectName == name {
                return true
            }
        }
        
        return false
    }

    static func name1(_ name1: String, andName2 name2: String, alreadyExistInFetchedResultsController controller: NSFetchedResultsController<NSManagedObject>, forKey1 key1: String, andKey2 key2: String) -> Bool {
        let objects = controller.fetchedObjects!

        for object in objects {
            let value1 = object.value(forKey: key1) as! String
            let value2 = object.value(forKey: key2) as! String
            if value1 == name1 && value2 == name2 {
                return true
            }
        }
        
        return false
    }

    static func currentAppVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }

    static func firstStartOfVersion(_ version: String) -> Bool {
        let key = String(format:"IsFirstLaunchV%@", version)
        if !UserDefaults.standard.bool(forKey: key) {
            UserDefaults.standard.set(true, forKey:key)
            UserDefaults.standard.synchronize()
            return true
        }
        return false
    }

    static func decimalFormatter() -> NumberFormatter {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        format.maximumFractionDigits = Constants.MAX_FRACTION_DIGITS
        format.decimalSeparator = Locale.current.decimalSeparator

        return format
    }

    static func priceFormatter() -> NumberFormatter {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        format.maximumFractionDigits = 2
        format.minimumFractionDigits = 2
        format.roundingMode = .halfUp
        format.decimalSeparator = Locale.current.decimalSeparator

        return format
    }

    /*
     Ensures that the value in the given String satisfies the following conditions:
     - value is not nil
     - value is not the empty string
     - value is a number (only digits and one decimal separator, no letters or special characters), i.e. is convertible to Double
     - double value is >= 0.0
    */
    static func ensureValidDouble(fromString numberString: String?, allowZero allow: Bool) -> NSNumber? {
        return ensureValidDouble(fromString: numberString, allowZero: allow, accordingToFormatter: GeneralHelper.decimalFormatter())
    }

    static func ensureValidPrice(fromString numberString: String?, allowZero allow: Bool) -> NSNumber? {
        return ensureValidDouble(fromString: numberString, allowZero: allow, accordingToFormatter: GeneralHelper.priceFormatter())
    }

    private static func ensureValidDouble(fromString numberString: String?, allowZero allow: Bool, accordingToFormatter formatter: NumberFormatter) -> NSNumber? {
        guard let numberString = numberString,
            numberString.isNotEmpty else {
                return allow ? formatter.number(from: "0") : nil
        }

        guard let number = formatter.number(from: numberString) else {
            return nil
        }

        if allow {
            guard number.doubleValue >= 0.0 else { return nil }
        } else {
            guard number.doubleValue > 0.0 else { return nil }
        }
        
        return number
    }

    /*
     Ensures that the value in the given String satisfies the following conditions:
     - value is not nil
     - value is not the empty string
     - value is a number (only digits and one decimal saparator, no letters or special characters), i.e. is convertible to Double
     - double value is >= 0.0
     */
    static func ensureValidInt(fromString numberString: String?, allowZero allow: Bool) -> Int? {
        guard let numberString = numberString,
            numberString.isNotEmpty else {
                return allow ? 0 : nil
        }

        guard let numberInt = Int(numberString) else { return nil }

        if allow {
            guard numberInt >= 0 else { return nil }
        } else {
            guard numberInt > 0 else { return nil }
        }

        return numberInt
    }

    static func getTopViewController() -> UIViewController {
        var topViewController = UIApplication.shared.keyWindow!.rootViewController!

        while (topViewController.presentedViewController != nil) {
            topViewController = topViewController.presentedViewController!
        }
        
        return topViewController;
    }

    static func convertTimeDurationString(_ string: String) -> Int {
        let components = string.components(separatedBy: ":")
        let hours = Int(components[0])!
        let minutes = Int(components[1])!
        let timeInMinutes = hours * 60 + minutes
        return timeInMinutes
    }

    static func convertTimeDurationInt(_ int: Int?) -> String {
        guard let int = int else { return "0:00" }
        let hoursString = String(int / 60)
        let minutes = int % 60
        let minutesString = minutes < 10 ? "0" + String(minutes) : String(minutes)
        return hoursString + ":" + minutesString
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension Recipe {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recipe> {
        return NSFetchRequest<Recipe>(entityName: "Recipe")
    }

    @NSManaged public var name: String?
    @NSManaged public var portions: NSNumber?
    @NSManaged public var possibleWithSupplies: NSNumber?
    @NSManaged public var categoriesForRecipe: NSSet?
    @NSManaged public var ingredientsForRecipe: NSSet?
    @NSManaged public var preparationsForRecipe: NSSet?
    @NSManaged public var id: NSNumber?

}

// MARK: Generated accessors for categoriesForRecipe
extension Recipe {

    @objc(addCategoriesForRecipeObject:)
    @NSManaged public func addToCategoriesForRecipe(_ value: RecipeCategory)

    @objc(removeCategoriesForRecipeObject:)
    @NSManaged public func removeFromCategoriesForRecipe(_ value: RecipeCategory)

    @objc(addCategoriesForRecipe:)
    @NSManaged public func addToCategoriesForRecipe(_ values: NSSet)

    @objc(removeCategoriesForRecipe:)
    @NSManaged public func removeFromCategoriesForRecipe(_ values: NSSet)

}

// MARK: Generated accessors for ingredientsForRecipe
extension Recipe {

    @objc(addIngredientsForRecipeObject:)
    @NSManaged public func addToIngredientsForRecipe(_ value: RecipeIngredient)

    @objc(removeIngredientsForRecipeObject:)
    @NSManaged public func removeFromIngredientsForRecipe(_ value: RecipeIngredient)

    @objc(addIngredientsForRecipe:)
    @NSManaged public func addToIngredientsForRecipe(_ values: NSSet)

    @objc(removeIngredientsForRecipe:)
    @NSManaged public func removeFromIngredientsForRecipe(_ values: NSSet)

}

// MARK: Generated accessors for preparationsForRecipe
extension Recipe {

    @objc(addPreparationsForRecipeObject:)
    @NSManaged public func addToPreparationsForRecipe(_ value: RecipePreparation)

    @objc(removePreparationsForRecipeObject:)
    @NSManaged public func removeFromPreparationsForRecipe(_ value: RecipePreparation)

    @objc(addPreparationsForRecipe:)
    @NSManaged public func addToPreparationsForRecipe(_ values: NSSet)

    @objc(removePreparationsForRecipe:)
    @NSManaged public func removeFromPreparationsForRecipe(_ values: NSSet)

}

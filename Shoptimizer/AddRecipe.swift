// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class AddRecipeViewController : UIViewController, AddEntityViewDelegate {
    func setTitle() {
        
    }
    

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    @IBOutlet fileprivate weak var recipeNameTextField: UITextField!
    @IBOutlet fileprivate weak var saveButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var recipePortionsTextField: UITextField!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    var fetchedRecipe: Recipe?
    var editingMode: EditingMode

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        recipeNameTextField.becomeFirstResponder()
        setTextFieldContents()
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit Recipe", comment: "")
        }
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        addDoneButtonToKeyboard(forTextFields: [recipeNameTextField, recipePortionsTextField], forTextViews: [], targeting: self, using: #selector(doneButtonAction))
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews, otherwise the scrollview will be positioned too low on the screen
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let specificSender = sender as? UIBarButtonItem, specificSender != self.saveButton {
            return true;
        }

        guard let enteredName = recipeNameTextField.text?.trimmingCharacters(in: .whitespaces), enteredName.isNotEmpty else {
            let enterPortionsString = NSLocalizedString("Please enter a name.", comment: "")
            GeneralHelper.presentInformationDialogWithText(enterPortionsString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false
        }

        if (self.editingMode == .create || enteredName != self.fetchedRecipe?.name)
            && Recipe.isRecipeAvailable(withName: enteredName) {
            let recipeAlreadyExistsString = NSLocalizedString("A recipe with this name already exists.", comment: "")
            GeneralHelper.presentInformationDialogWithText(recipeAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false
        }
        
        guard let enteredPortions = recipePortionsTextField.text?.trimmingCharacters(in: .whitespaces), enteredPortions.isNotEmpty else {
            let enterPortionsString = NSLocalizedString("Please enter a number of portions.", comment: "")
            GeneralHelper.presentInformationDialogWithText(enterPortionsString, andTitle: Constants.TITLE_ALERT_PORTIONS_MISSING)
            return false
        }

        guard let portions = GeneralHelper.ensureValidInt(fromString: self.recipePortionsTextField.text, allowZero: false) else {
            let portionsInvalidString = NSLocalizedString("Please enter a value > 0 for the number of portions", comment: "")
            GeneralHelper.presentInformationDialogWithText(portionsInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        if self.editingMode == .edit {
            self.fetchedRecipe?.name = enteredName
            self.fetchedRecipe?.portions = NSNumber(value: portions)
            DBHelper.instance.saveContext()
        } else if self.editingMode == .create {
            let _ = Recipe.insertNewRecipe(withName: enteredName, andPortions: NSNumber(value: portions), intoContext: DBHelper.instance.getManagedObjectContext())
            DBHelper.instance.saveContext()
        }

        return true
    }

    // *********************************************************
    //
    // MARK: - View Controller Life Cycle
    //
    // *********************************************************

    func setTextFieldDelegates() {
        // empty implementation
    }

    func setTextFieldContents() {
        recipeNameTextField.text = fetchedRecipe?.name
        if let portions = self.fetchedRecipe?.portions {
            recipePortionsTextField.text = String(describing: portions)
        }
    }

    func setTextViewDelegates() {
        // no text views in this view
    }

    func setTextViewContents() {
        // no text views in this view
    }

    /* Unfortunetly, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let info = (aNotification as NSNotification).userInfo,
           let value = info[UIResponder.keyboardFrameBeginUserInfoKey] {
            var kbHeight = (value as AnyObject).cgRectValue.size.height
            if kbHeight == 0.0 {
                kbHeight = (info[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
            }
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbHeight, right: 0.0)
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            var aRect = scrollView.frame;
            aRect.size.height -= kbHeight;

            if let activeTextField = UITextFieldHelper.instance.activeField,
                !aRect.contains(activeTextField.frame.origin) {
                scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                return
            }

            if let activeTextView = UITextViewHelper.instance.activeView {
                let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
                let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
                if !aRect.contains(transformedLowerLeftOfActiveTextView) {
                    let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                    let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height - 50) // 50 = height of done button toolbar
                    scrollView.scrollRectToVisible(rect, animated: true)
                }
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func doneButtonAction() {
        UITextFieldHelper.instance.activeField?.resignFirstResponder()
        UITextViewHelper.instance.activeView?.resignFirstResponder()
        keyboardWillBeHidden(Notification(name: Notification.Name(rawValue: "foo"))) // name is irrelevant since notification is not used
    }
}

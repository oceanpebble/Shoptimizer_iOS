// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

struct Constants {

    /**
     The index of the lists tab.
     */
    static let TAB_BAR_INDEX_LISTS = 0

    /**
     The index of the recipes tab.
     */
    static let TAB_BAR_INDEX_RECIPES = 1

    /**
     The index of the supplies tab.
     */
    static let TAB_BAR_INDEX_SUPPLIES = 2

    /**
     The index of the tasks tab.
     */
    static let TAB_BAR_INDEX_TASKS = 3

    /**
     The index of the info tab.
     */
    static let TAB_BAR_INDEX_INFO = 4

    /**
     Minimum duration for a tap to be detected as a long press.
    */
    static let LONG_TAP_DURATION = TimeInterval(floatLiteral: 0.5)

    /**
     The maximum number of digits after a decimal separator, currently set to 10.
    */
    static let MAX_FRACTION_DIGITS = 10

    /**
     Tolerance value when comparing double values for equality, currently set to 0.0000000001.
    */
    static let COMPARISON_TOLERANCE = 0.0000000001

    /**
     The name of the data model for CoreData.
    */
    static let DATA_MODEL_NAME = "Shoptimizer"

    /**
     The maximum allowed number of missing or insuffient ingredients for filtering recipes by supplies.
    */
    static let MAX_NUMBER_OF_MISSING_OR_INSUFFICIENT_INGREDIENTS = 2

    /**
     The maximum percentage of ingredients of a recipe that must be available for filtering recipes by supplies.
    */
    static let MIN_PERCENTAGE_OF_REMAINING_INGREDIENTS = 0.65

    /**
     Title for a standard user dialog informing that a name is missing.
    */
    static let TITLE_ALERT_NAME_MISSING = NSLocalizedString("No Name", comment: "")
    static let TITLE_ALERT_PORTIONS_MISSING = NSLocalizedString("No Portions", comment: "")

    /**
     Title for a standard user dialog informing that the currently entered information would result in a duplicate entry.
    */
    static let TITLE_ALERT_DUPLICATE = NSLocalizedString("Duplicate", comment: "")

    /**
     Title for a standard user dialog informing that the performed operation was successful.
    */
    static let TITLE_ALERT_SUCCESS = NSLocalizedString("Success", comment: "")

    /**
     Title for a standard user dialog informing that the performed operation was not successful.
    */
    static let TITLE_ALERT_ERROR = NSLocalizedString("Error", comment: "")

    /**
     Title for a standard user dialog informing that some of the entered informaion is invalid.
    */
    static let TITLE_ALERT_INVALID_DATA = NSLocalizedString("Invalid data", comment: "")

    /**
     Title for a standard user dialog informing that the sharing file is corrupt.
     */
    static let TITLE_CORRUPT_SHARE_FILE = NSLocalizedString("Corrupt file", comment: "")

    /**
     Title for a standard user dialog informing that the sharing file version is not supported.
     */
    static let TITLE_UNSUPPORTED_SHARE_FILE_VERSION = NSLocalizedString("Unsupported version", comment: "")

    /**
     Title for a standard user dialog informing that the sharing file type is not supported.
     */
    static let TITLE_UNSUPPORTED_SHARE_FILE_TYPE = NSLocalizedString("Unsupported type", comment: "")

    /**
     Name of the parameter 'type' for sharing files (for shopping lists, recipes)
    */
    static let SHARE_FILES_PARAMETER_NAME_TYPE = "type"

    /**
     Name of the parameter 'version' for sharing files (for shopping lists, recipes)
     */
    static let SHARE_FILES_PARAMETER_NAME_VERSION = "version"

    /**
     Identifier for sharing file type for shopping lists
     */
    static let SHARE_FILES_TYPE_SHOPPING_LIST = "shoppinglist"

    /**
     Identifier for sharing file type for recipes
     */
    static let SHARE_FILES_TYPE_RECIPE = "recipe"

    /**
     File name extension for sharing files
    */
    static let SHARE_FILES_FILE_EXTENSION = "shoptimizer"

    /**
     Key for shopping list items in sharing file
    */
    static let SHARE_FILES_ITEMS_KEY = "item"

    /**
     Key for recipe ingredients in sharing file
    */
    static let SHARE_FILES_INGREDIENTS_KEY = "ingredient"

    /**
     Key for recipe preparation steps in sharing file
     */
    static let SHARE_FILES_RECIPE_PREPARATION_KEY = "preparation"

    /**
     Key for recipe categories in sharing file
     */
    static let SHARE_FILES_RECIPE_CATEGORY_KEY = "category"

    /**
     Static lists
    */
    static let LIST_TYPE_STATIC = "Static"

    /**
     Dynamic lists
    */
    static let LIST_TYPE_DYNAMIC = "Dynamic"

    /**
     Marker for lists (in the lists overview) that are included in the aggregated list.
    */
    static let MARKER_FOR_LIST_INCLUDED = "\u{25C9}"

    /**
     Marker for lists (in the lists overview) that are not included in the aggregated list.
     */
    static let MARKER_FOR_LIST_NOT_INCLUDED = "\u{25CB}"

    /**
     Identifier into the user defaults for the task id counter
     */
    static let USER_DEFAULTS_TASK_ID_COUNTER = "TaskIDCounter"

    /**
     Identifier into the user defaults for the recipe id counter
     */
    static let USER_DEFAULTS_RECIPE_ID_COUNTER = "RecipeIDCounter"

    /**
     Identifier into the user defaults for the notification id counter
     */
    static let USER_DEFAULTS_NOTIFICATION_ID_COUNTER = "NotificationIDCounter"
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class RecipesTableViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    typealias EntityType = Recipe
    var reusableCellIdentifier: String { return "RecipePrototypeCell" }
    var swipedRecipe : Recipe?
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    @IBOutlet fileprivate weak var optionsButton: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSwipeRecognizers()
        self.setupTapRecognizers()
        self.fetchedResultsController = Recipe.recipesWithDelegate(self)
        self.setupSearchBar()
    }

    func setupSearchBar() {
        self.searchBar.delegate = self
    }

    func resetSearchBar() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    @IBAction fileprivate func unwindToList(_ segue: UIStoryboardSegue) {
        // guard let source = segue.source as? AddRecipeViewController else { return }

        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let specificSender = sender as? UITableViewCell {
            let targetController = segue.destination as? RecipeViewController
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedRecipe = self.fetchedResultsController!.object(at: indexPath!)
            targetController?.recipe = selectedRecipe
        } else if let specificSender = sender as? Recipe {
            let targetController = segue.destination as! AddRecipeViewController
            targetController.fetchedRecipe = specificSender
            targetController.editingMode = .edit
        } else {
            let targetController = segue.destination as? AddRecipeViewController
            targetController!.editingMode = .create
        }

        resetSearchBar()
        cancelSearch(self.searchBar)
    }

    // *********************************************************
    //
    // MARK: - Gesture Recognizers
    //
    // *********************************************************

    fileprivate func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(RecipesTableViewController.respondToRightSwipeGesture(_:)))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }

    func setupTapRecognizers() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(respondToLongPressGesture(_:)))
        longPressRecognizer.minimumPressDuration = Constants.LONG_TAP_DURATION
        self.tableView.addGestureRecognizer(longPressRecognizer)
    }

    @objc func respondToLongPressGesture(_ recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            let location = recognizer.location(in: self.view)

            if let swipedIndexPath = self.tableView.indexPathForRow(at: location),
                let longPressedRecipe = self.fetchedResultsController?.object(at: swipedIndexPath) {
                self.editRecipe(longPressedRecipe)
            }
        }
    }

    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: self.view)
        guard let swipedIndexPath = self.tableView.indexPathForRow(at: location) else { return }

        self.swipedRecipe = self.fetchedResultsController!.object(at: swipedIndexPath)

        let optionsString = String.localizedStringWithFormat(NSLocalizedString("Options for %@", comment: ""), (self.swipedRecipe!.value(forKey: RecipeEntity.COL_NAME) as? String)!)
        let createListString = String(NSLocalizedString("Create List", comment: ""))
        let subtractFromSuppliesString = String(NSLocalizedString("Subtract from Supplies", comment: ""))
        let editRecipeString = String(NSLocalizedString("Edit Recipe", comment: ""))
        let shareRecipeString = String(NSLocalizedString("Share Recipe", comment: ""))
        let prepareRecipeString = String(NSLocalizedString("Prepare Recipe", comment: ""))
        let scheduleRecipeString = String(NSLocalizedString("Schedule Recipe", comment: ""))
        let cancelString = String(NSLocalizedString("Cancel", comment: ""))

        let doneListOptions = UIAlertController(title: optionsString, message: nil, preferredStyle:.actionSheet)
        let createListOption = UIAlertAction(title: createListString, style: .default, handler: {_ in self.respondToListOption("Create List", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let subtractFromSuppliesOption = UIAlertAction(title: subtractFromSuppliesString, style: .default, handler: {_ in self.respondToListOption("Subtract Recipe", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let editRecipeOption = UIAlertAction(title: editRecipeString, style: .default, handler: {_ in self.respondToListOption("Edit Recipe", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let shareRecipeOption = UIAlertAction(title: shareRecipeString, style: .default, handler: {_ in self.respondToListOption("Share Recipe", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let prepareRecipeOption = UIAlertAction(title: prepareRecipeString, style: .default, handler: {_ in self.respondToListOption("Prepare Recipe", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let scheduleRecipeOption = UIAlertAction(title: scheduleRecipeString, style: .default, handler: {_ in self.respondToListOption("Schedule Recipe", recipe: self.swipedRecipe!, atIndexPath: swipedIndexPath)})
        let cancelOption = UIAlertAction(title: cancelString, style:.cancel, handler:nil)

        let popOver = doneListOptions.popoverPresentationController
        popOver?.sourceView = self.tableView.cellForRow(at: swipedIndexPath)
        popOver?.sourceRect = self.tableView.cellForRow(at: swipedIndexPath)!.bounds

        doneListOptions.addAction(createListOption)
        doneListOptions.addAction(subtractFromSuppliesOption)
        doneListOptions.addAction(editRecipeOption)
        doneListOptions.addAction(shareRecipeOption)
        doneListOptions.addAction(prepareRecipeOption)
        doneListOptions.addAction(scheduleRecipeOption)
        doneListOptions.addAction(cancelOption)
        self.present(doneListOptions, animated: true, completion: nil)

        DBHelper.instance.saveContext()
    }

    fileprivate func respondToListOption(_ option: String, recipe: Recipe, atIndexPath swipedIndexPath: IndexPath) {
        switch option {
        case "Create List": self.askForNumberOfPortions(.create_LIST, recipe: recipe)
        case "Subtract Recipe": self.askForNumberOfPortions(.subtract_RECIPE, recipe: recipe)
        case "Edit Recipe": self.editRecipe(recipe)
        case "Share Recipe": self.shareRecipe(recipe, atIndexPath: swipedIndexPath)
        case "Prepare Recipe": self.prepareRecipe(recipe)
        case "Schedule Recipe": self.scheduleRecipe(recipe)
        default: break;
        }
    }

    // *********************************************************
    //
    // MARK: - Options For Single Recipes
    //
    // *********************************************************

    func createListFromRecipe(_ recipe: Recipe, _ textField: UITextField?) {
        guard
            let enteredText = textField?.text,
            let numberOfPortionsFromUser = Double(enteredText),
            numberOfPortionsFromUser > 0
            else {
                return
        }

        guard
            let numberOfPortionsFromRecipe = recipe.portions?.doubleValue,
            numberOfPortionsFromRecipe > 0
            else {
                return
        }

        let portionsMultiplier = numberOfPortionsFromUser / numberOfPortionsFromRecipe
        let context = DBHelper.instance.getManagedObjectContext()
        guard
            let newShoppingList = ShoppingList.insertNewShoppingList(withName: recipe.name!, andType: "Dynamic", includeInAggregateList: true, intoContext: context)
            else {
                return
        }

        var ingredientCounter = 0;

        guard
            let ingredientsForRecipe = recipe.ingredientsForRecipe?.filter({($0 as! RecipeIngredient).includeInList! == true}) as? [RecipeIngredient]
            else {
                return
        }

        for ingredient in ingredientsForRecipe {
            let ingredientName = ingredient.name
            let ingredientQuantity = ingredient.quantity
            let ingredientUnit = ingredient.unit

            guard let quantity = ingredientQuantity?.doubleValue, quantity > 0 else { continue }

            let newListItem = ShoppingListItem.insertNewShoppingListItem(withName: ingredientName!, quantity: NSNumber(value: quantity * portionsMultiplier as Double), unit: ingredientUnit, doneStatus: false, brand: nil, forPrice: nil, atCurrency: nil, inShop: "", forList: newShoppingList, intoContext: context)
            AggregatedListHelper.instance.addItem(newListItem)
            ingredientCounter += 1;
        }

        if(ingredientCounter == 1) {
            let createdListString = NSLocalizedString("Created list with one item.", comment:"");
            GeneralHelper.presentInformationDialogWithText(createdListString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if(ingredientCounter > 1) {
            let createdListString = NSString.localizedStringWithFormat(NSLocalizedString("Created list with %i items.", comment:"") as NSString, ingredientCounter)
            GeneralHelper.presentInformationDialogWithText(createdListString as String, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else {
            context.delete(newShoppingList)
        }

        DBHelper.instance.saveContext();
    }

    func createListFromRecipe2(_ recipe: Recipe, _ textFields: [UITextField]?) {
        self.createListFromRecipe(recipe, textFields![0])
    }

    func subtractRecipeFromSupplies(_ recipe: Recipe, _ textField : UITextField?) {
        guard let enteredText = textField?.text, let numberOfPortionsFromUser = Double(enteredText), numberOfPortionsFromUser > 0 else {
            return
        }

        guard let numberOfPortionsFromRecipe = recipe.portions?.doubleValue, numberOfPortionsFromRecipe > 0 else {
            return
        }

        let portionsMultiplier = numberOfPortionsFromUser / numberOfPortionsFromRecipe
        guard let Ingredients = recipe.ingredientsForRecipe else { return }
        let controllerForSuppliesItems = SuppliesItem.getAll()
        var updateCounter = 0

        for ingredient in Ingredients {
            guard let ingredient = ingredient as? RecipeIngredient else { continue }
            let ingredientName = ingredient.name

            for suppliesItem in controllerForSuppliesItems {
                let suppliesItemName = suppliesItem.itemName

                if suppliesItemName == ingredientName {
                    guard let ingredientUnit = ingredient.unit, let suppliesItemUnit = suppliesItem.curAmountUnit else { continue }

                    if ingredientUnit == suppliesItemUnit || UnitPickerHelper.baseUnit(ingredientUnit, isConvertibleToTargetUnit: suppliesItemUnit) {
                        let ingredientAmount = ingredient.quantity
                        let ingredientAmountMapped = UnitPickerHelper.convertValue(ingredientAmount!, fromBaseUnit: ingredientUnit, toTargetUnit: suppliesItemUnit)
                        let ingredientAmountMappedDouble = ingredientAmountMapped.doubleValue * portionsMultiplier
                        guard let suppliesItemAmountDouble = suppliesItem.curAmountQuantity?.doubleValue else { continue }

                        let newSuppliesItemAmount = max(0, suppliesItemAmountDouble - ingredientAmountMappedDouble)
                        suppliesItem.setValue(NSNumber(value: newSuppliesItemAmount as Double), forKey: SuppliesItem.COL_CUR_AMOUNT_QUANTITY)
                        updateCounter += 1
                    }
                }
            }
        }

        if(updateCounter == 1) {
            let createdListString = NSLocalizedString("Updated one item.", comment:"");
            GeneralHelper.presentInformationDialogWithText(createdListString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if(updateCounter > 1) {
            let createdListString = NSString.localizedStringWithFormat(NSLocalizedString("Updated %i items.", comment:"") as NSString, updateCounter)
            GeneralHelper.presentInformationDialogWithText(createdListString as String, andTitle: Constants.TITLE_ALERT_SUCCESS)
        }

        DBHelper.instance.saveContext()
    }

    func subtractRecipeFromSupplies2(_ recipe: Recipe, _ textFields : [UITextField]?) {
        self.subtractRecipeFromSupplies(recipe, textFields![0])
    }

    func editRecipe(_ recipe: Recipe) {
        self.performSegue(withIdentifier: "EditRecipeSegue", sender: recipe)
    }

    func shareRecipe(_ recipe: Recipe, atIndexPath swipedIndexPath: IndexPath) {
        guard let url = recipe.exportToFileURL() else { return }
        let activityViewController = UIActivityViewController(activityItems: ["", url], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.addToReadingList, .assignToContact, .openInIBooks, .postToFacebook, .postToFlickr, .postToTencentWeibo, .postToTwitter, .postToVimeo, .postToWeibo, .print, .saveToCameraRoll]
        let popOver = activityViewController.popoverPresentationController
        popOver?.sourceView = self.tableView.cellForRow(at: swipedIndexPath)
        popOver?.sourceRect = self.tableView.cellForRow(at: swipedIndexPath)!.bounds
        present(activityViewController, animated: true, completion: nil)
    }

    func prepareRecipe(_ recipe: Recipe) {
        self.scheduleRecipe(recipe, toStartAt: NSDate(), skipFirstTask: true)
    }

    func scheduleRecipe(_ recipe: Recipe) {
        // present dialog for asking for a start time or a target time for the recipe, then either call
        // self.scheduleRecipe(recipe, at: #startTime) or self.scheduleRecipe(recipe, toFinishAt: #targetTime)
        
        if let pickerVC = storyboard?.instantiateViewController(withIdentifier: "DateAndTimeViewController") as? DateAndTimeViewController {
            pickerVC.setUp(for: recipe)
            
            pickerVC.onStartAtSelectedDateCallback = { date in
                self.dismiss(animated: true, completion: nil)
                self.scheduleRecipe(recipe, toStartAt: date.zeroSeconds! as NSDate, skipFirstTask: false)
            }

            pickerVC.onFinishAtSelectedDateCallback = {date in
                self.dismiss(animated: true, completion: nil)
                self.scheduleRecipe(recipe, toFinishAt: date.zeroSeconds! as NSDate)
            }

            pickerVC.onCancelCallback = {
                self.dismiss(animated: true, completion: nil)
            }
            
            pickerVC.onDateChangedCallback = {date in
                pickerVC.setButtonTitles(for: date, recipe: recipe)
                pickerVC.setButtonAvailability(for: date, recipe: recipe)
            }
            
            self.present(pickerVC, animated: true, completion: nil)
        }
    }

    func scheduleRecipe(_ recipe: Recipe, toFinishAt targetDate: NSDate) {
        if let totalTimeInMinutes = recipe.totalTimeInMinutes() {
            let startDate = targetDate.addingTimeInterval(Double(-totalTimeInMinutes * 60))
            scheduleRecipe(recipe, toStartAt: startDate, skipFirstTask: false)
        }
    }

    func scheduleRecipe(_ recipe: Recipe, toStartAt startTime: NSDate, skipFirstTask: Bool) {
        // create tasks and notifications for each of the recipe's steps

        // it would probably better to show an info if there are no preparations
        guard let recipePreparations = recipe.preparationsForRecipe?.allObjects as? [RecipePreparation],
        recipePreparations.count > 0 else { return }

        let calendar = Calendar.current
        var timeDue = startTime
        let notificationController = NotificationController.sharedInstance
        let sortedRecipePreparations = recipePreparations.sorted(by: { $0.number!.intValue < $1.number!.intValue })

        for index in 0..<sortedRecipePreparations.count {
            let preparation = sortedRecipePreparations[index]

            if !skipFirstTask || index > 0 {
                // task
                let taskBody = (preparation.summary == nil || preparation.summary!.isEmpty) ? preparation.preparation! : preparation.summary!
                let task = Task.insertNewTask(withTitle: recipe.name!, andBody: taskBody, andDateDue: timeDue, andDateCreated: NSDate(), intoContext: DBHelper.instance.getManagedObjectContext())

                // notification
                let dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: timeDue as Date)
                let notificationDetails = notificationController.prepareNotification(forCategory: .recipeTask, withTitle: recipe.name!, andBody: taskBody, toAppearAt: dateComponents, taskId: task.id!.intValue, recipeId: recipe.id!.intValue)
                notificationController.scheduleNotification(with: notificationDetails)
            }

            let prepTime = preparation.timePrepare?.intValue ?? 0
            let waitTime = preparation.timeWait?.intValue ?? 0
            let totalTime : TimeInterval = Double(prepTime + waitTime) * 60
            timeDue = timeDue.addingTimeInterval(totalTime)
        }
        
        DBHelper.instance.saveContext()
    }

    // *********************************************************
    //
    // MARK: - Options For All Recipes
    //
    // *********************************************************

    @IBAction fileprivate func optionsForRecipes() {
        let optionsString = NSLocalizedString("Options for recipes", comment: "")
        let removeFiltersString = NSLocalizedString("Remove Filters", comment: "")
        let filterBySuppliesString = NSLocalizedString("Filter by Supplies", comment: "")
        let cancelString = NSLocalizedString("Cancel", comment: "")

        let recipeOptions = UIAlertController(title: optionsString, message: nil, preferredStyle:.actionSheet)
        recipeOptions.popoverPresentationController?.barButtonItem = self.optionsButton

        let removeFiltersOption = UIAlertAction(title: removeFiltersString, style: .default, handler: {_ in self.removeFilters()})
        let filterBySuppliesOption = UIAlertAction(title: filterBySuppliesString, style: .default, handler: {_ in self.askForNumberOfPortions(.filter_BY_SUPPLIES, recipe:nil)})
        let cancelOption = UIAlertAction(title: cancelString, style:.cancel, handler:nil)
        recipeOptions.addAction(removeFiltersOption)
        recipeOptions.addAction(filterBySuppliesOption)
        recipeOptions.addAction(cancelOption)

        self.present(recipeOptions, animated: true, completion: nil)
    }

    func removeFilters() {
        self.navigationItem.title = NSLocalizedString("Recipes", comment: "")

        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeEntity.CACHE_NAME)
        self.fetchedResultsController!.fetchRequest.predicate = nil
        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        self.tableView.reloadData()
    }

    func filterRecipesBySuppliesItems(_ textField: UITextField?) {
        guard
            let enteredText = textField?.text,
            let numberOfPortionsFromUser = Double(enteredText),
            numberOfPortionsFromUser > 0
            else {
                return
        }

        // set column 'possible with supplies' to true if recipe can be cooked with the supplies, otherwise set it to false
        // generate nsfetchedresultscontroller with results where the aforementioned column is set to true
        let suppliesItems = SuppliesItem.getAll()

        for recipe in self.fetchedResultsController!.fetchedObjects! {
            var numberOfImpossibleIngredients = 0

            guard
                let numberOfPortionsFromRecipe = recipe.portions?.doubleValue,
                numberOfPortionsFromRecipe > 0
                else {
                    recipe.possibleWithSupplies = NSNumber(value: false)
                    continue
            }

            let portionsMultiplier = numberOfPortionsFromUser / numberOfPortionsFromRecipe

            guard
                let relevantIngredients = recipe.ingredientsForRecipe?.filter({($0 as! RecipeIngredient).includeInSearch == true}) as? [RecipeIngredient]
                else {
                    recipe.possibleWithSupplies = NSNumber(value: false)
                    continue
            }

            let numberOfRelevantIngredients = relevantIngredients.count
            if numberOfRelevantIngredients == 0 {
                recipe.possibleWithSupplies = NSNumber(value: true)
                continue
            }

            for ingredient in relevantIngredients {
                let ingredientName = ingredient.name
                let ingredientUnit = ingredient.unit
                var ingredientPossible = false

                for suppliesItem in suppliesItems {
                    let itemName = suppliesItem.itemName!
                    guard
                        let itemUnit = suppliesItem.curAmountUnit,
                        let itemQuantity = suppliesItem.curAmountQuantity
                        else {
                            return
                    }

                    if ingredientName == itemName && (ingredientUnit == itemUnit || UnitPickerHelper.baseUnit(ingredientUnit, isConvertibleToTargetUnit: itemUnit)) {
                        let ingredientQuantity = ingredient.quantity
                        let ingredientQuantityMapped = UnitPickerHelper.convertValue(ingredientQuantity, fromBaseUnit: ingredientUnit, toTargetUnit: itemUnit)
                        let ingredientQuantityMappedForNumberOfPortions = ingredientQuantityMapped.doubleValue * portionsMultiplier
                        if ingredientQuantityMappedForNumberOfPortions * (1 - RecipeEntity.QUANTITY_TOLERANCE) <= itemQuantity.doubleValue {
                            ingredientPossible = true
                            break;
                        }
                    }
                }

                if !ingredientPossible {
                    numberOfImpossibleIngredients += 1
                }
            }

            let remainingIngredients = Double(numberOfRelevantIngredients - numberOfImpossibleIngredients) / Double(numberOfRelevantIngredients)

            if remainingIngredients < Constants.MIN_PERCENTAGE_OF_REMAINING_INGREDIENTS {
                recipe.possibleWithSupplies = NSNumber(value: false)
            } else {
                recipe.possibleWithSupplies = NSNumber(value: true)
            }
        }

        DBHelper.instance.saveContext()

        self.navigationItem.title = NSLocalizedString("Recipes (filtered)", comment: "")

        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeEntity.CACHE_NAME)
        let filterPredicate = NSPredicate(format: "%K = %@", RecipeEntity.COL_POSSIBLE_WITH_SUPPLIES, NSNumber(value: true as Bool))
        self.fetchedResultsController!.fetchRequest.predicate = filterPredicate
        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        self.tableView.reloadData()
    }

    func filterBySuppliesItems(_ textFields : [UITextField]?) {
        self.filterRecipesBySuppliesItems(textFields![0])
    }

    // *********************************************************
    //
    // MARK: - Dialog For Information From User
    //
    // *********************************************************

    func askForNumberOfPortions(_ operation: RecipeOperations, recipe: Recipe?) {
        let originalNumberOfPortions = recipe?.portions

        let numberOfPortionsController = UIAlertController(title: nil, message: NSLocalizedString("How many portions?", comment: ""), preferredStyle: .alert)
        numberOfPortionsController.addTextField(configurationHandler: {textField in textField.keyboardType = .numberPad})
        numberOfPortionsController.textFields![0].text = originalNumberOfPortions?.stringValue
        switch operation {
        case .create_LIST: numberOfPortionsController.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in self.createListFromRecipe2(recipe!, numberOfPortionsController.textFields)}))
        case .filter_BY_SUPPLIES: numberOfPortionsController.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in self.filterBySuppliesItems(numberOfPortionsController.textFields)}))
        case .subtract_RECIPE: numberOfPortionsController.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in self.subtractRecipeFromSupplies2(recipe!, numberOfPortionsController.textFields)}))
        }
        numberOfPortionsController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        self.present(numberOfPortionsController, animated: true, completion: nil)
    }

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedRecipe = self.fetchedResultsController!.object(at: indexPath)
            let context = self.fetchedResultsController!.managedObjectContext
            context.delete(swipedRecipe )
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }

        let object = self.fetchedResultsController!.object(at: indexPath)
        let recipeName = (object.value(forKey: RecipeEntity.COL_NAME)! as! String).description
        let recipePortions = object.value(forKey: RecipeEntity.COL_PORTIONS) as! Int
        if recipePortions > 1 {
            let recipeNameDisplayString = String.localizedStringWithFormat(NSLocalizedString("%@ (%d portions)", comment:""), recipeName, recipePortions)
            cell.textLabel!.text = recipeNameDisplayString;
        } else {
            let recipeNameDisplayString = String.localizedStringWithFormat(NSLocalizedString("%@ (1 portion)", comment:""), recipeName)
            cell.textLabel!.text = recipeNameDisplayString;
        }
        cell.accessoryType = .disclosureIndicator;
    }

    // *********************************************************
    //
    // MARK: - UISearchBarDelegate
    //
    // *********************************************************

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            cancelSearch(searchBar)
        } else {
            let searchTextComponents = searchText.components(separatedBy: " ")
            var namePredicates = [NSPredicate]()
            var ingredientNamePredicates = [NSPredicate]()
            var categoryNamePredicates = [NSPredicate]()
            var compoundPredicates = [NSPredicate]()

            for component in searchTextComponents {

                if component.isEmpty || component.count < 2 {
                    continue
                }

                let namePredicate = NSPredicate(format: "%K contains[c] %@", RecipeEntity.COL_NAME, component)
                namePredicates.append(namePredicate)

                let ingredientNamePredicate = NSPredicate(format: "SUBQUERY(ingredientsForRecipe, $ing, $ing.includeInSearch = %@ and $ing.name contains[c] %@).@count > 0", NSNumber(booleanLiteral: true), component)
                ingredientNamePredicates.append(ingredientNamePredicate)

                let categoryNamePredicate = NSPredicate(format: "SUBQUERY(categoriesForRecipe, $cat, $cat.name contains[c] %@).@count > 0", component)
                categoryNamePredicates.append(categoryNamePredicate)
            }

            compoundPredicates.append(NSCompoundPredicate(type: .or, subpredicates: namePredicates))
            compoundPredicates.append(NSCompoundPredicate(type: .and, subpredicates: ingredientNamePredicates))
            compoundPredicates.append(NSCompoundPredicate(type: .or, subpredicates: categoryNamePredicates))
            
            let compoundPredicate = NSCompoundPredicate(type: .or, subpredicates: compoundPredicates)

            self.navigationItem.title = NSLocalizedString("Recipes (filtered)", comment: "")
            refreshTableView(withPredicate: compoundPredicate)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetSearchBar()
        cancelSearch(searchBar)
    }

    // *********************************************************
    //
    // MARK: - Cancel Search
    //
    // *********************************************************

    func cancelSearch(_ searchBar: UISearchBar) {
        self.navigationItem.title = NSLocalizedString("Recipes", comment: "")
        refreshTableView(withPredicate: nil)
    }

    func refreshTableView(withPredicate predicate: NSPredicate?) {
        self.fetchedResultsController.fetchRequest.predicate = predicate
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeEntity.CACHE_NAME)

        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        self.tableView.reloadData()
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

extension SuppliesItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SuppliesItem> {
        return NSFetchRequest<SuppliesItem>(entityName: "SuppliesItem")
    }

    @NSManaged public var curAmountQuantity: NSNumber?
    @NSManaged public var curAmountUnit: String?
    @NSManaged public var itemName: String?
    @NSManaged public var minAmountQuantity: NSNumber?
    @NSManaged public var minAmountUnit: String?
    @NSManaged public var categoriesForSuppliesItem: NSSet?

}

// MARK: Generated accessors for categoriesForSuppliesItem
extension SuppliesItem {

    @objc(addCategoriesForSuppliesItemObject:)
    @NSManaged public func addToCategoriesForSuppliesItem(_ value: SuppliesItemCategory)

    @objc(removeCategoriesForSuppliesItemObject:)
    @NSManaged public func removeFromCategoriesForSuppliesItem(_ value: SuppliesItemCategory)

    @objc(addCategoriesForSuppliesItem:)
    @NSManaged public func addToCategoriesForSuppliesItem(_ values: NSSet)

    @objc(removeCategoriesForSuppliesItem:)
    @NSManaged public func removeFromCategoriesForSuppliesItem(_ values: NSSet)

}

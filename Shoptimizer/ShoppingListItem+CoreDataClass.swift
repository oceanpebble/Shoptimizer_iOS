// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct ShoppingListItemEntity {
    static let CACHE_NAME = "listItems"
    static let COL_DONE = "done"
    static let COL_ITEM_AMOUNT_QUANTITY = "itemAmountQuantity"
    static let COL_ITEM_AMOUNT_UNIT = "itemAmountUnit"
    static let COL_ITEM_NAME = "itemName"
    static let COL_SHOP_NAME = "shopName"
    static let COL_BRAND_NAME = "brandName"
    static let COL_ITEM_PRICE = "itemPrice"
    static let COL_ITEM_CURRENCY = "itemCurrency"
    static let COL_NOTES = "notes"
    static let ENTITY_NAME = "ShoppingListItem"
    static let REF_LISTFORITEM = "listForItem"
}

public class ShoppingListItem: NSManagedObject {
    
    // MARK: - Create
    
    static func insert(name: String?, done: Bool?, amountQuantity: Double?, amountUnit: String?, price: Double?, currency: String?, shopName: String?, brandName: String?, notes: String?) -> ShoppingListItem {
        let item = ShoppingListItem(entity: entityDescription()!, insertInto: DBHelper.instance.getManagedObjectContext())
        item.itemName = name
        item.done = done != nil ? NSNumber(value: done!) : false
        item.itemAmountQuantity = amountQuantity != nil ? NSNumber(value: amountQuantity!) : nil
        item.itemAmountUnit = amountUnit
        item.itemPrice = price != nil ? NSNumber(value: price!) : nil
        item.itemCurrency = currency
        item.shopName = shopName
        item.brandName = brandName
        item.notes = notes
        DBHelper.instance.saveContext()
        return item
    }
    
    private static func entityDescription() -> NSEntityDescription? {
        return entityDescription(forEntityName: ShoppingListItemEntity.ENTITY_NAME)
    }
    
    // MARK: - Delete

    static func deleteAll() {
        deleteAll(entityName: ShoppingListItemEntity.ENTITY_NAME)
    }

    static func undoneItemsForListsContainedInAggregatedList() -> [ShoppingListItem] {
        var items = [ShoppingListItem]()

        for list in ShoppingList.shoppingLists() {
            if list.include_in_aggregate ==  NSNumber(value: true) {
                let undoneItems = list.undoneItems()
                items.append(contentsOf: undoneItems)
            }
        }

        return items
    }

    func delete() {
        guard let list = self.listForItem else { return }

        if (list.include_in_aggregate as! Bool)
            && !(self.done as! Bool) {
            AggregatedListHelper.instance.subtractItem(self)
        }
        DBHelper.instance.getManagedObjectContext().delete(self)
        DBHelper.instance.saveContext()
    }

    static func itemsForShoppingList(_ list:NSManagedObject, andDelegate delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<ShoppingListItem> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListItemEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let predicate = NSPredicate(format:"%K = %@", ShoppingListItemEntity.REF_LISTFORITEM, list)
        fetchRequest.predicate = predicate

        let sortDescriptorShopName = NSSortDescriptor(key:ShoppingListItemEntity.COL_SHOP_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptorDoneStatus = NSSortDescriptor(key:ShoppingListItemEntity.COL_DONE, ascending:true)
        let sortDescriptorItemName = NSSortDescriptor(key:ShoppingListItemEntity.COL_ITEM_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorShopName, sortDescriptorDoneStatus, sortDescriptorItemName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:ShoppingListItemEntity.COL_SHOP_NAME, cacheName:ShoppingListItemEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController
    }
    
    /**
     Creates a new ShoppingListItem instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency:String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList, intoContext context: NSManagedObjectContext) -> ShoppingListItem {
        let listItemEntity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in:context)
        let listItem = ShoppingListItem(entity: listItemEntity!, insertInto: context)
        listItem.itemName = itemName
        listItem.itemAmountQuantity = itemAmountQuantity
        listItem.itemAmountUnit = itemAmountUnit
        listItem.shopName = shopName
        listItem.done = NSNumber(value: doneStatus as Bool)
        listItem.brandName = brandName
        listItem.itemPrice = itemPrice
        listItem.itemCurrency = itemCurrency
        listItem.listForItem = listForItem
        listItem.notes = notes

        return listItem;
    }

    static func allItems() -> [ShoppingListItem]? {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListItemEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingListItem> = ShoppingListItem.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListItemEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptorShopName = NSSortDescriptor(key:ShoppingListItemEntity.COL_SHOP_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptorDoneStatus = NSSortDescriptor(key:ShoppingListItemEntity.COL_DONE, ascending:true)
        let sortDescriptorItemName = NSSortDescriptor(key:ShoppingListItemEntity.COL_ITEM_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorShopName, sortDescriptorDoneStatus, sortDescriptorItemName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:ShoppingListItemEntity.COL_SHOP_NAME, cacheName:ShoppingListItemEntity.CACHE_NAME)

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController.fetchedObjects
    }

    static func deleteObsoleteListItems() {
        if let allItems = self.allItems() {
            for item in allItems {
                if item.listForItem == nil {
                    DBHelper.instance.getManagedObjectContext().delete(item)
                }
            }
        }
    }
}

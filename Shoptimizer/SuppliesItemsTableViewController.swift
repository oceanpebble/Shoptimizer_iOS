// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class SuppliesItemsTableViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    typealias EntityType = SuppliesItem
    var reusableCellIdentifier: String { return "SuppliesItemTableViewCell" }
    fileprivate var suppliesItem: SuppliesItem?
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    @IBOutlet weak var searchBar: UISearchBar!

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchedResultsController = SuppliesItem.fetchedResultsController(delegate: self)
        self.setupSearchBar()
    }

    func setupSearchBar() {
        self.searchBar.delegate = self
    }

    func resetSearchBar() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    @IBAction func unwindToList(_ segue: UIStoryboardSegue) {
        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as? AddSuppliesItemViewController

        if let specificSender = sender as? UITableViewCell {
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedItem = self.fetchedResultsController?.object(at: indexPath!)
            targetController?.fetchedSuppliesItem = selectedItem;
            targetController?.editingMode = .edit;
        } else {
            targetController?.editingMode = .create;
        }

        resetSearchBar()
        cancelSearch(self.searchBar)
    }

    // *********************************************************
    //
    // MARK: - Options For Supplies Items
    //
    // *********************************************************

    @IBAction func createShoppingListFromSupplies(_ sender: AnyObject) {
        let context = DBHelper.instance.getManagedObjectContext()

        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let formattedDate = formatter.string(from: date)

        let newListName = NSLocalizedString("Refill supplies", comment: "") + " - \(formattedDate)"
        let newShoppingList = ShoppingList.insertNewShoppingList(withName: newListName, andType: "Dynamic", includeInAggregateList: true, intoContext: context)

        var numItems = 0
        for suppliesItem in (self.fetchedResultsController?.fetchedObjects)! {
            guard let itemMinAmountQuantity = suppliesItem.minAmountQuantity,
            let itemMinAmountUnit = suppliesItem.minAmountUnit,
            let itemCurAmountQuantity = suppliesItem.curAmountQuantity,
                let itemCurAmountUnit = suppliesItem.curAmountUnit else { continue }

            let itemName = suppliesItem.itemName!
            let itemMinAmountQuantityDouble = itemMinAmountQuantity.doubleValue
            let itemCurAmountQuantityMapped = UnitPickerHelper.convertValue(itemCurAmountQuantity, fromBaseUnit:itemCurAmountUnit, toTargetUnit:itemMinAmountUnit)

            if itemMinAmountUnit.isNotEmpty && (itemMinAmountUnit == itemCurAmountUnit || UnitPickerHelper.baseUnit(itemCurAmountUnit, isConvertibleToTargetUnit:itemMinAmountUnit)) {
                let amountToBuy = itemMinAmountQuantityDouble - itemCurAmountQuantityMapped.doubleValue
                if amountToBuy > 0 {
                    let newListItem = ShoppingListItem.insertNewShoppingListItem(withName: itemName, quantity: NSNumber(value: amountToBuy), unit: itemMinAmountUnit, doneStatus: false, brand: nil, forPrice: nil, atCurrency: nil, inShop: nil, forList: newShoppingList!, intoContext: context)
                    AggregatedListHelper.instance.addItem(newListItem)
                    numItems += 1
                }
            }
        }

        if(numItems == 1) {
            let createdListString = NSLocalizedString("Created list with one item.", comment:"");
            GeneralHelper.presentInformationDialogWithText(createdListString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if(numItems > 1) {
            let createdListString = String(format:NSLocalizedString("Created list with %i items.", comment:""), numItems)
            GeneralHelper.presentInformationDialogWithText(createdListString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else {
            context.delete(newShoppingList!)
        }

        DBHelper.instance.saveContext()
    }

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let swipedItem = self.fetchedResultsController!.object(at: indexPath)
            let context = self.fetchedResultsController!.managedObjectContext
            context.delete(swipedItem)
        } else if (editingStyle == .insert) {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        } else {
            //nothing going on here
        }
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }

        guard let object = self.fetchedResultsController?.object(at: indexPath) else { return }

        // Amount
        let minAmountQuantity = object.minAmountQuantity == nil ? NSNumber(value: 0) : object.minAmountQuantity!
        let convertedMinAmountQuantity = GeneralHelper.decimalFormatter().string(from: minAmountQuantity)
        let minAmountUnit = UnitPickerHelper.translate(unit: object.minAmountUnit ?? "l")
        let minimumAmount = String(format: "%@ %@", convertedMinAmountQuantity!, minAmountUnit)

        let curAmountQuantity = object.curAmountQuantity == nil ? NSNumber(value: 0) : object.curAmountQuantity!
        let convertedCurAmountQuantity = GeneralHelper.decimalFormatter().string(from: curAmountQuantity)
        let curAmountUnit = UnitPickerHelper.translate(unit: object.curAmountUnit ?? "l")
        let currentAmount = String(format:"%@ %@", convertedCurAmountQuantity!, curAmountUnit)
        let curAmountQuantityMapped = UnitPickerHelper.convertValue(curAmountQuantity, fromBaseUnit: curAmountUnit, toTargetUnit: minAmountUnit)

        let amountStringLocalizedFormat = NSLocalizedString("Minimum: %@\t\tCurrent: %@", comment:"")
        let amountString = String(format:amountStringLocalizedFormat, minimumAmount, currentAmount)
        cell.detailTextLabel?.text = amountString

        // Name
        let itemName = object.itemName!

        if curAmountUnit == minAmountUnit || UnitPickerHelper.baseUnit(curAmountUnit, isConvertibleToTargetUnit:minAmountUnit) {
            let curAmountQuantityDouble = curAmountQuantityMapped.doubleValue
            let minAmountQuantityDouble = minAmountQuantity.doubleValue
            if curAmountQuantityDouble <= 1.3 * minAmountQuantityDouble && curAmountQuantityDouble >= minAmountQuantityDouble {
                cell.textLabel?.attributedText = NSAttributedString(string: itemName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.orange])
                cell.accessibilityIdentifier = AccessibilityUtil.identifierForLowItems
            } else if curAmountQuantityDouble < minAmountQuantityDouble {
                cell.textLabel?.attributedText = NSAttributedString(string: itemName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                cell.accessibilityIdentifier = AccessibilityUtil.identifierForOutItems
            } else {
                cell.textLabel?.attributedText = NSAttributedString(string: itemName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.label])
                cell.accessibilityIdentifier = AccessibilityUtil.identifierForStockedItems
            }
        } else {
            cell.textLabel?.attributedText = NSAttributedString(string: itemName, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            cell.accessibilityIdentifier = AccessibilityUtil.identifierForStockedItems
        }
        cell.accessoryType = .disclosureIndicator
    }

    // *********************************************************
    //
    // MARK: - UISearchBarDelegate
    //
    // *********************************************************

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            cancelSearch(searchBar)
        } else {
            self.navigationItem.title = NSLocalizedString("Supplies (filtered)", comment: "")
            let predicate = SuppliesItem.searchPredicate(for: searchText)
            refreshTableView(withPredicate: predicate)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetSearchBar()
        cancelSearch(searchBar)
    }

    // *********************************************************
    //
    // MARK: - Cancel Search
    //
    // *********************************************************

    func cancelSearch(_ searchBar: UISearchBar) {
        self.navigationItem.title = NSLocalizedString("Supplies", comment: "")
        refreshTableView(withPredicate: nil)
    }

    func refreshTableView(withPredicate predicate: NSPredicate?) {
        self.fetchedResultsController.fetchRequest.predicate = predicate
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: SuppliesItem.CACHE_NAME)

        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        self.tableView.reloadData()
    }
}

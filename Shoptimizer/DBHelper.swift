// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class DBHelper {
    fileprivate var managedObjectContext: NSManagedObjectContext?
    fileprivate var managedObjectModel: NSManagedObjectModel?
    fileprivate var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    static var instance = DBHelper()

    fileprivate init() {

    }

    func applicationDocumentsDirectory() -> URL {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "de.florianschoeler.Shoptimizer" in the application's documents directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }

    func getManagedObjectModel() -> NSManagedObjectModel {
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        if self.managedObjectModel != nil {
            return self.managedObjectModel!
        }
        let modelURL = Bundle.main.url(forResource: Constants.DATA_MODEL_NAME, withExtension:"momd")!
        self.managedObjectModel = NSManagedObjectModel(contentsOf:modelURL)!
        return self.managedObjectModel!;
    }

    func getPersistentStoreCoordinator() -> NSPersistentStoreCoordinator {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
        if self.persistentStoreCoordinator != nil {
            return self.persistentStoreCoordinator!
        }

        // Create the coordinator and store
        self.persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel:self.getManagedObjectModel())
        let options = [NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true]
        let storeURL = self.applicationDocumentsDirectory().appendingPathComponent(Constants.DATA_MODEL_NAME + ".sqlite")
        let failureReason = "There was an error creating or loading the application's saved data."

        do {
            try self.persistentStoreCoordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at:storeURL, options: options)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "Shoptimizer", code: 4711, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }

        return self.persistentStoreCoordinator!
    }


    func getManagedObjectContext() -> NSManagedObjectContext {
        if self.managedObjectContext != nil {
            return self.managedObjectContext!
        }

        let coordinator = self.getPersistentStoreCoordinator()
        self.managedObjectContext = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        self.managedObjectContext!.persistentStoreCoordinator = coordinator
        self.managedObjectContext!.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return self.managedObjectContext!
    }

    func saveContext() {
        let managedObjectContext = self.getManagedObjectContext()
        if(managedObjectContext.hasChanges) {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

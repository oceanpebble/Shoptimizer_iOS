// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var dateCreated: NSDate?
    @NSManaged public var dateDue: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var body: String?
    @NSManaged public var dateCompleted: NSDate?
    @NSManaged public var done: NSNumber?
    @NSManaged public var id: NSNumber?
}

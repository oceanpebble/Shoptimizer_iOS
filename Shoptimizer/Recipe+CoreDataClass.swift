// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct RecipeEntity {
    static let CACHE_NAME = "recipes"
    static let COL_NAME = "name"
    static let COL_PORTIONS = "portions"
    static let COL_ID = "id"
    static let COL_POSSIBLE_WITH_SUPPLIES = "possibleWithSupplies"
    static let REF_INGREDIENTS_FOR_RECIPE = "ingredientsForRecipe"
    static let REF_PREPARATIONS_FOR_RECIPE = "preparationsForRecipe"
    static let ENTITY_NAME = "Recipe"
    static let QUANTITY_TOLERANCE = 0.05
}

public class Recipe: NSManagedObject {

    static var nextID : Int {
        let userDefaults = UserDefaults.standard
        let idFromDefaults = userDefaults.integer(forKey: Constants.USER_DEFAULTS_RECIPE_ID_COUNTER)
        let newId = idFromDefaults + 1
        userDefaults.set(newId, forKey: Constants.USER_DEFAULTS_RECIPE_ID_COUNTER)
        userDefaults.synchronize()
        return newId
    }

    static func recipes() -> [Recipe] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(Recipe.fetchRequest())
        } catch {
            return [Recipe]()
        }
    }
    
    static func deleteAllRecipes() {
        let context = DBHelper.instance.getManagedObjectContext()
        for recipe in recipes() {
            context.delete(recipe)
        }
        DBHelper.instance.saveContext()
    }

    /**
     Creates and returns an NSFetchedResultsController containing all recipes entered by the user. It additionally sets an NSFetchedResultsControllerDelegate for cases where the constructed controller is used in a table view controller.

     - parameter delegate: The delegate to set for the controller

     - returns: An NSFetchedResultsController with all available recipes, for which performFetch() has already been invoked.
     */
    static func recipesWithDelegate(_ delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<Recipe> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeEntity.CACHE_NAME)
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptionItemName = NSSortDescriptor(key:RecipeEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptionItemName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:nil, cacheName:RecipeEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController
    }

    static func recipe(withId id: Int) -> Recipe? {
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let idPredicate = NSPredicate(format: "%K = %d", RecipeEntity.COL_ID, id)
        fetchRequest.predicate = idPredicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
            if fetchResult.count == 1 {
                return fetchResult[0]
            }
            return nil
        } catch {
            return nil
        }
    }

    static func recipe(withName name: String) -> Recipe? {
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let idPredicate = NSPredicate(format: "%K = %@", RecipeEntity.COL_NAME, name)
        fetchRequest.predicate = idPredicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
            if fetchResult.count >= 1 {
                return fetchResult[0]
            }
            return nil
        } catch {
            return nil
        }
    }

    static func insert(name: String?, portions: NSNumber?, possibleWithSupplies: Bool?, id: Int?) -> Recipe {
        let recipeEntity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        let recipe = Recipe(entity: recipeEntity!, insertInto: DBHelper.instance.getManagedObjectContext())
        recipe.portions = portions
        recipe.name = name
        recipe.possibleWithSupplies = possibleWithSupplies != nil ? NSNumber(booleanLiteral: possibleWithSupplies!) : NSNumber(booleanLiteral: false)
        recipe.id = id != nil ? NSNumber(value: id!) : NSNumber(value: 0)
        DBHelper.instance.saveContext()

        return recipe;
    }
    
    static func insertNewRecipe(withName recipeName: String, andPortions recipePortions: NSNumber?, intoContext context: NSManagedObjectContext) -> Recipe {
        let recipeEntity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in:context)
        let recipe = Recipe(entity: recipeEntity!, insertInto: context)
        recipe.portions = recipePortions
        recipe.name = recipeName
        recipe.possibleWithSupplies = NSNumber(booleanLiteral: false)
        recipe.id = NSNumber(value: nextID)

        return recipe;
    }

    /**
     Tests if a recipe with the given name already exists. This must be tested before an entity with the given name is inserted into the NSManagedObjectContext.

     In case of an error true is returned to prevent the creation of duplicates.
     */
    static func isRecipeAvailable(withName name: String) -> Bool {
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest();

        let entity = NSEntityDescription.entity(forEntityName: RecipeEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let predicate = NSPredicate(format: "%K = %@", RecipeEntity.COL_NAME, name)
        fetchRequest.predicate = predicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
            return fetchResult.count > 0
        } catch {
            return true
        }
    }


    /**
     Reads and returns the number of ingredients for a given recipe.

     - returns: The number of ingredients of the given recipe or 0 in case of an error.
     */
    func numberOfIngredients() -> Int {
        return self.ingredientsForRecipe?.count ?? 0
    }

    func totalTimeInMinutes() -> Int? {
        return self.preparationsForRecipe?
            .map({($0 as! RecipePreparation).timeWait!.intValue + ($0 as! RecipePreparation).timePrepare!.intValue})
            .reduce(0, {(current, nextItem) in current + nextItem})
    }

    func workingTimeInMinutes() -> Int? {
        return self.preparationsForRecipe?
            .map({($0 as! RecipePreparation).timePrepare!.intValue})
            .reduce(0, {(current, nextItem) in current + nextItem})
    }

    /**
     Determines the currently largest number of all preparation steps for this recipe. Each preparation step has a number for sorting the preparation step. Each number can only be given once for a certain recipe.
     
     - returns: The largest number of all preparation steps of this recipe.
    */
    func largestPreparationNumber() -> NSNumber? {
        let sortDescriptorNumber = NSSortDescriptor(key: RecipePreparationEntity.COL_NUMBER, ascending: false, selector:nil)
        let sortedPreparations = self.preparationsForRecipe?.sortedArray(using: [sortDescriptorNumber]) as! [RecipePreparation]
        return sortedPreparations.first?.number
    }

    func updatePreparationNumbersIfNeeded(startingAt prep: RecipePreparation?) {
        guard let prep = prep, let preparationsForRecipe = preparationsForRecipe else { return }

        let startNumber = prep.number!.intValue

        let filteredPreparations = (preparationsForRecipe.sortedArray(using: [NSSortDescriptor(key: RecipePreparationEntity.COL_NUMBER, ascending: true)]) as! [RecipePreparation])
            .filter({$0 != prep})
            .filter({$0.number!.intValue >= startNumber })

        var currentNumber = startNumber
        for preparation in filteredPreparations  {
            let numberOfCurrentPreparation = preparation.number!.intValue
            if numberOfCurrentPreparation > currentNumber { break }
            let newNumberOfCurrentPreparation = preparation.number!.intValue + 1
            preparation.number = NSNumber(value: newNumberOfCurrentPreparation)
            currentNumber += 1
        }
    }

    func exportToFileURL() -> URL? {
        var contents = [String : Any]()

        // meta data
        contents[Constants.SHARE_FILES_PARAMETER_NAME_TYPE] = Constants.SHARE_FILES_TYPE_RECIPE
        contents[Constants.SHARE_FILES_PARAMETER_NAME_VERSION] = ShoptimizerVersions.v_3_1_1.rawValue

        // recipe data
        contents[RecipeEntity.COL_NAME] = name
        contents[RecipeEntity.COL_PORTIONS] = portions

        if let recipeIngredients = ingredientsForRecipe {
            var count = 0
            for ingredient in recipeIngredients {
                guard let ingredient = ingredient as? RecipeIngredient else { continue }
                
                var ingredientData = [String: Any]()
                ingredientData[RecipeIngredientEntity.COL_NAME] = ingredient.name
                ingredientData[RecipeIngredientEntity.COL_QUANTITY] = ingredient.quantity
                ingredientData[RecipeIngredientEntity.COL_UNIT] = ingredient.unit
                ingredientData[RecipeIngredientEntity.COL_INCLUDE_IN_LIST] = ingredient.includeInList
                ingredientData[RecipeIngredientEntity.COL_INCLUDE_IN_SEARCH] = ingredient.includeInSearch

                contents[Constants.SHARE_FILES_INGREDIENTS_KEY + String(count)] = ingredientData
                count += 1
            }
        }

        if let recipePreparations = preparationsForRecipe {
            var count = 0
            for preparation in recipePreparations {
                guard let preparation = preparation as? RecipePreparation else { continue }

                var preparationData = [String:Any]()
                preparationData[RecipePreparationEntity.COL_NUMBER] = preparation.number
                preparationData[RecipePreparationEntity.COL_TITLE] = preparation.title
                preparationData[RecipePreparationEntity.COL_TIME_WAIT] = preparation.timeWait
                preparationData[RecipePreparationEntity.COL_TIME_PREPARE] = preparation.timePrepare
                preparationData[RecipePreparationEntity.COL_PREPARATION] = preparation.preparation
                preparationData[RecipePreparationEntity.COL_SUMMARY] = preparation.summary

                contents[Constants.SHARE_FILES_RECIPE_PREPARATION_KEY + String(count)] = preparationData
                count += 1
            }
        }

        if let recipeCategories = categoriesForRecipe {
            var count = 0
            for category in recipeCategories {
                guard let category = category as? RecipeCategory else { continue }

                var categoryData = [String:Any]()
                categoryData[RecipeCategoryEntity.COL_NAME] = category.name

                contents[Constants.SHARE_FILES_RECIPE_CATEGORY_KEY + String(count)] = categoryData
                count += 1
            }
        }

        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }

        let saveFileURL = path.appendingPathComponent("/\(self.name!)." + Constants.SHARE_FILES_FILE_EXTENSION)
        (contents as NSDictionary).write(to: saveFileURL, atomically: true)
        return saveFileURL
    }

    static func importData(from importedInfo: [String:AnyObject]) {
        guard let version = importedInfo[Constants.SHARE_FILES_PARAMETER_NAME_VERSION] as? String else { return }

        switch version {
        case ShoptimizerVersions.v_2_2_0.rawValue:
            importDataForVersion2_2_0(from: importedInfo)
            DBHelper.instance.saveContext()
        case ShoptimizerVersions.v_3_0_0.rawValue:
            importDataForVersion3_0_0(from: importedInfo)
            DBHelper.instance.saveContext()
        case ShoptimizerVersions.v_3_1_1.rawValue:
            importDataForVersion3_1_1(from: importedInfo)
            DBHelper.instance.saveContext()
        default:
            let unsupportedVersionMessage = NSLocalizedString("The version of the file is not supported.", comment:"");
            GeneralHelper.presentInformationDialogWithText(unsupportedVersionMessage, andTitle: Constants.TITLE_UNSUPPORTED_SHARE_FILE_VERSION)
        }
    }

    static func importDataForVersion2_2_0(from importedInfo: [String: AnyObject]) {
        let name = importedInfo[RecipeEntity.COL_NAME] as! String
        let portions = importedInfo[RecipeEntity.COL_PORTIONS] as? NSNumber ?? 0
        let importedRecipe = Recipe.insertNewRecipe(withName: name, andPortions: portions, intoContext: DBHelper.instance.getManagedObjectContext())

        var count = 0
        while true {
            guard let ingredient = importedInfo[Constants.SHARE_FILES_INGREDIENTS_KEY + String(count)] as? [String : Any] else { break }
            let ingredientName = ingredient[RecipeIngredientEntity.COL_NAME] as! String
            let ingredientQuantity = ingredient[RecipeIngredientEntity.COL_QUANTITY] as? NSNumber ?? 0
            let ingredientUnit = ingredient[RecipeIngredientEntity.COL_UNIT] as! String
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: ingredientName, quantity: ingredientQuantity, andUnit: ingredientUnit, andIncludeInList: true, andIncludeInSearch: true, forRecipe: importedRecipe, intoContext: DBHelper.instance.getManagedObjectContext())
            count += 1
        }
    }

    static func importDataForVersion3_0_0(from importedInfo: [String: AnyObject]) {
        let name = importedInfo[RecipeEntity.COL_NAME] as! String
        let portions = importedInfo[RecipeEntity.COL_PORTIONS] as? NSNumber ?? 0
        let importedRecipe = Recipe.insertNewRecipe(withName: name, andPortions: portions, intoContext: DBHelper.instance.getManagedObjectContext())

        var count = 0
        while true {
            guard let ingredient = importedInfo[Constants.SHARE_FILES_INGREDIENTS_KEY + String(count)] as? [String : Any] else { break }
            let ingredientName = ingredient[RecipeIngredientEntity.COL_NAME] as! String
            let ingredientQuantity = ingredient[RecipeIngredientEntity.COL_QUANTITY] as? NSNumber ?? 0
            let ingredientUnit = ingredient[RecipeIngredientEntity.COL_UNIT] as! String
            let includeInList = ingredient[RecipeIngredientEntity.COL_INCLUDE_IN_LIST] as? NSNumber ?? 0
            let includeInSearch = ingredient[RecipeIngredientEntity.COL_INCLUDE_IN_SEARCH] as? NSNumber ?? 0
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: ingredientName, quantity: ingredientQuantity, andUnit: ingredientUnit, andIncludeInList: includeInList, andIncludeInSearch: includeInSearch, forRecipe: importedRecipe, intoContext: DBHelper.instance.getManagedObjectContext())
            count += 1
        }
    }

    static func importDataForVersion3_1_1(from importedInfo: [String: AnyObject]) {
        let context = DBHelper.instance.getManagedObjectContext()
        let name = importedInfo[RecipeEntity.COL_NAME] as? String ?? ""
        let portions = importedInfo[RecipeEntity.COL_PORTIONS] as? NSNumber ?? 0
        let importedRecipe = Recipe.insertNewRecipe(withName: name, andPortions: portions, intoContext: context)

        var count = 0
        while true {
            guard let ingredient = importedInfo[Constants.SHARE_FILES_INGREDIENTS_KEY + String(count)] as? [String : Any] else { break }
            let ingredientName = ingredient[RecipeIngredientEntity.COL_NAME] as? String ?? ""
            let ingredientQuantity = ingredient[RecipeIngredientEntity.COL_QUANTITY] as? NSNumber ?? 0
            let ingredientUnit = ingredient[RecipeIngredientEntity.COL_UNIT] as? String ?? ""
            let includeInList = ingredient[RecipeIngredientEntity.COL_INCLUDE_IN_LIST] as? NSNumber ?? 0
            let includeInSearch = ingredient[RecipeIngredientEntity.COL_INCLUDE_IN_SEARCH] as? NSNumber ?? 0
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: ingredientName, quantity: ingredientQuantity, andUnit: ingredientUnit, andIncludeInList: includeInList, andIncludeInSearch: includeInSearch, forRecipe: importedRecipe, intoContext: context)
            count += 1
        }

        count = 0
        while true {
            guard let preparation = importedInfo[Constants.SHARE_FILES_RECIPE_PREPARATION_KEY + String(count)] as? [String:Any] else { break }
            let number = preparation[RecipePreparationEntity.COL_NUMBER] as? NSNumber ?? NSNumber(integerLiteral: count)
            let title = preparation[RecipePreparationEntity.COL_TITLE] as? String ?? ""
            let timeWait = preparation[RecipePreparationEntity.COL_TIME_WAIT] as? NSNumber ?? NSNumber(integerLiteral: 0)
            let timePrepare = preparation[RecipePreparationEntity.COL_TIME_PREPARE] as? NSNumber ?? NSNumber(integerLiteral: 0)
            let preparationText = preparation[RecipePreparationEntity.COL_PREPARATION] as? String ?? ""
            let summary = preparation[RecipePreparationEntity.COL_SUMMARY] as? String ?? ""
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: number, andTitle: title, andPreparation: preparationText, andWaitingTime: timeWait, andPreparationTime: timePrepare, andSummary: summary, forRecipe: importedRecipe, intoContext: context)
            count += 1
        }

        count = 0
        while true {
            guard let category = importedInfo[Constants.SHARE_FILES_RECIPE_CATEGORY_KEY + String(count)] as? [String:Any] else { break }
            guard let name = category[RecipeCategoryEntity.COL_NAME] as? String, name != "" else { continue }

            if let availableCategory = RecipeCategory.category(withName: name) {
                availableCategory.addToRecipesForCategory(importedRecipe)
            } else {
                let availableCategory = RecipeCategory.insertNewRecipeCategory(withName: name, intoContext: context)
                availableCategory.addToRecipesForCategory(importedRecipe)
            }
            count += 1
        }
    }
}

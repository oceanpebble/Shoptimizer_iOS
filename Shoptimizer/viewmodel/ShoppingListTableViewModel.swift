// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class ShoppingListTableViewModel {
    
    private var fetchedResultsController: NSFetchedResultsController<ShoppingListItem>
    private var aggregatedListHelper: AggregatedListHelper
    private let dataManager: ShoppingListTableDataManager!
    
    init(fetchedResultsController: NSFetchedResultsController<ShoppingListItem>, aggregatedListHelper: AggregatedListHelper, dataManager: ShoppingListTableDataManager) {
        self.fetchedResultsController = fetchedResultsController
        self.aggregatedListHelper = aggregatedListHelper
        self.dataManager = dataManager
    }
    
    func getName(of list: ShoppingList) -> String {
        return dataManager.getName(of: list)
    }
    
    func isAggregatedList(_ listName: String) -> Bool {
        return dataManager.isAggregatedList(listName)
    }
    
    func isAggregatedList(_ list: ShoppingList) -> Bool {
        return dataManager.isAggregatedList(list)
    }
    
    func item(at indexPath: IndexPath) -> ShoppingListItem {
        return fetchedResultsController.object(at: indexPath)
    }
    
    func list(for item: ShoppingListItem) -> ShoppingList {
        return dataManager.list(for: item)
    }
    
    func done(for item: ShoppingListItem) -> Bool {
        return dataManager.done(for: item)
    }
    
    func name(of item: ShoppingListItem) -> String {
        return dataManager.name(for: item)
    }
    
    func shopName(of item: ShoppingListItem) -> String {
        return dataManager.shopName(for: item)
    }
    
    func brandName(of item: ShoppingListItem) -> String? {
        return dataManager.brandName(for: item)
    }
    
    func amountQuantity(of item: ShoppingListItem) -> NSNumber? {
        return dataManager.amountQuantity(for: item)
    }
    
    func amountUnit(of item: ShoppingListItem) -> String? {
        return dataManager.amountUnit(of: item)
    }
    
    func price(of item: ShoppingListItem) -> NSNumber? {
        return dataManager.price(of: item)
    }
    
    func currency(of item: ShoppingListItem) -> String? {
        return dataManager.currency(of: item)
    }
    
    func update(item: ShoppingListItem, done: Bool) {
        dataManager.update(item: item, done: done)
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        return dataManager.isListIncludedInAggregatedList(list)
    }
    
    func addItemToAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.addItem(item)
    }
    
    func subtractItemFromAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.subtractItem(item)
    }
    
    func deleteItemFromAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.deleteItem(item)
    }
    
    func undoneItemsNotOnAggregatedListWithName(_ itemName: String?, shopName: String?) -> [ShoppingListItem] {
        return dataManager.undoneItemsNotOnAggregatedListWithName(itemName, shopName: shopName)
    }
    
    func sections() -> [NSFetchedResultsSectionInfo] {
        return fetchedResultsController.sections!
    }
    
    func dominantCurrency(on list: ShoppingList, inShop shopName: String) -> String? {
        return dataManager.dominantCurrency(on: list, inShop: shopName)
    }
    
    func totalPriceOfAllItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        return dataManager.totalPriceOfAllItems(on: list, forCurrency: currency, inShop: shopName)
    }
    
    func totalPriceOfDoneItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        return dataManager.totalPriceOfDoneItems(on: list, forCurrency: currency, inShop: shopName)
    }
    
    func formatPrice(_ price: Double) -> String {
        return formatPrice(NSNumber(value: price))!
    }
    
    func formatPrice(_ price: NSNumber) -> String? {
        return GeneralHelper.priceFormatter().string(from: price)
    }
    
    func priceString(for list: ShoppingList, inShop shopName: String, objectCount: Int, numberOfDoneItems: Int) -> String {
        if isAggregatedList(list) {
            if let dominantCurrency = dominantCurrency(on: list, inShop: shopName) {
                let totalPriceValue = totalPriceOfAllItems(on: list, forCurrency: dominantCurrency, inShop: shopName)
                let totalPrice = formatPrice(totalPriceValue)
                return "\(shopName) (\(objectCount) - \(totalPrice) \(dominantCurrency))"
            } else {
                return "\(shopName) (\(objectCount))"
            }
        } else {
            if let dominantCurrency = dominantCurrency(on: list, inShop: shopName) {
                let totalPriceValue = totalPriceOfAllItems(on: list, forCurrency: dominantCurrency, inShop: shopName)
                let totalPrice = formatPrice(totalPriceValue)
                let donePriceValue = totalPriceOfDoneItems(on: list, forCurrency: dominantCurrency, inShop: shopName)
                let donePrice = formatPrice(donePriceValue)
                return "\(shopName) (\(numberOfDoneItems) - \(donePrice) \(dominantCurrency) / \(objectCount) - \(totalPrice) \(dominantCurrency))"
            } else {
                return "\(shopName) (\(numberOfDoneItems) / \(objectCount))"
            }
        }
    }
    
    func deleteItem(_ item: ShoppingListItem) {
        dataManager.deleteItem(item)
    }
    
    func convertAmountQuantity(_ amountQuantity: NSNumber) -> String? {
        return GeneralHelper.decimalFormatter().string(from: amountQuantity)
    }
    
    func detailLabelText(for item: ShoppingListItem) -> String {
        var detailLabelText = ""

        if let amountUnit = amountUnit(of: item), let amountQuantity = amountQuantity(of: item), amountQuantity.doubleValue > 0 {
            let convertedAmountQuantity = convertAmountQuantity(amountQuantity)
            detailLabelText += "\(convertedAmountQuantity!) \(amountUnit)"
        }

        if let price = price(of: item),
            price.doubleValue > 0,
           currency(of: item) != nil,
           let priceString = formatPrice(price) {
            if detailLabelText.isNotEmpty {
                detailLabelText += ": "
            }
            detailLabelText += "\(priceString) \(item.itemCurrency!)"
        }
        
        return detailLabelText
    }
    
    func mainLabelText(for item: ShoppingListItem) -> String {
        var mainLabelText = name(of: item)
        if let brandName = brandName(of: item), !brandName.isEmpty {
            mainLabelText += " (\(brandName))"
        }
        return mainLabelText
    }
}

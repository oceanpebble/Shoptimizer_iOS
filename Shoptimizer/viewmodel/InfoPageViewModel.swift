// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class InfoPageViewModel {
    func loadPageContents(useDarkStyle: Bool) -> [String] {
        let selectedLanguage = GeneralHelper.getPreferredLanguage()

        var manualFileName = "manual_"
        manualFileName.append(selectedLanguage)
        let manualFile = Bundle.main.path(forResource: manualFileName, ofType: "html")

        var aboutFileName = "about_"
        aboutFileName.append(selectedLanguage)
        let aboutFile =  Bundle.main.path(forResource: aboutFileName, ofType: "html")

        do {
            var manualString = try String.init(contentsOfFile: manualFile!)
            var aboutString = try String.init(contentsOfFile: aboutFile!)
            if useDarkStyle {
                manualString = manualString.replacingOccurrences(of: "<body>", with: "<body style=\"background-color:#1C1C1C;color:white;\">")
                aboutString = aboutString.replacingOccurrences(of: "<body>", with: "<body style=\"background-color:#1C1C1C;color:white;\">")
            }
            return [manualString, aboutString];
        } catch {
            if selectedLanguage == "de" {
                return ["Leere Anleitung", "Leere Informationen"]
            } else {
                return ["Empty Manual", "Empty Information"]
            }
        }
    }
}

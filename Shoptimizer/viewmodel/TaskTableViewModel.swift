// SPDX-FileCopyrightText: 2025 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData

class TaskTableViewModel {
    
    private var fetchedResultsController: NSFetchedResultsController<Task>!
    private var notificationController: NotificationController!
    
    init(fetchedResultsController: NSFetchedResultsController<Task>, notificationController: NotificationController) {
        self.fetchedResultsController = fetchedResultsController
        self.notificationController = notificationController
    }
    
    // - MARK: NSFetchedResultsController
    
    func task(at indexPath: IndexPath) -> Task {
        return fetchedResultsController.object(at: indexPath)
    }
    
    func sections() -> [NSFetchedResultsSectionInfo] {
        return fetchedResultsController.sections ?? []
    }
    
    // - MARK: Notifications
    
    func unscheduleNotification(withId id: Int) {
        notificationController.unscheduleNotification(withId: id)
    }
    
    func unscheduleAllTasks() {
        notificationController.unscheduleAllTasks()
    }
    
    // - MARK: Misc
    
    func done(for task: Task) -> Bool {
        return task.done!.boolValue
    }
    
    func update(task: Task, dateCompleted: NSDate?) {
        task.dateCompleted = dateCompleted
    }
    
    func update(task: Task, done: Bool) {
        task.done = NSNumber(value: done)
    }
    
    func deleteTask(_ task: Task) {
        DBHelper.instance.getManagedObjectContext().delete(task)
    }
    
    func recipe(for task: Task) -> Recipe? {
        return task.title != nil ? Recipe.recipe(withName: task.title!) : nil
    }
    
    func formatDateDue(for task: Task) -> String {
        guard let dateDue = task.dateDue else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return "\(dateFormatter.string(from: dateDue as Date))"
    }
    
    func deleteAllTasks() {
        Task.deleteAllTasks()
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class ShoppingListListTableViewModel {
    
    private var fetchedResultsController: NSFetchedResultsController<ShoppingList>!
    private var aggregatedListHelper: AggregatedListHelper
    private let dataManager: ShoppingListListTableDataManager!
    
    init(fetchedResultsController: NSFetchedResultsController<ShoppingList>, aggregatedListHelper: AggregatedListHelper, dataManager: ShoppingListListTableDataManager) {
        self.fetchedResultsController = fetchedResultsController
        self.aggregatedListHelper = aggregatedListHelper
        self.dataManager = dataManager
    }
    
    // MARK: Options for Aggregated List

    func updateAggregatedList() {
        aggregatedListHelper.updateAggregatedList()
    }
    
    // MARK: Options for Regular Lists
    
    func transferItemsFrom(_ list: ShoppingList, toSuppliesAndDeleteItems deleteItems: Bool) -> Int {
        let listItems = dataManager.itemsFor(list)
        let suppliesItems = dataManager.allSuppliesItems()
        var numberOfTransferredItems = 0

        for item in listItems {
            let listItemAmountQuantity = item.itemAmountQuantity
            let listItemAmountUnit = item.itemAmountUnit
            let doneStatus = item.done!.boolValue
            guard doneStatus, let am = listItemAmountQuantity?.doubleValue, am > 0 else { continue }

            for suppliesItem in suppliesItems {
                if item.itemName == suppliesItem.itemName {
                    let suppliesItemAmountQuantity = suppliesItem.curAmountQuantity
                    let suppliesItemAmountUnit = suppliesItem.curAmountUnit
                    let listItemAmountQuantityMapped = UnitPickerHelper.convertValue(listItemAmountQuantity, fromBaseUnit: listItemAmountUnit, toTargetUnit: suppliesItemAmountUnit!)
                    let newAmountQuantity = suppliesItemAmountQuantity!.doubleValue + listItemAmountQuantityMapped.doubleValue
                    if listItemAmountUnit == suppliesItemAmountUnit || UnitPickerHelper.baseUnit(listItemAmountUnit, isConvertibleToTargetUnit: suppliesItemAmountUnit!) {
                        dataManager.updateSuppliesItem(suppliesItem, curAmountQuantity: newAmountQuantity)
                        numberOfTransferredItems += 1

                        if(deleteItems) {
                            dataManager.deleteShoppingListItem(item)
                        }
                    }
                }
            }
        }

        DBHelper.instance.saveContext()
        return numberOfTransferredItems
    }
    
    func deleteDoneItemsFrom(_ list: ShoppingList) -> Int {
        let doneItems = dataManager.doneItemsFor(list)
        doneItems.forEach({dataManager.deleteShoppingListItem($0)})
        DBHelper.instance.saveContext()
        return doneItems.count
    }

    func uncheckItemsOn(_ list: ShoppingList) -> [ShoppingListItem] {
        let listInAggregatedList = dataManager.isListIncludedInAggregatedList(list)
        let doneItems = dataManager.doneItemsFor(list)
        
        for item in doneItems {
            dataManager.updateShoppingListLitem(item, done: false)
            if listInAggregatedList {
                dataManager.addItemToAggregatedList(item)
            }
        }

        DBHelper.instance.saveContext()
        return doneItems
    }
    
    func deleteItemsFrom(_ list: ShoppingList) -> Int {
        let listItems = dataManager.itemsFor(list)
        listItems.forEach({dataManager.deleteShoppingListItem($0)})
        DBHelper.instance.saveContext()
        return listItems.count
    }
    
    func exportToFileURL(_ list: ShoppingList) -> URL? {
        return dataManager.exportListToFileURL(list)
    }
    
    // MARK: - Display Stuff
    
    func listNameDisplayString(for list: ShoppingList) -> String {
        let listName = dataManager.getName(of: list)
        if dataManager.isAggregatedList(listName) {
            return listName
        } else if dataManager.isListIncludedInAggregatedList(list) {
            return "\(Constants.MARKER_FOR_LIST_INCLUDED) \(listName)"
        } else {
            return "\(Constants.MARKER_FOR_LIST_NOT_INCLUDED) \(listName)"
        }
    }
    
    func priceString(for list: ShoppingList) -> String {
        let totalItemsCount = dataManager.numberOfItemsOn(list) 
        let doneItemsCount = dataManager.numberOfDoneItemsOn(list) 
        
        if dataManager.isAggregatedList(list) {
            return "\(totalItemsCount)"
        } else if let dominantCurrency = dataManager.dominantCurrencyOf(list),
                  let totalPrice = GeneralHelper.priceFormatter().string(from: NSNumber(value: dataManager.totalPriceOfAllItems(forCurrency: dominantCurrency, on: list))),
                  let boughtPrice = GeneralHelper.priceFormatter().string(from: NSNumber(value: dataManager.totalPriceOfDoneItems(forCurrency: dominantCurrency, on: list))) {
            return "\(doneItemsCount) - \(boughtPrice) \(dominantCurrency) / \(totalItemsCount) - \(totalPrice) \(dominantCurrency)"
        } else {
            return "\(doneItemsCount) / \(totalItemsCount)"
        }
    }
    
    // MARK: - NSFetchedResultsController
    
    func list(at indexPath: IndexPath) -> ShoppingList {
        return fetchedResultsController.object(at: indexPath)
    }
    
    // MARK: - Misc
    
    func includeExcludeItems(for list: ShoppingList, oldIncludeExclude: Bool) {
        let shouldIncludeItemsInAggregatedList = dataManager.isListIncludedInAggregatedList(list)
        let itemsToProcess = dataManager.itemsFor(list).filter({!$0.done!.boolValue && oldIncludeExclude != shouldIncludeItemsInAggregatedList})
        
        shouldIncludeItemsInAggregatedList
        ? itemsToProcess.forEach({dataManager.addItemToAggregatedList($0)})
        : itemsToProcess.forEach({dataManager.subtractItemFromAggregatedList($0)})
        
        DBHelper.instance.saveContext()
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        return dataManager.isListIncludedInAggregatedList(list)
    }
    
    func isAggregatedList(_ list: ShoppingList) -> Bool {
        return dataManager.isAggregatedList(list)
    }
    
    func deleteItemsFromAggregatedListForList(_ list: ShoppingList) {
        aggregatedListHelper.deleteItemsFromAggregatedListForList(list)
    }
    
    func deleteShoppingList(_ list: ShoppingList) {
        return dataManager.deleteShoppingList(list)
    }
}

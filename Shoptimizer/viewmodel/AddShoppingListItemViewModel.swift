// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class AddShoppingListItemViewModel {
    
    private let aggregatedListHelper: AggregatedListHelper
    private let dataManager: AddShoppingListItemDataManager
    
    init(aggregatedListHelper: AggregatedListHelper, dataManager: AddShoppingListItemDataManager) {
        self.aggregatedListHelper = aggregatedListHelper
        self.dataManager = dataManager
    }
    
    func name(of item: ShoppingListItem) -> String {
        return dataManager.name(of: item)
    }
    
    func shopName(of item: ShoppingListItem) -> String? {
        return dataManager.shopName(of: item)
    }
    
    func amountUnit(of item: ShoppingListItem) -> String {
        return dataManager.amountUnit(of: item)
    }
    
    func currency(of item: ShoppingListItem) -> String? {
        return dataManager.currency(of: item)
    }
    
    func brandName(of item: ShoppingListItem) -> String? {
        return dataManager.brandName(of: item)
    }
    
    func list(for item: ShoppingListItem?) -> ShoppingList? {
        return dataManager.list(for: item)
    }
    
    func done(for item: ShoppingListItem?) -> Bool {
        return dataManager.done(for: item)
    }
    
    func isListItemAvailable(withName name: String, andShop shop: String, inList list: ShoppingList) -> Bool {
        return dataManager.isListItemAvailable(withName: name, andShop: shop, inList: list)
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList?) -> Bool {
        return dataManager.isListIncludedInAggregatedList(list)
    }
    
    func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency:String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList) -> ShoppingListItem {
        return dataManager.insertNewShoppingListItem(withName: itemName, quantity: itemAmountQuantity, unit: itemAmountUnit, doneStatus: doneStatus, brand: brandName, forPrice: itemPrice, atCurrency: itemCurrency, inShop: shopName, notes: notes, forList: listForItem)
    }
    
    func updateShoppingListItem(_ item: ShoppingListItem, itemName: String, shopName: String, amountQuantity: NSNumber, amountUnit: String, brandName: String, price: NSNumber, currency: String?, notes: String?) {
        dataManager.updateShoppingListItem(item, itemName: itemName, shopName: shopName, amountQuantity: amountQuantity, amountUnit: amountUnit, brandName: brandName, price: price, currency: currency, notes: notes)
    }
    
    func ensureValidDouble(fromString numberString: String?, allowZero allow: Bool) -> NSNumber? {
        return GeneralHelper.ensureValidDouble(fromString: numberString, allowZero: allow)
    }
    
    func ensureValidPrice(fromString numberString: String?, allowZero allow: Bool) -> NSNumber? {
        return GeneralHelper.ensureValidPrice(fromString: numberString, allowZero: allow)
    }
    
    func subtractItemFromAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.subtractItem(item)
    }
    
    func addItemToAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.addItem(item)
    }
    
    func addItemToAggregatedList(_ itemName: String, forShop shopName: String?, amountQuantity: NSNumber?, andAmountUnit amountUnit: String?, andBrand brandName: String?, forPrice price: NSNumber?, atCurrency currency: String?) {
        aggregatedListHelper.addItem(itemName, forShop: shopName, amountQuantity: amountQuantity, andAmountUnit: amountUnit, andBrand: brandName, forPrice: price, atCurrency: currency)
    }
    
    func quantityFieldText(for item: ShoppingListItem) -> String? {
        guard let quantity = dataManager.amountQuantity(of: item) else { return nil }
        let convertedQuantity = GeneralHelper.decimalFormatter().string(from: quantity)
        return String(format: "%@", convertedQuantity!)
    }
    
    func priceFieldText(for item: ShoppingListItem) -> String? {
        guard let price = dataManager.price(of: item) else { return nil }
        let convertedPrice = GeneralHelper.priceFormatter().string(from: price)
        return String(describing: convertedPrice!)
    }
}

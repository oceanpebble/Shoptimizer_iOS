// SPDX-FileCopyrightText: 2024 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class AddShoppingListViewModel {
    
    func trimName(_ name: String?) -> String {
        return name?.trimmingCharacters(in: .whitespaces) ?? ""
    }

    func name(of list: ShoppingList?) -> String? {
        return list?.name
    }
    
    func setName(_ name: String, for list: ShoppingList?) {
        list?.name = name
    }
    
    func setListIncludedInAggregatedList(_ list: ShoppingList?, isIncluded: Bool) {
        list?.include_in_aggregate = NSNumber(value: isIncluded)
    }
    
    func existsShoppingList(withName name: String) -> Bool {
        return ShoppingList.isShoppingListAvailable(withName: name)
    }
    
    func insertNewShoppingList(withName name: String, andType type: ShoppingListType, includeInAggregatedList: Bool) -> ShoppingList? {
        return ShoppingList.insertNewShoppingList(withName: name, andType: type.rawValue, includeInAggregateList: includeInAggregatedList, intoContext: DBHelper.instance.getManagedObjectContext())
    }
}

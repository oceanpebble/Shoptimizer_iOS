// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class ShoppingListListTableDataManager {
    
    private let aggregatedListHelper: AggregatedListHelper!
    private let shoppingListRepository: ShoppingListRepository!
    private let shoppingListItemRepository: ShoppingListItemRepository!
    private let suppliesItemRepository: SuppliesItemRepository!
    
    init(aggregatedListHelper: AggregatedListHelper, shoppingListRepository: ShoppingListRepository, shoppingListItemRepository: ShoppingListItemRepository, suppliesItemRepository: SuppliesItemRepository) {
        self.aggregatedListHelper = aggregatedListHelper
        self.shoppingListRepository = shoppingListRepository
        self.shoppingListItemRepository = shoppingListItemRepository
        self.suppliesItemRepository = suppliesItemRepository
    }

    // MARK: - Create
    
    // MARK: - Read
    
    func itemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        return shoppingListRepository.itemsFor(list)
    }
    
    func allSuppliesItems() -> [SuppliesItem] {
        return suppliesItemRepository.allItems()
    }
    
    func doneItemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        return shoppingListRepository.doneItemsFor(list)
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        return shoppingListRepository.isListIncludedInAggregatedList(list)
    }
    
    func getName(of list: ShoppingList) -> String {
        var listName = shoppingListRepository.nameFor(list)
        if isAggregatedList(listName) {
            listName = NSLocalizedString("Aggregated List",  comment: "")
        }
        return listName
    }
    
    func isAggregatedList(_ listName: String) -> Bool {
        return aggregatedListHelper.isAggregatedList(listName)
    }
    
    func isAggregatedList(_ list: ShoppingList) -> Bool {
        return aggregatedListHelper.isAggregatedList(list)
    }
    
    func numberOfItemsOn(_ list: ShoppingList) -> Int {
        return shoppingListRepository.numberOfItemsOn(list)
    }
    
    func numberOfDoneItemsOn(_ list: ShoppingList) -> Int {
        return shoppingListRepository.numberOfDoneItemsOn(list)
    }
    
    func dominantCurrencyOf(_ list: ShoppingList) -> String? {
        return shoppingListRepository.dominantCurrencyOf(list)
    }
    
    func totalPriceOfAllItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        return shoppingListRepository.totalPriceOfAllItems(forCurrency: currency, on: list)
    }
    
    func totalPriceOfDoneItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        return shoppingListRepository.totalPriceOfDoneItems(forCurrency: currency, on: list)
    }
    
    // MARK: - Update
    
    func updateSuppliesItem(_ item: SuppliesItem, curAmountQuantity: Double) {
        suppliesItemRepository.updateItem(item, curAmountQuantity: curAmountQuantity)
    }
    
    func updateShoppingListLitem(_ item: ShoppingListItem, done: Bool) {
        shoppingListItemRepository.updateItem(item, done: done)
    }
    
    func addItemToAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.addItem(item)
    }
    
    func subtractItemFromAggregatedList(_ item: ShoppingListItem) {
        aggregatedListHelper.subtractItem(item)
    }
    
    // MARK: - Delete
    
    func deleteShoppingListItem(_ item: ShoppingListItem) {
        aggregatedListHelper.subtractItem(item)
        shoppingListItemRepository.deleteItem(item)
    }    
    
    func deleteShoppingList(_ list: ShoppingList) {
        shoppingListRepository.deleteShoppingList(list)
    }
    
    // MARK: - Misc
    
    func exportListToFileURL(_ list: ShoppingList) -> URL? {
        var contents = [String : Any]()

        // meta data
        contents[Constants.SHARE_FILES_PARAMETER_NAME_TYPE] = Constants.SHARE_FILES_TYPE_SHOPPING_LIST
        contents[Constants.SHARE_FILES_PARAMETER_NAME_VERSION] = ShoptimizerVersions.v_3_2_0.rawValue

        // list data
        contents[ShoppingListEntity.COL_NAME] = shoppingListRepository.nameFor(list)
        contents[ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE] = shoppingListRepository.isListIncludedInAggregatedList(list)

        // list item data
        var count = 0
        for item in shoppingListRepository.itemsFor(list) {
            var itemData = [String: Any]()
            itemData[ShoppingListItemEntity.COL_ITEM_NAME] = shoppingListItemRepository.nameFor(item)
            itemData[ShoppingListItemEntity.COL_SHOP_NAME] = shoppingListItemRepository.shopNameFor(item)
            itemData[ShoppingListItemEntity.COL_ITEM_AMOUNT_QUANTITY] = shoppingListItemRepository.amountQuantityFor(item)
            itemData[ShoppingListItemEntity.COL_ITEM_AMOUNT_UNIT] = shoppingListItemRepository.amountUnitFor(item)
            itemData[ShoppingListItemEntity.COL_DONE] = NSNumber(value: shoppingListItemRepository.doneFor(item))
            itemData[ShoppingListItemEntity.COL_BRAND_NAME] = shoppingListItemRepository.brandNameFor(item)
            itemData[ShoppingListItemEntity.COL_ITEM_PRICE] = shoppingListItemRepository.priceFor(item)
            itemData[ShoppingListItemEntity.COL_ITEM_CURRENCY] = shoppingListItemRepository.currencyFor(item)
            itemData[ShoppingListItemEntity.COL_NOTES] = shoppingListItemRepository.notesFor(item)
            
            contents[Constants.SHARE_FILES_ITEMS_KEY + String(count)] = itemData
            count += 1
        }

        // export data
        guard let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let saveFileURL = path.appendingPathComponent("/\(shoppingListRepository.nameFor(list))." + Constants.SHARE_FILES_FILE_EXTENSION)
        (contents as NSDictionary).write(to: saveFileURL, atomically: true)
        return saveFileURL
    }
}

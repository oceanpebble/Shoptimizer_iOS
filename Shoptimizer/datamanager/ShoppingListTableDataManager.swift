// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class ShoppingListTableDataManager {
    
    private let aggregatedListHelper: AggregatedListHelper!
    private let shoppingListRepository: ShoppingListRepository!
    private let shoppingListItemRepository: ShoppingListItemRepository!
    
    init(aggregatedListHelper: AggregatedListHelper, shoppingListRepository: ShoppingListRepository, shoppingListItemRepository: ShoppingListItemRepository) {
        self.aggregatedListHelper = aggregatedListHelper
        self.shoppingListRepository = shoppingListRepository
        self.shoppingListItemRepository = shoppingListItemRepository
    }
    
    // MARK: - Create
    
    // MARK: - Read
    
    func getName(of list: ShoppingList) -> String {
        return shoppingListRepository.nameFor(list)
    }
    
    func isAggregatedList(_ listName: String) -> Bool {
        return aggregatedListHelper.isAggregatedList(listName)
    }
    
    func isAggregatedList(_ list: ShoppingList) -> Bool {
        return aggregatedListHelper.isAggregatedList(list)
    }
    
    func list(for item: ShoppingListItem) -> ShoppingList {
        return shoppingListItemRepository.listFor(item)
    }
    
    func done(for item: ShoppingListItem) -> Bool {
        return shoppingListItemRepository.doneFor(item)
    }
    
    func name(for item: ShoppingListItem) -> String {
        return shoppingListItemRepository.nameFor(item)
    }
    
    func shopName(for item: ShoppingListItem) -> String {
        return shoppingListItemRepository.shopNameFor(item) ?? ""
    }
    
    func brandName(for item: ShoppingListItem) -> String? {
        return shoppingListItemRepository.brandNameFor(item)
    }
    
    func amountQuantity(for item: ShoppingListItem) -> NSNumber? {
        return shoppingListItemRepository.amountQuantityFor(item)
    }    
    
    func amountUnit(of item: ShoppingListItem) -> String? {
        let unit = shoppingListItemRepository.amountUnitFor(item)
        return unit != nil ? UnitPickerHelper.translate(unit: unit!) : nil
    }
    
    func price(of item: ShoppingListItem) -> NSNumber? {
        return shoppingListItemRepository.priceFor(item)
    }
    
    func currency(of item: ShoppingListItem) -> String? {
        return shoppingListItemRepository.currencyFor(item)
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        return shoppingListRepository.isListIncludedInAggregatedList(list)
    }
    
    func undoneItemsNotOnAggregatedListWithName(_ itemName: String?, shopName: String?) -> [ShoppingListItem] {
        return shoppingListItemRepository.undoneItemsNotOnAggregatedListWithName(itemName, andShop: shopName)
    }
    
    func dominantCurrency(on list: ShoppingList, inShop shopName: String) -> String? {
        return shoppingListRepository.dominantCurrency(on: list, in: shopName)
    }
    
    func totalPriceOfAllItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        return shoppingListRepository.totalPriceOfAllItems(forCurrency: currency, inShop: shopName, on: list)
    }
    
    func totalPriceOfDoneItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        return shoppingListRepository.totalPriceOfDoneItems(forCurrency: currency, inShop: shopName, on: list)
    }
    
    // MARK: - Update
    
    func update(item: ShoppingListItem, done: Bool) {
        shoppingListItemRepository.updateItem(item, done: done)
    }
    
    // MARK: - Delete
    
    func deleteItem(_ item: ShoppingListItem) {
        shoppingListItemRepository.deleteItem(item)
    }
}

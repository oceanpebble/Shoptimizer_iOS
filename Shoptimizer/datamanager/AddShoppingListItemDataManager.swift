// SPDX-FileCopyrightText: 2024 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation

class AddShoppingListItemDataManager {
    
    private let shoppingListRepository: ShoppingListRepository!
    private let shoppingListItemRepository: ShoppingListItemRepository!
    
    init(shoppingListRepository: ShoppingListRepository, shoppingListItemRepository: ShoppingListItemRepository) {
        self.shoppingListRepository = shoppingListRepository
        self.shoppingListItemRepository = shoppingListItemRepository
    }
    
    func name(of item: ShoppingListItem) -> String {
        return shoppingListItemRepository.nameFor(item)
    }
    
    func shopName(of item: ShoppingListItem) -> String? {
        return shoppingListItemRepository.shopNameFor(item)
    }
    
    func amountUnit(of item: ShoppingListItem) -> String {
        return shoppingListItemRepository.amountUnitFor(item) ?? "g"
    }
    
    func amountQuantity(of item: ShoppingListItem) -> NSNumber? {
        return shoppingListItemRepository.amountQuantityFor(item)
    }
    
    func currency(of item: ShoppingListItem) -> String? {
        return shoppingListItemRepository.currencyFor(item)
    }
    
    func price(of item: ShoppingListItem) -> NSNumber? {
        return shoppingListItemRepository.priceFor(item)
    }
    
    func brandName(of item: ShoppingListItem) -> String? {
        return shoppingListItemRepository.brandNameFor(item)
    }
    
    func list(for item: ShoppingListItem?) -> ShoppingList? {
        return item != nil ? shoppingListItemRepository.listFor(item!) : nil
    }
    
    func done(for item: ShoppingListItem?) -> Bool {
        return item != nil ? shoppingListItemRepository.doneFor(item!) : false
    }
    
    func isListItemAvailable(withName name: String, andShop shop: String, inList list: ShoppingList) -> Bool {
        return shoppingListRepository.isListItemAvailable(withName: name, andShop: shop, inList: list)
    }
    
    func isListIncludedInAggregatedList(_ list: ShoppingList?) -> Bool {
        shoppingListRepository.isListIncludedInAggregatedList(list)
    }
    
    func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency:String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList) -> ShoppingListItem {
        return shoppingListItemRepository.insertNewShoppingListItem(withName: itemName, quantity: itemAmountQuantity, unit: itemAmountUnit, doneStatus: doneStatus, brand: brandName, forPrice: itemPrice, atCurrency: itemCurrency, inShop: shopName, notes: notes, forList: listForItem)
    }
    
    func updateShoppingListItem(_ item: ShoppingListItem, itemName: String, shopName: String, amountQuantity: NSNumber, amountUnit: String, brandName: String, price: NSNumber, currency: String?, notes: String?) {
        shoppingListItemRepository.updateShoppingListItem(item, itemName: itemName, shopName: shopName, amountQuantity: amountQuantity, amountUnit: amountUnit, brandName: brandName, price: price, currency: currency, notes: notes)
    }
}

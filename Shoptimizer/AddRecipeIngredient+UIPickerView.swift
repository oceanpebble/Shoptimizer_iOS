// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

extension AddRecipeIngredientViewController : UIPickerViewDataSource, UIPickerViewDelegate {

    // *********************************************************
    //
    // MARK: - UIPickerViewDataSource
    //
    // *********************************************************

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return UnitPickerHelper.getUnitPickerData().count
    }

    // *********************************************************
    //
    // MARK: - UIPickerViewDelegate
    //
    // *********************************************************

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return UnitPickerHelper.getUnitPickerData()[row]
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension RecipeIngredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecipeIngredient> {
        return NSFetchRequest<RecipeIngredient>(entityName: "RecipeIngredient")
    }

    @NSManaged public var name: String?
    @NSManaged public var quantity: NSNumber?
    @NSManaged public var unit: String?
    @NSManaged public var includeInSearch: NSNumber?
    @NSManaged public var includeInList: NSNumber?
    @NSManaged public var recipeForIngredient: Recipe?
    
}

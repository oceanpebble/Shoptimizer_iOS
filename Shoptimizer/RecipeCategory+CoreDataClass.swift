// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct RecipeCategoryEntity {
    static let CACHE_NAME = "recipeCategories"
    static let COL_NAME = "name"
    static let REF_RECIPES_FOR_CATEGORY = "recipesForCategory"
    static let ENTITY_NAME = "RecipeCategory"
}

public class RecipeCategory: NSManagedObject {

    /*********************************************************************************************************************
     *
     * Function: categories
     *
     * Creates an NSFetchedResultsController containing all recipe categories.
     *
     * Parameters: None
     *
     ********************************************************************************************************************/
    static func categories() -> [RecipeCategory] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(RecipeCategory.fetchRequest())
        } catch {
            return [RecipeCategory]()
        }
    }

    static func categories(for recipe: Recipe) -> [RecipeCategory] {
        let fetchRequest: NSFetchRequest<RecipeCategory> = RecipeCategory.fetchRequest()

        let predicate = NSPredicate(format: "%K contains %@", RecipeCategoryEntity.REF_RECIPES_FOR_CATEGORY, recipe)
        fetchRequest.predicate = predicate

        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
        } catch {
            return [RecipeCategory]()
        }
    }

    static func categories(_ delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<RecipeCategory> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeCategoryEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<RecipeCategory> = RecipeCategory.fetchRequest()
        fetchRequest.fetchBatchSize = 20

        let sortDescriptorName = NSSortDescriptor(key:RecipeCategoryEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:nil, cacheName:RecipeCategoryEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipe categories")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController;
    }

    /**
     Creates a new RecipeIngredient instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewRecipeCategory(withName categoryName: String, intoContext context: NSManagedObjectContext) -> RecipeCategory {
        let categoryEntity = NSEntityDescription.entity(forEntityName: RecipeCategoryEntity.ENTITY_NAME, in:context)
        let category = RecipeCategory(entity: categoryEntity!, insertInto: context)
        category.name = categoryName

        return category;
    }

    /**
     Tests if the given recipe category with the given name is already available. This must be tested before an entity with the given name is inserted into the NSManagedObjectContext.

     In case of an error true is returned to prevent the creation of duplicates.
     */
    static func isCategoryAvailable(withName name: String) -> Bool {
        let fetchRequest: NSFetchRequest<RecipeCategory> = RecipeCategory.fetchRequest();

        let entity = NSEntityDescription.entity(forEntityName: RecipeCategoryEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let predicate = NSPredicate(format: "%K = %@", RecipeCategoryEntity.COL_NAME, name)
        fetchRequest.predicate = predicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            return fetchResult.count > 0
        } catch {
            return true
        }
    }

    static func category(withName name: String) -> RecipeCategory? {
        let fetchRequest: NSFetchRequest<RecipeCategory> = RecipeCategory.fetchRequest();

        let entity = NSEntityDescription.entity(forEntityName: RecipeCategoryEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let predicate = NSPredicate(format: "%K = %@", RecipeCategoryEntity.COL_NAME, name)
        fetchRequest.predicate = predicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            return fetchResult.first as? RecipeCategory
        } catch {
            return nil
        }
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

extension AddShoppingListItemViewController: UIPickerViewDataSource, UIPickerViewDelegate {

    // *********************************************************
    //
    // MARK: - UIPickerViewDataSource
    //
    // *********************************************************

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.unitPicker {
            return UnitPickerHelper.getUnitPickerData().count
        } else {
            return CurrencyPickerHelper.getCurrencyPickerData().count
        }
    }

    // *********************************************************
    //
    // MARK: - UIPickerViewDelegate
    //
    // *********************************************************

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.unitPicker {
            return UnitPickerHelper.getUnitPickerData()[row]
        } else {
            return CurrencyPickerHelper.getCurrencyPickerData()[row]
        }
    }
}

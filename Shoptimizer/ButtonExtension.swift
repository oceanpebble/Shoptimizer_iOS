// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import UIKit

extension UIButton {
    // based on http://www.iosinsight.com/uibutton-with-right-arrow-disclosure-indicator/
    func disclosureButton(baseColor:UIColor) {
        let imageTemp = traitCollection.userInterfaceStyle == .dark
            ? UIImage(named: "DisclosureIcon")?.withTintColor(.white).withRenderingMode(.alwaysTemplate)
            : UIImage(named: "DisclosureIcon")?.withRenderingMode(.alwaysTemplate)
        guard let image = imageTemp else { return }
        guard let imageHighlight = UIImage(named: "DisclosureIcon")?.withRenderingMode(.alwaysTemplate) else { return }
        self.imageView?.contentMode = .scaleAspectFit
        self.setImage(image, for: .normal)
        self.setImage(imageHighlight, for: .highlighted)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: self.bounds.size.width - image.size.width * 1.5, bottom: 0, right: 0);
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
}

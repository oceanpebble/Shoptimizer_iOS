// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class InfoPageContentViewController: UIViewController {
    var labelText = ""
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.loadHTMLString(self.labelText, baseURL:nil)
        self.webView.scrollView.bounces = false
    }
}

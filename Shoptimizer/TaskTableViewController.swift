// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class TaskTableViewController: UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate {

    // MARK: - Properties

    typealias EntityType = Task
    var reusableCellIdentifier: String { return "TaskPrototypeCell" }
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    
    private var viewModel: TaskTableViewModel!

    // MARK: - UIView
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchedResultsController = Task.tasks(self)
        self.setupSwipeRecognizers()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 10
        
        viewModel = TaskTableViewModel(fetchedResultsController: fetchedResultsController, notificationController: NotificationController.sharedInstance)
    }

    // MARK: - View Controller Life Cycle

    func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target:self, action:#selector(self.respondToRightSwipeGesture))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }

    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: self.view)

        guard let swipedIndexPath = self.tableView.indexPathForRow(at: location) else {
            return
        }

        let swipedTask = viewModel.task(at: swipedIndexPath) 
        if viewModel.done(for: swipedTask) {
            viewModel.update(task: swipedTask, dateCompleted: nil)
            viewModel.update(task: swipedTask, done: false)
        } else {
            viewModel.update(task: swipedTask, dateCompleted: NSDate())
            viewModel.update(task: swipedTask, done: true)
            viewModel.unscheduleNotification(withId: swipedTask.id!.intValue)
        }

        DBHelper.instance.saveContext()
        self.tableView.reloadData()
    }

    // MARK: - UITableViewController

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        let sections = viewModel.sections()
        let section = sections[section]
        guard let firstSectionItem = section.objects?.first as? Task else { return "" }
        return viewModel.done(for: firstSectionItem) ? NSLocalizedString("Done", comment: "") : ""
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedTask = viewModel.task(at: indexPath)
            let idOfSwipedTask = swipedTask.id

            viewModel.deleteTask(swipedTask)
            viewModel.unscheduleNotification(withId: idOfSwipedTask!.intValue)
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedTask = viewModel.task(at: indexPath)

        if let recipe = viewModel.recipe(for: selectedTask) {
            let topViewController = GeneralHelper.getTopViewController() as! UITabBarController
            topViewController.selectedIndex = Constants.TAB_BAR_INDEX_RECIPES
            let tabs = topViewController.children
            let recipesTab = tabs[Constants.TAB_BAR_INDEX_RECIPES]
            let recipesTVC = recipesTab.children[0] as! UITableViewController
            let recipesNC = recipesTVC.navigationController!
            while recipesNC.popViewController(animated: false) != nil {
                
            }
            let recipeVC = topViewController.storyboard!.instantiateViewController(withIdentifier: "RecipeViewController") as! RecipeViewController
            recipeVC.recipe = recipe
            recipesNC.pushViewController(recipeVC, animated: true)
        }
    }

    // MARK: - NSFetchedResultsController

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell  as? TaskTableViewCell else { return }
        let task = viewModel.task(at: indexPath)

        cell.titleLabel.text = "\(viewModel.formatDateDue(for: task)): \(task.title!)"
        cell.bodyLabel.text = task.body!
        cell.accessoryType = .disclosureIndicator
    }

    // MARK: - Commands
    
    @IBAction func deleteAllTasks() {
        viewModel.deleteAllTasks()
        viewModel.unscheduleAllTasks()
    }
}

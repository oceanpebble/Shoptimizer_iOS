// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct RecipePreparationEntity {
    static let CACHE_NAME = "recipePreparations"
    static let COL_NUMBER = "number"
    static let COL_TITLE = "title"
    static let COL_PREPARATION = "preparation"
    static let COL_TIME_WAIT = "timeWait"
    static let COL_TIME_PREPARE = "timePrepare"
    static let COL_SUMMARY = "summary"
    static let REF_RECIPE_FOR_PREPARATION = "recipeForPreparation"
    static let ENTITY_NAME = "RecipePreparation"
}

public class RecipePreparation: NSManagedObject {

    /**
     Creates a new RecipePreparation instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewRecipePreparation(withNumber number: NSNumber, andTitle title: String, andPreparation preparationText: String, andWaitingTime waitingTime: NSNumber, andPreparationTime preparationTime: NSNumber, andSummary summary: String?, forRecipe recipe: Recipe, intoContext context: NSManagedObjectContext) -> RecipePreparation {
        let preparationEntity = NSEntityDescription.entity(forEntityName: RecipePreparationEntity.ENTITY_NAME, in:context)
        let preparation = RecipePreparation(entity: preparationEntity!, insertInto: context)
        preparation.number = number
        preparation.title = title
        preparation.preparation = preparationText
        preparation.recipeForPreparation = recipe
        preparation.timeWait = waitingTime
        preparation.timePrepare = preparationTime
        preparation.summary = summary

        return preparation;
    }

    static func preparationsForRecipe(_ recipe: NSManagedObject, andDelegate delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<RecipePreparation> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipePreparationEntity.CACHE_NAME)
        let fetchRequest: NSFetchRequest<RecipePreparation> = RecipePreparation.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: RecipePreparationEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let predicate = NSPredicate(format:"%K = %@", RecipePreparationEntity.REF_RECIPE_FOR_PREPARATION, recipe)
        fetchRequest.predicate = predicate

        let sortDescriptorNumber = NSSortDescriptor(key: RecipePreparationEntity.COL_NUMBER, ascending:true, selector:nil)
        let sortDescriptors = [sortDescriptorNumber]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:nil, cacheName:RecipePreparationEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController
    }
}

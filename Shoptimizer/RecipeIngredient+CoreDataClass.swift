// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct RecipeIngredientEntity {
    static let ENTITY_NAME = "RecipeIngredient"
    static let COL_NAME = "name"
    static let COL_QUANTITY = "quantity"
    static let COL_UNIT = "unit"
    static let COL_INCLUDE_IN_LIST = "includeInList"
    static let COL_INCLUDE_IN_SEARCH = "includeInSearch"
    static let REF_RECIPEFORINGREDIENT = "recipeForIngredient"
    static let CACHE_NAME = "recipeIngredients"
}

public class RecipeIngredient: NSManagedObject {

    /**
     Creates a new RecipeIngredient instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewRecipeIngredient(withName ingredientName: String, quantity ingredientQuantity: NSNumber?, andUnit ingredientUnit: String?, andIncludeInList includeInList: NSNumber?, andIncludeInSearch includeInSearch: NSNumber?, forRecipe recipe: Recipe, intoContext context: NSManagedObjectContext) -> RecipeIngredient {
        let ingredientEntity = NSEntityDescription.entity(forEntityName: RecipeIngredientEntity.ENTITY_NAME, in:context)
        let ingredient = RecipeIngredient(entity: ingredientEntity!, insertInto: context)
        ingredient.name = ingredientName
        ingredient.quantity = ingredientQuantity
        ingredient.unit = ingredientUnit
        ingredient.recipeForIngredient = recipe
        ingredient.includeInList = includeInList
        ingredient.includeInSearch = includeInSearch

        return ingredient;
    }

    /**
     Creates a new RecipeIngredient instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewRecipeIngredient(withName ingredientName: String, quantity ingredientQuantity: NSNumber?, andUnit ingredientUnit: String?, forRecipe recipe: Recipe, intoContext context: NSManagedObjectContext) -> RecipeIngredient {
        let ingredientEntity = NSEntityDescription.entity(forEntityName: RecipeIngredientEntity.ENTITY_NAME, in:context)
        let ingredient = RecipeIngredient(entity: ingredientEntity!, insertInto: context)
        ingredient.name = ingredientName
        ingredient.quantity = ingredientQuantity
        ingredient.unit = ingredientUnit
        ingredient.recipeForIngredient = recipe

        return ingredient;
    }

    /**
     Tests if the given recipe ingredient with the given name is already contained in the given recipe. This must be tested before an entity with the given name is inserted into the NSManagedObjectContext.

     In case of an error true is returned to prevent the creation of duplicates.
     */
    static func isIngredientAvailable(withName name: String, inRecipe recipe: Recipe) -> Bool {
        let fetchRequest: NSFetchRequest<RecipeIngredient> = RecipeIngredient.fetchRequest();

        let entity = NSEntityDescription.entity(forEntityName: RecipeIngredientEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let predicate = NSPredicate(format: "%K = %@AND %K = %@", RecipeIngredientEntity.REF_RECIPEFORINGREDIENT, recipe, RecipeIngredientEntity.COL_NAME, name)
        fetchRequest.predicate = predicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            return fetchResult.count > 0
        } catch {
            return true
        }
    }
    
    static func ingredientsForRecipe(_ recipe: NSManagedObject, andDelegate delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<RecipeIngredient> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeIngredientEntity.CACHE_NAME)
        let fetchRequest: NSFetchRequest<RecipeIngredient> = RecipeIngredient.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: RecipeIngredientEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let predicate = NSPredicate(format:"%K = %@", RecipeIngredientEntity.REF_RECIPEFORINGREDIENT, recipe)
        fetchRequest.predicate = predicate

        let sortDescriptorName = NSSortDescriptor(key:RecipeIngredientEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:RecipeIngredientEntity.COL_NAME, cacheName:RecipeIngredientEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController
    }
    
    static func allIngredients() -> [RecipeIngredient]? {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeIngredientEntity.CACHE_NAME)
        
        let fetchRequest: NSFetchRequest<RecipeIngredient> = RecipeIngredient.fetchRequest()
        
        let entity = NSEntityDescription.entity(forEntityName: RecipeIngredientEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptorName = NSSortDescriptor(key:RecipeIngredientEntity.COL_NAME, ascending:true, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorName]
        fetchRequest.sortDescriptors = sortDescriptors
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext: DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath: nil, cacheName: RecipeIngredientEntity.CACHE_NAME)
        
        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching shopping lists.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController.fetchedObjects
    }
}

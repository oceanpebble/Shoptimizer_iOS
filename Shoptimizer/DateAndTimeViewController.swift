// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class DateAndTimeViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var startAtSelectedDateButton: UIButton!
    @IBOutlet weak var finishAtSelectedDateButton: UIButton!

    var onStartAtSelectedDateCallback : ((Date) -> Void)?
    var onFinishAtSelectedDateCallback : ((Date) -> Void)?
    var onCancelCallback : (() -> Void)?
    var onDateChangedCallback : ((Date) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setUp(`for` recipe: Recipe) {
        providesPresentationContextTransitionStyle = true
        definesPresentationContext = true
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        contentView.layer.cornerRadius = 10
        setButtonAvailability(for: Date(), recipe: recipe)
        setButtonTitles(for: Date(), recipe: recipe)
    }
    
    @IBAction
    func startAtSelectedDateButtonTapped() {
        onStartAtSelectedDateCallback?(datePicker.date)
    }
    
    @IBAction
    func finishAtSelectedDateButtonTapped() {
        onFinishAtSelectedDateCallback?(datePicker.date)
    }
    
    @IBAction
    func cancelButtonTapped() {
        onCancelCallback?()
    }
    
    @IBAction
    func setButtonAvailability(sender: UIDatePicker) {
        onDateChangedCallback?(sender.date)
    }

    func setButtonAvailability(`for` date: Date, recipe: Recipe) {
        guard let dateWithZeroSeconds = date.zeroSeconds else { return }

        setStartAtSelectedDateButtonEnabled(isStartDateValid(dateWithZeroSeconds, forRecipe: recipe))

        let totalTimeInMinutes = recipe.totalTimeInMinutes() ?? 0
        let totalTimeInSeconds = Double(totalTimeInMinutes * 60)
        let computedStartDate = dateWithZeroSeconds.addingTimeInterval(-totalTimeInSeconds)
        setFinishAtSelectedDateButtonEnabled(isStartDateValid(computedStartDate, forRecipe: recipe))
    }

    func setStartAtSelectedDateButtonEnabled(_ enabled: Bool) {
        startAtSelectedDateButton.isEnabled = enabled
    }

    func setFinishAtSelectedDateButtonEnabled(_ enabled: Bool) {
        finishAtSelectedDateButton.isEnabled = enabled
    }

    func isStartDateValid(_ date: Date, forRecipe recipe: Recipe) -> Bool {
        return date.compare(Date()) == .orderedDescending
    }

    func setButtonTitles(`for` date: Date, recipe: Recipe) {
        let totalTimeInMinutes = recipe.totalTimeInMinutes() ?? 0
        let totalTimeInSeconds = Double(totalTimeInMinutes * 60)
        let computedFinishDate = date.addingTimeInterval(totalTimeInSeconds)
        let computedStartDate = date.addingTimeInterval(-totalTimeInSeconds)

        let componentsToExtract : Set<Calendar.Component> = [.day, .month, .hour, .minute]
        let componentsOfCurrentDate = Calendar.current.dateComponents(componentsToExtract, from: date)
        let componentsOfComputedStartDate = Calendar.current.dateComponents(componentsToExtract, from: computedStartDate)
        let componentsOfComputedFinishDate = Calendar.current.dateComponents(componentsToExtract, from: computedFinishDate)

        func makeTwoCharacters(_ value: Int) -> String {
            if value < 10 { return "0" + String(value)}
            return String(value)
        }
        func printStringFromDateComponents(_ components: DateComponents) -> String {
            return "\(makeTwoCharacters(components.day!)).\(makeTwoCharacters(components.month!)). \(makeTwoCharacters(components.hour!)):\(makeTwoCharacters(components.minute!))"
        }

        startAtSelectedDateButton.setTitle(NSLocalizedString("\(printStringFromDateComponents(componentsOfCurrentDate)) -> \(printStringFromDateComponents(componentsOfComputedFinishDate))", comment: ""), for: .normal)
        finishAtSelectedDateButton.setTitle(NSLocalizedString("\(printStringFromDateComponents(componentsOfComputedStartDate)) -> \(printStringFromDateComponents(componentsOfCurrentDate))", comment: ""), for: .normal)

    }
}

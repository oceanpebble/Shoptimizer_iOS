// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

public class SuppliesItem: NSManagedObject {

    // MARK: - Constants

    static let ENTITY_NAME = "SuppliesItem";
    static let CACHE_NAME = "suppliesItems";
    static let COL_ITEM_NAME = "itemName";
    static let COL_CUR_AMOUNT_QUANTITY = "curAmountQuantity";
    static let COL_CUR_AMOUNT_UNIT = "curAmountUnit";
    static let COL_MIN_AMOUNT_QUANTITY = "minAmountQuantity";
    static let COL_MIN_AMOUNT_UNIT = "minAmountUnit";

    // MARK: - Create

    static func insert(name: String?, minAmountQuantity: NSNumber?, minAmountUnit: String?, curAmountQuantity: NSNumber?, curAmountUnit: String?, categories: String?) -> SuppliesItem {
        let suppliesItem = SuppliesItem(entity: entityDescription()!, insertInto: DBHelper.instance.getManagedObjectContext())
        suppliesItem.itemName = name
        suppliesItem.curAmountQuantity = curAmountQuantity
        suppliesItem.curAmountUnit = curAmountUnit
        suppliesItem.minAmountQuantity = minAmountQuantity
        suppliesItem.minAmountUnit = minAmountUnit

        if let categories = categories?.components(separatedBy: ";") {
            for category in categories {
                let cat = SuppliesItemCategory.categories(for: category)
                if cat.count == 1 {
                    suppliesItem.addToCategoriesForSuppliesItem(cat.first!)
                }
            }
        }

        DBHelper.instance.saveContext()

        return suppliesItem;
    }

    // MARK: - Read

    static func getAll() -> [SuppliesItem] {
        return fetch(SuppliesItem.fetchRequest())
    }

    static func items(for searchString: String) -> [SuppliesItem] {
        let searchPredicate = SuppliesItem.searchPredicate(for: searchString)
        return SuppliesItem.items(for: searchPredicate)
    }

    private static func items(for predicate: NSPredicate?) -> [SuppliesItem] {
        let request = standardFetchRequest()
        request.predicate = predicate
        return fetch(request)
    }

    static func fetchedResultsController(delegate: NSFetchedResultsControllerDelegate) -> NSFetchedResultsController<SuppliesItem> {
        deleteCache()
        let request = standardFetchRequest()
        request.fetchBatchSize = 20
        request.sortDescriptors = [sortDescriptor(forStringKey: SuppliesItem.COL_ITEM_NAME)]

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath: nil, cacheName: SuppliesItem.CACHE_NAME)
        aFetchedResultsController.delegate = delegate

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching supplies items.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController
    }

    static func isSuppliesItemAvailable(withName name: String) -> Bool {
        let predicate = NSPredicate(format: "%K = %@", SuppliesItem.COL_ITEM_NAME, name)
        return items(for: predicate).count > 0
    }

    // MARK: - Update

    // MARK: - Delete

    static func deleteAll() {
        deleteAll(entityName: SuppliesItem.ENTITY_NAME)
    }

    // MARK: - Util

    private static func standardFetchRequest() -> NSFetchRequest<SuppliesItem> {
        let fetchRequest: NSFetchRequest<SuppliesItem> = SuppliesItem.fetchRequest()
        fetchRequest.entity = entityDescription()
        return fetchRequest
    }

    private static func deleteCache() {
        deleteCache(cacheName: SuppliesItem.CACHE_NAME)
    }

    private static func entityDescription() -> NSEntityDescription? {
        return entityDescription(forEntityName: SuppliesItem.ENTITY_NAME)
    }

    public static func searchPredicate(for searchString: String) -> NSPredicate? {
        if searchString.isEmpty || searchString.count < 2 {
            return nil
        }
        
        let searchTextComponents = searchString.components(separatedBy: " ")
        var namePredicates = [NSPredicate]()
        var categoryNamePredicates = [NSPredicate]()

        for component in searchTextComponents {
            if component.isEmpty || component.count < 2 {
                continue
            }

            namePredicates.append(NSPredicate(format: "%K contains[c] %@", SuppliesItem.COL_ITEM_NAME, component))

            let categoryNamePredicate = NSPredicate(format: "SUBQUERY(categoriesForSuppliesItem, $cat, $cat.name contains[c] %@).@count > 0", component)
            categoryNamePredicates.append(categoryNamePredicate)
        }

        var compoundPredicates = [NSCompoundPredicate]()
        let nameCompundPredicate = NSCompoundPredicate(type: .or, subpredicates: namePredicates)
        let categoryNameCompundPredicate = NSCompoundPredicate(type: .or, subpredicates: categoryNamePredicates)
        compoundPredicates.append(nameCompundPredicate)
        compoundPredicates.append(categoryNameCompundPredicate)
        return NSCompoundPredicate(type: .or, subpredicates: compoundPredicates)
    }
}

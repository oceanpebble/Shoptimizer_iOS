// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class ShoppingListListTableViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate {
    
    // MARK: - Properties

    typealias EntityType = ShoppingList
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    var reusableCellIdentifier: String { return "ShoppingListPrototypeCell" }
    var swipedList: ShoppingList?
    var oldIncludeExclude = false
    private var viewModel: ShoppingListListTableViewModel!

    // MARK: - UIView

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController = ShoppingList.shoppingLists(self)
        let aggregatedListHelper = AggregatedListHelper.instance
        let dataManager = ShoppingListListTableDataManager(aggregatedListHelper: aggregatedListHelper, shoppingListRepository: ShoppingListRepository(), shoppingListItemRepository: ShoppingListItemRepository(), suppliesItemRepository: SuppliesItemRepository())
        viewModel = ShoppingListListTableViewModel(fetchedResultsController: fetchedResultsController, aggregatedListHelper: aggregatedListHelper, dataManager: dataManager)
        setupSwipeRecognizers()
        setupTapRecognizers()
    }
    
    // MARK: - Segue Handling

    @IBAction func unwindToList(_ segue: UIStoryboardSegue) {
        guard let source = segue.source as? AddShoppingListViewController else { return }

        if source.editingMode == .edit {
            viewModel.includeExcludeItems(for: source.fetchedList!, oldIncludeExclude: oldIncludeExclude)
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let specificSender = sender as? UITableViewCell {
            let targetController = segue.destination as! ShoppingListViewController
            let indexPath = tableView.indexPath(for: specificSender)
            let selectedList = viewModel.list(at: indexPath!)
            targetController.shoppingList = selectedList
        } else if let specificSender = sender as? ShoppingList {
            let targetController = segue.destination as! AddShoppingListViewController
            targetController.fetchedList = specificSender
            targetController.editingMode = .edit
        } else {
            let targetController = segue.destination as! AddShoppingListViewController
            targetController.editingMode = .create
        }
    }

    // MARK: - View Controller Life Cycle

    func setupTapRecognizers() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(respondToLongPressGesture(_:)))
        longPressRecognizer.minimumPressDuration = Constants.LONG_TAP_DURATION
        self.tableView.addGestureRecognizer(longPressRecognizer)
    }
    
    @objc func respondToLongPressGesture(_ recognizer: UILongPressGestureRecognizer) {
        guard recognizer.state == .began else { return }
        
        let location = recognizer.location(in: view)
        guard let swipedIndexPath = tableView.indexPathForRow(at: location) else { return }
        
        let longPressedList = viewModel.list(at: swipedIndexPath)
        guard longPressedList.name != AggregatedListHelper.instance.nameOfAggregatedList() else { return }
        
        editList(longPressedList)
    }
    
    func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target:self, action:#selector(respondToRightSwipeGesture(_:)))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: view)
        guard let swipedIndexPath = tableView.indexPathForRow(at: location) else { return }

        swipedList = viewModel.list(at: swipedIndexPath)
        swipedList!.name == AggregatedListHelper.instance.nameOfAggregatedList()
            ? presentOptionsForAggregatedList(at: swipedIndexPath)
            : presentOptionsForRegularList(at: swipedIndexPath)

        DBHelper.instance.saveContext()
    }
    
    func presentOptionsForAggregatedList(at path: IndexPath) {
        let optionsAlert = UIAlertController(title: NSLocalizedString("Options for Aggregated List", comment: ""), message: nil, preferredStyle: .actionSheet)
        let popOver = optionsAlert.popoverPresentationController
        popOver?.sourceView = tableView.cellForRow(at: path)
        popOver?.sourceRect = tableView.cellForRow(at: path)!.bounds

        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Update", comment: ""),
            handler: {_ in self.viewModel.updateAggregatedList()}))
        optionsAlert.addAction(UIAlertAction(
            title: NSLocalizedString("Cancel", comment:""), style:.cancel,
            handler:nil))

        present(optionsAlert, animated: true, completion: nil)
    }
    
    func presentOptionsForRegularList(at path: IndexPath) {
        let alertTitle = String(format: NSLocalizedString("Options for list %@", comment:""), swipedList?.value(forKey: ShoppingListEntity.COL_NAME) as! String)
        let optionsAlert = UIAlertController(title: alertTitle, message: nil, preferredStyle:.actionSheet)
        let popOver = optionsAlert.popoverPresentationController
        popOver?.sourceView = tableView.cellForRow(at: path)
        popOver?.sourceRect = tableView.cellForRow(at: path)!.bounds

        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Transfer done items to supplies", comment:""),
            handler: {_ in self.transferItemsFromList(self.swipedList!, andDeleteItems: false)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Transfer and delete done items", comment:""),
            handler: {_ in self.transferItemsFromList(self.swipedList!, andDeleteItems: true)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Delete done items", comment:""),
            handler: {_ in self.deleteDoneItemsFromList(self.swipedList!)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Uncheck all items", comment:""),
            handler: {_ in self.uncheckItemsOnList(self.swipedList!)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Clear list", comment:""),
            handler: {_ in self.clearList(self.swipedList!)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Edit list", comment:""),
            handler: {_ in self.editList(self.swipedList!)}))
        optionsAlert.addAction(alertActionforListOption(
            title: NSLocalizedString("Share list", comment: ""),
            handler: {_ in self.shareList(self.swipedList!, atIndexPath: path)}))
        optionsAlert.addAction(UIAlertAction(
            title: NSLocalizedString("Cancel", comment:""), style: .cancel,
            handler: nil))
        
        present(optionsAlert, animated: true, completion: nil)
    }
    
    private func alertActionforListOption(title: String, handler: @escaping ((UIAlertAction) -> Void)) -> UIAlertAction {
        return UIAlertAction(title: title, style: .default, handler: handler)
    }

    // MARK: - Options For Regular Lists
    
    func transferItemsFromList(_ list: ShoppingList, andDeleteItems deleteItems: Bool) {
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: deleteItems)
        if numberOfTransferredItems == 1 {
            let transferredItemsString = NSLocalizedString("Transferred one item to supplies.", comment:"")
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if numberOfTransferredItems > 1 {
            let transferredItemsString = String(format:NSLocalizedString("Transferred %i items to supplies.", comment:""), numberOfTransferredItems)
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        }
        tableView.reloadData()
    }

    func deleteDoneItemsFromList(_ list: ShoppingList) {
        let numberOfDeletedItems = viewModel.deleteDoneItemsFrom(list)
        if numberOfDeletedItems == 1 {
            let deletedItemsString = NSLocalizedString("Deleted one item.", comment:"")
            GeneralHelper.presentInformationDialogWithText(deletedItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if numberOfDeletedItems > 1 {
            let deletedItemsString = String(format:NSLocalizedString("Deleted %i items.", comment:""), numberOfDeletedItems)
            GeneralHelper.presentInformationDialogWithText(deletedItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        }
        tableView.reloadData()
    }
    
    func uncheckItemsOnList(_ list: ShoppingList) {
        let doneItems = viewModel.uncheckItemsOn(list)
        if doneItems.count == 1 {
            let transferredItemsString = NSLocalizedString("Unchecked one item.", comment:"")
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if doneItems.count > 1 {
            let transferredItemsString = String(format:NSLocalizedString("Unchecked %i items.", comment:""), doneItems.count)
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        }
        tableView.reloadData()
    }

    func clearList(_ list: ShoppingList) {
        let numberOfItemsOnList = viewModel.deleteItemsFrom(list)
        if numberOfItemsOnList == 1 {
            let transferredItemsString = NSLocalizedString("Deleted one item.", comment:"")
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        } else if numberOfItemsOnList > 1 {
            let transferredItemsString = String(format:NSLocalizedString("Deleted %i items.", comment:""), numberOfItemsOnList)
            GeneralHelper.presentInformationDialogWithText(transferredItemsString, andTitle: Constants.TITLE_ALERT_SUCCESS)
        }
        tableView.reloadData()
    }
    
    func editList(_ list: ShoppingList) {
        oldIncludeExclude = viewModel.isListIncludedInAggregatedList(list)
        performSegue(withIdentifier: "EditListSegue", sender: list)
    }
    
    func shareList(_ list: ShoppingList, atIndexPath swipedIndexPath: IndexPath) {
        guard let url = viewModel.exportToFileURL(list) else { return }
        let activityViewController = UIActivityViewController(activityItems: ["", url], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.addToReadingList, .assignToContact, .openInIBooks, .postToFacebook, .postToFlickr, .postToTencentWeibo, .postToTwitter, .postToVimeo, .postToWeibo, .print, .saveToCameraRoll]
        let popOver = activityViewController.popoverPresentationController
        popOver?.sourceView = tableView.cellForRow(at: swipedIndexPath)
        popOver?.sourceRect = tableView.cellForRow(at: swipedIndexPath)!.bounds
        present(activityViewController, animated: true, completion: nil)
    }

    // MARK: - UITableViewController
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let swipedList = viewModel.list(at: indexPath)
        return !viewModel.isAggregatedList(swipedList)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedList = viewModel.list(at: indexPath)
            viewModel.deleteItemsFromAggregatedListForList(swipedList)
            viewModel.deleteShoppingList(swipedList)
        }
    }

    // MARK: - NSFetchedResultsController
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        etdControllerWillChangeContent(controller, inTableView: tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        etdControllerDidChangeContent(controller, inTableView: tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }
        let list = viewModel.list(at: indexPath)
        cell.textLabel?.attributedText = NSAttributedString(
            string: viewModel.listNameDisplayString(for: list),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.black]);
        cell.detailTextLabel?.text = viewModel.priceString(for: list)
        cell.accessoryType = .disclosureIndicator;
    }
}

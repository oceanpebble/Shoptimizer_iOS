// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class RecipeCategoriesTableViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    // MARK: - Properties

    typealias EntityType = RecipeCategory
    var reusableCellIdentifier: String { return "RecipeCategoryPrototypeCell" }
    var recipe : Recipe!
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    var categoriesForRecipe: [RecipeCategory]?
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: - UIView

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoriesForRecipe = RecipeCategory.categories(for: self.recipe)
        self.fetchedResultsController = RecipeCategory.categories(self)
        self.setupSwipeRecognizers()
        self.setupSearchBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    func setupSearchBar() {
        self.searchBar.delegate = self
    }

    func resetSearchBar() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    @IBAction fileprivate func unwindToList(_ segue: UIStoryboardSegue) {
        // guard let source = segue.source as? AddRecipeCategoryViewController else { return }

        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! AddRecipeCategoryViewController

        if let specificSender = sender as? UITableViewCell {
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedCategory = self.fetchedResultsController.object(at: indexPath!)
            targetController.fetchedCategory = selectedCategory
            targetController.editingMode = .edit
        } else {
            targetController.editingMode = .create
        }
    }

    // *********************************************************
    //
    // MARK: - View Controller Life Cycle
    //
    // *********************************************************

    func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target:self, action:#selector(self.respondToRightSwipeGesture))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }

    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: self.view)
        guard let swipedIndexPath = self.tableView.indexPathForRow(at: location) else {
            return
        }

        guard let swipedCategory = self.fetchedResultsController?.object(at: swipedIndexPath) else {
            return
        }

        if let categories = swipedCategory.recipesForCategory,
                categories.contains(self.recipe!) {
            swipedCategory.removeFromRecipesForCategory(self.recipe)
        } else {
            swipedCategory.addToRecipesForCategory(self.recipe)
        }

        DBHelper.instance.saveContext()
        self.categoriesForRecipe = RecipeCategory.categories(for: self.recipe)
        self.tableView.reloadData()
    }

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedCategory = self.fetchedResultsController!.object(at: indexPath)
            DBHelper.instance.getManagedObjectContext().delete(swipedCategory)
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }

        let category = self.fetchedResultsController.object(at: indexPath)
        let foregroundColor = self.categoriesForRecipe!.contains(category) ? UIColor.green : UIColor.label;
        
        cell.textLabel?.attributedText = NSAttributedString(string: category.name!, attributes:[NSAttributedString.Key.foregroundColor: foregroundColor])
        cell.accessoryType = .disclosureIndicator
    }

    // *********************************************************
    //
    // MARK: - UISearchBarDelegate
    //
    // *********************************************************

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            cancelSearch(searchBar)
        } else {
            self.navigationItem.title = NSLocalizedString("Recipe Categories (filtered)", comment: "")

            let searchTextComponents = searchText.components(separatedBy: " ")
            var namePredicates = [NSPredicate]()

            for component in searchTextComponents {

                if component.isEmpty || component.count < 2 {
                    continue
                }

                let namePredicate = NSPredicate(format: "%K contains[c] %@", RecipeCategoryEntity.COL_NAME, component)
                namePredicates.append(namePredicate)
            }

            let compoundPredicate = NSCompoundPredicate(type: .and, subpredicates: namePredicates)
            refreshTableView(withPredicate: compoundPredicate)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetSearchBar()
        cancelSearch(searchBar)
    }

    // *********************************************************
    //
    // MARK: - Cancel Search
    //
    // *********************************************************

    func cancelSearch(_ searchBar: UISearchBar) {
        self.navigationItem.title = NSLocalizedString("Recipe Categories", comment: "")
        refreshTableView(withPredicate: nil)
    }

    func refreshTableView(withPredicate predicate: NSPredicate?) {
        self.fetchedResultsController.fetchRequest.predicate = predicate
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: RecipeCategoryEntity.CACHE_NAME)

        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        self.tableView.reloadData()
    }
}

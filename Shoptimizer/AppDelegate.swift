// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import StoreKit
import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    var window: UIWindow?
    let keyLaunchCounter = "LaunchCounter"

    // *********************************************************
    //
    // MARK: - Application Life Cycle
    //
    // *********************************************************

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // loading example date, migrating data
        if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_3_2_0.rawValue) {
            if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_3_0_0.rawValue) {
                if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_2_3_0.rawValue) {
                    if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_2_1_0.rawValue) {
                        if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_2_0_0.rawValue) {
                            if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_1_1_0.rawValue) {
                                if GeneralHelper.firstStartOfVersion(ShoptimizerVersions.v_1_0_0.rawValue) {
                                    self.loadExampleDataForVersion1_1_0()
                                } else {
                                    self.migrateDataToVersion1_1_0()
                                }
                            }
                            self.loadExampleDataForVersion2_0_0()
                        }
                        ShoppingListItem.deleteObsoleteListItems()
                    }
                    self.loadExampleDataForVersion2_3_0()
                }
                self.migrateDataToVersion3_0_0()
                self.loadExampleDataForVersion3_0_0()
            }
            loadExampleDataForVersion3_2_0()
        }

        // rename the aggregated list, if the language has changed
        self.renameAggregatedList()

        // ask permission for notifications
        NotificationController.sharedInstance.requestAuthorization()

//         handleLaunchArguments()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        DBHelper.instance.saveContext()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        DBHelper.instance.saveContext()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        DBHelper.instance.saveContext()
    }

    // *********************************************************
    //
    // MARK: - Launch Counter
    //
    // *********************************************************

    func resetLaunchCounter() {
        UserDefaults.standard.set(0, forKey: keyLaunchCounter)
    }

    func incrementLaunchCounterByOne() -> Int {
        let newValue = UserDefaults.standard.integer(forKey: keyLaunchCounter) + 1
        UserDefaults.standard.set(newValue, forKey: keyLaunchCounter)
        return newValue
    }

    // *********************************************************
    //
    // MARK: - Importing .shoptimizer files
    //
    // *********************************************************

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        guard url.pathExtension == Constants.SHARE_FILES_FILE_EXTENSION,
            let dictionary = NSDictionary(contentsOf: url),
            let importedInfo = dictionary as? [String: AnyObject],
            let type = importedInfo[Constants.SHARE_FILES_PARAMETER_NAME_TYPE] as? String else {
                let corruptFileMessage = NSLocalizedString("The shared file is corrupt.", comment:"");
                GeneralHelper.presentInformationDialogWithText(corruptFileMessage, andTitle: Constants.TITLE_CORRUPT_SHARE_FILE)
                tryToDeleteFile(at: url)
                return false
        }

        switch type {
        case Constants.SHARE_FILES_TYPE_RECIPE: Recipe.importData(from: importedInfo)
        case Constants.SHARE_FILES_TYPE_SHOPPING_LIST: ShoppingList.importData(from: importedInfo)
        default:
            let unsupportedTypeMessage = NSLocalizedString("The type of the shared file is unsupported.", comment:"");
            GeneralHelper.presentInformationDialogWithText(unsupportedTypeMessage, andTitle: Constants.TITLE_UNSUPPORTED_SHARE_FILE_TYPE)
            tryToDeleteFile(at: url)
            return false
        }

        tryToDeleteFile(at: url)
        return true
    }

    func tryToDeleteFile(at url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print("Failed to remove item from Inbox")
        }
    }

    // *********************************************************
    //
    // MARK: - Example data for various versions
    //
    // *********************************************************

    func loadExampleDataForVersion1_1_0() {
        let context = DBHelper.instance.getManagedObjectContext()

        if GeneralHelper.getPreferredLanguage() == "de" {
            if let aggregatedList = ShoppingList.insertNewShoppingList(withName: AggregatedListHelper.instance.nameOfAggregatedList(), andType:"Static",includeInAggregateList:false, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milch", quantity:NSNumber(value: 2.0 as Double), unit:"l", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Becher", quantity:NSNumber(value: 20.0 as Double), unit:"Stück", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Chips", quantity:NSNumber(value: 10.0 as Double), unit:"Packung", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Clownkostüm", quantity:NSNumber(value: 1.0 as Double), unit:"Stück", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"KOSTÜMGESCHÄFT", forList:aggregatedList, intoContext:context)
            }

            if let groceriesList = ShoppingList.insertNewShoppingList(withName: "Wocheneinkauf", andType:"Dynamic", includeInAggregateList:true, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milch", quantity:NSNumber(value: 2.0 as Double), unit:"l", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:groceriesList, intoContext:context)
            }

            if let partyList = ShoppingList.insertNewShoppingList(withName: "Kostümparty", andType:"Dynamic", includeInAggregateList:true, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Bier", quantity:NSNumber(value: 40.0 as Double), unit:"l", doneStatus:true, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Becher", quantity:NSNumber(value: 20.0 as Double), unit:"Stück", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Chips", quantity:NSNumber(value: 10.0 as Double), unit:"Packung", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"SUPERMARKT", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Clownkostüm", quantity:NSNumber(value: 1.0 as Double), unit:"Stück", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"KOSTÜMGESCHÄFT", forList:partyList, intoContext:context)
            }

            let _ = SuppliesItem.insert(name: "Kartoffeln", minAmountQuantity: NSNumber(value: 1.0 as Double), minAmountUnit: "kg", curAmountQuantity: NSNumber(value: 0.5 as Double), curAmountUnit: "kg", categories: nil)
            let _ = SuppliesItem.insert(name: "Orangen", minAmountQuantity: NSNumber(value: 5.0 as Double), minAmountUnit: "Stück", curAmountQuantity: NSNumber(value: 10 as Double), curAmountUnit: "Stück", categories: nil)
            let _ = SuppliesItem.insert(name: "Olivenöl", minAmountQuantity: NSNumber(value: 0.5 as Double), minAmountUnit: "l", curAmountQuantity: NSNumber(value: 0.6 as Double), curAmountUnit: "l", categories: nil)
        } else {
            if let aggregatedList = ShoppingList.insertNewShoppingList(withName: AggregatedListHelper.instance.nameOfAggregatedList(), andType:"Static", includeInAggregateList:false, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milk", quantity:NSNumber(value: 2.0 as Double), unit:"l", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Cups", quantity:NSNumber(value: 20.0 as Double), unit:"piece", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Chips", quantity:NSNumber(value: 10.0 as Double), unit:"package", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:aggregatedList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Clown Costume", quantity:NSNumber(value: 1.0 as Double), unit:"piece", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"COSTUME STORE", forList:aggregatedList, intoContext:context)
            }

            if let groceriesList = ShoppingList.insertNewShoppingList(withName: "Weekly Groceries", andType:"Dynamic", includeInAggregateList:true, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milk", quantity:NSNumber(value: 2.0 as Double), unit:"l", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:groceriesList, intoContext:context)
            }

            if let partyList = ShoppingList.insertNewShoppingList(withName: "Costume Party", andType:"Dynamic", includeInAggregateList:true, intoContext:context) {
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Beer", quantity:NSNumber(value: 40.0 as Double), unit:"l", doneStatus:true, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Cups", quantity:NSNumber(value: 20.0 as Double), unit:"piece", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Chips", quantity:NSNumber(value: 10.0 as Double), unit:"package", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"GROCERY STORE", forList:partyList, intoContext:context)
                let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Clown Costume", quantity:NSNumber(value: 1.0 as Double), unit:"piece", doneStatus:false, brand: nil, forPrice: nil, atCurrency: nil, inShop:"COSTUME STORE", forList:partyList, intoContext:context)
            }

            let _ = SuppliesItem.insert(name: "Potatoes", minAmountQuantity: NSNumber(value: 1.0 as Double), minAmountUnit: "kg", curAmountQuantity: NSNumber(value: 0.5 as Double), curAmountUnit: "kg", categories: nil)
            let _ = SuppliesItem.insert(name: "Oranges", minAmountQuantity: NSNumber(value: 5.0 as Double), minAmountUnit: "piece", curAmountQuantity: NSNumber(value: 10 as Double), curAmountUnit: "piece", categories: nil)
            let _ = SuppliesItem.insert(name: "Olive Oil", minAmountQuantity: NSNumber(value: 0.5 as Double), minAmountUnit: "l", curAmountQuantity: NSNumber(value: 0.6 as Double), curAmountUnit: "l", categories: nil)
        }

        DBHelper.instance.saveContext()
    }

    func loadExampleDataForVersion2_0_0() {
        let context = DBHelper.instance.getManagedObjectContext()

        if GeneralHelper.getPreferredLanguage() == "de" {
            let friedRice = Recipe.insertNewRecipe(withName: "Gebratener Reis", andPortions:NSNumber(value: 3 as Int32), intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Reis (Jasmin)", quantity:NSNumber(value: 300 as Int32), andUnit:"g",forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Paprika", quantity:NSNumber(value: 1 as Int32), andUnit:"Stück", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Ei", quantity:NSNumber(value: 2 as Int32), andUnit:"Stück", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Sprossen", quantity:NSNumber(value: 200 as Int32), andUnit:"g", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Röstzwiebel", quantity:NSNumber(value: 10 as Int32), andUnit:"g", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Karotte", quantity:NSNumber(value: 2 as Int32), andUnit:"Stück", forRecipe:friedRice,intoContext:context)

            let risotto = Recipe.insertNewRecipe(withName: "Risotto", andPortions:NSNumber(value: 2 as Int32), intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Reis (Risotto)", quantity:NSNumber(value: 200 as Int32), andUnit:"g", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Wein (weiß)", quantity:NSNumber(value: 100 as Int32), andUnit:"ml", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Brühe", quantity:NSNumber(value: 10 as Int32), andUnit:"g", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Zucchini", quantity:NSNumber(value: 1 as Int32), andUnit:"Stück", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Karotte", quantity:NSNumber(value: 1 as Int32), andUnit:"Stück", forRecipe:risotto, intoContext:context)

            let _ = SuppliesItem.insert(name: "Reis (Jasmin)", minAmountQuantity: NSNumber(value: 200 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 600 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Paprika", minAmountQuantity: NSNumber(value: 1 as Double), minAmountUnit: "Stück", curAmountQuantity: NSNumber(value: 2 as Double), curAmountUnit: "Stück", categories: nil)
            let _ = SuppliesItem.insert(name: "Ei", minAmountQuantity: NSNumber(value: 5 as Double), minAmountUnit: "Stück", curAmountQuantity: NSNumber(value: 9 as Double), curAmountUnit: "Stück", categories: nil)
            let _ = SuppliesItem.insert(name: "Sprossen", minAmountQuantity: NSNumber(value: 0 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 300 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Röstzwiebel", minAmountQuantity: NSNumber(value: 50 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 75 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Karotte", minAmountQuantity: NSNumber(value: 2 as Double), minAmountUnit: "Stück", curAmountQuantity: NSNumber(value: 3 as Double), curAmountUnit: "Stück", categories: nil)
        } else {
            let friedRice = Recipe.insertNewRecipe(withName: "Fried Rice", andPortions:NSNumber(value: 3 as Int32), intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Rice (Jasmin)", quantity:NSNumber(value: 300 as Int32), andUnit:"g", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Paprika", quantity:NSNumber(value: 1 as Int32), andUnit:"piece", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Egg", quantity:NSNumber(value: 2 as Int32), andUnit:"piece", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Sprouts", quantity:NSNumber(value: 200 as Int32), andUnit:"g", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Roasted Onions", quantity:NSNumber(value: 10 as Int32), andUnit:"g", forRecipe:friedRice, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Carot", quantity:NSNumber(value: 2 as Int32), andUnit:"piece", forRecipe:friedRice, intoContext:context)

            let risotto = Recipe.insertNewRecipe(withName: "Risotto", andPortions:NSNumber(value: 2 as Int32), intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Rice (Risotto)", quantity:NSNumber(value: 200 as Int32), andUnit:"g", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Wine (white)", quantity:NSNumber(value: 100 as Int32), andUnit:"ml", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Broth", quantity:NSNumber(value: 10 as Int32), andUnit:"g", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Zucchini", quantity:NSNumber(value: 1 as Int32), andUnit:"piece", forRecipe:risotto, intoContext:context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Carot", quantity:NSNumber(value: 1 as Int32), andUnit:"piece", forRecipe:risotto, intoContext:context)

            let _ = SuppliesItem.insert(name: "Rice (Jasmin)", minAmountQuantity: NSNumber(value: 200 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 600 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Paprika", minAmountQuantity: NSNumber(value: 1 as Double), minAmountUnit: "piece", curAmountQuantity: NSNumber(value: 2 as Double), curAmountUnit: "piece", categories: nil)
            let _ = SuppliesItem.insert(name: "Egg", minAmountQuantity: NSNumber(value: 5 as Double), minAmountUnit: "piece", curAmountQuantity: NSNumber(value: 9 as Double), curAmountUnit: "piece", categories: nil)
            let _ = SuppliesItem.insert(name: "Sprouts", minAmountQuantity: NSNumber(value: 0 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 300 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Roasted Onions", minAmountQuantity: NSNumber(value: 50 as Double), minAmountUnit: "g", curAmountQuantity: NSNumber(value: 75 as Double), curAmountUnit: "g", categories: nil)
            let _ = SuppliesItem.insert(name: "Carot", minAmountQuantity: NSNumber(value: 2 as Double), minAmountUnit: "piece", curAmountQuantity: NSNumber(value: 3 as Double), curAmountUnit: "piece", categories: nil)
        }

        DBHelper.instance.saveContext()
    }

    func loadExampleDataForVersion2_3_0() {
        let context = DBHelper.instance.getManagedObjectContext()

        if GeneralHelper.getPreferredLanguage() == "de" {
            let list = ShoppingList.insertNewShoppingList(withName: "Neue Funktionen", andType: "Dynamic", includeInAggregateList: false, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Apfel", quantity: 5, unit: "Stück", doneStatus: true, brand: "Marke A", forPrice: 0.95, atCurrency: "EUR", inShop: "Geschäft 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Müsli", quantity: 1, unit: "Packung", doneStatus: false, brand: "Marke B", forPrice: 2.99, atCurrency: "EUR", inShop: "Geschäft 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Zahnbürste", quantity: 2, unit: "Stück", doneStatus: false, brand: "Marke A", forPrice: 1.49, atCurrency: "EUR", inShop: "Geschäft 2", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milch", quantity: 1, unit: "l", doneStatus: true, brand: "Marke A", forPrice: 0.49, atCurrency: "EUR", inShop: "Geschäft 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Shampoo", quantity: 1, unit: "Packung", doneStatus: false, brand: "Marke C", forPrice: 1.29, atCurrency: "EUR", inShop: "Geschäft 2", forList: list!, intoContext: context)
        } else {
            let list = ShoppingList.insertNewShoppingList(withName: "New Features", andType: "Dynamic", includeInAggregateList: false, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Apple", quantity: 5, unit: "piece", doneStatus: true, brand: "Brand A", forPrice: 0.95, atCurrency: "EUR", inShop: "Shop 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Cereal", quantity: 1, unit: "package", doneStatus: false, brand: "Brand B", forPrice: 2.99, atCurrency: "EUR", inShop: "Shop 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Tooth Brush", quantity: 2, unit: "piece", doneStatus: false, brand: "Brand A", forPrice: 1.49, atCurrency: "EUR", inShop: "Shop 2", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Milk", quantity: 1, unit: "l", doneStatus: true, brand: "Brand A", forPrice: 0.49, atCurrency: "EUR", inShop: "Shop 1", forList: list!, intoContext: context)
            let _ = ShoppingListItem.insertNewShoppingListItem(withName: "Shampoo", quantity: 1, unit: "package", doneStatus: false, brand: "Brand C", forPrice: 1.29, atCurrency: "EUR", inShop: "Shop 2", forList: list!, intoContext: context)
        }

        DBHelper.instance.saveContext()
    }

    func loadExampleDataForVersion3_0_0() {
        let context = DBHelper.instance.getManagedObjectContext()

        if GeneralHelper.getPreferredLanguage() == "de" {
            let categories = ["Dessert", "Suppe", "Pasta", "Party", "Grill", "Brot", "Vorspeise", "Hauptspeise", "Fleisch", "Vegan", "Vegetarisch", "Laktosefrei", "Eintopf", "Asiatisch", "Scharf", "Kuchen", "Torte"]
            for category in categories {
                let _ = RecipeCategory.insertNewRecipeCategory(withName: category, intoContext: context)
            }

            let sauerteig = Recipe.insertNewRecipe(withName: "Sauerteig", andPortions: NSNumber(value: 1), intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Roggenmehl, Type 1150", quantity: NSNumber(value: 310), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteig, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Wasser", quantity: NSNumber(value: 350), andUnit: "ml", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: false), forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 1), andTitle: "Erster Ansatz", andPreparation: "Sauerteig wird über mehrere Tage angefüttert, damit er nach und nach sauer werden kann. Am ersten Tag werden 100 g Roggenmehl, Type 1150 und 110 ml lauwarmes Wasser (d.h., max. 30 Grad Celsius) vermischt.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "100 g Roggenmehl mit 110 ml Wasser mischen", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 2), andTitle: "Erstes Füttern", andPreparation: "Heute werden weitere 60 g Roggenmehl, Type 1150 und 70 ml lauwarmes Wasser vermischt.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "60 g Roggenmehl und 70 ml Wasser dazumischen.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 3), andTitle: "Zweites Füttern", andPreparation: "Heute werden weitere 60 g Roggenmehl, Type 1150 und 70 ml lauwarmes Wasser vermischt.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "60 g Roggenmehl und 70 ml Wasser dazumischen.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 4), andTitle: "Drittes Füttern", andPreparation: "Heute werden weitere 50 g Roggenmehl, Type 1150 und 50 ml lauwarmes Wasser vermischt.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "50 g Roggenmehl und 50 ml Wasser dazumischen.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 5), andTitle: "Finales Füttern", andPreparation: "Heute werden weitere 40 g Roggenmehl, Type 1150 und 50 ml lauwarmes Wasser vermischt. Nach weiteren 24 h Wartezeit ist der Sauerteig einsatzbereit. ", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "40 g Roggenmehl und 50 ml Wasser dazumischen.", forRecipe: sauerteig, intoContext: context)

            let sauerteigBrot = Recipe.insertNewRecipe(withName: "Sauerteig-Brot", andPortions: NSNumber(value: 1), intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Roggenmehl, Type 1150", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Weizenmehl, Vollkorn", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Dinkelmehl, Vollkorn", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Salz", quantity: NSNumber(value: 12), andUnit: "g", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Wasser", quantity: NSNumber(value: 600), andUnit: "ml", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: false), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Haselnusskern", quantity: NSNumber(value: 50), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 1), andTitle: "Anstellgut rausstellen", andPreparation: "Nimm das Anstellgut aus dem Kühlschrank damit es Zimmertemperatur annehmen kann.", andWaitingTime: NSNumber(value: 60), andPreparationTime: NSNumber(value: 2), andSummary: "Anstellgut rausstellen", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 2), andTitle: "Sauerteig vorbereiten", andPreparation: "Um den Sauerteig vorzubereiten, vermische das Anstellgut in einer großen Rührschüssel mit 300g Roggenmehl, Type 1150 sowie 300ml (bzw. g) Wasser. Rühre solange, bis sich eine konsistente Mischung ergeben hat und kein Mehl mehr zu sehen ist. Anschließend muss der Teig ruhen und durchsäuern. Die exakte Ruhezeit kann je nach Stärke des Anstellguts etwas variieren, in der Regel sollten zwölf Stunden ausreichen.", andWaitingTime: NSNumber(value: 720), andPreparationTime: NSNumber(value: 10), andSummary: "Zutaten für Sauerteig vermischen.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 3), andTitle: "Finalen Teig vorbereiten", andPreparation: "Nun wird der eigentliche Brotteig vorbereitet. Nimm dafür zunächst ca. 60g des Auerteigs als neues Anstellgut zur Seite und stelle es wieder in den Kühlschrank. Vermische den Rest des Sauerteigs mit 300g Weizenmehl, Vollkorn sowie 300g Dinkelmehl, Vollkorn. Außerdem kommen noch 12g Salz und weitere 300g Wasser hinzu. Die Haselnusskerne folgen später. Auch jetzt muss wieder alles gründlich vermischt werden. Anschließend darf der Teig wieder etwas ruhen.", andWaitingTime: NSNumber(value: 30), andPreparationTime: NSNumber(value: 15), andSummary: "Anstellgut abzweigen und restliche Zutaten mit Sauerteig vermischen.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 4), andTitle: "Laib formen", andPreparation: "Jetzt darf das Brot Form annehmen. Nimm den Teig aus der Schüssel auf eine gut bemehlte Arbeitsfläche und verteile darauf etwas Mehl sowie die 50g Haselnusskerne. Knete den Teig nun von Hand, um die Haselnusskerne einzuarbeiten und einen Laib zu formen. Dieser kann rund oder länglich sein, wie es gefällt. Falls vorhanden kann der Laib nun in ein Gärkörbchen gegeben werden oder direkt auf das kalte Backblech. Nun folgt die letzte Ruhephase vor dem Backen, in der das Brot an Volumen zunimmt. Auch hier kann die tatsächliche Zeit variieren, zwei Stunden sind aber ein guter Richtwert.", andWaitingTime: NSNumber(value: 120), andPreparationTime: NSNumber(value: 10), andSummary: "Teig auf Arbeitsfläche kneten und zu Laib formen.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 5), andTitle: "Backofen vorheizen", andPreparation: "Brot wird bei sehr hoher Anfangstemperatur und anschließend bei fallender Temperatur gebacken. Dies garantiert zum Einen eine schöne knackige Kruste und zum Anderen eine saftige Krume im Inneren. Daher wird nun der Backofen auf 250 Grad Celsius vorgeheizt.", andWaitingTime: NSNumber(value: 9), andPreparationTime: NSNumber(value: 1), andSummary: "Backofen auf 250 Grad Celsius vorheizen.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 6), andTitle: "Brot backen - Teil 1", andPreparation: "Nun kann das Brot gebacken werden. Bevor es in den Backofen kommt, schneide das Brot mehrmals tief mit einem scharfen Messer ein. Das gibt zum Einen eine schöne Optik, zum Anderen sorgt es dafür, dass das Brot nicht an ungewünschten Stellen aufreißt. Gib das Backblech mit dem Brot nun in den Backofen und stelle die Uhr auf 10 Minuten. Gib außerdem noch etwas Wasser auf den Boden des Backofens. Das Wasser verdampft dadurch (Vorsicht, gefährlich!) und erzeugt eine Mini-Wolke im Backofen. Man nennt dies 'Schwaden'. Das sorgt für eine besonders knusprige Kruste.", andWaitingTime: NSNumber(value: 10), andPreparationTime: NSNumber(value: 1), andSummary: "Brot einschießen und schwaden.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 7), andTitle: "Brot backen - Teil 2", andPreparation: "Stelle die Temperatur nun auf 220 Grad Celsius und warte weitere 15 Minuten.", andWaitingTime: NSNumber(value: 15), andPreparationTime: NSNumber(value: 1), andSummary: "Temperatur auf 220 Grad Celsius reduzieren und 15 min warten.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 8), andTitle: "Brot backen - Teil 3", andPreparation: "Die Temperatur fällt weiter, diesmal auf 190 Grad Celsius für weitere 20 Minuten.", andWaitingTime: NSNumber(value: 20), andPreparationTime: NSNumber(value: 1), andSummary: "Temperatur auf 190 Grad Celsius reduzieren und 20 min warten.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 9), andTitle: "Brot backen - Teil 4", andPreparation: "Jetzt startet die letzte Backphase: 15 Minuten bei 160 Grad Celsius.", andWaitingTime: NSNumber(value: 15), andPreparationTime: NSNumber(value: 1), andSummary: "Temperatur auf 160 Grad Celsius reduzieren und 15 min warten.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 10), andTitle: "Brot abkühlen", andPreparation: "Das Brot ist fertig! Jetzt muss es mehrere Stunden abkühlen. Stelle das Brot dazu unabgedeckt auf ein Gitter. Das Gitter ist wichtig, da sich ansonsten unter dem Brot Feuchtigkeit ansammeln würde, wodurch es schnell wabbelig würde. Sobald das Brot abgekühlt ist (hierfür mindestens drei Stunden warten), kann es geschnitten und mit beliebigem Aufschnitt oder Aufstrich (oder einfach pur) genossen werden. Zur Lagerung kann das Brot in ein Gefrierfach gegeben werden, ansonsten trocknet es recht schnell aus, da es ja keine Zusatzstoffe beinhaltet.", andWaitingTime: NSNumber(value: 0), andPreparationTime: NSNumber(value: 1), andSummary: "Brot herausnehmen und auf Gitter stellen.", forRecipe: sauerteigBrot, intoContext: context)
        } else {
            let categories = ["Dessert", "Soup", "Pasta", "Party", "BBQ", "Bread", "Starter", "Main Dish", "Meat", "Vegan", "Vegetarian", "Lactose-free", "Stew", "Asian", "Spicy", "Cake", "Pie"]
            for category in categories {
                let _ = RecipeCategory.insertNewRecipeCategory(withName: category, intoContext: context)
            }

            let sauerteig = Recipe.insertNewRecipe(withName: "Sourdough", andPortions: NSNumber(value: 1), intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Rye Flour, Type 1150", quantity: NSNumber(value: 310), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteig, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Water", quantity: NSNumber(value: 350), andUnit: "ml", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: false), forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 1), andTitle: "Initial mix", andPreparation: "Sourdough must be fed over the course of several days, for getting sour bit by bit. On the first day 100 rye flour, type 1150 and 100 ml lukewarm water (i.e., no warmer than 30 Celsius) are mixed together.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "Mix 100 g rye flour with 110 ml water.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 2), andTitle: "First feeding", andPreparation: "Today another 60 g rye flour, type 1150 and 70 ml lukewarm water are added.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "Add 60 g rye flour and 70 ml water.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 3), andTitle: "Second feeding", andPreparation: "Today another 60 g rye flour, type 1150 and 70 ml lukewarm water are added.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "Add 60 g rye flour and 70 ml water.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 4), andTitle: "Third feeding", andPreparation: "Today another 50 g rye flour, type 1150 und 50 ml lukearm water are added.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "Add 50 g rye flour and 50 ml water.", forRecipe: sauerteig, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 5), andTitle: "Final feeding", andPreparation: "Today another 40 g rye flour, type 1150 und 50 ml lukewarm water are added. After another 24 h of waiting the sourdough is ready to be turned into bread.", andWaitingTime: NSNumber(value: 1440), andPreparationTime: NSNumber(value: 5), andSummary: "Add 40 g rye flour and 50 ml water.", forRecipe: sauerteig, intoContext: context)

            let sauerteigBrot = Recipe.insertNewRecipe(withName: "Sourdough Bread", andPortions: NSNumber(value: 1), intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Rye Flour, Type 1150", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Wheat Flour, Whole Grain", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Spelt Flour, Whole Grain", quantity: NSNumber(value: 300), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Salt", quantity: NSNumber(value: 12), andUnit: "g", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Water", quantity: NSNumber(value: 600), andUnit: "ml", andIncludeInList: NSNumber(value: false), andIncludeInSearch: NSNumber(value: false), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipeIngredient.insertNewRecipeIngredient(withName: "Hazelnut", quantity: NSNumber(value: 50), andUnit: "g", andIncludeInList: NSNumber(value: true), andIncludeInSearch: NSNumber(value: true), forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 1), andTitle: "Take out starter", andPreparation: "Take the sourdough starter out of the fridge so that it can take on room temperature.", andWaitingTime: NSNumber(value: 60), andPreparationTime: NSNumber(value: 2), andSummary: "Take out starter", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 2), andTitle: "Prepare sourdough", andPreparation: " For the sourdough, take a large bowl and in it mix the starter together with 300g rye flour, type 1150 and 300ml (or g) water. Mix it until no more flour can be seen and a consistent mixture has formed. After that the dough must rest and, well, get sour. The exact time might vary depending on the strength of the startet but usually twelve hours should suffice.", andWaitingTime: NSNumber(value: 720), andPreparationTime: NSNumber(value: 10), andSummary: "Mix ingredients for sourdough.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 3), andTitle: "Prepare final dough", andPreparation: "Now the actual bread dough is prepared. But first, take ca. 60g of the sourdough and put it in a jar, then place it back in the fridge. Mix the remaining sourdough with 300g wheat flour, whole grain and 300g spelt flour, whole grain. Additionally, add 300g of water and 12g of salt. The hazelnuts will be added later. Now everything has to be mixed well again. Afterwards the dough may rest again.", andWaitingTime: NSNumber(value: 30), andPreparationTime: NSNumber(value: 15), andSummary: "Take away new starter and mix remaining ingredients with the sourdough.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 4), andTitle: "Form a loaf", andPreparation: "Now the bread might take form. Take the dough out of the bowl onto a well floured work surface. Sprad some flour and 50g hazelnuts on the dough. Knead the dough by hand and form a loaf. The loaf may be oblong or round, whichever way you like best. If at hand, you can also put the loaf into a bread form. Otherwise put it directly onto a cold baking tray. Now follows the last resting phase before baking, in which the loaf will rise. Here, also, the actual time might vary but two hours is a good estimate.", andWaitingTime: NSNumber(value: 120), andPreparationTime: NSNumber(value: 10), andSummary: "Knead the dough and form a loaf.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 5), andTitle: "Pre-heat oven", andPreparation: "Bread is baked at a very high initial temperature followed by a period of falling temperature. This guarantees a nice crunchy crust and a soft crumb. Therefore, pre-heat the oven to 250 Celsius.", andWaitingTime: NSNumber(value: 9), andPreparationTime: NSNumber(value: 1), andSummary: "Pre-heat oven to 250 Celsius.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 6), andTitle: "Baking - Part 1", andPreparation: "Now the bread can be baked. Before putting it into the oven, make several deep cuts into the loaf. This will on the hand result in a nice looking bread and on the other hand ensure that the crust will not break in unwanted places. Put the baking tray in the oven and set the counter to ten minutes. Also, add some water to the base of the oven. The water will vaporize (Caution, dangerous!) immediately, resulting in a small cloud in the oven. This will ensure a particularly crunchy crust.", andWaitingTime: NSNumber(value: 10), andPreparationTime: NSNumber(value: 1), andSummary: "Put bread and water into the oven.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 7), andTitle: "Baking - Part 2", andPreparation: "Set the temperature to 220 Celsius and wait for another 15 minutes.", andWaitingTime: NSNumber(value: 15), andPreparationTime: NSNumber(value: 1), andSummary: "Reduce temperature to 220 Celsius and wait for 15 min.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 8), andTitle: "Baking - Part 3", andPreparation: "Now the temperature drops to 190 Celsius and the counter is set to 20 minutes.", andWaitingTime: NSNumber(value: 20), andPreparationTime: NSNumber(value: 1), andSummary: "Reduce temperature to 190 Celsius and wait for 20 min.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 9), andTitle: "Baking - Part 4", andPreparation: "Now the last baking phase starts. 15 minutes at 160 Celsius.", andWaitingTime: NSNumber(value: 15), andPreparationTime: NSNumber(value: 1), andSummary: "Reduce temperatur to 160 Celsius and wait for 15 min.", forRecipe: sauerteigBrot, intoContext: context)
            let _ = RecipePreparation.insertNewRecipePreparation(withNumber: NSNumber(value: 10), andTitle: "Cool down the bread", andPreparation: "The bread is done! Now it has to cool off for several hours. Put the bread on a grille, uncovered. The grille is important because otherwise moisture would gather under the bread and make it flabby. When the bread has cooled off (for this, wait at least three hours) it can be sliced and served with spreads or cold-cuts to your liking (or you enjoy it pure). For conservation you can put the slices in a freezer, otherwise it will dry it rather quickly, since there are no additives in the bread.", andWaitingTime: NSNumber(value: 0), andPreparationTime: NSNumber(value: 1), andSummary: "Take bread out and put it on a grille.", forRecipe: sauerteigBrot, intoContext: context)
        }
        
        DBHelper.instance.saveContext()
    }

    func loadExampleDataForVersion3_2_0 () {
        if GeneralHelper.getPreferredLanguage() == "de" {
            let _ = SuppliesItemCategory.insert(name: "Gewürz")
            let _ = SuppliesItemCategory.insert(name: "Molkerei-Produkt")
            let _ = SuppliesItemCategory.insert(name: "Fleisch")
            let _ = SuppliesItemCategory.insert(name: "Obst")
            let _ = SuppliesItemCategory.insert(name: "Gemüse")
            let _ = SuppliesItemCategory.insert(name: "Brotaufstrich")
            let _ = SuppliesItemCategory.insert(name: "Zu Hause")
            let _ = SuppliesItemCategory.insert(name: "Arbeit")
            let _ = SuppliesItemCategory.insert(name: "Ferienwohnung")
        } else {
            let _ = SuppliesItemCategory.insert(name: "Spice")
            let _ = SuppliesItemCategory.insert(name: "Dairy Product")
            let _ = SuppliesItemCategory.insert(name: "Meat")
            let _ = SuppliesItemCategory.insert(name: "Fruit")
            let _ = SuppliesItemCategory.insert(name: "Vegetable")
            let _ = SuppliesItemCategory.insert(name: "Spread")
            let _ = SuppliesItemCategory.insert(name: "At Home")
            let _ = SuppliesItemCategory.insert(name: "Work")
            let _ = SuppliesItemCategory.insert(name: "Holiday Home")
        }
        DBHelper.instance.saveContext()
    }

    // *********************************************************
    //
    // MARK: - Migrate data to new versions
    //
    // *********************************************************

    func migrateDataToVersion1_1_0() {
        let dbHelper = DBHelper.instance
        let context = dbHelper.getManagedObjectContext()

        let allLists = ShoppingList.shoppingLists()
        let _ = ShoppingList.insertNewShoppingList(withName: AggregatedListHelper.instance.nameOfAggregatedList(), andType:"Static", includeInAggregateList:false, intoContext:context)

        for list in allLists {
            list.setValue("Dynamic", forKey:ShoppingListEntity.COL_TYPE)
            list.setValue(NSNumber(value: true as Bool), forKey:ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE)

            guard let listItems = list.itemsForList else { return }
            for listItem in listItems {
                guard let listItem = listItem as? ShoppingListItem else { continue }
                let doneStatus = listItem.done!.boolValue
                if !doneStatus {
                    AggregatedListHelper.instance.addItem(listItem)
                }
            }
        }

        dbHelper.saveContext()
    }

    func migrateDataToVersion3_0_0() {
        let dbHelper = DBHelper.instance
        let allRecipes = Recipe.recipes()

        for recipe in allRecipes {
            recipe.id = NSNumber(value: Recipe.nextID)

            guard let ingredients = recipe.ingredientsForRecipe else { continue }
            for ingredient in ingredients {
                guard let ingredient = ingredient as? RecipeIngredient else { continue }
                ingredient.includeInList = true
                ingredient.includeInSearch = true
            }
        }

        dbHelper.saveContext()
    }

    func renameAggregatedList() {
        let currentPreferredLanguage = GeneralHelper.getPreferredLanguage()
        let key = "preferredLanguage"
        let oldPreferredLanguage = UserDefaults.standard.string(forKey: key)
        if oldPreferredLanguage != nil && currentPreferredLanguage != oldPreferredLanguage {
            UserDefaults.standard.setValue(currentPreferredLanguage, forKey:key)
            UserDefaults.standard.synchronize()

            let nameOfAggregatedList = AggregatedListHelper.instance.nameOfAggregatedListForLanguage(currentPreferredLanguage)
            let aggregatedList = AggregatedListHelper.instance.aggregatedListForLanguage(oldPreferredLanguage!)
            aggregatedList.setValue(nameOfAggregatedList, forKey:ShoppingListEntity.COL_NAME)
            DBHelper.instance.saveContext()
        } else if oldPreferredLanguage == nil {
            UserDefaults.standard.setValue(currentPreferredLanguage, forKey:key)
            UserDefaults.standard.synchronize()
        }
    }
}

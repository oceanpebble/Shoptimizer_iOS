// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

extension SuppliesItemCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SuppliesItemCategory> {
        return NSFetchRequest<SuppliesItemCategory>(entityName: "SuppliesItemCategory")
    }

    @NSManaged public var name: String?
    @NSManaged public var suppliesItemsForCategory: NSSet?
}

// MARK: Generated accessors for suppliesItemsForCategory
extension SuppliesItemCategory {

    @objc(addSuppliesItemsForCategoryObject:)
    @NSManaged public func addToSuppliesItemsForCategory(_ value: SuppliesItem)

    @objc(removeSuppliesItemsForCategoryObject:)
    @NSManaged public func removeFromSuppliesItemsForCategory(_ value: SuppliesItem)

    @objc(addSuppliesItemsForCategory:)
    @NSManaged public func addToSuppliesItemsForCategory(_ values: NSSet)

    @objc(removeSuppliesItemsForCategory:)
    @NSManaged public func removeFromSuppliesItemsForCategory(_ values: NSSet)
}

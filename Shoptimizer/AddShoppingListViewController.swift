// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class AddShoppingListViewController: UIViewController, AddEntityViewDelegate {

    // MARK: - IBOutlets

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var shoppingListName: UITextField!
    @IBOutlet weak var includeInAggregateSwitch: UISwitch!
    @IBOutlet weak var scrollView: UIScrollView!

    // MARK: - Properties

    var fetchedList: ShoppingList?
    var editingMode: EditingMode
    
    private var viewModel: AddShoppingListViewModel!

    // MARK: - View Controller Life Cycle

    required init?(coder aDecoder: NSCoder) {
        self.editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        
        viewModel = AddShoppingListViewModel();
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews(), otherwise the scroll view will be positioned too low on the screen
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterFromKeyboardNotifications()
    }

    // MARK: - Segue Handling

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if sender as? UIBarButtonItem != self.saveButton {
            return true
        }

        let enteredName = viewModel.trimName(shoppingListName.text)
        if enteredName.isEmpty {
            let enterNameString = NSLocalizedString("Please enter a name.", comment:"")
            GeneralHelper.presentInformationDialogWithText(enterNameString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false
        }

        if (editingMode == .create
            || enteredName != viewModel.name(of: fetchedList))
            && viewModel.existsShoppingList(withName: enteredName) {
            let listAlreadyExistsString = NSLocalizedString("A list with this name already exists.", comment:"");
            GeneralHelper.presentInformationDialogWithText(listAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false
        }

        if editingMode == .edit {
            viewModel.setName(enteredName, for: fetchedList)
            viewModel.setListIncludedInAggregatedList(fetchedList, isIncluded: includeInAggregateSwitch.isOn)
            DBHelper.instance.saveContext()
        } else if editingMode == .create {
            let _ = viewModel.insertNewShoppingList(withName: enteredName, andType: ShoppingListType.dynamic, includeInAggregatedList: includeInAggregateSwitch.isOn)
            DBHelper.instance.saveContext()
        }

        return true
    }

    // MARK: - AddEntityViewDelegate

    func setTextFieldDelegates() {
        shoppingListName.delegate = self
    }

    func setTextFieldContents() {
        guard let list = fetchedList else { return }
        shoppingListName.text = list.name
        includeInAggregateSwitch.isOn = list.include_in_aggregate!.boolValue
    }

    func setTextViewDelegates() {
        // no text views in this view
    }

    func setTextViewContents() {
        // no text views in this view
    }

    func setTitle() {
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit List", comment: "")
        } else {
            navigationItem.title = NSLocalizedString("Add List", comment: "")
        }
    }

    // MARK: - UITextFieldDelegate (via AddEntityViewDelegate)

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    // MARK: - Keyboard Handling

    /* Unfortunetly, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        guard let notificationUserInfo = (aNotification as NSNotification).userInfo else { return }
        guard let keyboardBeginRectangle = notificationUserInfo[UIResponder.keyboardFrameBeginUserInfoKey] else { return }

        var keyboardHeight = (keyboardBeginRectangle as AnyObject).cgRectValue.size.height
        if keyboardHeight == 0.0 {
            guard let keyboardEndRectangle = notificationUserInfo[UIResponder.keyboardFrameEndUserInfoKey] else { return }
            keyboardHeight = (keyboardEndRectangle as AnyObject).cgRectValue.size.height
        }

        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var visibleScrollViewFrame = scrollView.frame
        visibleScrollViewFrame.size.height -= keyboardHeight

        if let activeTextField = UITextFieldHelper.instance.activeField,
            !visibleScrollViewFrame.contains(activeTextField.frame.origin) {
            scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
            return
        }

        if let activeTextView = UITextViewHelper.instance.activeView {
            let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
            let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
            if !visibleScrollViewFrame.contains(transformedLowerLeftOfActiveTextView) {
                let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height)
                scrollView.scrollRectToVisible(rect, animated: true)
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

enum TextFieldType {
    case timeDuration
    case none
}

struct AssociatedKeys {
    static var textFieldType: UInt8 = 0
}

extension UITextField {
    var textFieldType: TextFieldType {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.textFieldType) as? TextFieldType else {
                return .none
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.textFieldType, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

class UITextFieldHelper: NSObject, UITextFieldDelegate {
    static var instance = UITextFieldHelper()
    var activeField: UITextField?
    
    fileprivate override init() {
    }
    
    func textFieldShouldReturn(_ theTextField: UITextField) -> Bool {
        theTextField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.textFieldType {
        case .timeDuration: return timeDurationTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
        default: return true
        }
    }
    
    func timeDurationTextField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string != "" {
            var newString = textField.text! + string
            
            if newString.first == "0" {
                newString.removeFirst()
            }
            
            if let indexOfColon = newString.firstIndex(of: ":") {
                newString.remove(at: indexOfColon)
                newString.insert(":", at: newString.index(newString.endIndex, offsetBy: -2))
            }
            
            textField.text = newString
        } else { // aka delete
            var newString = textField.text!
            
            if newString == "0:00" {
                return false
            }
            
            newString.removeLast()
            
            if let indexOfColon = newString.firstIndex(of: ":") {
                newString.remove(at: indexOfColon)
                newString.insert(":", at: newString.index(newString.endIndex, offsetBy: -2))
            }
            
            if newString.first == ":" {
                newString.insert("0", at: newString.startIndex)
            }
            
            textField.text = newString
        }
        
        return false
    }
}

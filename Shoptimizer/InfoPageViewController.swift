// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class InfoPageViewController: UIViewController, UIPageViewControllerDataSource {

    private var pageViewController: UIPageViewController!
    private var infoTexts = [""]
    private var viewModel: InfoPageViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pageControl = UIPageControl.appearance()
        pageControl.currentPageIndicatorTintColor = UIColor.label
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.backgroundColor = UIColor.secondarySystemGroupedBackground
        
        viewModel = InfoPageViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        infoTexts = viewModel.loadPageContents(useDarkStyle: traitCollection.userInterfaceStyle == .dark)
        self.pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation:.horizontal, options:nil)

        parent?.view.backgroundColor = UIColor.systemBackground
        
        self.pageViewController.dataSource = self;

        if let startViewController = self.viewControllerAtIndex(0) {
            let viewControllers = [startViewController]
            self.pageViewController.setViewControllers(viewControllers, direction:.forward, animated:false, completion:nil)
        }
        self.pageViewController.didMove(toParent: self)

        self.addChild(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)

        self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 50)
    }

    func viewControllerAtIndex(_ index: Int) -> InfoPageContentViewController? {
        if(self.infoTexts.count == 0 || index >= self.infoTexts.count) {
            return nil;
        }

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "InfoPageContentViewController") as! InfoPageContentViewController
        controller.labelText = self.infoTexts[index];

        return controller;
    }

    func indexOfViewController(_ viewController: InfoPageContentViewController) -> Int {
        return self.infoTexts.firstIndex(of: viewController.labelText)!
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! InfoPageContentViewController)

        if(index == 0 || index == NSNotFound) {
            return nil;
        }

        index -= 1;
        return self.viewControllerAtIndex(index)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! InfoPageContentViewController)

        if(index == NSNotFound) {
            return nil;
        }

        index += 1;

        if(index >= self.infoTexts.count) {
            return nil;
        }
        
        return self.viewControllerAtIndex(index)
    }

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.infoTexts.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0;
    }
}

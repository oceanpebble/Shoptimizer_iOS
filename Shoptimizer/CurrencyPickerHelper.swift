// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class CurrencyPickerHelper {

    static func getCurrencyPickerData() -> [String] {
        return ["EUR", "USD", "GBP", "ALL", "BGN", "CZK", "CHF", "CZK", "DKK", "HUF", "ISK", "NOK", "PLN", "RON", "SEK", "ZAR"]
    }

    static func getIndexOfCurrency(_ currency: String?) -> Int {
        guard let currency = currency else { return 0 }
        let currencyPickerData = self.getCurrencyPickerData()
        for i in 0 ..< currencyPickerData.count {
            let currentCurrency = currencyPickerData[i]
            if currentCurrency == currency {
                return i
            }
        }

        return 0
    }
}

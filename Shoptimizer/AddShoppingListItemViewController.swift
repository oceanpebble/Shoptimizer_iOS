// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class AddShoppingListItemViewController: UIViewController, AddEntityViewDelegate {
    func setTitle() {

    }

    // MARK: - Properties

    var fetchedListItem: ShoppingListItem?
    var listForItem: ShoppingList?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var itemNameField: UITextField!
    @IBOutlet weak var shopField: UITextField!
    @IBOutlet weak var unitPicker: UIPickerView!
    @IBOutlet weak var quantityField: UITextField!
    @IBOutlet weak var brandNameField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var notesTextView: UITextView!
    var editingMode: EditingMode
    private var viewModel: AddShoppingListItemViewModel!

    // MARK: - UIView

    required init?(coder aDecoder: NSCoder) {
        editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataManager = AddShoppingListItemDataManager(shoppingListRepository: ShoppingListRepository(), shoppingListItemRepository: ShoppingListItemRepository())
        viewModel = AddShoppingListItemViewModel(aggregatedListHelper: AggregatedListHelper.instance, dataManager: dataManager)
        
        setTextFieldDelegates()
        setTextFieldContents()
        setTextViewContents()
        setTextViewDelegates()
        setUnitPickerValue()
        setCurrencyPickerValue()
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit Item", comment: "")
        }
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        addDoneButtonToKeyboard(forTextFields: [priceField, quantityField, itemNameField, shopField, brandNameField], forTextViews: [notesTextView], targeting: self, using: #selector(doneButtonAction))

        notesTextView.layer.borderWidth = 0.5
        notesTextView.layer.borderColor = UIColor.lightGray.cgColor
        notesTextView.layer.cornerRadius = 5
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews(), otherwise the scroll view will be positioned too low on the screen
    }

    // MARK: - Segue Handling

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if sender as? UIBarButtonItem != saveButton {
            return true
        }

        guard let newItemName = itemNameField.text?.trimmingCharacters(in: .whitespaces), newItemName.isNotEmpty else {
            let enterNameString = NSLocalizedString("Please enter a name.", comment:"");
            GeneralHelper.presentInformationDialogWithText(enterNameString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false
        }

        let newShopName = self.shopField.text!.capitalized
        if
            (self.fetchedListItem != nil && (newItemName != viewModel.name(of: self.fetchedListItem!) || !StringUtil.similarStrings(s1: newShopName, s2: viewModel.shopName(of: self.fetchedListItem!))) && viewModel.isListItemAvailable(withName: newItemName, andShop: newShopName, inList: self.listForItem!))
        ||
                (self.fetchedListItem == nil && viewModel.isListItemAvailable(withName: newItemName, andShop: newShopName, inList: self.listForItem!))
        {
            let listItemAlreadyExistsString = NSLocalizedString("An item with this name already exists for this shop.", comment:"");
            GeneralHelper.presentInformationDialogWithText(listItemAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false
        }

        guard let quantity = viewModel.ensureValidDouble(fromString: self.quantityField.text, allowZero: true) else {
            let portionsInvalidString = NSLocalizedString("Please enter a value >= 0.0 for the quantity.", comment: "")
            GeneralHelper.presentInformationDialogWithText(portionsInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        let unit = UnitPickerHelper.getUnitPickerData()[self.unitPicker.selectedRow(inComponent: 0)]
        let listForItem = viewModel.list(for: self.fetchedListItem)

        guard let price = viewModel.ensureValidPrice(fromString: self.priceField.text, allowZero: true) else {
            let portionsInvalidString = NSLocalizedString("Please enter a value >= 0.0 for the price.", comment: "")
            GeneralHelper.presentInformationDialogWithText(portionsInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        let currency = price.doubleValue > 0.0
            ? CurrencyPickerHelper.getCurrencyPickerData()[self.currencyPicker.selectedRow(inComponent: 0)]
            : nil
        let brandName = self.brandNameField.text!
        let notes = notesTextView.text

        if self.editingMode == .edit,
            let fetchedListItem = self.fetchedListItem {
            if viewModel.isListIncludedInAggregatedList(listForItem) && !viewModel.done(for: self.fetchedListItem) {
                viewModel.subtractItemFromAggregatedList(fetchedListItem)
                viewModel.addItemToAggregatedList(newItemName, forShop: newShopName, amountQuantity: quantity, andAmountUnit: unit, andBrand: brandName, forPrice: price, atCurrency: currency)
            }
            viewModel.updateShoppingListItem(fetchedListItem, itemName: newItemName, shopName: newShopName.capitalized, amountQuantity: quantity, amountUnit: unit, brandName: brandName, price: price, currency: currency, notes: notes)
        } else if (self.editingMode == .create) {
            let newShoppingListItem = viewModel.insertNewShoppingListItem(withName: newItemName, quantity: quantity, unit: unit, doneStatus: false, brand: brandName, forPrice: price, atCurrency: currency, inShop: newShopName, notes: notes, forList: self.listForItem!)

            if viewModel.isListIncludedInAggregatedList(self.listForItem) {
                viewModel.addItemToAggregatedList(newShoppingListItem)
            }

            DBHelper.instance.saveContext()
        }
        
        return true
    }

    // MARK: - View Controller Life Cycle

    func setUnitPickerValue() {
        self.unitPicker.dataSource = self;
        self.unitPicker.delegate = self;

        if self.fetchedListItem != nil {
            let unit = UnitPickerHelper.translate(unit: viewModel.amountUnit(of: self.fetchedListItem!))
            self.unitPicker.selectRow(UnitPickerHelper.getIndexOfUnit(unit), inComponent: 0, animated: false)
        }
    }

    func setCurrencyPickerValue() {
        self.currencyPicker.dataSource = self
        self.currencyPicker.delegate = self

        if self.fetchedListItem != nil {
            self.currencyPicker.selectRow(CurrencyPickerHelper.getIndexOfCurrency(viewModel.currency(of: self.fetchedListItem!)), inComponent: 0, animated: false)
        }
    }

    func setTextFieldDelegates() {
        self.itemNameField.delegate = UITextFieldHelper.instance
        self.shopField.delegate = UITextFieldHelper.instance
        self.quantityField.delegate = UITextFieldHelper.instance
        self.priceField.delegate = UITextFieldHelper.instance
        self.brandNameField.delegate = UITextFieldHelper.instance
    }

    func setTextFieldContents() {
        guard let item =  self.fetchedListItem else { return }
        self.itemNameField.text = viewModel.name(of: item)
        self.shopField.text = viewModel.shopName(of: item)
        self.quantityField.text = viewModel.quantityFieldText(for: item)
        self.brandNameField.text = viewModel.brandName(of: item)
        self.priceField.text = viewModel.priceFieldText(for: item)
    }

    func setTextViewDelegates() {
        notesTextView.delegate = UITextViewHelper.instance
    }

    func setTextViewContents() {
        notesTextView.text = fetchedListItem?.notes
    }

    /* Unfortunately, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let info = (aNotification as NSNotification).userInfo,
           let value = info[UIResponder.keyboardFrameBeginUserInfoKey] {
            var kbHeight = (value as AnyObject).cgRectValue.size.height
            if kbHeight == 0.0 {
                kbHeight = (info[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
            }
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbHeight, right: 0.0)
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            var aRect = scrollView.frame;
            aRect.size.height -= kbHeight;

            if let activeTextField = UITextFieldHelper.instance.activeField,
                !aRect.contains(activeTextField.frame.origin) {
                scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                return
            }

            if let activeTextView = UITextViewHelper.instance.activeView {
                let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
                let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
                if !aRect.contains(transformedLowerLeftOfActiveTextView) {
                    let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                    let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height - 50) // 50 = height of done button toolbar
                    scrollView.scrollRectToVisible(rect, animated: true)
                }
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func doneButtonAction() {
        UITextFieldHelper.instance.activeField?.resignFirstResponder()
        UITextViewHelper.instance.activeView?.resignFirstResponder()
        keyboardWillBeHidden(Notification(name: Notification.Name(rawValue: "foo"))) // name is irrelevant since notification is not used
    }
}

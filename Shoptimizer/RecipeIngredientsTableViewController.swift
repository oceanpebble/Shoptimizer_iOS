// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class RecipeIngredientsTableViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    typealias EntityType = RecipeIngredient
    var reusableCellIdentifier: String { return "RecipeIngredientPrototypeCell" }
    var recipe : Recipe!
    var fetchedResultsController: NSFetchedResultsController<EntityType>!

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchedResultsController = RecipeIngredient.ingredientsForRecipe(self.recipe, andDelegate: self)
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    @IBAction fileprivate func unwindToList(_ segue: UIStoryboardSegue) {
        // guard let source = segue.source as? AddRecipeIngredientViewController else { return }

        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! AddRecipeIngredientViewController
        targetController.recipeForIngredient = self.recipe

        if let specificSender = sender as? UITableViewCell {
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedIngredient = self.fetchedResultsController.object(at: indexPath!)
            targetController.fetchedIngredient = selectedIngredient
            targetController.editingMode = .edit
        } else {
            targetController.editingMode = .create
        }
    }

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedIngredient = self.fetchedResultsController!.object(at: indexPath)
            DBHelper.instance.getManagedObjectContext().delete(swipedIngredient)
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }

        let ingredient = self.fetchedResultsController.object(at: indexPath)

        cell.textLabel?.text = ingredient.name

        var quantityText = ""
        if let quantity = ingredient.quantity,
            let convertedQuantity = GeneralHelper.decimalFormatter().string(from: quantity) {
            let unit = UnitPickerHelper.translate(unit: ingredient.unit!)
            quantityText = "\(convertedQuantity) \(unit) / "
        }

        let includeInListText = NSLocalizedString("include in list", comment: "")
        var includeInListAttributedText = NSAttributedString()
        if let includeInList = ingredient.includeInList, includeInList == true {
            includeInListAttributedText = NSAttributedString(string: includeInListText, attributes:[NSAttributedString.Key.foregroundColor:UIColor.green])
        } else {
            includeInListAttributedText = NSAttributedString(string: includeInListText, attributes:[NSAttributedString.Key.foregroundColor:UIColor.red])
        }

        let includeInSearchText = NSLocalizedString("include in search", comment: "")
        var includeInSearchAttributedText = NSAttributedString()
        if let includeInSearch = ingredient.includeInSearch, includeInSearch == true {
            includeInSearchAttributedText = NSAttributedString(string: includeInSearchText, attributes:[NSAttributedString.Key.foregroundColor:UIColor.green])
        } else {
            includeInSearchAttributedText = NSAttributedString(string: includeInSearchText, attributes:[NSAttributedString.Key.foregroundColor:UIColor.red])
        }

        let combinedAttributedText = NSMutableAttributedString(string: quantityText)
        combinedAttributedText.append(includeInListAttributedText)
        combinedAttributedText.append(NSAttributedString(string: " / "))
        combinedAttributedText.append(includeInSearchAttributedText)

        cell.detailTextLabel?.attributedText = combinedAttributedText

        cell.accessoryType = .disclosureIndicator
    }
}

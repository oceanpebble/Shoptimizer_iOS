// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension ShoppingListItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingListItem> {
        return NSFetchRequest<ShoppingListItem>(entityName: "ShoppingListItem");
    }

    @NSManaged public var brandName: String?
    @NSManaged public var done: NSNumber?
    @NSManaged public var itemAmountQuantity: NSNumber?
    @NSManaged public var itemAmountUnit: String?
    @NSManaged public var itemCurrency: String?
    @NSManaged public var itemName: String?
    @NSManaged public var itemPrice: NSNumber?
    @NSManaged public var shopName: String?
    @NSManaged public var notes: String?
    @NSManaged public var listForItem: ShoppingList?

}

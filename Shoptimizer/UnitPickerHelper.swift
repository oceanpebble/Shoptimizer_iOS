// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class UnitPickerHelper {

    static func getUnitPickerData() -> [String] {
        let selectedLanguage = GeneralHelper.getPreferredLanguage()
        if selectedLanguage == "de" {
            return ["l", "ml", "kg", "g", "m", "cm", "mm", "Bund", "Dose", "Flasche", "Glas", "Laib", "Packung", "Stück"]
        } else {
            return ["l", "ml", "kg", "g", "m", "cm", "mm", "bottle", "bunch", "can", "jar", "loaf", "package", "piece"]
        }
    }

    static func getIndexOfUnit(_ unit: String?) -> Int {
        guard let unit = unit else {
            return 0
        }

        let unitPickerData = self.getUnitPickerData()
        for i in 0 ..< unitPickerData.count {
            let currentUnit = unitPickerData[i]
            if currentUnit == unit {
                return i
            }
        }

        return 0
    }

    static func convertValue(_ sourceValue: NSNumber?, fromBaseUnit baseUnit: String?, toTargetUnit targetUnit: String?) -> NSNumber {
        guard let sourceValue = sourceValue else { return NSNumber(value:0.0) }

        guard let baseUnit = baseUnit, let targetUnit = targetUnit else { return sourceValue }

        if baseUnit == "kg" && targetUnit == "g" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble * 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "g" && targetUnit == "kg"{
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble / 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "l" && targetUnit == "ml"{
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble * 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "ml" && targetUnit == "l"{
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble / 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "mm" && targetUnit == "cm" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble / 10.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "mm" && targetUnit == "m" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble / 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "cm" && targetUnit == "mm" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble * 10.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "cm" && targetUnit == "m" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble / 100.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "m" && targetUnit == "mm" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble * 1000.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "m" && targetUnit == "cm" {
            let sourceDouble = sourceValue.doubleValue
            let targetDouble = sourceDouble * 100.0
            return NSNumber(value: targetDouble as Double)
        } else if baseUnit == "Bund" && targetUnit == "bunch" {
            return sourceValue
        } else if baseUnit == "bunch" && targetUnit == "Bund" {
            return sourceValue
        } else if baseUnit == "Dose" && targetUnit == "can" {
            return sourceValue
        } else if baseUnit == "can" && targetUnit == "Dose" {
            return sourceValue
        } else if baseUnit == "Flasche" && targetUnit == "bottle" {
            return sourceValue
        } else if baseUnit == "bottle" && targetUnit == "Flasche" {
            return sourceValue
        } else if baseUnit == "Glas" && targetUnit == "jar" {
            return sourceValue
        } else if baseUnit == "jar" && targetUnit == "Glas" {
            return sourceValue
        } else if baseUnit == "Laib" && targetUnit == "loaf" {
            return sourceValue
        } else if baseUnit == "loaf" && targetUnit == "Laib" {
            return sourceValue
        } else if baseUnit == "Packung" && targetUnit == "package" {
            return sourceValue
        } else if baseUnit == "package" && targetUnit == "Packung" {
            return sourceValue
        } else if baseUnit == "Stück" && targetUnit == "piece" {
            return sourceValue
        } else if baseUnit == "piece" && targetUnit == "Stück" {
            return sourceValue
        }

        return sourceValue
    }

    static func baseUnit(_ baseUnit: String?, isConvertibleToTargetUnit targetUnit: String?) -> Bool {
        guard let baseUnit = baseUnit, let targetUnit = targetUnit else { return false }

        if (baseUnit == "kg" && targetUnit == "g")
            || (baseUnit == "g" && targetUnit == "kg")
            || (baseUnit == "ml" && targetUnit == "l")
            || (baseUnit == "l" && targetUnit == "ml")
            || (baseUnit == "mm" && targetUnit == "cm")
            || (baseUnit == "mm" && targetUnit == "m")
            || (baseUnit == "cm" && targetUnit == "mm")
            || (baseUnit == "cm" && targetUnit == "m")
            || (baseUnit == "m" && targetUnit == "mm")
            || (baseUnit == "m" && targetUnit == "cm")
            || (baseUnit == "Bund" && targetUnit == "bunch")
            || (baseUnit == "bunch" && targetUnit == "Bund")
            || (baseUnit == "Dose" && targetUnit == "can")
            || (baseUnit == "can" && targetUnit == "Dose")
            || (baseUnit == "Flasche" && targetUnit == "bottle")
            || (baseUnit == "bottle" && targetUnit == "Flasche")
            || (baseUnit == "Glas" && targetUnit == "jar")
            || (baseUnit == "jar" && targetUnit == "Glas")
            || (baseUnit == "Laib" && targetUnit == "loaf")
            || (baseUnit == "loaf" && targetUnit == "Laib")
            || (baseUnit == "Packung" && targetUnit == "package")
            || (baseUnit == "package" && targetUnit == "Packung")
            || (baseUnit == "Stück" && targetUnit == "piece")
            || (baseUnit == "piece" && targetUnit == "Stück") {
            return true
        }

        return false
    }

    static func translate(unit u: String) -> String {
        let language = GeneralHelper.getPreferredLanguage()
        if language == "de" {
            switch u {
            case "bunch": return "Bund"
            case "can": return "Dose"
            case "bottle": return "Flasche"
            case "jar": return "Glas"
            case "loaf": return "Laib"
            case "package": return "Packung"
            case "piece": return "Stück"
            default: return u
            }
        } else {
            switch u {
            case "Bund": return "bunch"
            case "Dose": return "can"
            case "Flasche": return "bottle"
            case "Glas": return "jar"
            case "Laib": return "loaf"
            case "Packung": return "package"
            case "Stück": return "piece"
            default: return u
            }
        }
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class RecipePreparationTableViewController: UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate {

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    typealias EntityType = RecipePreparation
    var reusableCellIdentifier: String { return "RecipePreparationPrototypeCell" }
    var recipe : Recipe!
    var fetchedResultsController: NSFetchedResultsController<EntityType>!

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchedResultsController = RecipePreparation.preparationsForRecipe(self.recipe, andDelegate: self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    @IBAction fileprivate func unwindToList(_ segue: UIStoryboardSegue) {
        // guard let source = segue.source as? AddRecipePreparationViewController else { return }

        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! AddRecipePreparationViewController
        targetController.recipeForPreparation = self.recipe

        if let specificSender = sender as? UITableViewCell {
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedPreparation = self.fetchedResultsController.object(at: indexPath!)
            targetController.fetchedPreparation = selectedPreparation
            targetController.editingMode = .edit
        } else {
            targetController.editingMode = .create
        }
    }

    // *********************************************************
    //
    // MARK: - UITableViewController
    //
    // *********************************************************

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedPreparation = self.fetchedResultsController!.object(at: indexPath)
            DBHelper.instance.getManagedObjectContext().delete(swipedPreparation)
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    // *********************************************************
    //
    // MARK: - NSFetchedResultsController
    //
    // *********************************************************

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell as? RecipePreparationTableViewCell else { return }

        let preparation = self.fetchedResultsController.object(at: indexPath)

        let cellTitle = "\(preparation.number!.stringValue). \(preparation.title!) (\(GeneralHelper.convertTimeDurationInt(preparation.timePrepare?.intValue)) / \(GeneralHelper.convertTimeDurationInt(preparation.timeWait?.intValue)))"

        let boldTitle = NSAttributedString(string: cellTitle, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18.0)])
        cell.titleLabel?.attributedText = boldTitle
        cell.preparationLabel?.text = preparation.preparation
        
        cell.accessoryType = .disclosureIndicator
    }
}

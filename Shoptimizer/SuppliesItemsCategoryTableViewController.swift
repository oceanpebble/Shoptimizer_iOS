// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class SuppliesItemCategoriesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, EntityTableViewDelegate, UISearchBarDelegate {

    // MARK: - Properties

    typealias EntityType = SuppliesItemCategory
    var reusableCellIdentifier: String {
        return "SuppliesItemCategoryPrototypeCell"
    }
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    var suppliesItem: SuppliesItem?
    var categoriesForSuppliesItem: [SuppliesItemCategory]?
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: - UIView

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController = SuppliesItemCategory.categoriesWithDelegate(self)
        categoriesForSuppliesItem = SuppliesItemCategory.categories(for: suppliesItem)
        setupSwipeRecognizers()
        setupSearchBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    func setupSearchBar() {
        searchBar.delegate = self
    }

    func resetSearchBar() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }

    // MARK: - Segue Handling

    @IBAction fileprivate func unwindToList(_ segue: UIStoryboardSegue) {
        // nothing to do here at the moment
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! AddSuppliesItemCategoryViewController

        if let specificSender = sender as? UITableViewCell {
            let indexPath = self.tableView.indexPath(for: specificSender)
            let selectedCategory = self.fetchedResultsController.object(at: indexPath!)
            targetController.fetchedCategory = selectedCategory
            targetController.editingMode = .edit
        } else {
            targetController.editingMode = .create
        }
    }

    // MARK: - View Controller Life Cycle

    func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target:self, action:#selector(self.respondToRightSwipeGesture))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }

    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: self.view)
        guard let swipedIndexPath = self.tableView.indexPathForRow(at: location) else {
            return
        }

        guard let swipedCategory = self.fetchedResultsController?.object(at: swipedIndexPath) else { return }

        if suppliesItem == nil {
            if selectedCategories.contains(swipedCategory.name!) {
                let index = selectedCategories.firstIndex(of: swipedCategory.name!)
                selectedCategories.remove(at: index!)
            } else {
                selectedCategories.append(swipedCategory.name!)
            }
            self.tableView.reloadData()
            return
        }

        if let categories = swipedCategory.suppliesItemsForCategory,
            categories.contains(suppliesItem!) {
            swipedCategory.removeFromSuppliesItemsForCategory(suppliesItem!)
        } else {
            swipedCategory.addToSuppliesItemsForCategory(suppliesItem!)
        }

        DBHelper.instance.saveContext()
        self.categoriesForSuppliesItem = SuppliesItemCategory.categories(for: suppliesItem)
        self.tableView.reloadData()
    }

    // MARK: - UITableViewController

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return "";
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let swipedCategory = fetchedResultsController!.object(at: indexPath)
            let context = fetchedResultsController!.managedObjectContext
            context.delete(swipedCategory )
            DBHelper.instance.saveContext()
        } else if editingStyle == .insert {
        } else {
        }
    }

    // MARK: - NSFetchedResultsController

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerWillChangeContent(controller, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        self.etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: self.tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject:Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        self.etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: self.tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.etdControllerDidChangeContent(controller, inTableView: self.tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }

        let category = fetchedResultsController.object(at: indexPath)
        let name = category.value(forKey: SuppliesItemCategory.COL_NAME) as! String

        if let contains = categoriesForSuppliesItem?.contains(category), contains || selectedCategories.contains(name) {
            cell.textLabel?.attributedText = NSAttributedString(string: name, attributes: [NSAttributedString.Key.foregroundColor: UIColor.green])
            cell.accessibilityIdentifier = AccessibilityUtil.identifierForSelectedCategory
        } else {
            cell.textLabel?.attributedText = NSAttributedString(string: name, attributes: [NSAttributedString.Key.foregroundColor: UIColor.label])
            cell.accessibilityIdentifier = nil
        }

        cell.textLabel?.text = name
        cell.accessoryType = .disclosureIndicator
    }

    // MARK: - UISearchBarDelegate

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            cancelSearch(searchBar)
        } else {
            self.navigationItem.title = NSLocalizedString("Categories (filtered)", comment: "")
            let predicate = SuppliesItemCategory.predicate(for: searchText)
            refreshTableView(withPredicate: predicate)
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetSearchBar()
        cancelSearch(searchBar)
    }

    func cancelSearch(_ searchBar: UISearchBar) {
        self.navigationItem.title = NSLocalizedString("Categories", comment: "")
        refreshTableView(withPredicate: nil)
    }

    func refreshTableView(withPredicate predicate: NSPredicate?) {
        self.fetchedResultsController.fetchRequest.predicate = predicate
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: SuppliesItemCategory.CACHE_NAME)

        do {
            try self.fetchedResultsController!.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of supplies item categories")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        self.tableView.reloadData()
    }
}

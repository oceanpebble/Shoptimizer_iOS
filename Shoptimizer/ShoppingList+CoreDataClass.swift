// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct ShoppingListEntity {
    static let CACHE_NAME = "lists"
    static let COL_INCLUDE_IN_AGGREGATE = "include_in_aggregate"
    static let COL_NAME = "name"
    static let COL_TYPE = "type"
    static let ENTITY_NAME = "ShoppingList"
    static let REF_ITEMSFORLIST = "itemsForList"
}

enum ShoppingListType: String {
    case dynamic = "Dynamic"
    case `static` = "Static"
}

public class ShoppingList: NSManagedObject {
    
    // MARK: - Create
    
    static func insert(name: String?, includedInAggregatedList: Bool?) -> ShoppingList {
        let list = ShoppingList(entity: entityDescription()!, insertInto: DBHelper.instance.getManagedObjectContext())
        list.name = name
        list.include_in_aggregate = NSNumber(value: includedInAggregatedList ?? false)
        DBHelper.instance.saveContext()
        return list
    }
    
    private static func entityDescription() -> NSEntityDescription? {
        return entityDescription(forEntityName: ShoppingListEntity.ENTITY_NAME)
    }
    
    // MARK: - Delete

    static func deleteAll() {
        deleteAll(entityName: ShoppingListEntity.ENTITY_NAME)
    }

    /*********************************************************************************************************************
     *
     * Function: shoppingLists
     *
     * Creates an NSFetchedResultsController containing all shopping lists.
     *
     * Parameters: None
     *
     ********************************************************************************************************************/
    static func shoppingLists() -> [ShoppingList] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(ShoppingList.fetchRequest())
        } catch {
            return [ShoppingList]()
        }
    }

    static func shoppingLists(_ delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<ShoppingList> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: ShoppingListEntity.CACHE_NAME)

        let fetchRequest: NSFetchRequest<ShoppingList> = ShoppingList.fetchRequest()
        fetchRequest.fetchBatchSize = 20

        let sortDescriptorListType = NSSortDescriptor(key: ShoppingListEntity.COL_TYPE, ascending: false, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptorListName = NSSortDescriptor(key: ShoppingListEntity.COL_NAME, ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        let sortDescriptors = [sortDescriptorListType, sortDescriptorListName]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath: ShoppingListEntity.COL_TYPE, cacheName: ShoppingListEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while performing fetch of recipes")
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }
        
        return aFetchedResultsController;
    }

    /*********************************************************************************************************************
     *
     * Function: totalItemsCountForList
     *
     * Determines the total number of items (done and undone) on a shopping list.
     *
     ********************************************************************************************************************/
    func totalItemsCount() -> Int {
        return itemsForList?.count ?? 0
    }

    /**
     Creates a new ShoppingList instance, inserts it into the given NSManagedContext, and returns it
     */
    static func insertNewShoppingList(withName listName: String, andType type: String, includeInAggregateList include: Bool, intoContext context: NSManagedObjectContext) -> ShoppingList? {
        if type == "Static" || type == "Dynamic" {
            let listEntity = NSEntityDescription.entity(forEntityName: ShoppingListEntity.ENTITY_NAME, in:context)
            let shoppingList = ShoppingList(entity: listEntity!, insertInto: context)
            shoppingList.name = listName
            shoppingList.type = type
            shoppingList.include_in_aggregate = NSNumber(value: include as Bool)
            return shoppingList;
        } else {
            return nil;
        }
    }

    /*********************************************************************************************************************
     *
     * Function: doneItemCountForList
     *
     * Determines the number of done items on a shopping list.
     *
     ********************************************************************************************************************/
    func doneItemsCount() -> Int {
        return doneItems().count
    }

    func doneItems() -> [ShoppingListItem] {
        return itemsForList?
            .filter({($0 as? ShoppingListItem)!.done!.boolValue == true}) as! [ShoppingListItem]
    }

    /*********************************************************************************************************************
     *
     * Function: undoneItemCountForList
     *
     * Determines the number of undone items on a shopping list.
     *
     * Parameters:
     * - shoppingList (NSManagedObject): The shopping list for which to determine the number of undone items
     *
     ********************************************************************************************************************/
    func undoneItemsCount() -> Int {
        return undoneItems().count
    }

    func undoneItems() -> [ShoppingListItem] {
        return itemsForList?
            .filter({($0 as? ShoppingListItem)!.done!.boolValue == false}) as! [ShoppingListItem]
    }

    /**
     Tests if a shopping list with the given name already exists. This must be tested before an entity with the given name is inserted into the NSManagedObjectContext.

     In case of an error true is returned to prevent the creation of duplicates.
     */
    static func isShoppingListAvailable(withName name: String) -> Bool {
        return shoppingList(for: name) != nil
    }

    static func shoppingList(for name: String) -> ShoppingList? {
        let fetchRequest: NSFetchRequest<ShoppingList> = ShoppingList.fetchRequest();

        let entity = NSEntityDescription.entity(forEntityName: ShoppingListEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity

        let predicate = NSPredicate(format: "%K = %@", ShoppingListEntity.COL_NAME, name)
        fetchRequest.predicate = predicate

        do {
            let fetchResult = try DBHelper.instance.getManagedObjectContext().fetch(fetchRequest)
            return fetchResult.first
        } catch {
            return nil
        }
    }

    /**
     Computes the sum of prices of all items that have the given currency.
     */
    func getTotalPriceOfAllItems(forCurrency currency: String) -> Double {
        let totalPrice = itemsForList?
            .filter({($0 as? ShoppingListItem)!.itemCurrency == currency})
            .map({($0 as? ShoppingListItem)!.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})

        return totalPrice ?? 0.0
    }

    /**
     Computes the sum of prices of the undone items in a certain shop that have the given currency.
     */
    func getTotalPriceOfUndoneItems(forCurrency currency: String, inShop shop: String) -> Double {
        let totalPrice = itemsForList?
            .filter({($0 as? ShoppingListItem)!.done!.boolValue == false})
            .filter({($0 as? ShoppingListItem)!.shopName == shop})
            .filter({($0 as? ShoppingListItem)!.itemCurrency == currency})
            .map({($0 as? ShoppingListItem)!.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})

        return totalPrice ?? 0.0
    }

    /**
     Computes the sum of prices of the undone items that have the given currency.
     */
    func getTotalPriceOfUndoneItems(forCurrency currency: String) -> Double {
        let totalPrice = itemsForList?
            .filter({($0 as? ShoppingListItem)!.done!.boolValue == false})
            .filter({($0 as? ShoppingListItem)!.itemCurrency == currency})
            .map({($0 as? ShoppingListItem)!.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})

        return totalPrice ?? 0.0
    }

    /**
     Computes the sum of prices of the done items that have the given currency.
     */
    func getTotalPriceOfDoneItems(forCurrency currency: String) -> Double {
        let totalPrice = itemsForList?
            .filter({($0 as? ShoppingListItem)!.done!.boolValue == true})
            .filter({($0 as? ShoppingListItem)!.itemCurrency == currency})
            .map({($0 as? ShoppingListItem)!.itemPrice?.doubleValue})
            .reduce(0.0, {$0 + ($1 ?? 0.0)})

        return totalPrice ?? 0.0
    }

    static func importData(from importedInfo: [String:AnyObject]) {
        guard let version = importedInfo[Constants.SHARE_FILES_PARAMETER_NAME_VERSION] as? String else { return }

        switch version {
        case ShoptimizerVersions.v_2_2_0.rawValue:
            importDataForVersion2_2_0(from: importedInfo)
            DBHelper.instance.saveContext()
        case ShoptimizerVersions.v_2_3_0.rawValue:
            importDataForVersion2_3_0(from: importedInfo)
            DBHelper.instance.saveContext()
        case ShoptimizerVersions.v_3_2_0.rawValue:
            importDataForVersion3_2_0(from: importedInfo)
            DBHelper.instance.saveContext()
        default:
            let unsupportedVersionMessage = NSLocalizedString("The version of the file is not supported.", comment:"");
            GeneralHelper.presentInformationDialogWithText(unsupportedVersionMessage, andTitle: Constants.TITLE_UNSUPPORTED_SHARE_FILE_VERSION)
        }
    }

    static func importDataForVersion2_2_0(from importedInfo: [String:AnyObject]) {
        let name = importedInfo[ShoppingListEntity.COL_NAME] as! String
        let included = importedInfo[ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE] as? Bool ?? false
        let importedList = ShoppingList.insertNewShoppingList(withName: name, andType: Constants.LIST_TYPE_DYNAMIC, includeInAggregateList: included, intoContext: DBHelper.instance.getManagedObjectContext())

        var count = 0
        while true {
            guard let item = importedInfo[Constants.SHARE_FILES_ITEMS_KEY + String(count)] as? [String : Any] else { break }
            let itemName = item[ShoppingListItemEntity.COL_ITEM_NAME] as! String
            let itemShop = item[ShoppingListItemEntity.COL_SHOP_NAME] as! String
            let itemQuantity = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_QUANTITY] as? NSNumber ?? 0
            let itemUnit = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_UNIT] as! String
            let itemDone = item[ShoppingListItemEntity.COL_DONE] as? Bool ?? false
            let importedItem = ShoppingListItem.insertNewShoppingListItem(withName: itemName, quantity: itemQuantity, unit: itemUnit, doneStatus: itemDone, brand:nil, forPrice: nil, atCurrency: nil, inShop: itemShop, forList: importedList!, intoContext: DBHelper.instance.getManagedObjectContext())
            if included && !itemDone {
                AggregatedListHelper.instance.addItem(importedItem)
            }
            count += 1
        }
    }

    static func importDataForVersion2_3_0(from importedInfo: [String:AnyObject]) {
        let name = importedInfo[ShoppingListEntity.COL_NAME] as! String
        let included = importedInfo[ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE] as? Bool ?? false
        let importedList = ShoppingList.insertNewShoppingList(withName: name, andType: Constants.LIST_TYPE_DYNAMIC, includeInAggregateList: included, intoContext: DBHelper.instance.getManagedObjectContext())

        var count = 0
        while true {
            guard let item = importedInfo[Constants.SHARE_FILES_ITEMS_KEY + String(count)] as? [String : Any] else { break }
            let itemName = item[ShoppingListItemEntity.COL_ITEM_NAME] as! String
            let itemShop = item[ShoppingListItemEntity.COL_SHOP_NAME] as! String
            let itemQuantity = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_QUANTITY] as? NSNumber ?? 0
            let itemUnit = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_UNIT] as! String
            let itemDone = item[ShoppingListItemEntity.COL_DONE] as? Bool ?? false
            let itemBrand = item[ShoppingListItemEntity.COL_BRAND_NAME] as? String ?? nil
            let itemPrice = item[ShoppingListItemEntity.COL_ITEM_PRICE] as? NSNumber ?? 0
            let itemCurrency = item[ShoppingListItemEntity.COL_ITEM_CURRENCY] as? String ?? nil
            let importedItem = ShoppingListItem.insertNewShoppingListItem(withName: itemName, quantity: itemQuantity, unit: itemUnit, doneStatus: itemDone, brand:itemBrand, forPrice: itemPrice, atCurrency: itemCurrency, inShop: itemShop, forList: importedList!, intoContext: DBHelper.instance.getManagedObjectContext())

            if included && !itemDone {
                AggregatedListHelper.instance.addItem(importedItem)
            }
            count += 1
        }
    }

    static func importDataForVersion3_2_0(from importedInfo: [String:AnyObject]) {
        let name = importedInfo[ShoppingListEntity.COL_NAME] as! String
        let included = importedInfo[ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE] as? Bool ?? false
        let importedList = ShoppingList.insertNewShoppingList(withName: name, andType: Constants.LIST_TYPE_DYNAMIC, includeInAggregateList: included, intoContext: DBHelper.instance.getManagedObjectContext())

        var count = 0
        while true {
            guard let item = importedInfo[Constants.SHARE_FILES_ITEMS_KEY + String(count)] as? [String : Any] else { break }
            let itemName = item[ShoppingListItemEntity.COL_ITEM_NAME] as! String
            let itemShop = item[ShoppingListItemEntity.COL_SHOP_NAME] as! String
            let itemQuantity = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_QUANTITY] as? NSNumber ?? 0
            let itemUnit = item[ShoppingListItemEntity.COL_ITEM_AMOUNT_UNIT] as! String
            let itemDone = item[ShoppingListItemEntity.COL_DONE] as? Bool ?? false
            let itemBrand = item[ShoppingListItemEntity.COL_BRAND_NAME] as? String ?? nil
            let itemPrice = item[ShoppingListItemEntity.COL_ITEM_PRICE] as? NSNumber ?? 0
            let itemCurrency = item[ShoppingListItemEntity.COL_ITEM_CURRENCY] as? String ?? nil
            let itemNotes = item[ShoppingListItemEntity.COL_NOTES] as? String ?? nil
            let importedItem = ShoppingListItem.insertNewShoppingListItem(withName: itemName, quantity: itemQuantity, unit: itemUnit, doneStatus: itemDone, brand:itemBrand, forPrice: itemPrice, atCurrency: itemCurrency, inShop: itemShop, notes: itemNotes, forList: importedList!, intoContext: DBHelper.instance.getManagedObjectContext())

            if included && !itemDone {
                AggregatedListHelper.instance.addItem(importedItem)
            }
            count += 1
        }
    }

    func dominantCurrency() -> String? {
        guard let itemsForList = itemsForList else { return nil }

        var currenciesDict = [String:Int]()
        for item in itemsForList.allObjects {
            guard let item = item as? ShoppingListItem, let currency = item.itemCurrency else { continue }

            if currenciesDict[currency] == nil {
                currenciesDict[currency] = 1
            } else {
                currenciesDict[currency]! += 1
            }
        }

        let sorted = currenciesDict.sorted(by: {$0.value > $1.value})
        let topValue = sorted.first?.value
        let topValues = sorted.filter({$0.value == topValue})
        let sortedTopValues = topValues.sorted(by: <)
        return sortedTopValues.first?.key
    }
}

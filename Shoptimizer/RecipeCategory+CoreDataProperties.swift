// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData


extension RecipeCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecipeCategory> {
        return NSFetchRequest<RecipeCategory>(entityName: "RecipeCategory");
    }

    @NSManaged public var name: String?
    @NSManaged public var recipesForCategory: NSSet?

}

// MARK: Generated accessors for recipesForCategory
extension RecipeCategory {

    @objc(addRecipesForCategoryObject:)
    @NSManaged public func addToRecipesForCategory(_ value: Recipe)

    @objc(removeRecipesForCategoryObject:)
    @NSManaged public func removeFromRecipesForCategory(_ value: Recipe)

    @objc(addRecipesForCategory:)
    @NSManaged public func addToRecipesForCategory(_ values: NSSet)

    @objc(removeRecipesForCategory:)
    @NSManaged public func removeFromRecipesForCategory(_ values: NSSet)
    
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class StringUtil {
    static func similarStrings(s1: String?, s2: String?) -> Bool {
        return (s1 == nil && s2 == nil)
            || (s1 == nil && s2 == "")
            || (s1 == "" && s2 == nil)
            || (s1 == s2)
    }
}

extension String {
    var isNotEmpty : Bool {
        return !self.isEmpty
    }
    
    var isBlank : Bool {
        return trimmingCharacters(in: .whitespaces).isEmpty
    }
    
    var isNotBlank : Bool {
        return !self.isBlank
    }

    subscript(n: Int) -> String {
        if n < 0 || n >= self.count {
            return ""
        } else {
            return String(self[self.index(self.startIndex, offsetBy: n)])
        }
    }

    subscript(start: Int, end: Int) -> String {
        var n1 = start
        var n2 = end

        if n1 < 0 {
            n1 = 0
        }
        if n1 > self.count {
            n1 = self.count
        }
        if n2 < 0 {
            n2 = 0
        }
        if n2 > self.count {
            n2 = self.count
        }

        if n2 < n1 {
            return ""
        } else {
            return String(self[self.index(self.startIndex, offsetBy: n1) ..< self.index(self.startIndex, offsetBy: n2)])
        }
    }

    subscript(rng:Range<Int>) -> String {
        return self[rng.lowerBound, rng.upperBound]
    }

    static func ~= (left:String, right: String) -> Bool {
        return left.compare(right, options:NSString.CompareOptions.caseInsensitive) == ComparisonResult.orderedSame
    }

    func matches(regex: NSRegularExpression) -> Bool {
        return regex.matches(in: self, options: [], range: NSMakeRange(0, self.count)).count > 0
    }
}

extension Optional where Wrapped == String {
    var isBlank : Bool {
        return self?.isBlank ?? true
    }
    
    var isNotBlank : Bool {
        return !self.isBlank
    }
}

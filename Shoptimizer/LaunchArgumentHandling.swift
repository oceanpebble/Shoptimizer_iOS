// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

func handleLaunchArguments() {
    let arguments = ProcessInfo.processInfo.arguments

    if arguments.contains("ClearAll") {
        deleteAllShoppingListItems()
        deleteAllShoppingLists()
        deleteAllSuppliesItems()
        deleteAllSuppliesItemCategories()
        deleteAllRecipeIngredients()
        deleteAllRecipes()
    }

    for argument in arguments {
        let tokens = argument.components(separatedBy: "/")
        guard let firstToken = tokens.first else { continue }
        switch firstToken {
        case "CreateShoppingList": createShoppingList(from: tokens)
        case "CreateShoppingListItem": createShoppingListItem(from: tokens)
        case "CreateSuppliesItem": createSuppliesItem(from: tokens)
        case "CreateSuppliesItemCategory": createSuppliesItemCatgory(from: tokens)
        case "CreateRecipe": createRecipe(from: tokens)
        case "CreateRecipeIngredient": createRecipeIngredient(from: tokens)
        default: continue
        }
    }

    DBHelper.instance.saveContext()
}

func deleteAllShoppingListItems() {
    let context = DBHelper.instance.getManagedObjectContext()
    if let items = ShoppingListItem.allItems() {
        for item in items {
            context.delete(item)
        }
    }
}

func deleteAllShoppingLists() {
    let context = DBHelper.instance.getManagedObjectContext()
    for list in ShoppingList.shoppingLists().filter( { $0.name != AggregatedListHelper.instance.nameOfAggregatedList() } ) {
        context.delete(list)
    }
}

func deleteAllSuppliesItems() {
    let context = DBHelper.instance.getManagedObjectContext()
    for item in SuppliesItem.getAll() {
        context.delete(item)
    }
}

func deleteAllSuppliesItemCategories() {
    let context = DBHelper.instance.getManagedObjectContext()
    for category in SuppliesItemCategory.categories() {
        context.delete(category)
    }
}

func deleteAllRecipeIngredients() {
    let context = DBHelper.instance.getManagedObjectContext()
    if let ingredients = RecipeIngredient.allIngredients() {
        for ingredient in ingredients {
            context.delete(ingredient)
        }
    }
}

func deleteAllRecipes() {
    let context = DBHelper.instance.getManagedObjectContext()
    for recipe in Recipe.recipes() {
        context.delete(recipe)
    }
}

func createShoppingList(from tokens: [String]) {
    let _ = ShoppingList.insertNewShoppingList(withName: tokens[1], andType: "Dynamic", includeInAggregateList: Bool(tokens[2])!, intoContext: DBHelper.instance.getManagedObjectContext())
}

func createShoppingListItem(from tokens: [String]) {
    let shoppingListName = tokens[9]
    guard let shoppingList = ShoppingList.shoppingList(for: shoppingListName) else { return }
    let _ = ShoppingListItem.insertNewShoppingListItem(withName: tokens[1], quantity: NSNumber(value: Double(tokens[4])!), unit: unitOrL(from : tokens[5]), doneStatus: Bool(tokens[8])!, brand: stringOrNil(from: tokens[2]), forPrice: NSNumber(value: Double(tokens[6])!), atCurrency: stringOrNil(from: tokens[7]), inShop: stringOrNil(from: tokens[3]), notes: stringOrNil(from: tokens[10]), forList: shoppingList, intoContext: DBHelper.instance.getManagedObjectContext())
}

func createSuppliesItem(from tokens: [String]) {
    let _ = SuppliesItem.insert(name: tokens[1], minAmountQuantity: NSNumber(value: Double(tokens[2])!), minAmountUnit: tokens[3], curAmountQuantity: NSNumber(value: Double(tokens[4])!), curAmountUnit: tokens[5], categories: tokens[6])
}

func createSuppliesItemCatgory(from tokens: [String]) {
    let _ = SuppliesItemCategory.insert(name: tokens[1])
}

func createRecipe(from tokens: [String]) {
    let _ = Recipe.insertNewRecipe(withName: tokens[1], andPortions: NSNumber(value: UInt(tokens[2])!), intoContext: DBHelper.instance.getManagedObjectContext())
}

func createRecipeIngredient(from tokens: [String]) {
    guard let recipe = Recipe.recipe(withName: tokens[4]) else { return }
    let _ = RecipeIngredient.insertNewRecipeIngredient(withName: tokens[1], quantity: NSNumber(value: Double(tokens[2])!), andUnit: unitOrL(from : tokens[3]), andIncludeInList: NSNumber(value: Bool(tokens[5])!), andIncludeInSearch: NSNumber(value: Bool(tokens[6])!), forRecipe: recipe, intoContext: DBHelper.instance.getManagedObjectContext())
}

private func stringOrNil(from token: String) -> String? {
    return token == "" ? nil : token
}

private func unitOrL(from token: String) -> String {
    return token == "" ? "l" : token
}

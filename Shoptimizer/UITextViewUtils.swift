// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import UIKit

class UITextViewHelper: NSObject, UITextViewDelegate {
    static var instance = UITextViewHelper()
    var activeView: UITextView?

    fileprivate override init() {
    }

    func textFieldShouldReturn(_ theTextField: UITextField) -> Bool {
        theTextField.resignFirstResponder()
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        self.activeView = textView
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        self.activeView = nil
    }
}

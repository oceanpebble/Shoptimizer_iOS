// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import CoreData

struct TaskEntity {
    static let CACHE_NAME = "tasks";
    static let COL_TITLE = "title";
    static let COL_BODY = "body";
    static let COL_DONE = "done";
    static let COL_DATE_CREATED = "dateCreated";
    static let COL_DATE_DUE = "dateDue";
    static let COL_DATE_COMPLETED = "dateCompleted";
    static let ENTITY_NAME = "Task";
}

public class Task: NSManagedObject {

    static var nextID : Int {
        let userDefaults = UserDefaults.standard
        let idFromDefaults = userDefaults.integer(forKey: Constants.USER_DEFAULTS_TASK_ID_COUNTER)
        let newId = idFromDefaults + 1
        userDefaults.set(newId, forKey: Constants.USER_DEFAULTS_TASK_ID_COUNTER)
        userDefaults.synchronize()
        return newId
    }

    static func tasks() -> [Task] {
        do {
            return try DBHelper.instance.getManagedObjectContext().fetch(Task.fetchRequest())
        } catch {
            return [Task]()
        }
    }

    static func tasks(_ delegate: NSFetchedResultsControllerDelegate?) -> NSFetchedResultsController<Task> {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: TaskEntity.CACHE_NAME)
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()

        let entity = NSEntityDescription.entity(forEntityName: TaskEntity.ENTITY_NAME, in:DBHelper.instance.getManagedObjectContext())
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20

        let sortDescriptorDoneStatus = NSSortDescriptor(key:TaskEntity.COL_DONE, ascending:true)
        let sortDescriptorDateDue = NSSortDescriptor(key:TaskEntity.COL_DATE_DUE, ascending:true, selector:nil)
        let sortDescriptors = [sortDescriptorDoneStatus, sortDescriptorDateDue]
        fetchRequest.sortDescriptors = sortDescriptors

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest:fetchRequest, managedObjectContext:DBHelper.instance.getManagedObjectContext(), sectionNameKeyPath:TaskEntity.COL_DONE, cacheName:TaskEntity.CACHE_NAME)

        if delegate != nil {
            aFetchedResultsController.delegate = delegate
        }

        do {
            try aFetchedResultsController.performFetch()
        } catch {
            NSLog("Unresolved error while fetching tasks.");
            let errorMessageString = NSLocalizedString("There has been an error loading the data, please try again.", comment: "");
            GeneralHelper.presentInformationDialogWithText(errorMessageString, andTitle: Constants.TITLE_ALERT_ERROR)
        }

        return aFetchedResultsController
    }
    
    
    static func insert(title: String?, body: String?, dateDue: NSDate?, dateCreated: NSDate?, dateCompleted: NSDate?, done: Bool?, id: Int?) -> Task {
        let taskEntity = NSEntityDescription.entity(forEntityName: TaskEntity.ENTITY_NAME, in: DBHelper.instance.getManagedObjectContext())
        let task = Task(entity: taskEntity!, insertInto: DBHelper.instance.getManagedObjectContext())
        task.title = title
        task.body = body
        task.dateDue = dateDue
        task.dateCreated = dateCreated
        task.dateCompleted = dateCompleted
        task.done = done != nil ? NSNumber(booleanLiteral: done!) : NSNumber(booleanLiteral: false)
        task.id = id != nil ? NSNumber(value: id!) : NSNumber(value: 0)
        DBHelper.instance.saveContext()

        return task;
    }


    static func insertNewTask(withTitle title: String, andBody body: String, andDateDue dateDue: NSDate, andDateCreated dateCreated: NSDate, intoContext context: NSManagedObjectContext) -> Task {
        let taskEntity = NSEntityDescription.entity(forEntityName: TaskEntity.ENTITY_NAME, in:context)
        let task = Task(entity: taskEntity!, insertInto: context)
        task.title = title
        task.body = body
        task.dateDue = dateDue
        task.dateCreated = dateCreated
        task.dateCompleted = nil
        task.done = NSNumber(booleanLiteral: false)
        task.id = NSNumber(value: nextID)

        return task;
    }

    static func markTaskAsDone(withId id: Int) {
        let tasks = Task.tasks().filter( { $0.id?.intValue == id } )
        if tasks.count == 1 {
            tasks[0].done = NSNumber(booleanLiteral: true)
            tasks[0].dateCompleted = NSDate()
            DBHelper.instance.saveContext()
        }
    }

    static func deleteAllTasks() {
        let context = DBHelper.instance.getManagedObjectContext()
        for task in tasks() {
            context.delete(task)
        }
        DBHelper.instance.saveContext()
    }
}

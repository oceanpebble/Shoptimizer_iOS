// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class AddRecipeCategoryViewController : UIViewController, AddEntityViewDelegate {
    func setTitle() {
        
    }
    

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!

    var fetchedCategory: RecipeCategory?
    var editingMode: EditingMode

    // *********************************************************
    //
    // MARK: - UIViewController
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        self.editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextFieldDelegates()
        setTextFieldContents()
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit Category", comment: "")
        }
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        addDoneButtonToKeyboard(forTextFields: [nameTextField], forTextViews: [], targeting: self, using: #selector(doneButtonAction))
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews, otherwise the scrollview will be positioned too low on the screen
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let specificSender = sender as? UIBarButtonItem, specificSender != saveButton {
            return true;
        }

        guard let newCategoryName = nameTextField.text, newCategoryName.isNotBlank else {
            let enterNameString = NSLocalizedString("Please enter a name.", comment: "")
            GeneralHelper.presentInformationDialogWithText(enterNameString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false;
        }

        if (self.editingMode == .create || newCategoryName != self.fetchedCategory?.name)
            && RecipeCategory.isCategoryAvailable(withName: newCategoryName) {
            let categoryAlreadyExistsString = NSLocalizedString("A category with this name already exists.", comment: "")
            GeneralHelper.presentInformationDialogWithText(categoryAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false;
        }

        if self.editingMode == .edit {
            self.fetchedCategory?.name = newCategoryName
            DBHelper.instance.saveContext()
        } else {
            let _ = RecipeCategory.insertNewRecipeCategory(withName: newCategoryName, intoContext: DBHelper.instance.getManagedObjectContext())
            DBHelper.instance.saveContext()
        }
        
        return true
    }

    // *********************************************************
    //
    // MARK: - View Controller Life Cycle
    //
    // *********************************************************

    func setTextFieldDelegates() {
        self.nameTextField.delegate = UITextFieldHelper.instance
    }

    func setTextFieldContents() {
        self.nameTextField.text = self.fetchedCategory?.name
    }

    func setTextViewDelegates() {
        // no text views in this view
    }

    func setTextViewContents() {
        // no text views in this view
    }

    /* Unfortunetly, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let info = (aNotification as NSNotification).userInfo,
           let value = info[UIResponder.keyboardFrameBeginUserInfoKey] {
            var kbHeight = (value as AnyObject).cgRectValue.size.height
            if kbHeight == 0.0 {
                kbHeight = (info[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
            }
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbHeight, right: 0.0)
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            var aRect = scrollView.frame;
            aRect.size.height -= kbHeight;

            if let activeTextField = UITextFieldHelper.instance.activeField,
                !aRect.contains(activeTextField.frame.origin) {
                scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                return
            }

            if let activeTextView = UITextViewHelper.instance.activeView {
                let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
                let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
                if !aRect.contains(transformedLowerLeftOfActiveTextView) {
                    let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                    let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height - 50) // 50 = height of done button toolbar
                    scrollView.scrollRectToVisible(rect, animated: true)
                }
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func doneButtonAction() {
        UITextFieldHelper.instance.activeField?.resignFirstResponder()
        UITextViewHelper.instance.activeView?.resignFirstResponder()
        keyboardWillBeHidden(Notification(name: Notification.Name(rawValue: "foo"))) // name is irrelevant since notification is not used
    }
}

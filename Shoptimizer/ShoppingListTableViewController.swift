// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class ShoppingListViewController : UITableViewController, EntityTableViewDelegate, NSFetchedResultsControllerDelegate {

    // MARK: - Properties

    typealias EntityType = ShoppingListItem
    var reusableCellIdentifier: String { return "ShoppingListItemTableViewCell" }
    var fetchedResultsController: NSFetchedResultsController<EntityType>!
    var shoppingList: ShoppingList!
    private var aggregatedListHelper: AggregatedListHelper!
    private var viewModel: ShoppingListTableViewModel!

    // MARK: - UIView

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController = ShoppingListItem.itemsForShoppingList(shoppingList, andDelegate: self)
        aggregatedListHelper = AggregatedListHelper.instance
        let dataManager = ShoppingListTableDataManager(aggregatedListHelper: aggregatedListHelper, shoppingListRepository: ShoppingListRepository(), shoppingListItemRepository: ShoppingListItemRepository())
        viewModel = ShoppingListTableViewModel(fetchedResultsController: fetchedResultsController, aggregatedListHelper: aggregatedListHelper, dataManager: dataManager)
        let shoppingListName = viewModel.getName(of: shoppingList)
        navigationItem.title = shoppingListName;
        if viewModel.isAggregatedList(shoppingListName) {
            navigationItem.setRightBarButton(nil, animated: false)
        }
        setupSwipeRecognizers()
    }

    // MARK: - Segue Handling

    @IBAction func unwindToList(_ segue: UIStoryboardSegue) {
        // this is needed to return to the list screen from an item's details; if deleted the app would return to the overview of all lists
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let specificSender = sender as? UITableViewCell {
            let indexPath = tableView.indexPath(for: specificSender)
            let selectedItem = viewModel.item(at: indexPath!)
            let listOfSelectedItem = viewModel.list(for: selectedItem)
            let listName = viewModel.getName(of: listOfSelectedItem)
            if viewModel.isAggregatedList(listName) {
                tableView.deselectRow(at: indexPath!, animated: false)
                return false
            }
            return true
        } else if sender is UIBarButtonItem {
            return !viewModel.isAggregatedList(shoppingList)
        }

        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let targetController = segue.destination as! AddShoppingListItemViewController
        targetController.listForItem = shoppingList;

        if let specificSender = sender as? UITableViewCell {
            let indexPath = tableView.indexPath(for: specificSender)
            let selectedItem = viewModel.item(at: indexPath!)
            targetController.fetchedListItem = selectedItem;
            targetController.editingMode = .edit;
        } else {
            targetController.editingMode = .create;
        }
    }

    // MARK: - Gesture Recognizers

    func setupSwipeRecognizers() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target:self, action:#selector(self.respondToRightSwipeGesture))
        rightSwipeRecognizer.direction = .right
        view.addGestureRecognizer(rightSwipeRecognizer)
    }

    @objc func respondToRightSwipeGesture(_ recognizer: UISwipeGestureRecognizer) {
        let location = recognizer.location(in: view)
        guard let swipedIndexPath = tableView.indexPathForRow(at: location) else {
            return
        }

        let swipedItem = viewModel.item(at: swipedIndexPath)
        let listForSwipedItem = viewModel.list(for: swipedItem)

        if !viewModel.isAggregatedList(listForSwipedItem) {
            let currentStatus = viewModel.done(for: swipedItem)
            viewModel.update(item: swipedItem, done: !currentStatus)
            if viewModel.isListIncludedInAggregatedList(listForSwipedItem) {
                currentStatus
                ? viewModel.addItemToAggregatedList(swipedItem)
                : viewModel.subtractItemFromAggregatedList(swipedItem)
            }
            DBHelper.instance.saveContext()
        } else {
            let itemsToMarkAsDone = viewModel.undoneItemsNotOnAggregatedListWithName(viewModel.name(of: swipedItem), shopName: viewModel.shopName(of: swipedItem))
            for item in itemsToMarkAsDone {
                let listForItem = viewModel.list(for: item)
                if viewModel.isListIncludedInAggregatedList(listForItem) {
                    viewModel.update(item: item, done: true)
                }
            }
            viewModel.deleteItemFromAggregatedList(swipedItem)
            DBHelper.instance.saveContext()
        }

        tableView.reloadData()
    }

    // MARK: - UITableViewController

    override func numberOfSections(in tableView: UITableView) -> Int {
        return etdNumberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return etdTableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return etdTableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionsOfFrc = viewModel.sections()
        if sectionsOfFrc.count > section {
            let objectsOfSection = sectionsOfFrc[section].objects as! [ShoppingListItem]
            var numberOfDoneItems = 0
            for item in objectsOfSection {
                if viewModel.done(for: item) {
                    numberOfDoneItems += 1
                }
            }

            let firstObject = objectsOfSection[0]
            let shopName = viewModel.shopName(of: firstObject)
            return viewModel.priceString(for: shoppingList, inShop: shopName, objectCount: objectsOfSection.count, numberOfDoneItems: numberOfDoneItems)
        }
        
        return nil;
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let swipedItem = viewModel.item(at: indexPath)
        let listOfSwipedItem = viewModel.list(for: swipedItem)
        return !viewModel.isAggregatedList(listOfSwipedItem)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let swipedItem = viewModel.item(at: indexPath)
            if(viewModel.isListIncludedInAggregatedList(viewModel.list(for: swipedItem)) && !viewModel.done(for: swipedItem)) {
                aggregatedListHelper.subtractItem(swipedItem)
            }
            viewModel.deleteItem(swipedItem)
        }
    }

    // MARK: - NSFetchedResultsController

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        etdControllerWillChangeContent(controller, inTableView: tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        etdController(controller, didChange: sectionInfo, atSectionIndex: sectionIndex, for: type, inTableView: tableView)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        etdController(controller, didChange: anObject, at: indexPath, for: type, newIndexPath: newIndexPath, inTableView: tableView)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        etdControllerDidChangeContent(controller, inTableView: tableView)
    }

    func configure(cell aCell: UITableViewCell?, atIndexPath indexPath: IndexPath) {
        guard let cell = aCell else { return }
        let listItem = viewModel.item(at: indexPath)

        let mainLabelText = viewModel.mainLabelText(for: listItem)
        if viewModel.done(for: listItem) {
            cell.textLabel!.attributedText = NSAttributedString(string: mainLabelText, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.thick.rawValue, NSAttributedString.Key.strikethroughColor: UIColor.green])
            cell.accessibilityIdentifier = AccessibilityUtil.identifierForDoneItems
        } else {
            cell.textLabel!.attributedText = NSAttributedString(string: mainLabelText)
            cell.accessibilityIdentifier = nil
        }

        let listForItem = viewModel.list(for: listItem)
        if !viewModel.isAggregatedList(listForItem) {
            cell.accessoryType = .disclosureIndicator
        }

        cell.detailTextLabel?.text = viewModel.detailLabelText(for: listItem)
    }
}

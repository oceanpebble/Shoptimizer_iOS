// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
import UIKit

class AddSuppliesItemViewController: UIViewController, AddEntityViewDelegate {
    func setTitle() {
        
    }
    

    // *********************************************************
    //
    // MARK: - Properties
    //
    // *********************************************************

    var fetchedSuppliesItem: SuppliesItem?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var minAmountUnitPicker: UIPickerView!
    @IBOutlet weak var curAmountUnitPicker: UIPickerView!
    @IBOutlet weak var minAmountQuantityField: UITextField!
    @IBOutlet weak var curAmountQuantityField: UITextField!
    @IBOutlet weak var itemNameField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var categoriesButton: UIButton!
    var editingMode: EditingMode

    // *********************************************************
    //
    // MARK: - UIView
    //
    // *********************************************************

    required init?(coder aDecoder: NSCoder) {
        editingMode = .create
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextFieldDelegates()
        setTextFieldContents()
        setUnitPickerValues()
        if editingMode == .edit {
            navigationItem.title = NSLocalizedString("Edit Item", comment: "")
        }
        registerForKeyboardNotifications(shownSelector: #selector(keyboardWasShown(_:)), hiddenSelector: #selector(keyboardWillBeHidden(_:)))
        addDoneButtonToKeyboard(forTextFields: [minAmountQuantityField, curAmountQuantityField, itemNameField], forTextViews: [], targeting: self, using: #selector(doneButtonAction))
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupScrollView(scrollView) // this has to be called here in viewDidLayoutSubviews, otherwise the scrollview will be positioned too low on the screen
        view.layoutIfNeeded()
        categoriesButton.disclosureButton(baseColor: view.tintColor)
    }

    // *********************************************************
    //
    // MARK: - Segue Handling
    //
    // *********************************************************

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sender = sender as? UIButton,
            sender == categoriesButton {
            let target = segue.destination as? SuppliesItemCategoriesTableViewController
            target?.suppliesItem = fetchedSuppliesItem
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if sender as? UIBarButtonItem != saveButton {
            return true
        }

        guard let newName = itemNameField.text?.trimmingCharacters(in: .whitespaces), newName.isNotEmpty else {
            let enterNameString = NSLocalizedString("Please enter a name.", comment: "");
            GeneralHelper.presentInformationDialogWithText(enterNameString, andTitle: Constants.TITLE_ALERT_NAME_MISSING)
            return false
        }

        if (self.editingMode == .create
            || newName != self.fetchedSuppliesItem!.value(forKey: SuppliesItem.COL_ITEM_NAME) as? String)
            && SuppliesItem.isSuppliesItemAvailable(withName: newName) {
            let listAlreadyExistsString = NSLocalizedString("A supplies item with this name already exists.", comment: "")
            GeneralHelper.presentInformationDialogWithText(listAlreadyExistsString, andTitle: Constants.TITLE_ALERT_DUPLICATE)
            return false
        }

        guard let minAmountQuantity = GeneralHelper.ensureValidDouble(fromString: self.minAmountQuantityField.text, allowZero: true) else {
            let portionsInvalidString = NSLocalizedString("Please enter a value >= 0.0 for the minimum amount.", comment: "")
            GeneralHelper.presentInformationDialogWithText(portionsInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        guard let curAmountQuantity = GeneralHelper.ensureValidDouble(fromString: self.curAmountQuantityField.text, allowZero: true) else {
            let portionsInvalidString = NSLocalizedString("Please enter a value >= 0.0 for the current amount.", comment: "")
            GeneralHelper.presentInformationDialogWithText(portionsInvalidString, andTitle: Constants.TITLE_ALERT_INVALID_DATA)
            return false
        }

        let minAmountUnit = UnitPickerHelper.getUnitPickerData()[self.minAmountUnitPicker.selectedRow(inComponent: 0)]
        let curAmountUnit = UnitPickerHelper.getUnitPickerData()[self.curAmountUnitPicker.selectedRow(inComponent: 0)]
        if (self.editingMode == .edit) {
            self.fetchedSuppliesItem!.itemName = self.itemNameField.text
            self.fetchedSuppliesItem!.minAmountQuantity = minAmountQuantity
            self.fetchedSuppliesItem!.minAmountUnit = minAmountUnit
            self.fetchedSuppliesItem!.curAmountQuantity = curAmountQuantity
            self.fetchedSuppliesItem!.curAmountUnit = curAmountUnit
            DBHelper.instance.saveContext()
        } else if (self.editingMode == .create) {
            let _ = SuppliesItem.insert(name: newName, minAmountQuantity: minAmountQuantity, minAmountUnit: minAmountUnit, curAmountQuantity: curAmountQuantity, curAmountUnit: curAmountUnit, categories: selectedCategories.joined(separator: "; "))
            DBHelper.instance.saveContext()
            selectedCategories.removeAll()
        }

        return true
    }

    // *********************************************************
    //
    // MARK: - View Controller Life Cycle
    //
    // *********************************************************

    func setUnitPickerValues() {
        self.minAmountUnitPicker.dataSource = self;
        self.minAmountUnitPicker.delegate = self;
        self.curAmountUnitPicker.dataSource = self;
        self.curAmountUnitPicker.delegate = self;

        if let suppliesItem = self.fetchedSuppliesItem {
            let minAmountUnit = UnitPickerHelper.translate(unit: suppliesItem.minAmountUnit!)
            self.minAmountUnitPicker.selectRow(UnitPickerHelper.getIndexOfUnit(minAmountUnit), inComponent:0, animated:false)
            let curAmountUnit = UnitPickerHelper.translate(unit: suppliesItem.curAmountUnit!)
            self.curAmountUnitPicker.selectRow(UnitPickerHelper.getIndexOfUnit(curAmountUnit), inComponent:0, animated:false)
        }
    }

    func setTextFieldDelegates() {
        self.itemNameField.delegate = UITextFieldHelper.instance
        self.minAmountQuantityField.delegate = UITextFieldHelper.instance
        self.curAmountQuantityField.delegate = UITextFieldHelper.instance
    }

    func setTextFieldContents() {
        if let suppliesItem = self.fetchedSuppliesItem {
            self.itemNameField.text = suppliesItem.itemName!

            let convertedMinQuantity = GeneralHelper.decimalFormatter().string(from: suppliesItem.value(forKey: SuppliesItem.COL_MIN_AMOUNT_QUANTITY) as! NSNumber)
            self.minAmountQuantityField.text = String(convertedMinQuantity!)

            let convertedCurQuantity = GeneralHelper.decimalFormatter().string(from: suppliesItem.value(forKey: SuppliesItem.COL_CUR_AMOUNT_QUANTITY) as! NSNumber)
            self.curAmountQuantityField.text = String(convertedCurQuantity!)
        }
    }

    func setTextViewDelegates() {
        // no text views in this view
    }

    func setTextViewContents() {
        // no text views in this view
    }

    /* Unfortunetly, the following methods can not be generalized in the AddEntityViewDelegate because of the @objc notation. */

    @objc func keyboardWasShown(_ aNotification: Notification) {
        if let info = (aNotification as NSNotification).userInfo,
           let value = info[UIResponder.keyboardFrameBeginUserInfoKey] {
            var kbHeight = (value as AnyObject).cgRectValue.size.height
            if kbHeight == 0.0 {
                kbHeight = (info[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size.height
            }
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbHeight, right: 0.0)
            scrollView.contentInset = contentInsets;
            scrollView.scrollIndicatorInsets = contentInsets;
            var aRect = scrollView.frame;
            aRect.size.height -= kbHeight;

            if let activeTextField = UITextFieldHelper.instance.activeField,
                !aRect.contains(activeTextField.frame.origin) {
                scrollView.scrollRectToVisible(activeTextField.frame, animated: true)
                return
            }

            if let activeTextView = UITextViewHelper.instance.activeView {
                let lowerLeftOfActiveTextView = CGPoint(x: activeTextView.frame.origin.x, y: activeTextView.frame.origin.y + activeTextView.frame.height)
                let transformedLowerLeftOfActiveTextView = activeTextView.convert(lowerLeftOfActiveTextView, to: scrollView)
                if !aRect.contains(transformedLowerLeftOfActiveTextView) {
                    let scrollRectOrigin = activeTextView.convert(activeTextView.frame.origin, to: scrollView)
                    let rect = CGRect(x: scrollRectOrigin.x, y: scrollRectOrigin.y, width: scrollView.frame.width, height: activeTextView.frame.height - 50) // 50 = height of done button toolbar
                    scrollView.scrollRectToVisible(rect, animated: true)
                }
            }
        }
    }

    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func doneButtonAction() {
        UITextFieldHelper.instance.activeField?.resignFirstResponder()
        UITextViewHelper.instance.activeView?.resignFirstResponder()
        keyboardWillBeHidden(Notification(name: Notification.Name(rawValue: "foo"))) // name is irrelevant since notification is not used
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import Foundation
@testable import Shoptimizer

class NSFetchedResultsControllerShoppingListMock: NSFetchedResultsController<ShoppingList> {
    var didCallObjectAt = false
    
    override func object(at indexPath: IndexPath) -> ShoppingList {
        didCallObjectAt = true
        return ShoppingList()
    }
}

class NSFetchedResultsControllerShoppingListItemMock: NSFetchedResultsController<ShoppingListItem> {
    var didCallObjectAt = false
    var didCallSections = false
    
    override func object(at indexPath: IndexPath) -> ShoppingListItem {
        didCallObjectAt = true
        return ShoppingListItem()
    }
    
    override var sections: [NSFetchedResultsSectionInfo]? {
        didCallSections = true
        return []
    }
}

class NSFetchedResultsControllerTaskMock: NSFetchedResultsController<Task> {
    var didCallObjectAt = false
    var didCallSections = false
    
    override func object(at indexPath: IndexPath) -> Task {
        didCallObjectAt = true
        return Task()
    }
    
    override var sections: [NSFetchedResultsSectionInfo]? {
        didCallSections = true
        return []
    }
}

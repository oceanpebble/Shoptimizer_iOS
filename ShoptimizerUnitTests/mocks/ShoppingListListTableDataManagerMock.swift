// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class ShoppingListListTableDataManagerMock: ShoppingListListTableDataManager {
    
    var didCallItemsFor = false
    var didCallAllSuppliesItems = false
    var didCallDoneItemsFor = false
    var didCallIsListIncludedInAggregatedList = false
    var didCallGetName = false
    var didCallIsAggregatedListByString = false
    var didCallIsAggregatedListByList = false
    var didCallNumberOfItemsOn = false
    var didCallNumberOfDoneItemsOn = false
    var didCallDominantCurrencyOf = false
    var didCallTotalPriceOfAllItems = false
    var didCallTotalPriceOfDoneItems = false
    var didCallUpdateSuppliesItemQuantity = false
    var didCallUpdateShoppingListItemDone = false
    var didCallAddItemToAggregatedList = false
    var didCallSubtractItemFromAggregatedList = false
    var didCallDeleteShoppingListItem = false
    var didCallDeleteShoppingList = false
    var didCallExportListToFeilUrl = false
    
    override func itemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        didCallItemsFor = true
        return super.itemsFor(list)
    }
    
    override func allSuppliesItems() -> [SuppliesItem] {
        didCallAllSuppliesItems = true
        return super.allSuppliesItems()
    }
    
    override func doneItemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        didCallDoneItemsFor = true
        return super.doneItemsFor(list)
    }
    
    override func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        didCallIsListIncludedInAggregatedList = true
        return super.isListIncludedInAggregatedList(list)
    }
    
    override func getName(of list: ShoppingList) -> String {
        didCallGetName = true
        return super.getName(of: list)
    }
    
    override func isAggregatedList(_ listName: String) -> Bool {
        didCallIsAggregatedListByString = true
        return super.isAggregatedList(listName)
    }
    
    override func isAggregatedList(_ list: ShoppingList) -> Bool {
        didCallIsAggregatedListByList = true
        return super.isAggregatedList(list)
    }
    
    override func numberOfItemsOn(_ list: ShoppingList) -> Int {
        didCallNumberOfItemsOn = true
        return super.numberOfItemsOn(list)
    }
    
    override func numberOfDoneItemsOn(_ list: ShoppingList) -> Int {
        didCallNumberOfDoneItemsOn = true
        return super.numberOfDoneItemsOn(list)
    }
    
    override func dominantCurrencyOf(_ list: ShoppingList) -> String? {
        didCallDominantCurrencyOf = true
        return super.dominantCurrencyOf(list)
    }
    
    override func totalPriceOfAllItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfAllItems = true
        return super.totalPriceOfAllItems(forCurrency: currency, on: list)
    }
    
    override func totalPriceOfDoneItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfDoneItems = true
        return super.totalPriceOfDoneItems(forCurrency: currency, on: list)
    }
    
    
    override func updateSuppliesItem(_ item: SuppliesItem, curAmountQuantity: Double) {
        didCallUpdateSuppliesItemQuantity = true
        super.updateSuppliesItem(item, curAmountQuantity: curAmountQuantity)
    }
    
    override func updateShoppingListLitem(_ item: ShoppingListItem, done: Bool) {
        didCallUpdateShoppingListItemDone = true
        super.updateShoppingListLitem(item, done: done)
    }
    
    override func addItemToAggregatedList(_ item: ShoppingListItem) {
        didCallAddItemToAggregatedList = true
        super.addItemToAggregatedList(item)
    }
    
    override func subtractItemFromAggregatedList(_ item: ShoppingListItem) {
        didCallSubtractItemFromAggregatedList = true
        super.subtractItemFromAggregatedList(item)
    }
    
    override func deleteShoppingListItem(_ item: ShoppingListItem) {
        didCallDeleteShoppingListItem = true
        super.deleteShoppingListItem(item)
    }
    
    override func deleteShoppingList(_ list: ShoppingList) {
        didCallDeleteShoppingList = true
        super.deleteShoppingList(list)
    }
    
    override func exportListToFileURL(_ list: ShoppingList) -> URL? {
        didCallExportListToFeilUrl = true
        return super.exportListToFileURL(list)
    }
}

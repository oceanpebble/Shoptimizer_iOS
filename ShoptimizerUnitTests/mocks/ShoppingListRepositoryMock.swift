// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

@testable import Shoptimizer

class ShoppingListRepositoryMock: ShoppingListRepository {
    var didCallListItemsFor = false
    var didCallDoneItemsFor = false
    var didCallIsIncludedInAggregatedList = false
    var didCallNumberOfItemsOn = false
    var didCallNumberOfDoneItemsOn = false
    var didCallDominantCurrencyOf = false
    var didCallDominantCurrencyOnIn = false
    var didCallTotalPriceOfAllItems = false
    var didCallTotalPriceOfAllItemsForInOn = false
    var didCallTotalPriceOfDoneItems = false
    var didCallTotalPriceOfDoneItemsForInOn = false
    var didCallDeleteShoppingList = false
    var didCallNameFor = false
    var didCallPriceFor = false
    var didCallIsListItemAvailable = false
    
    override func itemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        didCallListItemsFor = true
        return super.itemsFor(list)
    }
    
    override func doneItemsFor(_ list: ShoppingList) -> [ShoppingListItem] {
        didCallDoneItemsFor = true
        return super.doneItemsFor(list)
    }
    
    override func isListIncludedInAggregatedList(_ list: ShoppingList?) -> Bool {
        didCallIsIncludedInAggregatedList = true
        return super.isListIncludedInAggregatedList(list)
    }
    
    override func numberOfItemsOn(_ list: ShoppingList) -> Int {
        didCallNumberOfItemsOn = true
        return super.numberOfItemsOn(list)
    }
    
    override func numberOfDoneItemsOn(_ list: ShoppingList) -> Int {
        didCallNumberOfDoneItemsOn = true
        return super.numberOfDoneItemsOn(list)
    }
    
    override func dominantCurrencyOf(_ list: ShoppingList) -> String? {
        didCallDominantCurrencyOf = true
        return super.dominantCurrencyOf(list)
    }
    
    override func dominantCurrency(on list: ShoppingList, in shop: String) -> String? {
        didCallDominantCurrencyOnIn = true
        return super.dominantCurrency(on: list, in: shop)
    }
    
    override func totalPriceOfAllItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfAllItems = true
        return super.totalPriceOfAllItems(forCurrency: currency, on: list)
    }
    
    override func totalPriceOfAllItems(forCurrency currency: String, inShop shop: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfAllItemsForInOn = true
        return super.totalPriceOfAllItems(forCurrency: currency, inShop: shop, on: list)
    }
    
    override func totalPriceOfDoneItems(forCurrency currency: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfDoneItems = true
        return super.totalPriceOfDoneItems(forCurrency: currency, on: list)
    }
    
    override func totalPriceOfDoneItems(forCurrency currency: String, inShop shop: String, on list: ShoppingList) -> Double {
        didCallTotalPriceOfDoneItemsForInOn = true
        return super.totalPriceOfDoneItems(forCurrency: currency, inShop: shop, on: list)
    }
    
    override func deleteShoppingList(_ list: ShoppingList) {
        didCallDeleteShoppingList = true
        return super.deleteShoppingList(list)
    }
    
    override func nameFor(_ list: ShoppingList) -> String {
        didCallNameFor = true
        return super.nameFor(list)
    }
    
    override func isListItemAvailable(withName name: String, andShop shop: String, inList list: ShoppingList) -> Bool {
        didCallIsListItemAvailable = true
        return super.isListItemAvailable(withName: name, andShop: shop, inList: list)
    }
}

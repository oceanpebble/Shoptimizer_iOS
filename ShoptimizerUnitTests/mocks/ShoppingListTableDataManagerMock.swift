// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
@testable import Shoptimizer

class ShoppingListTableDataManagerMock: ShoppingListTableDataManager {
    var didCallGetNameOf = false
    var didCallIsAggregatedListByString = false
    var didCallIsAggregatedListByList = false
    var didCallListFor = false
    var didCallDoneFor = false
    var didCallNameFor = false
    var didCallShopNameFor = false
    var didCallBrandNameFor = false
    var didCallAmountQuantityFor = false
    var didCallAmountUnitFor = false
    var didCallPriceOf = false
    var didCallCurrencyOf = false
    var didCallIsListIncludedInAggregatedList = false
    var didCallUndoneItemsNotOnAggregatedListWithName = false
    var didCallDominantCurrency = false
    var didCallTotalPriceOfAllItems = false
    var didCallTotalPriceOfDoneItems = false
    var didCallUpdateDone = false
    var didCallDeleteItem = false
    
    override func getName(of list: ShoppingList) -> String {
        didCallGetNameOf = true
        return super.getName(of: list)
    }
    
    override func isAggregatedList(_ listName: String) -> Bool {
        didCallIsAggregatedListByString = true
        return super.isAggregatedList(listName)
    }
    
    override func isAggregatedList(_ list: ShoppingList) -> Bool {
        didCallIsAggregatedListByList = true
        return super.isAggregatedList(list)
    }
    
    override func list(for item: ShoppingListItem) -> ShoppingList {
        didCallListFor = true
        return super.list(for: item)
    }
    
    override func done(for item: ShoppingListItem) -> Bool {
        didCallDoneFor = true
        return super.done(for: item)
    }
    
    override func name(for item: ShoppingListItem) -> String {
        didCallNameFor = true
        return super.name(for: item)
    }
    
    override func shopName(for item: ShoppingListItem) -> String {
        didCallShopNameFor = true
        return super.shopName(for: item)
    }
    
    override func brandName(for item: ShoppingListItem) -> String? {
        didCallBrandNameFor = true
        return super.brandName(for: item)
    }
    
    override func amountQuantity(for item: ShoppingListItem) -> NSNumber? {
        didCallAmountQuantityFor = true
        return super.amountQuantity(for: item)
    }
    
    override func amountUnit(of item: ShoppingListItem) -> String? {
        didCallAmountUnitFor = true
        return super.amountUnit(of: item)
    }
    
    override func price(of item: ShoppingListItem) -> NSNumber? {
        didCallPriceOf = true
        return super.price(of: item)
    }
    
    override func currency(of item: ShoppingListItem) -> String? {
        didCallCurrencyOf = true
        return super.currency(of: item)
    }
    
    override func isListIncludedInAggregatedList(_ list: ShoppingList) -> Bool {
        didCallIsListIncludedInAggregatedList = true
        return super.isListIncludedInAggregatedList(list)
    }
    
    override func undoneItemsNotOnAggregatedListWithName(_ itemName: String?, shopName: String?) -> [ShoppingListItem] {
        didCallUndoneItemsNotOnAggregatedListWithName = true
        return super.undoneItemsNotOnAggregatedListWithName(itemName, shopName: shopName)
    }
    
    override func dominantCurrency(on list: ShoppingList, inShop shopName: String) -> String? {
        didCallDominantCurrency = true
        return super.dominantCurrency(on: list, inShop: shopName)
    }
    
    override func totalPriceOfAllItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        didCallTotalPriceOfAllItems = true
        return super.totalPriceOfAllItems(on: list, forCurrency: currency, inShop: shopName)
    }
    
    override func totalPriceOfDoneItems(on list: ShoppingList, forCurrency currency: String, inShop shopName: String) -> Double {
        didCallTotalPriceOfDoneItems = true
        return super.totalPriceOfDoneItems(on: list, forCurrency: currency, inShop: shopName)
    }
    
    // MARK: - Update
    
    override func update(item: ShoppingListItem, done: Bool) {
        didCallUpdateDone = true
        super.update(item: item, done: done)
    }
    
    // MARK: - Delete
    
    override func deleteItem(_ item: ShoppingListItem) {
        didCallDeleteItem = true
        super.deleteItem(item)
    }
}

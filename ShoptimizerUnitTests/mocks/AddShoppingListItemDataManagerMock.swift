// SPDX-FileCopyrightText: 2024 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
@testable import Shoptimizer

class AddShoppingListItemDataManagerMock: AddShoppingListItemDataManager {
    var didCallNameOf = false
    var didCallShopNameOf = false
    var didCallAmountUnitOf = false
    var didCallCurrencyOf = false
    var didCallBrandNameOf = false
    var didCallListFor = false
    var didCallDoneFor = false
    var didCallIsListItemAvailable = false
    var didCallIsListIncludedInAggregatedList = false
    var didCallInsertNewShoppingListItem = false
    var didCallUpdateShoppingListItem = false
    
    override func name(of item: ShoppingListItem) -> String {
        didCallNameOf = true
        return super.name(of: item)
    }
    
    override func shopName(of item: ShoppingListItem) -> String? {
        didCallShopNameOf = true
        return super.shopName(of: item)
    }
    
    override func amountUnit(of item: ShoppingListItem) -> String {
        didCallAmountUnitOf = true
        return super.amountUnit(of: item)
    }
    
    override func currency(of item: ShoppingListItem) -> String? {
        didCallCurrencyOf = true
        return super.currency(of: item)
    }
    
    override func brandName(of item: ShoppingListItem) -> String? {
        didCallBrandNameOf = true
        return super.brandName(of: item)
    }
    
    override func list(for item: ShoppingListItem?) -> ShoppingList? {
        didCallListFor = true
        return super.list(for: item)
    }
    
    override func done(for item: ShoppingListItem?) -> Bool {
        didCallDoneFor = true
        return super.done(for: item)
    }
    
    override func isListItemAvailable(withName name: String, andShop shop: String, inList list: ShoppingList) -> Bool {
        didCallIsListItemAvailable = true
        return super.isListItemAvailable(withName: name, andShop: shop, inList: list)
    }
    
    override func isListIncludedInAggregatedList(_ list: ShoppingList?) -> Bool {
        didCallIsListIncludedInAggregatedList = true
        return super.isListIncludedInAggregatedList(list)
    }
    
    override func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency: String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList) -> ShoppingListItem {
        didCallInsertNewShoppingListItem = true
        return super.insertNewShoppingListItem(withName: itemName, quantity: itemAmountQuantity, unit: itemAmountUnit, doneStatus: doneStatus, brand: brandName, forPrice: itemPrice, atCurrency: itemCurrency, inShop: shopName, notes: notes, forList: listForItem)
    }
    
    override func updateShoppingListItem(_ item: ShoppingListItem, itemName: String, shopName: String, amountQuantity: NSNumber, amountUnit: String, brandName: String, price: NSNumber, currency: String?, notes: String?) {
        didCallUpdateShoppingListItem = true
        return super.updateShoppingListItem(item, itemName: itemName, shopName: shopName, amountQuantity: amountQuantity, amountUnit: amountUnit, brandName: brandName, price: price, currency: currency, notes: notes)
    }
}

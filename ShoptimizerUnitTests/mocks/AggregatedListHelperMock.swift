// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
@testable import Shoptimizer

class AggregatedListHelperMock: AggregatedListHelper {
    var didCallUpdateAggregatedList = false
    var didCallAddItemToAggregatedList = false
    var didCallAddItemToaggregatedListByDetails = false
    var didCallSubtractItemFromAggregatedList = false
    var didCallIsAggregatedListByString = false
    var didCallIsAggregatedListByList = false
    var didCallDeleteItemsFromAggregatedListForList = false
    var didCallDeleteItemFromAggregatedList = false
    
    override func updateAggregatedList() {
        didCallUpdateAggregatedList = true;
        super.updateAggregatedList()
    }
    
    override func addItem(_ listItem: ShoppingListItem) {
        didCallAddItemToAggregatedList = true;
        super.addItem(listItem)
    }
    
    override func addItem(_ itemName: String, forShop shopName: String?, amountQuantity: NSNumber?, andAmountUnit amountUnit: String?, andBrand brandName: String?, forPrice price: NSNumber?, atCurrency currency: String?) {
        didCallAddItemToaggregatedListByDetails = true
        super.addItem(itemName, forShop: shopName, amountQuantity: amountQuantity, andAmountUnit: amountUnit, andBrand: brandName, forPrice: price, atCurrency: currency)
    }
    
    override func subtractItem(_ listItem: ShoppingListItem) {
        didCallSubtractItemFromAggregatedList = true;
        super.subtractItem(listItem)
    }
    
    override func isAggregatedList(_ listName: String) -> Bool {
        didCallIsAggregatedListByString = true
        return super.isAggregatedList(listName)
    }
    
    override func isAggregatedList(_ list: ShoppingList) -> Bool {
        didCallIsAggregatedListByList = true
        return super.isAggregatedList(list)
    }
    
    override func deleteItemsFromAggregatedListForList(_ list: ShoppingList) {
        didCallDeleteItemsFromAggregatedListForList = true
        super.deleteItemsFromAggregatedListForList(list)
    }
    
    override func deleteItem(_ item: ShoppingListItem) {
        didCallDeleteItemFromAggregatedList = true
        super.deleteItem(item)
    }
}



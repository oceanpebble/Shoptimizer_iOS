// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

@testable import Shoptimizer

class SuppliesItemRepositoryMock: SuppliesItemRepository {
    var didCallAllItems = false
    var didCallUpdateCurAmountQuantity = false
    
    override func allItems() -> [SuppliesItem] {
        didCallAllItems = true
        return super.allItems()
    }
    
    override func updateItem(_ item: SuppliesItem, curAmountQuantity: Double) {
        didCallUpdateCurAmountQuantity = true
        super.updateItem(item, curAmountQuantity: curAmountQuantity)
    }
}

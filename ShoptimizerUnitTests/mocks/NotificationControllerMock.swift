// SPDX-FileCopyrightText: 2025 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

@testable import Shoptimizer

class NotificationControllerMock: NotificationController {
    var didCallUnscheduleNotification = false
    var didCallUnscheduleAllTasks = false
    
    override func unscheduleNotification(withId id: Int) {
        didCallUnscheduleNotification = true
        super.unscheduleNotification(withId: id)
    }
    
    override func unscheduleAllTasks() {
        didCallUnscheduleAllTasks = true
        super.unscheduleAllTasks()
    }
}

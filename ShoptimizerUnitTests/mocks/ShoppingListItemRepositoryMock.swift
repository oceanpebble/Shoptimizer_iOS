// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

@testable import Shoptimizer
import CoreData

class ShoppingListItemRepositoryMock: ShoppingListItemRepository {
    
    var didCallUpdateDone = false
    var didCallDeleteItem = false
    var didCallNameFor = false
    var didCallBrandNameFor = false
    var didCallAmountQuantityFor = false
    var didCallPriceFor = false
    var didCallCurrencyFor = false
    var didCallListFor = false
    var didCallDoneFor = false
    var didCallUndoneItemsNotOnAggregatedListWithName = false
    var didCallShopNameFor = false
    var didCallAmountUnitFor = false
    var didCallInsertNewShoppingListItem = false
    var didCallUpdateShoppingListItem = false
    
    override func updateItem(_ item: ShoppingListItem, done: Bool) {
        didCallUpdateDone = true
        super.updateItem(item, done: done)
    }
    
    override func deleteItem(_ item: ShoppingListItem) {
        didCallDeleteItem = true
        super.deleteItem(item)
    }
    
    override func nameFor(_ item: ShoppingListItem) -> String {
        didCallNameFor = true
        return super.nameFor(item)
    }
    
    override func brandNameFor(_ item: ShoppingListItem) -> String? {
        didCallBrandNameFor = true
        return super.brandNameFor(item)
    }
    
    override func amountQuantityFor(_ item: ShoppingListItem) -> NSNumber? {
        didCallAmountQuantityFor = true
        return super.amountQuantityFor(item)
    }
    
    override func priceFor(_ item: ShoppingListItem) -> NSNumber? {
        didCallPriceFor = true
        return super.priceFor(item)
    }
    
    override func currencyFor(_ item: ShoppingListItem) -> String? {
        didCallCurrencyFor = true
        return super.currencyFor(item)
    }
    
    override func listFor(_ item: ShoppingListItem) -> ShoppingList {
        didCallListFor = true
        return super.listFor(item)
    }
    
    override func doneFor(_ item: ShoppingListItem) -> Bool {
        didCallDoneFor = true
        return super.doneFor(item)
    }
    
    override func undoneItemsNotOnAggregatedListWithName(_ name: String?, andShop shop: String?) -> [ShoppingListItem] {
        didCallUndoneItemsNotOnAggregatedListWithName = true
        return super.undoneItemsNotOnAggregatedListWithName(name, andShop: shop)
    }
    
    override func shopNameFor(_ item: ShoppingListItem) -> String? {
        didCallShopNameFor = true
        return super.shopNameFor(item)
    }
    
    override func amountUnitFor(_ item: ShoppingListItem) -> String? {
        didCallAmountUnitFor = true
        return super.amountUnitFor(item)
    }
    
    override func insertNewShoppingListItem(withName itemName: String, quantity itemAmountQuantity: NSNumber?, unit itemAmountUnit: String?, doneStatus: Bool, brand brandName: String?, forPrice itemPrice: NSNumber?, atCurrency itemCurrency: String?, inShop shopName: String?, notes: String? = nil, forList listForItem: ShoppingList) -> ShoppingListItem {
        didCallInsertNewShoppingListItem = true
        return super.insertNewShoppingListItem(withName: itemName, quantity: itemAmountQuantity, unit: itemAmountUnit, doneStatus: doneStatus, brand: brandName, forPrice: itemPrice, atCurrency: itemCurrency, inShop: shopName, notes: notes, forList: listForItem)
    }
    
    override func updateShoppingListItem(_ item: ShoppingListItem, itemName: String, shopName: String, amountQuantity: NSNumber, amountUnit: String, brandName: String, price: NSNumber, currency: String?, notes: String?) {
        didCallUpdateShoppingListItem = true
        super.updateShoppingListItem(item, itemName: itemName, shopName: shopName, amountQuantity: amountQuantity, amountUnit: amountUnit, brandName: brandName, price: price, currency: currency, notes: notes)
    }
}

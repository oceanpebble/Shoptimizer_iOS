// SPDX-FileCopyrightText: 2025 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class RecipeBuilder {
    private var name: String?
    private var portions: NSNumber?
    private var possibleWithSupplies: Bool?
    private var id: Int?

    private init() {
    }
    
    func with(name: String?) -> RecipeBuilder {
        self.name = name
        return self
    }
    
    func with(portions: NSNumber?) -> RecipeBuilder {
        self.portions = portions
        return self
    }
    
    func with(possibleWithSupplies: Bool?) -> RecipeBuilder {
        self.possibleWithSupplies = possibleWithSupplies
        return self
    }
    
    func with(id: Int?) -> RecipeBuilder {
        self.id = id
        return self
    }  

    static func create() -> RecipeBuilder {
        return RecipeBuilder()
    }
    
    func insert() -> Recipe {
        return Recipe.insert(name: name, portions: portions, possibleWithSupplies: possibleWithSupplies, id: id)
    }
}

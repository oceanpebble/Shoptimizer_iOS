// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class ShoppingListItemBuilder {
    private var name: String?
    private var done: Bool?
    private var amountQuantity: Double?
    private var amountUnit: String?
    private var price: Double?
    private var currency: String?
    private var shopName: String?
    private var brandName: String?
    private var notes: String?
    
    private init() {

    }

    static func create() -> ShoppingListItemBuilder {
        return ShoppingListItemBuilder()
    }

    func with(name: String?) -> ShoppingListItemBuilder {
        self.name = name
        return self
    }
    
    func with(done: Bool?) -> ShoppingListItemBuilder {
        self.done = done
        return self
    }
    
    func with(amountQuantity: Double?) -> ShoppingListItemBuilder {
        self.amountQuantity = amountQuantity
        return self
    }
    
    func with(amountUnit: String?) -> ShoppingListItemBuilder {
        self.amountUnit = amountUnit
        return self
    }
    
    func with(price: Double?) -> ShoppingListItemBuilder {
        self.price = price
        return self
    }
    
    func with(currency: String?) -> ShoppingListItemBuilder {
        self.currency = currency
        return self
    }
    
    func with(shopName: String?) -> ShoppingListItemBuilder {
        self.shopName = shopName
        return self
    }
    
    func with(brandName: String?) -> ShoppingListItemBuilder {
        self.brandName = brandName
        return self
    }
    
    func with(notes: String?) -> ShoppingListItemBuilder {
        self.notes = notes
        return self
    }

    func insert() -> ShoppingListItem {
        return ShoppingListItem.insert(name: name, done: done, amountQuantity: amountQuantity, amountUnit: amountUnit, price: price, currency: currency, shopName: shopName, brandName: brandName, notes: notes)
    }
}

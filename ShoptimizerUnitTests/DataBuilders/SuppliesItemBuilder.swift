// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class SuppliesItemBuilder {
    var name: String?
    var categories: String?
    var curAmountQuantity: Double?
    var curAmountUnit: String?

    private init() {

    }

    static func create() -> SuppliesItemBuilder {
        return SuppliesItemBuilder()
    }

    func with(name: String) -> SuppliesItemBuilder {
        self.name = name
        return self
    }

    func with(categories: String) -> SuppliesItemBuilder {
        self.categories = categories
        return self
    }
    
    func with(curAmountQuantity: Double?) -> SuppliesItemBuilder {
        self.curAmountQuantity = curAmountQuantity
        return self
    }
    
    func with(curAmountUnit: String?) -> SuppliesItemBuilder {
        self.curAmountUnit = curAmountUnit
        return self
    }

    func insertItem() -> SuppliesItem {
        return SuppliesItem.insert(name: name, minAmountQuantity: nil, minAmountUnit: nil, curAmountQuantity: NSNumber(value: curAmountQuantity ?? 0.0), curAmountUnit: curAmountUnit, categories: categories)
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class ShoppingListBuilder {
    var name: String?
    var includedInAggregatedList: Bool?

    private init() {

    }

    static func create() -> ShoppingListBuilder {
        return ShoppingListBuilder()
    }

    func with(name: String) -> ShoppingListBuilder {
        self.name = name
        return self
    }
    
    func with(includedInAggregatedList: Bool) -> ShoppingListBuilder {
        self.includedInAggregatedList = includedInAggregatedList
        return self
    }

    func insert() -> ShoppingList {
        return ShoppingList.insert(name: name, includedInAggregatedList: includedInAggregatedList)
    }
}

// SPDX-FileCopyrightText: 2025 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class TaskBuilder {
    private var done: Bool?
    private var dateCreated: NSDate?
    private var dateDue: NSDate?
    private var title: String?
    private var body: String?
    private var dateCompleted: NSDate?
    private var id: Int?

    private init() {
    }

    static func create() -> TaskBuilder {
        return TaskBuilder()
    }
   
    func with(done: Bool?) -> TaskBuilder {
        self.done = done
        return self
    }
    
    func with(dateCreated: NSDate?) -> TaskBuilder {
        self.dateCreated = dateCreated
        return self
    }
    
    func with(dateDue: NSDate?) -> TaskBuilder {
        self.dateDue = dateDue
        return self
    }
    
    func with(title: String?) -> TaskBuilder {
        self.title = title
        return self
    }
    
    func with(body: String?) -> TaskBuilder {
        self.body = body
        return self
    }
    
    func with(dateCompleted: NSDate?) -> TaskBuilder {
        self.dateCompleted = dateCompleted
        return self
    }
    
    func with(id: Int?) -> TaskBuilder {
        self.id = id
        return self
    }

    func insert() -> Task {
        return Task.insert(title: title, body: body, dateDue: dateDue,  dateCreated: dateCreated,  dateCompleted: dateCompleted, done: done, id: id)
    }
}

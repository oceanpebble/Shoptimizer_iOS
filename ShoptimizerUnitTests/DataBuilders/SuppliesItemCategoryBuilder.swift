// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
@testable import Shoptimizer

class SuppliesItemCategoryBuilder {
    var name: String?

    private init() {

    }

    static func create() -> SuppliesItemCategoryBuilder {
        return SuppliesItemCategoryBuilder()
    }

    func with(name: String) -> SuppliesItemCategoryBuilder {
        self.name = name
        return self
    }

    func insertCategory() {
        let _ = SuppliesItemCategory.insert(name: name)
    }
}

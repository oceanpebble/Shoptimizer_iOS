// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class SuppliesItemCategoryTests: ShoptimizerTestCase {
    
    override func setUp() {
        super.setUp()
        SuppliesItemCategory.deleteAll()
    }

    func test_FindNoCategories_WhenNoCatgoriesPresent() {
        let categories = SuppliesItemCategory.categories(for: "Category1")
        XCTAssertTrue(categories.isEmpty)
    }

    func test_findAllCategories_WithEmptySearchString() {
        SuppliesItemCategoryBuilder.create().with(name: "Category1").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category2").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "")
        XCTAssertTrue(categories.count == 2)
    }

    func test_findAllCategories_WithOneCharacterSearchString() {
        SuppliesItemCategoryBuilder.create().with(name: "Category1").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category2").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "X")
        XCTAssertTrue(categories.count == 2)
    }

    func test_FindNoCategories_WhenCategoryNotAvailable() {
        SuppliesItemCategoryBuilder.create().with(name: "Category1").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "Category2")
        XCTAssertTrue(categories.isEmpty)
    }

    func test_FindCategory_WithFullNameWithoutSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category1").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category2").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "Category1")
        XCTAssertFalse(categories.isEmpty)
        XCTAssertTrue(categories.first!.name == "Category1")
    }

    func test_FindCategory_WithPartialNameWithoutSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category1").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category2").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "gory1")
        XCTAssertFalse(categories.isEmpty)
        XCTAssertTrue(categories.first!.name == "Category1")
    }

    func test_FindCategory_WithFullNameWithSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category One").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category Two").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "Category One")
        XCTAssertFalse(categories.isEmpty)
        XCTAssertTrue(categories.first!.name == "Category One")
    }

    func test_FindCategory_WithPartialNameWithSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category One").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Category Two").insertCategory()
        let categories = SuppliesItemCategory.categories(for: "gory On")
        XCTAssertFalse(categories.isEmpty)
        XCTAssertTrue(categories.first!.name == "Category One")
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class SuppliesItemRepositoryTest: ShoptimizerTestCase {
    
    private var repository: SuppliesItemRepository!
    
    override func setUp() {
        super.setUp()
        SuppliesItem.deleteAll()
        repository = SuppliesItemRepository()
    }
    
    // MARK: - All items
    
    func test_givenNoSuppliesItems_whenAllSuppliesItems_thenReturnEmptyArray() {
        let items = repository.allItems()
        XCTAssertEqual(items, [])
    }
    
    func test_givenOneSuppliesItem_whenAllSuppliesItems_thenReturnArrayWithThatOneItem() {
        let item = SuppliesItemBuilder.create().with(name: "foo supplies").insertItem()
        let items = repository.allItems()
        XCTAssertEqual(items, [item])
    }
    
    func test_givenTwoSuppliesItems_whenAllSuppliesItems_thenReturnArrayWithThoseTwoItems() {
        let item1 = SuppliesItemBuilder.create().with(name: "foo supplies 1").insertItem()
        let item2 = SuppliesItemBuilder.create().with(name: "foo supplies 2").insertItem()
        
        let items = repository.allItems().sorted(by: {$0.itemName! < $1.itemName!})
        
        XCTAssertEqual(items, [item1, item2])
    }
    
    // MARK: - Update supplies item - amount quantity
    
    func test_givenSuppliesItem_whenUpdateAmountQuantity_thenTheSuppliesItemHasTheNewAmountQuantity() {
        let item = SuppliesItemBuilder.create().with(name: "foo item").insertItem()
        repository.updateItem(item, curAmountQuantity: 1.0)
        XCTAssertEqual(item.curAmountQuantity, 1.0)
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class ShoppingListItemRepositoryTest: ShoptimizerTestCase {
    
    private var repository: ShoppingListItemRepository!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        repository = ShoppingListItemRepository()
    }
    
    // MARK: - Update shopping list item - done
    
    func test_givenShoppingListItem_whenUpdatDone_thenTheShoppingListItemHasTheNewDone() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        repository.updateItem(item, done: true)
        XCTAssertEqual(item.done, true)
    }
    
    // MARK: - Delete shopping list item
    
    func test_givenShoppingListItem_whenDeleteShoppingListItem_thenTheItemIsDeleted() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        
        repository.deleteItem(item)

        XCTAssertEqual(ShoppingListItem.allItems()!.count, 0)
    }
    
    // MARK: - Name for
    
    func test_givenShoppingListItem_whenNameFor_thenReturnTheName() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let name = repository.nameFor(item)
        XCTAssertEqual(name, "foo item")
    }
    
    // MARK: - Shop name for
    
    func test_givenShoppingListItemWithShopName_whenShopNameFor_thenReturnTheShopName() {
        let item = ShoppingListItemBuilder.create().with(shopName: "foo shop").insert()
        let name = repository.shopNameFor(item)
        XCTAssertEqual(name, "foo shop")
    }
    
    func test_givenShoppingListItemWithoutShopName_whenShopNameFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let name = repository.shopNameFor(item)
        XCTAssertEqual(name, nil)
    }
    
    // MARK: - Amount quantity for
    
    func test_givenShoppingListItemWithAmountQuantity_whenAmountQuantityFor_thenReturnTheAmountQuantity() {
        let item = ShoppingListItemBuilder.create().with(amountQuantity: 13.37).insert()
        let quantity = repository.amountQuantityFor(item)
        XCTAssertEqual(quantity, 13.37)
    }
    
    func test_givenShoppingListItemWithoutAmountQuantity_whenAmountQuantityFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let quantity = repository.amountQuantityFor(item)
        XCTAssertEqual(quantity, nil)
    }
    
    // MARK: - Amount unit for
    
    func test_givenShoppingListItemWithAmountUnit_whenAmountUnitFor_thenReturnTheAmountUnit() {
        let item = ShoppingListItemBuilder.create().with(amountUnit: "kg").insert()
        let unit = repository.amountUnitFor(item)
        XCTAssertEqual(unit, "kg")
    }
    
    func test_givenShoppingListItemWithoutAmountUnit_whenAmountUnitFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let unit = repository.amountUnitFor(item)
        XCTAssertEqual(unit, nil)
    }
    
    // MARK: - Done for
    
    func test_givenAnUndoneShoppingListItem_whenDoneFor_thenReturnFalse() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        let done = repository.doneFor(item)
        XCTAssertEqual(done, false)
    }
    
    func test_givenADoneShoppingListItem_whenDoneFor_thenReturnTrue() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        let done = repository.doneFor(item)
        XCTAssertEqual(done, true)
    }
    
    func test_givenShoppingListWithoutDone_whenDoneFor_thenReturnFalss() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: nil).insert()
        let done = repository.doneFor(item)
        XCTAssertEqual(done, false)
    }
    
    // MARK: - Brand name for
    
    func test_givenShoppingListItemWithBrandName_whenBrandNameFor_thenReturnTheBrandName() {
        let item = ShoppingListItemBuilder.create().with(brandName: "foo brand").insert()
        let name = repository.brandNameFor(item)
        XCTAssertEqual(name, "foo brand")
    }
    
    func test_givenShoppingListItemWithoutBrandName_whenBrandNameFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let name = repository.brandNameFor(item)
        XCTAssertEqual(name, nil)
    }
    
    // MARK: - Price for
    
    func test_givenShoppingListItemWithPrice_whenPriceFor_thenReturnThePrice() {
        let item = ShoppingListItemBuilder.create().with(price: 8.15).insert()
        let price = repository.priceFor(item)
        XCTAssertEqual(price, 8.15)
    }
    
    func test_givenShoppingListItemWithoutPrice_whenPriceFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let price = repository.priceFor(item)
        XCTAssertEqual(price, nil)
    }
    
    // MARK: - Currency for
    
    func test_givenShoppingListItemWithCurrency_whenCurrencyFor_thenReturnTheCurrency() {
        let item = ShoppingListItemBuilder.create().with(currency: "EUR").insert()
        let currency = repository.currencyFor(item)
        XCTAssertEqual(currency, "EUR")
    }
    
    func test_givenShoppingListItemWithoutCurrency_whenCurrencyFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let currency = repository.currencyFor(item)
        XCTAssertEqual(currency, nil)
    }
    
    // MARK: - Notes for
    
    func test_givenShoppingListItemWithNotes_whenNotesFor_thenReturnTheNotes() {
        let item = ShoppingListItemBuilder.create().with(notes: "foo notes").insert()
        let notes = repository.notesFor(item)
        XCTAssertEqual(notes, "foo notes")
    }
    
    func test_givenShoppingListItemWithoutNotes_whenNotesFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let notes = repository.currencyFor(item)
        XCTAssertEqual(notes, nil)
    }
    
    // MARK: - List for item
    
    func test_givenOneShoppingListAndAnItemFromThatShoppingList_whenListForItem_thenReturnThatShoppingList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let listForItem = repository.listFor(item)
        
        XCTAssertEqual(listForItem, list)
    }
    
    func test_givenTwoShoppingListAndAnItemFromOneOfTheShoppingLists_whenListForItem_thenReturnTheCorrectShoppingList() {
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let listForItem = repository.listFor(item1)
        
        XCTAssertEqual(listForItem, list1)
    }
    
    // MARK: - Undone items with name
    
    func test_givenNoShoppingLists_whenUndoneItemsNotOnAggregatedListWithName_thenReturnEmptyArray() {
        let _ = insertAggregatedList()
        let items = repository.undoneItemsNotOnAggregatedListWithName("", andShop: "")
        XCTAssertEqual(items, [])
    }
    
    func test_givenShoppingListWithNoItems_whenUndoneItemsNotOnAggregatedListWithName_thenReturnEmptyArray() {
        let _ = insertAggregatedList()
        let _ = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("", andShop: "")
        
        XCTAssertEqual(items, [])
    }
    
    func test_givenShoppingListWithOneDoneItem_whenUndoneItemsNotOnAggregatedListWithName_thenReturnEmptyArray() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("", andShop: "")
        
        XCTAssertEqual(items, [])
    }
    
    func test_givenShoppingListWithOneUndoneItemWithDifferentName_whenUndoneItemsNotOnAggregatedListWithName_thenReturnEmptyArray() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("", andShop: "")
        
        XCTAssertEqual(items, [])
    }
    
    func test_givenShoppingListWithOneUndoneItemWithFittingName_whenUndoneItemsNotOnAggregatedListWithName_thenReturnThatOneItem() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("foo item", andShop: nil)
        
        XCTAssertEqual(items, [item])
    }
    
    func test_givenShoppingListWithOneDoneItemAndOneUndoneItemWitDifferentName_whenUndoneItemsNotOnAggregatedListWithName_thenReturnEmptyArray() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("", andShop: "")
        
        XCTAssertEqual(items, [])
    }
    
    func test_givenShoppingListWithOneDoneItemAndOneUndoneItemWitFittingName_whenUndoneItemsNotOnAggregatedListWithName_thenReturThatOneItem() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("foo item 1", andShop: "")
        
        XCTAssertEqual(items, [])
    }
    
    func test_givenTwoShoppingListsWithEachOneUndoneItemWithFittingName_whenUndoneItemsNotOnAggregatedListWithName_thenReturnThoseTwoItems() {
        let _ = insertAggregatedList()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("foo item", andShop: nil)
        
        XCTAssertEqual(items, [item1, item2])
    }
    
    func test_givenShoppingListWithTwoUndoneItemsWithFittingNamesInTwoStores_whenUndoneItemsNotOnAggregatedListWithName_thenReturnOnlyOneItem() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 1").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 2").with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.undoneItemsNotOnAggregatedListWithName("foo item", andShop: "foo shop 1")
        
        XCTAssertEqual(items, [item1])
    }
    
    // MARK: - Insert new shopping list item
    
    func test_givenDetailsOfShoppingListItem_whenInsertNewShoppingListItem_thenTheItemIsSaved() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let insertedItem = repository.insertNewShoppingListItem(withName: "foo item", quantity: NSNumber(value: 1), unit: "kg", doneStatus: true, brand: "foo brand", forPrice: NSNumber(value: 2), atCurrency: "EUR", inShop: "foo shop", notes: "foo notes", forList: list)
        
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(insertedItem.itemName, "foo item")
        XCTAssertEqual(insertedItem.itemAmountQuantity, 1)
        XCTAssertEqual(insertedItem.itemAmountUnit, "kg")
        XCTAssertEqual(insertedItem.done, NSNumber(value: true))
        XCTAssertEqual(insertedItem.brandName, "foo brand")
        XCTAssertEqual(insertedItem.itemPrice, 2)
        XCTAssertEqual(insertedItem.itemCurrency, "EUR")
        XCTAssertEqual(insertedItem.shopName, "foo shop")
        XCTAssertEqual(insertedItem.notes, "foo notes")
        XCTAssertEqual(insertedItem.listForItem, list)
        XCTAssertEqual(ShoppingListItem.allItems()?.count, 1)
    }
    
    // MARK: - Update shopping list item
    
    func test_givenAnyDetails_whenUpdateShoppingListItem_thenTheItemsIsUpdated() {
        let item = ShoppingListItemBuilder.create().with(name: "fubar").insert()
        repository.updateShoppingListItem(item, itemName: "foo item", shopName: "foo shop", amountQuantity: NSNumber(value: 1), amountUnit: "kg", brandName: "foo brand", price: NSNumber(value: 2), currency: "EUR", notes: "foo notes")
        
        XCTAssertEqual(item.itemName, "foo item")
        XCTAssertEqual(item.itemAmountQuantity, 1)
        XCTAssertEqual(item.itemAmountUnit, "kg")
        XCTAssertEqual(item.brandName, "foo brand")
        XCTAssertEqual(item.itemPrice, 2)
        XCTAssertEqual(item.itemCurrency, "EUR")
        XCTAssertEqual(item.shopName, "foo shop")
        XCTAssertEqual(item.notes, "foo notes")
    }
}

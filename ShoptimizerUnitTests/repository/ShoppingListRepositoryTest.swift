// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class ShoppingListRepositoryTest: ShoptimizerTestCase {
    
    private var repository: ShoppingListRepository!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        repository = ShoppingListRepository()
    }
    
    // MARK: - Items for
    
    func test_givenEmptyList_whenItemsFor_thenReturnEmptyArray() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let items = repository.itemsFor(list)
        XCTAssertEqual(items, [])
    }
    
    func test_givenListWithOneItem_whenItemsFor_thenReturnArrayWithThatOneItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let items = repository.itemsFor(list)
        
        XCTAssertEqual(items, [item])
    }
    
    func test_givenListWithTwoItems_whenItemsFor_thenReturnArrayWithThoseTwoItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.itemsFor(list)
        
        let sortedItems = items.sorted(by: {$0.itemName! < $1.itemName!})
        XCTAssertEqual(sortedItems, [item1, item2])
    }
    
    // MARK: - Done items for
    
    func test_givenEmptyList_whenDoneItemsFor_thenReturnEmptyArray() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let items = repository.doneItemsFor(list)
        XCTAssertEqual(items, [])
    }
    
    func test_givenListWithOneDoneItem_whenDoneItemsFor_thenReturnArrayWithThatOneItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let items = repository.doneItemsFor(list)
        
        XCTAssertEqual(items, [item])
    }
    
    func test_givenListWithTwoDoneItems_whenDoneItemsFor_thenReturnArrayWithThoseTwoItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.doneItemsFor(list).sorted(by: {$0.itemName! < $1.itemName!})
        
        XCTAssertEqual(items, [item1, item2])
    }
    
    func test_givenListWithOneDoneItemAndOneUndoneItem_whenDoneItemsFor_thenReturnArrayWithTheDoneItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let items = repository.doneItemsFor(list)
        
        XCTAssertEqual(items, [item1])
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenNilList_whenIsListIncludedInAggregatedList_thenReturnFalse() {
        let included = repository.isListIncludedInAggregatedList(nil)
        XCTAssertEqual(included, false)
    }
    
    func test_givenListNotIncluded_whenIsListIncludedInAggregatedList_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let included = repository.isListIncludedInAggregatedList(list)
        XCTAssertEqual(included, false)
    }
    
    func test_givenListIncluded_whenIsListIncludedInAggregatedList_thenReturnTrue() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let included = repository.isListIncludedInAggregatedList(list)
        XCTAssertEqual(included, true)
    }
    
    func test_givenListWithIncludedNil_whenIsListIncludedInAggregatedList_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        list.setValue(nil, forKey: ShoppingListEntity.COL_INCLUDE_IN_AGGREGATE)
        DBHelper.instance.saveContext()
        
        let included = repository.isListIncludedInAggregatedList(list)
        
        XCTAssertEqual(included, false)
    }
    
    // MARK: - Name for
    
    func test_givenAnyList_whenNameFor_thenReturnTheListsName() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let name = repository.nameFor(list)
        XCTAssertEqual(name, "foo list")
    }
    
    // MARK: - Number of items on
    
    func test_givenEmptyList_whenNumberOfItemsOn_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let numberOfItems = repository.numberOfItemsOn(list)
        XCTAssertEqual(numberOfItems, 0)
    }
    
    func test_givenListWithOneItem_whenNumberOfItemsOn_thenReturnOne() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfItems = repository.numberOfItemsOn(list)
        
        XCTAssertEqual(numberOfItems, 1)
    }
    
    func test_givenListWithTwoItems_whenNumberOfItemsItemsOn_thenReturnTwo() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfItems = repository.numberOfItemsOn(list)
        
        XCTAssertEqual(numberOfItems, 2)
    }
    
    // MARK: - Number of done items on
    
    func test_givenEmptyList_whenNumberOfDoneItemsFor_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let numberOfDoneItems = repository.numberOfDoneItemsOn(list)
        XCTAssertEqual(numberOfDoneItems, 0)
    }
    
    func test_givenListWithOneDoneItem_whenNumberOfDoneItemsFor_thenReturnOne() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfDoneItems = repository.numberOfDoneItemsOn(list)
        
        XCTAssertEqual(numberOfDoneItems, 1)
    }
    
    func test_givenListWithTwoDoneItems_whenNumberOfDoneItemsFor_thenReturnTwo() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfDoneItems = repository.numberOfDoneItemsOn(list)
        
        XCTAssertEqual(numberOfDoneItems, 2)
    }
    
    func test_givenListWithOneDoneItemAndOneUndoneItem_whenNumberOfDoneItemsFor_thenReturnOne() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfDoneItems = repository.numberOfDoneItemsOn(list)
        
        XCTAssertEqual(numberOfDoneItems, 1)
    }
    
    // MARK: - Dominant ourrency of
    
    func test_givenEmptyList_whenDominantCurrencyOf_thenReturnNil() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let dominantCurrency = repository.dominantCurrencyOf(list)
        XCTAssertEqual(dominantCurrency, nil)
    }
    
    func test_givenListWithOneItemWithoutCurrencyOf_whenDominantCurrencyOf_thenReturnNil() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrencyOf(list)
        
        XCTAssertEqual(dominantCurrency, nil)
    }
    
    func test_givenListWithOneItemWithCurrency_whenDominantCurrencyOf_thenReturnThatCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrencyOf(list)
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    func test_givenListWithOneItemWithCurrencyAndOneItemWithoutCurrency_whenDominantCurrencyOf_thenReturnThatCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(currency: "EUR").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrencyOf(list)
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    func test_givenListWithTwoItemsWithSameCurrency_whenDominantCurrencyOf_thenReturnThatCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(currency: "EUR").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(currency: "EUR").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrencyOf(list)
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    func test_givenListWithTwoItemsWithDifferentCurrencies_whenDominantCurrencyOf_thenReturnTheOneThatIsLexicographicallySmaller() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(currency: "EUR").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(currency: "USD").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrencyOf(list)
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    // MARK: - Total price of all items
    
    func test_givenEmptyList_whenTotalPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithoutCurrency_whenTotalPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithFittingCurrencyButNoPrice_whenTotalPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithFittingCurrencyAndPrice_whenTotalPriceOfAllItems_thenReturnThatOnePrice() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.99)
    }
    
    func test_givenListWithTwoItemsWithFittingCurrencyAndPrice_whenTotalPriceOfAllItems_thenReturnTheSumOfTheTwoPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(currency: "EUR").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 1.48)
    }
    
    func test_givenListWithOneItemWithNonFittingCurrencyAndPrice_whenTotalPriceOfAllItems_thenReturnZeroe() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "USD").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.00)
    }
    
    func test_givenListWithTwoItemsOneWithWithFittingCurrencyOneWithNonFittingCurrencyAndPrice_whenTotalPriceOfAllItems_thenReturnTheCorrectSinglePrice() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(currency: "USD").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.49)
    }
    
    // MARK: - Total price of done items
    
    func test_givenEmptyList_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithUndonOneItemWithoutCurrency_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemWithFittingCurrencyButNoPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(currency: "EUR").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemWithFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(currency: "EUR").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithDonOneItemWithoutCurrency_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemWithFittingCurrencyButNoPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(currency: "EUR").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemWithFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnThatOnePrice() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(currency: "EUR").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.99)
    }
    
    func test_givenListWithTwoUndoneItemsWithFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "EUR").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithTwoDoneItemsWithFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnTheSumOfTheTwoPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).with(currency: "EUR").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 1.48)
    }
    
    func test_givenListWithOneUndoneAndOneDoneItemWithFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnThePriceOfTheDoneItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).with(currency: "EUR").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.99)
    }
    
    func test_givenListWithOneUndoneItemWithNonFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(currency: "USD").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.00)
    }
    
    func test_givenListWithOneDoneItemWithNonFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(currency: "USD").with(price: 0.99).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.00)
    }
    
    func test_givenListWithTwoUndoneItemsOneWithWithFittingCurrencyOneWithNonFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "USD").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithTwoDoneItemsOneWithWithFittingCurrencyOneWithNonFittingCurrencyAndPrice_whenTotalPriceOfDoneItems_thenReturnTheCorrectSinglePrice() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).with(currency: "USD").with(price: 0.99).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "EUR").with(price: 0.49).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        
        XCTAssertEqual(price, 0.49)
    }
    
    // MARK: - Delete shopping list
    
    func test_givenShoppingList_whenDeleteShoppingList_thenTheListIsDeleted() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        repository.deleteShoppingList(list)
        XCTAssertEqual(ShoppingList.shoppingLists().count, 0)
    }
    
    // MARK: - Dominant currency
    
    func test_givenEmptyShoppingList_whenDominantCurrency_thenReturnNil() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let dominantCurrency = repository.dominantCurrency(on: list, in: "")
        XCTAssertEqual(dominantCurrency, nil)
    }
    
    func test_givenShoppingListWithItemInDifferentShop_whenDominantCurrency_thenReturnNil() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrency(on: list, in: "")
        
        XCTAssertEqual(dominantCurrency, nil)
    }
    
    func test_givenShoppingListWithItemInFittingShop_whenDominantCurrency_thenReturnThatItemsCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrency(on: list, in: "foo shop")
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    func test_givenShoppingListWithTwoItemsInTwoDifferentShops_whenDominantCurrency_thenReturnCorrectCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 1").with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 2").with(currency: "USD").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrency(on: list, in: "foo shop 2")
        
        XCTAssertEqual(dominantCurrency, "USD")
    }
    
    func test_givenShoppingListWithThreeItemsInFittingShops_whenDominantCurrency_thenReturnCurrencyOccuringMOreOften() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").insert()
        let item3 = ShoppingListItemBuilder.create().with(name: "foo item 3").with(shopName: "foo shop").with(currency: "USD").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        list.addToItemsForList(item3)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrency(on: list, in: "foo shop")
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    func test_givenShoppingListWithTwotemsInFittingShops_whenDominantCurrency_thenReturnTheLexicographicallySmallerCurrency() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "USD").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let dominantCurrency = repository.dominantCurrency(on: list, in: "foo shop")
        
        XCTAssertEqual(dominantCurrency, "EUR")
    }
    
    // MARK: - Total price of all items
    
    func test_givenEmptyShoppingList_whenPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let price = repository.totalPriceOfAllItems(forCurrency: "", inShop: "", on: list)
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemNotFittingCriteria_whenPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithFittingShopButNonFittingCurrency_whenPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithFittingCurrencyButNonFittingShop_whenPriceOfAllItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneItemWithFittingShopAndgCurrency_whenPriceOfAllItems_thenReturnThePriceOfThatItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 13.37)
    }
    
    func test_givenListWithTwoItemsWithFittingShopAndgCurrency_whenPriceOfAllItems_thenReturnTheSumOfThePrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 21.52)
    }
    
    func test_givenListWithTwoItemsOneFittingOneNonFitting_whenPriceOfAllItems_thenReturnThePriceOfTheFittingItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "USD").with(price: 08.15).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let price = repository.totalPriceOfAllItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 13.37)
    }
    
    // MARK: - Total price of done items
    
    func test_givenEmptyShoppingList_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let price = repository.totalPriceOfDoneItems(forCurrency: "", inShop: "", on: list)
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemNotFittingCriteria_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: false).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemNotFittingCriteria_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemWithFittingShopButNonFittingCurrency_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: false).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemWithFittingShopButNonFittingCurrency_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemWithFittingCurrencyButNonFittingShop_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: false).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemWithFittingCurrencyButNonFittingShop_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneUndoneItemWithFittingShopAndCurrency_whenPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: false).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 0.0)
    }
    
    func test_givenListWithOneDoneItemWithFittingShopAndCurrency_whenPriceOfDoneItems_thenReturnThePriceOfThatItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        list.addToItemsForList(item)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 13.37)
    }
    
    func test_givenListWithTwoDoneItemsBothFittingCurrencyOneNotFittingShop_whenPriceOfDoneItems_thenReturnThePriceOfTheFullyFittingItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop bar").with(currency: "EUR").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 13.37)
    }
    
    func test_givenListWithTwoDoneItemsBothFittingShopOneNotFittingCurrency_whenPriceOfDoneItems_thenReturnThePriceOfTheFullyFittingItem() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "USD").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 08.15)
    }
    
    func test_givenListWithTwoDoneItemsBothFitting_whenPriceOfDoneItems_thenReturnTheSumOfThePricesOfBothItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let price = repository.totalPriceOfDoneItems(forCurrency: "EUR", inShop: "foo shop", on: list)
        
        XCTAssertEqual(price, 21.52)
    }
    
    // MARK: - Is list item available
    
    func test_givenEmptyList_whenIsListItemAvailable_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let available = repository.isListItemAvailable(withName: "foo item", andShop: "foo shop", inList: list)
        XCTAssertEqual(available, false)
    }
    
    func test_givenListwithNonFittingItem_whenIsListItemAvailable_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").insert()
        list.addToItemsForList(item)
        
        let available = repository.isListItemAvailable(withName: "foo", andShop: "bar", inList: list)
        
        XCTAssertEqual(available, false)
    }
    
    func test_givenListWithItemWithFittingName_whenIsListItemAVailable_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").insert()
        list.addToItemsForList(item)
        
        let available = repository.isListItemAvailable(withName: "foo item", andShop: "bar", inList: list)
        
        XCTAssertEqual(available, false)
    }
    
    func test_givenListWithItemWithFittingShopName_whenIsListItemAVailable_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").insert()
        list.addToItemsForList(item)
        
        let available = repository.isListItemAvailable(withName: "foo", andShop: "foo shop", inList: list)
        
        XCTAssertEqual(available, false)
    }
    
    func test_givenListWithFittingItem_whenIsListItemAVailable_thenReturnTrue() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").insert()
        list.addToItemsForList(item)
        
        let available = repository.isListItemAvailable(withName: "foo item", andShop: "foo shop", inList: list)
        
        XCTAssertEqual(available, true)
    }
}

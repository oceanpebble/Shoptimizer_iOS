// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest
@testable import Shoptimizer

class ShoptimizerTestCase: XCTestCase {
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
    }
    
    // MARK: - Util functions
    
    func insertAggregatedList() -> ShoppingList {
        let list = ShoppingList.insertNewShoppingList(withName: AggregatedListHelper.instance.nameOfAggregatedList(), andType: "Static", includeInAggregateList: false, intoContext: DBHelper.instance.getManagedObjectContext())!
        DBHelper.instance.saveContext()
        return list
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class ShoppingListTableDataManagerTest: ShoptimizerTestCase {
    
    private var aggregatedListHelperMock: AggregatedListHelperMock!
    private var shoppingListRepositoryMock: ShoppingListRepositoryMock!
    private var shoppingListItemRepositoryMock: ShoppingListItemRepositoryMock!
    private var dataManager: ShoppingListTableDataManager!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        aggregatedListHelperMock = AggregatedListHelperMock()
        shoppingListRepositoryMock = ShoppingListRepositoryMock()
        shoppingListItemRepositoryMock = ShoppingListItemRepositoryMock()
        dataManager = ShoppingListTableDataManager(aggregatedListHelper: aggregatedListHelperMock, shoppingListRepository: shoppingListRepositoryMock, shoppingListItemRepository: shoppingListItemRepositoryMock)
    }

    // MARK: - Get name of
    
    func test_givenNormalList_whenGetNameOf_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let _ = dataManager.getName(of: list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallNameFor, true)
    }
    
    // MARK: - Is aggregated list
    
    func test_givenAnyString_whenIsAggregatedListByString_thenTheHelperFunctionIsCalled() {
        let _ = dataManager.isAggregatedList("")
        XCTAssertEqual(aggregatedListHelperMock.didCallIsAggregatedListByString, true)
    }
    
    func test_givenAnyList_whenIsAggregatedListByList_thenTheHelperFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.isAggregatedList(list)
        XCTAssertEqual(aggregatedListHelperMock.didCallIsAggregatedListByList, true)
    }
    
    // MARK: - List for item
    
    func test_givenAnyShoppingListItem_whenListForItem_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let _ = dataManager.list(for: item)
        
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallListFor, true)
    }
    
    // MARK: - Done for
    
    func test_givenAnyShoppingListItem_whenDoneFor_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.done(for: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallDoneFor, true)
    }
    
    // MARK: - Name for
    
    func test_givenAnyShoppingListItem_whenNameFor_thenTheRepositoryFunctionIsCall() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = dataManager.name(for: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallNameFor, true)
    }
    
    // MARK: - Shop name for
    
    func test_givenAShoppingListItemWithoutShopName_whenShopNameFor_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().insert()
        let shopName = dataManager.shopName(for: item)
        XCTAssertEqual(shopName, "")
    }
    
    func test_givenAShoppingListItemWithShopName_whenShopNameFor_thenReturnTheShopName() {
        let item = ShoppingListItemBuilder.create().with(shopName: "foo shop").insert()
        let shopName = dataManager.shopName(for: item)
        XCTAssertEqual(shopName, "foo shop")
    }
    
    // MARK: - Brand name for
    
    func test_givenAnyShoppingListItem_whenBrandNameFor_thenTheRepositoryFunctionIsCall() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.brandName(for: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallBrandNameFor, true)
    }
    
    // MARK: - Amount quantity for
    
    func test_givenAnyShoppingListItem_whenAmountQuantityFor_thenTheRepositoryFunctionIsCall() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.amountQuantity(for: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallAmountQuantityFor, true)
    }
    
    // MARK: - Amount unit of
    
    func test_givenShoppingListItemWithAmountUnit_whenAmountUnitOf_thenReturnTheAmountUnit() {
        let item = ShoppingListItemBuilder.create().with(amountUnit: "kg").insert()
        let amountUnit = dataManager.amountUnit(of: item)
        XCTAssertEqual(amountUnit, "kg")
    }
    
    func test_givenShoppingListItemWithoutAmountUnit_whenAmountUnitOf_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().insert()
        let amountUnit = dataManager.amountUnit(of: item)
        XCTAssertEqual(amountUnit, nil)
    }
    
    // MARK: - Price of
    
    func test_givenAnyShoppingListItem_whenPriceOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.price(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallPriceFor, true)
    }
    
    // MARK: - Price of
    
    func test_givenAnyShoppingListItem_whenCurrencyOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.currency(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallCurrencyFor, true)
    }
    
    // MARK: - Update item done
    
    func test_givenAnyShoppingListItem_whenUpdateItemDone_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.update(item: item, done: true)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallUpdateDone, true)
    }
    
    // MARK: - Is list included in Aggregated List
    
    func test_givenAnyShoppingList_whenIsListIncludedInAggregatedList_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.isListIncludedInAggregatedList(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallIsIncludedInAggregatedList, true)
    }
    
    // MARK: - Undone items with name
    
    func test_givenAnyShoppingList_whenUndoneItemsNotOnAggregatedListWithName_thenTheRepositoryFunctionIsCalled() {
        let _ = insertAggregatedList()
        let _ = dataManager.undoneItemsNotOnAggregatedListWithName("", shopName: "")
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallUndoneItemsNotOnAggregatedListWithName, true)
    }
    
    // MARK: - Dominant currency
    
    func test_givenAnyShoppingList_whenDominantCurrency_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.dominantCurrency(on: list, inShop: "")
        XCTAssertEqual(shoppingListRepositoryMock.didCallDominantCurrencyOnIn, true)
    }
    
    // MARK: - Total price of all items
    
    func test_givenAnyShoppingList_whenTotalPriceOfAllItems_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.totalPriceOfAllItems(on: list, forCurrency: "", inShop: "")
        XCTAssertEqual(shoppingListRepositoryMock.didCallTotalPriceOfAllItemsForInOn, true)
    }
    
    // MARK: - Total price of done items
    
    func test_givenAnyShoppingList_whenPriceOfDoneItems_thenTherepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.totalPriceOfDoneItems(on: list, forCurrency: "", inShop: "")
        XCTAssertEqual(shoppingListRepositoryMock.didCallTotalPriceOfDoneItemsForInOn, true)
    }
    
    // MARK: - Delete done
    
    func test_givenAnyShoppingListItem_whenDaleteItem_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.deleteItem(item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallDeleteItem, true)
    }
}

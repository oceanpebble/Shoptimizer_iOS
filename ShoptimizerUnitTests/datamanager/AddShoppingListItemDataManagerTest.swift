// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class AddShoppingListItemDataManagerTest: ShoptimizerTestCase {
    
    private var dataManager: AddShoppingListItemDataManager!
    private var shoppingListRepositoryMock: ShoppingListRepositoryMock!
    private var shoppingListItemRepositoryMock: ShoppingListItemRepositoryMock!
    
    override func setUp() {
        ShoppingListItem.deleteAll()
        shoppingListRepositoryMock = ShoppingListRepositoryMock()
        shoppingListItemRepositoryMock = ShoppingListItemRepositoryMock()
        dataManager = AddShoppingListItemDataManager(shoppingListRepository: shoppingListRepositoryMock, shoppingListItemRepository: shoppingListItemRepositoryMock)
    }
    
    // MARK: - Name of
    
    func test_givenAnyShoppingListItem_whenNameOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = dataManager.name(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallNameFor, true)
    }
    
    // MARK: - Shop name of
    
    func test_givenAnyShoppingListItem_whenShopNameOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.shopName(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallShopNameFor, true)
    }
    
    // MARK: - Amount unit of
    
    func test_givenShoppingListItemWithoutAmountUnit_whenAmountUnitOf_thenReturnUnitG() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let unit = dataManager.amountUnit(of: item)
        XCTAssertEqual(unit, "g")
    }
    
    func test_givenAnyShoppingListItemWithAmountUnit_whenAmountUnitOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(amountUnit: "kg").insert()
        let _ = dataManager.amountUnit(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallAmountUnitFor, true)
    }
    
    // MARK: - Amount quantity of
    
    func test_givenAnyShoppingListItem_whenAmountQuantityOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _  = dataManager.amountQuantity(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallAmountQuantityFor, true)
    }
    
    // MARK: - Currency of
    
    func test_givenAnyShoppingListItem_whenCurrencyOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.currency(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallCurrencyFor, true)
    }
    
    // MARK: - Price of
    
    func test_givenAnyShoppingListItem_whenPriceOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = dataManager.price(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallPriceFor, true)
    }
    
    // MARK: - Brand name of
    
    func test_givenShoppingListItem_whenBrandNameOf_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _  = dataManager.brandName(of: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallBrandNameFor, true)
    }
    
    // MARK: - List for
    
    func test_givenNoShoppingListItem_whenListFor_thenReturnNil() {
        let listForItem = dataManager.list(for: nil)
        XCTAssertEqual(listForItem, nil)
    }
    
    func test_givenAnyShoppingListItem_whenListFor_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let _ = dataManager.list(for: item)
        
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallListFor, true)
    }
    
    // MARK: - Done for
    
    func test_givenNilShoppingListItem_whenDoneFor_thenReturnFalse() {
        let done = dataManager.done(for: nil)
        XCTAssertEqual(done, false)
    }
    
    func test_givenAnyShoppingListItem_whenDoneFor_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(done: false).insert()
        let _ = dataManager.done(for: item)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallDoneFor, true)
    }
    
    // MARK: - Is list item available
    
    func test_givenAnyList_whenIsListItemAvailable_thenTheRepositoryFunctionisCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.isListItemAvailable(withName: "", andShop: "", inList: list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallIsListItemAvailable, true)
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenAnyList_whenIsListIncludedInAggregatedList_thenTheRepositoryFunctionIsCalled() {
        let _ = dataManager.isListIncludedInAggregatedList(nil)
        XCTAssertEqual(shoppingListRepositoryMock.didCallIsIncludedInAggregatedList, true)
    }
    
    // MARK: - Insert new shopping list item
    
    func test_givenAnyDetails_whenInsertNewShoppingListItem_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.insertNewShoppingListItem(withName: "foo item", quantity: NSNumber(value: 1), unit: "kg", doneStatus: true, brand: "foo brand", forPrice: NSNumber(value: 2), atCurrency: "EUR", inShop: "foo shop", notes: "foo notes", forList: list)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallInsertNewShoppingListItem, true)
    }
    
    // MARK: - Update shopping list item
    
    func test_givenAnyDetails_whenUpdateShoppingListItem_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "fubar").insert()
        dataManager.updateShoppingListItem(item, itemName: "foo item", shopName: "foo shop", amountQuantity: NSNumber(value: 1), amountUnit: "kg", brandName: "foo brand", price: NSNumber(value: 2), currency: "EUR", notes: "foo notes")
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallUpdateShoppingListItem, true)
    }
}

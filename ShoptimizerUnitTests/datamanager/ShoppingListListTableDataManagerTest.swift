// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class ShoppingListListTableDataManagerTest: ShoptimizerTestCase {
    
    private var aggregatedListHelperMock: AggregatedListHelperMock!
    private var dataManager: ShoppingListListTableDataManager!
    private var shoppingListRepositoryMock: ShoppingListRepositoryMock!
    private var shoppingListItemRepositoryMock: ShoppingListItemRepositoryMock!
    private var suppliesItemRepositoryMock: SuppliesItemRepositoryMock!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        SuppliesItem.deleteAll()
        aggregatedListHelperMock = AggregatedListHelperMock()
        shoppingListRepositoryMock = ShoppingListRepositoryMock()
        shoppingListItemRepositoryMock = ShoppingListItemRepositoryMock()
        suppliesItemRepositoryMock = SuppliesItemRepositoryMock()
        dataManager = ShoppingListListTableDataManager(aggregatedListHelper: aggregatedListHelperMock, shoppingListRepository: shoppingListRepositoryMock, shoppingListItemRepository: shoppingListItemRepositoryMock, suppliesItemRepository: suppliesItemRepositoryMock)
    }
    
    // MARK: - Items for
    
    func test_givenAnyList_whenItemsFor_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.itemsFor(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallListItemsFor, true)
    }
    
    // MARK: - All supplies items
    
    func test_givenSuppliesItem_whenAllSuppliesItems_thenTheRepositoryFunctionIsCalled() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supplies").insertItem()
        let _ = dataManager.allSuppliesItems()
        XCTAssertEqual(suppliesItemRepositoryMock.didCallAllItems, true)
    }
    
    // MARK: - Done items for
    
    func test_givenAnyList_whenDoneItemsFor_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.doneItemsFor(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallDoneItemsFor, true)
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenAnyList_whenIsIncludedInAggregatedList_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.isListIncludedInAggregatedList(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallIsIncludedInAggregatedList, true)
    }
    
    // MARK: - Get name of
    
    func test_givenNormalList_whenGetNameOf_thenReturnTheNameOfTheList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let name = dataManager.getName(of: list)
        XCTAssertEqual(name, "foo list")
    }
    
    func test_givenAggregatedList_whenGetNameOf_thenReturnTheNameOfTheAggregatedList() {
        let list = insertAggregatedList()
        let name = dataManager.getName(of: list)
        XCTAssertEqual(name, "Aggregated List")
    }
    
    // MARK: - Is aggregated list
    
    func test_givenAnyString_whenIsAggregatedListByString_thenTheHelperFunctionIsCalled() {
        let _ = dataManager.isAggregatedList("")
        XCTAssertEqual(aggregatedListHelperMock.didCallIsAggregatedListByString, true)
    }
    
    func test_givenAnyList_whenIsAggregatedListByList_thenTheHelperFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.isAggregatedList(list)
        XCTAssertEqual(aggregatedListHelperMock.didCallIsAggregatedListByList, true)
    }
    
    // MARK: - Number of items on
    
    func test_givenAnyList_whenNumberOfItemsOn_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.numberOfItemsOn(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallNumberOfItemsOn, true)
    }
    
    // MARK: - Number of done items on
    
    func test_givenAnyList_whenNumberOfDoneItemsOn_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.numberOfDoneItemsOn(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallNumberOfDoneItemsOn, true)
    }
    
    // MARK: - Dominant ourrency of
    
    func test_givenAnyList_whenDominantCurrencyOf_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.dominantCurrencyOf(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallDominantCurrencyOf, true)
    }
    
    // MARK: - Total price of all items
    
    func test_givenAnyList_whenTotalPriceOfAllItems_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.totalPriceOfAllItems(forCurrency: "EUR", on: list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallTotalPriceOfAllItems, true)
    }
    
    // MARK: - Total price of done items
    
    func test_givenEmptyList_whenTotalPriceOfDoneItems_thenReturnZero() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = dataManager.totalPriceOfDoneItems(forCurrency: "EUR", on: list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallTotalPriceOfDoneItems, true)
    }
    
    // MARK: - Update supplies item - amount quantity
    
    func test_givenSuppliesItem_whenUpdateAmountQuantity_thenTheSuppliesItemHasTheNewAmountQuantity() {
        let item = SuppliesItemBuilder.create().with(name: "foo item").insertItem()
        dataManager.updateSuppliesItem(item, curAmountQuantity: 1.0)
        XCTAssertEqual(suppliesItemRepositoryMock.didCallUpdateCurAmountQuantity, true)
    }
    
    // MARK: - Update shopping list item - done
    
    func test_givenShoppingListItem_whenUpdatDone_thenTheShoppingListItemHasTheNewDoney() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        dataManager.updateShoppingListLitem(item, done: true)
        XCTAssertEqual(shoppingListItemRepositoryMock.didCallUpdateDone, true)
    }
    
    // MARK: - Add item to aggregated list
    
    func test_whenAddItemToAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        dataManager.addItemToAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallAddItemToAggregatedList)
    }
    
    // MARK: - Subtract item from aggregated list
    
    func test_whenSubtractItemFromAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        dataManager.subtractItemFromAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallSubtractItemFromAggregatedList)
    }
    
    // MARK: - Delete shopping list item
    
    func test_givenShoppingListItem_whenDeleteShoppingListItem_thenTheRepositoryFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        
        dataManager.deleteShoppingListItem(item)

        XCTAssertEqual(shoppingListItemRepositoryMock.didCallDeleteItem, true)
    }
    
    func test_givenShoppingListItem_whenDeleteShoppingListItem_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()

        dataManager.deleteShoppingListItem(item)

        XCTAssertEqual(aggregatedListHelperMock.didCallSubtractItemFromAggregatedList, true)
    }
    
    // MARK: - Export fo file URL
    
    func test_givenAList_whenExportToFileURL_thenAURLIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let url = dataManager.exportListToFileURL(list)
        XCTAssertNotNil(url)
    }
    
    // MARK: - Delete shopping list
    
    func test_givenShoppingList_whenDeleteShoppingList_thenTheRepositoryFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        dataManager.deleteShoppingList(list)
        XCTAssertEqual(shoppingListRepositoryMock.didCallDeleteShoppingList, true)
    }
}

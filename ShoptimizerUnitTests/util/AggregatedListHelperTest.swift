// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import XCTest
@testable import Shoptimizer

class AggregatedListHelperTest: ShoptimizerTestCase {
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        let context = DBHelper.instance.getManagedObjectContext()
        let _ = ShoppingList.insertNewShoppingList(withName: AggregatedListHelper.instance.nameOfAggregatedList(), andType: "Static", includeInAggregateList: false, intoContext: context)
        DBHelper.instance.saveContext()
    }
    
    // MARK: - Update aggregated list
    
    func test_givenThereAreNoOtherLists_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListIsEmpty() {
        AggregatedListHelper.instance.updateAggregatedList()
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList?.count, 0)
    }
    
    func test_givenAnEmptyListNotIncluded_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListIsEmpty() {
        let _ = ShoppingListBuilder.create().with(name: "foo").with(includedInAggregatedList: false).insert()
        AggregatedListHelper.instance.updateAggregatedList()
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList?.count, 0)
    }
    
    func test_givenANonEmptyListNotIncluded_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListIsEmpty() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        list.addToItemsForList(item)
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList?.count, 0)
    }
    
    func givenAnEmptyListIncluded_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListIsEmpty() {
        let _ = ShoppingListBuilder.create().with(name: "foo").with(includedInAggregatedList: true).insert()
        AggregatedListHelper.instance.updateAggregatedList()
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList?.count, 0)
    }
    
    func test_givenAListIncludedWithOneItem_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasThatOneItem() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        list.addToItemsForList(item)
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
    }
    
    func test_givenAListIncludedWithTwoItems_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasThoseTwoItems() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList! as! Set<ShoppingListItem>
        let sortedItems = itemsOnAggregatedList.sorted(by: {$0.itemName ?? "" < $1.itemName ?? ""})
        XCTAssertEqual(sortedItems.count, 2)
        XCTAssertEqual((sortedItems[0]).itemName, "foo item 1")
        XCTAssertEqual((sortedItems[1]).itemName, "foo item 2")
    }
    
    func test_givenAnEmptyAndNonEmptyList_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasJustTheOneItem() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let _ = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: false).insert()
        list1.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
    }
    
    func test_GivenTwoListsWithItemsWithSameNameAndUnit_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasCombinationOfThoseTwoItems() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 1.0).with(amountUnit: "kg").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 0.5).with(amountUnit: "kg").insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemAmountQuantity, NSNumber(value: 1.5))
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemAmountUnit, "kg")
    }
    
    func test_GivenTwoListsWithItemsWithSameNameAndUnitOnwOfWhichIsDone_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasJustOneItem() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 1.0).with(amountUnit: "kg").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 0.5).with(amountUnit: "kg").with(done: true).insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemAmountQuantity, NSNumber(value: 1.0))
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemAmountUnit, "kg")
    }
    
    func test_GivenTwoListsWithItemsWithDifferentNameAndSameUnit_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasBothItems() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(amountQuantity: 1.0).with(amountUnit: "kg").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(amountQuantity: 0.5).with(amountUnit: "kg").insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList! as! Set<ShoppingListItem>
        let sortedItems = itemsOnAggregatedList.sorted(by: {$0.itemName ?? "" < $1.itemName ?? ""})
        XCTAssertEqual(sortedItems.count, 2)
        XCTAssertEqual(sortedItems[0].itemName, "foo item 1")
        XCTAssertEqual(sortedItems[0].itemAmountQuantity, NSNumber(value: 1.0))
        XCTAssertEqual(sortedItems[0].itemAmountUnit, "kg")
        XCTAssertEqual(sortedItems[1].itemName, "foo item 2")
        XCTAssertEqual(sortedItems[1].itemAmountQuantity, NSNumber(value: 0.5))
        XCTAssertEqual(sortedItems[1].itemAmountUnit, "kg")
    }
    
    func test_GivenTwoListsWithItemsWithSameNameAndCurrency_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasCombinationOfThoseTwoItems() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 2.0).with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 2.5).with(currency: "EUR").insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemPrice, NSNumber(value: 4.5))
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemCurrency, "EUR")
    }
    
    func test_GivenTwoListsWithItemsWithSameNameAndCurrencyOneOfWhichIsDone_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasJustOneItem() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 2.0).with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 2.5).with(currency: "EUR").with(done: true).insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList!.allObjects
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemName, "foo item")
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemPrice, NSNumber(value: 2.0))
        XCTAssertEqual((itemsOnAggregatedList[0] as! ShoppingListItem).itemCurrency, "EUR")
    }
    
    func test_GivenTwoListsWithItemsWithDifferentNameAndSameCurrency_givenEmptyAggregatedList_whenUpdateAggregatedList_thenAggregatedListHasBothItems() {
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(price: 2.0).with(currency: "EUR").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(price: 2.5).with(currency: "EUR").insert()
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").with(includedInAggregatedList: true).insert()
        let list2 = ShoppingListBuilder.create().with(name: "foo list 2").with(includedInAggregatedList: true).insert()
        list1.addToItemsForList(item1)
        list2.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        AggregatedListHelper.instance.updateAggregatedList()
        
        let itemsOnAggregatedList = AggregatedListHelper.instance.getAggregatedList().itemsForList! as! Set<ShoppingListItem>
        let sortedItems = itemsOnAggregatedList.sorted(by: {$0.itemName ?? "" < $1.itemName ?? ""})
        XCTAssertEqual(sortedItems.count, 2)
        XCTAssertEqual(sortedItems[0].itemName, "foo item 1")
        XCTAssertEqual(sortedItems[0].itemPrice, NSNumber(value: 2.0))
        XCTAssertEqual(sortedItems[0].itemCurrency, "EUR")
        XCTAssertEqual(sortedItems[1].itemName, "foo item 2")
        XCTAssertEqual(sortedItems[1].itemPrice, NSNumber(value: 2.5))
        XCTAssertEqual(sortedItems[1].itemCurrency, "EUR")
    }
    
    // MARK: - Add item to aggregated list
    
    func test_givenEmptyAggregatedListAndSingleItem_whenAddToAggregatedList_thenTheAggregatedListHasExactlyThatItem() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
    }
    
    func test_givenEmptyAggregatedListAndSingleDoneItem_whenAddToAggregatedList_thenTheAggregatedListRemainsEmpty() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 0)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameName_whenAddToAggregatedList_thenItemOnAggregatedListIsNotDouble() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList!.count, 1)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithDifferentName_whenAddToAggregatedList_thenItemOnAggregatedListHasBothItems() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]).sorted(by: {$0.itemName! < $1.itemName!})
        XCTAssertEqual(items.count, 2)
        XCTAssertEqual(items[0].itemName, "foo item 1")
        XCTAssertEqual(items[1].itemName, "foo item 2")
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameInDifferentShops_whenAddToAggregatedList_thenItemOnAggregatedListHasBothItems() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 1").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 2").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]).sorted(by: {$0.shopName! < $1.shopName!})
        XCTAssertEqual(items.count, 2)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].shopName, "foo shop 1")
        XCTAssertEqual(items[1].itemName, "foo item")
        XCTAssertEqual(items[1].shopName, "foo shop 2")
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndCurrency_whenAddToAggregatedList_thenTheAggregatedListHasTheCombinedItem() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 1.0).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 0.5).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemCurrency, "EUR")
        XCTAssertEqual(items[0].itemPrice, 1.5)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndDifferentCurrency_whenAddToAggregatedList_thenTheAggregatedListHasBothItems() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 1.0).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "USD").with(price: 0.5).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]).sorted(by: {$0.itemCurrency! < $1.itemCurrency!})
        XCTAssertEqual(items.count, 2)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemCurrency, "EUR")
        XCTAssertEqual(items[0].itemPrice, 1.0)
        XCTAssertEqual(items[1].itemName, "foo item")
        XCTAssertEqual(items[1].itemCurrency, "USD")
        XCTAssertEqual(items[1].itemPrice, 0.5)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndUnit_whenAddToAggregatedList_thenTheAggregatedListHasTheCombinedItem() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 0.7).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 1.9)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndConvertibleUnit_whenAddToAggregatedList_thenTheAggregatedListHasTheCombinedItem() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "g").with(amountQuantity: 700).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 1.9)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndDifferentUnit_whenAddToAggregatedList_thenTheAggregatedListHasBothItems() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "l").with(amountQuantity: 700).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.addItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]).sorted(by: {$0.itemAmountUnit! < $1.itemAmountUnit!})
        XCTAssertEqual(items.count, 2)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 1.2)
        XCTAssertEqual(items[1].itemName, "foo item")
        XCTAssertEqual(items[1].itemAmountUnit, "l")
        XCTAssertEqual(items[1].itemAmountQuantity, 700)
    }
    
    // MARK: - Subtract item from aggregated list
    
    func test_givenEmptyAggregatedListAndSingleItem_whenSubtractFromAggregatedList_thenTheAggregatedListRemainsEmpty() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        AggregatedListHelper.instance.subtractItem(item)
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList!.count, 0)
    }
    
    func test_givenEmptyAggregatedListAndSingleDoneItem_whenSubtractFromAggregatedList_thenTheAggregatedListRemainsEmpty() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        AggregatedListHelper.instance.subtractItem(item)
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList!.count, 0)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameName_whenSubtractFromAggregatedList_thenAggregatedListIsEmpty() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList!.count, 0)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithDifferentName_whenSubtractFromAggregatedList_thenItemOnAggregatedIsUnchanged() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem])
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item 1")
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameInDifferentShops_whenSubtractFromAggregatedList_thenItemOnAggregatedListIsUnchanged() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 1").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop 2").insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = (AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem])
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndCurrency_whenSubtrctFromAggregatedList_thenTheItemOnTheAggregatedListIsUpdated() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 0.5).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemCurrency, "EUR")
        XCTAssertEqual(items[0].itemPrice, 0.7)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndDifferentCurrency_whenSubtractFromAggregatedList_thenTheAggregatedListIsUnchanged() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").with(price: 1.0).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "USD").with(price: 0.5).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemCurrency, "EUR")
        XCTAssertEqual(items[0].itemPrice, 1.0)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndUnit_whenSubtractFromAggregatedList_thenTheItemOnTheAggregatedListisUpdated() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 0.7).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 0.5)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndConvertibleUnit_whenSubtractFromAggregatedList_thenTheItemOnTheAggregatedListIsUpdated() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "g").with(amountQuantity: 700).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 0.5)
    }
    
    func test_givenAggregatedListWithOneItemAndSingleItemWithSameNameAndDifferentUnit_whenSubtractFromAggregatedList_thenTheAggregatedListIsUnchanged() {
        let aggregatedListItem = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 1.2).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "l").with(amountQuantity: 700).insert()
        AggregatedListHelper.instance.getAggregatedList().addToItemsForList(aggregatedListItem)
        
        AggregatedListHelper.instance.subtractItem(item)
        
        let items = AggregatedListHelper.instance.getAggregatedList().itemsForList?.allObjects as! [ShoppingListItem]
        XCTAssertEqual(items.count, 1)
        XCTAssertEqual(items[0].itemName, "foo item")
        XCTAssertEqual(items[0].itemAmountUnit, "kg")
        XCTAssertEqual(items[0].itemAmountQuantity, 1.2)
    }
    
    // MARK: - Is aggregated list
    
    func test_givenEmptyString_whenIsAggregatedList_thenReturnFalse() {
        let isAggregatedList = AggregatedListHelper.instance.isAggregatedList("")
        XCTAssertEqual(isAggregatedList, false)
    }
    
    func test_givenNonEmptyString_whenIsAggregatedList_thenReturnFalse() {
        let isAggregatedList = AggregatedListHelper.instance.isAggregatedList("foo list")
        XCTAssertEqual(isAggregatedList, false)
    }
    
    func test_givenStringAggregatedList_whenIsAggregatedList_thenReturnTrue() {
        let isAggregatedList = AggregatedListHelper.instance.isAggregatedList("Aggregated List")
        XCTAssertEqual(isAggregatedList, true)
    }
    
    func test_givenNormalList_whenIsAggregatedList_thenReturnFalse() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let isAggregatedList = AggregatedListHelper.instance.isAggregatedList(list)
        XCTAssertEqual(isAggregatedList, false)
    }
    
    func test_givenAggregatedList_whenIsAggregatedList_thenReturnTrue() {
        let list = insertAggregatedList()
        let isAggregatedList = AggregatedListHelper.instance.isAggregatedList(list)
        XCTAssertEqual(isAggregatedList, true)
    }
}

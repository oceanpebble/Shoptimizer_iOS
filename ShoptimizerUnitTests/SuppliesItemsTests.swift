// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class SuppliesItemsTests: ShoptimizerTestCase {
    
    override func setUp() {
        super.setUp()
        SuppliesItem.deleteAll()
        SuppliesItemCategory.deleteAll()
    }

    // MARK: - Search items by name

    func test_FindNoItems_WhenNoItemsPresent() {
        let items = SuppliesItem.items(for: "Item")
        XCTAssertTrue(items.count == 0)
    }

    func test_FindAllItems_WhenSearchingForEmptyString() {
        SuppliesItemBuilder.create().with(name: "Item1").insertItem()
        SuppliesItemBuilder.create().with(name: "Item2").insertItem()

        let items = SuppliesItem.items(for: "")

        XCTAssertTrue(items.count == 2)
        XCTAssertTrue(items[0].itemName == "Item1")
        XCTAssertTrue(items[1].itemName == "Item2")
    }

    func test_FindAllItems_WhenSearchingForOneCharacterString() {
        SuppliesItemBuilder.create().with(name: "Item1").insertItem()
        SuppliesItemBuilder.create().with(name: "Item2").insertItem()

        let items = SuppliesItem.items(for: "X")

        XCTAssertTrue(items.count == 2)
        XCTAssertTrue(items[0].itemName == "Item1")
        XCTAssertTrue(items[1].itemName == "Item2")
    }

    func test_FindSingleItem_WithPartialNameWithoutSpaces() {
        SuppliesItemBuilder.create().with(name: "Item1").insertItem()
        SuppliesItemBuilder.create().with(name: "Item2").insertItem()

        let items = SuppliesItem.items(for: "m1")

        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item1")
    }

    func test_FindSingleItem_WihFullNameWithoutSpaces() {
        SuppliesItemBuilder.create().with(name: "Item1").insertItem()
        SuppliesItemBuilder.create().with(name: "Item2").insertItem()

        let items = SuppliesItem.items(for: "Item1")

        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items.first?.itemName == "Item1")
    }

    func test_FindItems_WithFullNameWithSpaces() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()

        let items = SuppliesItem.items(for: "Item One")

        XCTAssertTrue(items.count == 2)
    }

    func test_FindItems_WithOneCharacterInSecondComponent() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()

        let items = SuppliesItem.items(for: "Item O")

        XCTAssertTrue(items.count == 2)
    }

    func test_FindItems_WithPartialNameWithSpaces() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()

        let items = SuppliesItem.items(for: "Ite On")

        XCTAssertTrue(items.count == 2)
    }

    func test_FindItems_WithPartialNameWithSpaces_InDifferentOrder() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "On Ite")
        XCTAssertTrue(items.count == 2)
    }

    func test_FindItems_WithPartialNameWithSpaces_InLowerCase() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "ite on")
        XCTAssertTrue(items.count == 2)
    }

    func test_FindItems_WithOneCharacterSecondSearchTerm() {
        SuppliesItemBuilder.create().with(name: "Item One").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "ite w")
        XCTAssertTrue(items.count == 2)
    }

    // MARK: - Search items by category

    func test_FindNoItems_WhenCategoryWrong() {
        SuppliesItemCategoryBuilder.create().with(name: "Spices").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Spices").insertItem()
        let items = SuppliesItem.items(for: "Foo")
        XCTAssertTrue(items.count == 0)
    }

    func test_FindSingleItem_WithFullCategoryWithoutSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Spices").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Spices").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "Spices")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindSingleItem_WithPartialCategoryWithoutSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Spices").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Spices").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "Spic")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindSingleItem_WithFullCategoryWithSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category Umpteen").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Category Umpteen").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "Category Umpteen")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindSingleItem_WithPartialCategoryWithSpaces() {
        SuppliesItemCategoryBuilder.create().with(name: "Category Umpteen").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Category Umpteen").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "Cat Ump")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindSingleItem_WithPartialCategoryWithSpaces_InDifferentOrder() {
        SuppliesItemCategoryBuilder.create().with(name: "Category Umpteen").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Category Umpteen").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "Ump Cat")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindSingleItem_WithPartialCategoryWithSpaces_InLowerCase() {
        SuppliesItemCategoryBuilder.create().with(name: "Category Umpteen").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Category Umpteen").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").insertItem()
        let items = SuppliesItem.items(for: "ump cat")
        XCTAssertTrue(items.count == 1)
        XCTAssertTrue(items[0].itemName == "Item One")
    }

    func test_FindAllItems_WithMultipleCategories() {
        SuppliesItemCategoryBuilder.create().with(name: "Spices").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Dairy Products").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Spices; Dairy Products").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").with(categories: "Spices").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Three").with(categories: "Dairy Products").insertItem()
        let items = SuppliesItem.items(for: "Spi odu")
        XCTAssertTrue(items.count == 3)
    }

    // MARK: - Search items by name and category

    func test_FindAllItems_WithFullNameAndFullCategory() {
        SuppliesItemCategoryBuilder.create().with(name: "Spices").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Dairy Products").insertCategory()
        SuppliesItemCategoryBuilder.create().with(name: "Tools").insertCategory()
        SuppliesItemBuilder.create().with(name: "Item One").with(categories: "Spices").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Two").with(categories: "Dairy Products").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Three").with(categories: "Tools").insertItem()
        SuppliesItemBuilder.create().with(name: "Item Four").insertItem()
        let items = SuppliesItem.items(for: "Item One Spices")
        XCTAssertTrue(items.count == 4)
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import XCTest
@testable import Shoptimizer

class ShoppingListListTableViewModelTest: ShoptimizerTestCase {
    
    private var aggregatedListHelperMock: AggregatedListHelperMock!
    private var dataManagerMock: ShoppingListListTableDataManagerMock!
    private var shoppingListRepositoryMock: ShoppingListRepositoryMock!
    private var shoppingListItemRepositoryMock: ShoppingListItemRepositoryMock!
    private var suppliesItemRepositoryMock: SuppliesItemRepositoryMock!
    private var fetchedResultsController: NSFetchedResultsControllerShoppingListMock!
    private var viewModel: ShoppingListListTableViewModel!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        SuppliesItem.deleteAll()
        aggregatedListHelperMock = AggregatedListHelperMock()
        fetchedResultsController = NSFetchedResultsControllerShoppingListMock()
        shoppingListRepositoryMock = ShoppingListRepositoryMock()
        shoppingListItemRepositoryMock = ShoppingListItemRepositoryMock()
        suppliesItemRepositoryMock = SuppliesItemRepositoryMock()
        dataManagerMock = ShoppingListListTableDataManagerMock(aggregatedListHelper: aggregatedListHelperMock, shoppingListRepository: shoppingListRepositoryMock, shoppingListItemRepository: shoppingListItemRepositoryMock, suppliesItemRepository: suppliesItemRepositoryMock)
        viewModel = ShoppingListListTableViewModel(fetchedResultsController: fetchedResultsController, aggregatedListHelper: aggregatedListHelperMock, dataManager: dataManagerMock)
    }
    
    // MARK: - Upgrade List
    
    func test_whenUpdateAggregatedList_thenAggregatedListHelperIsCalled() {
        let _ = insertAggregatedList()
        viewModel.updateAggregatedList()
        XCTAssertTrue(aggregatedListHelperMock.didCallUpdateAggregatedList)
    }
    
    // MARK: - Transfer items from list to supplies
    
    func test_givenEmptyListAndEmptyPantry_whenTransferItemsAndDontDelete_thenThePantryIsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(SuppliesItem.getAll().count, 0)
    }
    
    func test_givenEmptyListAndEmptyPantry_whenTransferItemsAndDelete_thenThePantryIsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(SuppliesItem.getAll().count, 0)
    }
    
    func test_givenNonEmptyListAndEmptyPantry_whenTransferItemsAndDontDelete_thenThePantryIsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(SuppliesItem.getAll().count, 0)
    }
    
    func test_givenNonEmptyListAndEmptyPantry_whenTransferItemsAndDelete_thenThePantryIsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(SuppliesItem.getAll().count, 0)
    }
    
    func test_givenEmptyListAndNonEmptyPantry_whenTransferItemsAndDontDelete_thenThePantryIsUnchanged() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").insertItem()
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
    }
    
    func test_givenEmptyListAndNonEmptyPantry_whenTransferItemsAndDelete_thenThePantryIsUnchanged() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").insertItem()
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
    }
    
    func test_givenListWithItemNotInPantry_whenTransferItemsAndDontDelete_thenThePantryIsUnchanged() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
    }
    
    func test_givenListWithItemNotInPantry_whenTransferItemsAndDelete_thenThePantryIsUnchanged() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
    }
    
    func test_givenListWithDoneItemInPantryWithSameUnit_whenTransferItemsAndDontDelete_thenOnlythePantryIsUpdated() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "kg").with(amountQuantity: 2.3).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 1)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 3.3)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_givenListWithDoneItemInPantryWithSameUnit_whenTransferItemsAndDelete_thenListAndPantryAreUpdated() {
        let _ = insertAggregatedList()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "kg").with(amountQuantity: 2.3).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 1)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 3.3)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    func test_givenListWithUndoneItemInPantryWithSameUnit_whenTransferItemsAndDontDelete_thenthePantryIsUnchanged() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "kg").with(amountQuantity: 2.3).with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.0)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_givenListWithUndoneItemInPantryWithSameUnit_whenTransferItemsAndDelete_thenThePantryIsUnchanged() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "kg").with(amountQuantity: 2.3).with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.0)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_givenListWithDoneItemInPantryWithConvertibleUnit_whenTransferItemsAndDontDelete_thenOnlythePantryIsUpdated() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "g").with(amountQuantity: 2300).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 1)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 3.3)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_givenListWithDoneItemInPantryWithConvertibleUnit_whenTransferItemsAndDelete_thenListAndPantryAreUpdated() {
        let _ = insertAggregatedList()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "g").with(amountQuantity: 2300).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 1)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 3.3)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    func test_givenListWithDoneItemInPantryWithNonConvertibleUnit_whenTransferItemsAndDontDelete_thenOnlythePantryIsUpdated() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "piece").with(amountQuantity: 2300).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.0)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_givenListWithDoneItemInPantryWithNonConvertibleUnit_whenTransferItemsAndDelete_thenListAndPantryAreUpdated() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo supply").with(amountUnit: "piece").with(amountQuantity: 2300).with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll()
        XCTAssertEqual(numberOfTransferredItems, 0)
        XCTAssertEqual(suppliesItems.count, 1)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.0)
        XCTAssertEqual(list.itemsForList?.count, 1)
    }
    
    func test_ListWithDoneItemsAndPantrywithTwoFittingItems_whenTransferItemsAndDontDelete_thenBothItemsAreUpdated() {
        let _ = SuppliesItemBuilder.create().with(name: "foo supply 1").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply 2").with(curAmountUnit: "l").with(curAmountQuantity: 0.5).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo supply 1").with(amountUnit: "kg").with(amountQuantity: 0.2).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo supply 2").with(amountUnit: "l").with(amountQuantity: 1.2).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: false)
        
        let suppliesItems = SuppliesItem.getAll().sorted(by: {$0.itemName ?? "" < $1.itemName ?? ""})
        XCTAssertEqual(numberOfTransferredItems, 2)
        XCTAssertEqual(suppliesItems.count, 2)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply 1")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.2)
        XCTAssertEqual(suppliesItems[1].itemName, "foo supply 2")
        XCTAssertEqual(suppliesItems[1].curAmountUnit, "l")
        XCTAssertEqual(suppliesItems[1].curAmountQuantity, 1.7)
        XCTAssertEqual(list.itemsForList?.count, 2)
    }
    
    func test_ListWithDoneItemsAndPantrywithTwoFittingItems_whenTransferItemsAndDelete_thenBothItemsAreUpdated() {
        let _ = insertAggregatedList()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply 1").with(curAmountUnit: "kg").with(curAmountQuantity: 1.0).insertItem()
        let _ = SuppliesItemBuilder.create().with(name: "foo supply 2").with(curAmountUnit: "l").with(curAmountQuantity: 0.5).insertItem()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo supply 1").with(amountUnit: "kg").with(amountQuantity: 0.2).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo supply 2").with(amountUnit: "l").with(amountQuantity: 1.2).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfTransferredItems = viewModel.transferItemsFrom(list, toSuppliesAndDeleteItems: true)
        
        let suppliesItems = SuppliesItem.getAll().sorted(by: {$0.itemName ?? "" < $1.itemName ?? ""})
        XCTAssertEqual(numberOfTransferredItems, 2)
        XCTAssertEqual(suppliesItems.count, 2)
        XCTAssertEqual(suppliesItems[0].itemName, "foo supply 1")
        XCTAssertEqual(suppliesItems[0].curAmountUnit, "kg")
        XCTAssertEqual(suppliesItems[0].curAmountQuantity, 1.2)
        XCTAssertEqual(suppliesItems[1].itemName, "foo supply 2")
        XCTAssertEqual(suppliesItems[1].curAmountUnit, "l")
        XCTAssertEqual(suppliesItems[1].curAmountQuantity, 1.7)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    // MARK: - Delete done items
    
    func test_givenEmptyList_whenDeleteDoneItems_thenTheListRemainsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let numberOfDeletedItems = viewModel.deleteDoneItemsFrom(list)
        
        XCTAssertEqual(numberOfDeletedItems, 0)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    func test_givenListWithUndoneItem_whenDeleteDoneItems_thenTheListIsUnchanged() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfDeletedItems = viewModel.deleteDoneItemsFrom(list)
        
        let itemsOnList = list.itemsForList!.allObjects
        XCTAssertEqual(numberOfDeletedItems, 0)
        XCTAssertEqual(itemsOnList.count, 1)
        XCTAssertEqual((itemsOnList[0] as! ShoppingListItem).itemName, "foo item")
    }
    
    func test_givenListWithDoneItem_whenDeleteDoneItems_thenTheItemIsDeleted() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfDeletedItems = viewModel.deleteDoneItemsFrom(list)
        
        let itemsOnList = list.itemsForList!.allObjects
        XCTAssertEqual(numberOfDeletedItems, 1)
        XCTAssertEqual(itemsOnList.count, 0)
    }
    
    func test_givenListWithDoneAndUndoneItems_whenDeleteDoneItems_thenOnlyTheDoneItemIsDeleted() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfDeletedItems = viewModel.deleteDoneItemsFrom(list)
        
        let itemsOnList = list.itemsForList!.allObjects
        XCTAssertEqual(numberOfDeletedItems, 1)
        XCTAssertEqual(itemsOnList.count, 1)
        XCTAssertEqual((itemsOnList[0] as! ShoppingListItem).itemName, "foo item 2")
    }
    
    // MARK: Uncheck items
    
    func test_givenEmptyList_whenUncheckItems_thenTheListRemainsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let numberOfUncheckedItems = viewModel.uncheckItemsOn(list).count
        
        XCTAssertEqual(numberOfUncheckedItems, 0)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    func test_givenListWithUndoneItem_whenUncheckItems_thenTheListIsUnchanged() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfUncheckedItems = viewModel.uncheckItemsOn(list).count
        
        let itemsOnList = list.itemsForList!.allObjects
        XCTAssertEqual(numberOfUncheckedItems, 0)
        XCTAssertEqual(itemsOnList.count, 1)
        XCTAssertEqual((itemsOnList[0] as! ShoppingListItem).done?.boolValue, false)
    }
    
    func test_givenListWithDoneItem_whenUncheckItems_thenTheItemIsDeleted() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfUncheckedItems = viewModel.uncheckItemsOn(list).count
        
        let itemsOnList = list.itemsForList!.allObjects
        XCTAssertEqual(numberOfUncheckedItems, 1)
        XCTAssertEqual(itemsOnList.count, 1)
        XCTAssertEqual((itemsOnList[0] as! ShoppingListItem).done?.boolValue, false)
    }
    
    func test_givenListWithDoneAndUndoneItems_whenUncheckItems_thenOnlyTheDoneItemIsUnchecked() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfUncheckedItems = viewModel.uncheckItemsOn(list).count
        
        let itemsOnList = list.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(numberOfUncheckedItems, 1)
        XCTAssertEqual(itemsOnList.count, 2)
        XCTAssertEqual(itemsOnList[0].done?.boolValue, false)
        XCTAssertEqual(itemsOnList[1].done?.boolValue, false)
    }
    
    // MARK: - Delete items
    
    func test_givenEmptyList_whenDeleteItems_thenTheListRemainsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        
        let numberOfDeletedItems = viewModel.deleteItemsFrom(list)
        
        XCTAssertEqual(numberOfDeletedItems, 0)
        XCTAssertEqual(list.itemsForList?.count, 0)
    }
    
    func test_givenListWithOneItem_whenDeleteItems_thenTheItemIsDeleted() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let numberOfDeletedItems = viewModel.deleteItemsFrom(list)
        
        XCTAssertEqual(numberOfDeletedItems, 1)
        XCTAssertEqual(list.itemsForList!.count, 0)
    }
    
    func test_givenListWithTwoItems_whenDeleteItems_thenTheItemsAreDeleted() {
        let _ = insertAggregatedList()
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let numberOfDeletedItems = viewModel.deleteItemsFrom(list)
        
        XCTAssertEqual(numberOfDeletedItems, 2)
        XCTAssertEqual(list.itemsForList!.count, 0)
    }
    
    // MARK: - Export fo file URL
    
    func test_givenAnyShoppingList_whenExportToFileURL_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.exportToFileURL(list)
        XCTAssertEqual(dataManagerMock.didCallExportListToFeilUrl, true)
    }
    
    // MARK: - List name display string
    
    func test_givenAggregatedList_whenListNameDisplayString_ThenTheCorrectDisplayStringIsReturned() {
        let list = insertAggregatedList()
        let displayName = viewModel.listNameDisplayString(for: list)
        XCTAssertEqual(displayName, AggregatedListHelper.instance.nameOfAggregatedList())
    }
    
    func test_givenListNotIncluded_whenListNameDisplayString_ThenTheCorrectDisplayStringIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let displayName = viewModel.listNameDisplayString(for: list)
        XCTAssertEqual(displayName, Constants.MARKER_FOR_LIST_NOT_INCLUDED + " foo list")
    }
    
    func test_givenListIncluded_whenListNameDisplayString_ThenTheCorrectDisplayStringIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let displayName = viewModel.listNameDisplayString(for: list)
        XCTAssertEqual(displayName, Constants.MARKER_FOR_LIST_INCLUDED + " foo list")
    }
    
    // MARK: - Price string
    
    func test_givenEmptyAggregatedList_whenPriceString_thenAStringContainingZeroIsReturned() {
        let list = insertAggregatedList()
        let priceString = viewModel.priceString(for: list)
        XCTAssertEqual(priceString, "0")
    }
    
    func test_givenAggregatedListWithOneItem_whenPriceString_thenAStringContainingOneIsReturned() {
        let list = insertAggregatedList()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1")
    }
    
    func test_givenEmptyList_whenPriceString_thenStringContainingZeorOfZeroIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let priceString = viewModel.priceString(for: list)
        XCTAssertEqual(priceString, "0 / 0")
    }
    
    func test_givenListWithOneUndoneItem_whenPriceString_thenStringContainingZeroOfOneIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "0 / 1")
    }
    
    func test_givenListWithOneDoneItem_whenPriceString_thenStringContainingOneOfOneIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 / 1")
    }
    
    func test_givenListWithOneUndoneAndOneDoneItem_whenPriceString_thenStringContainingOneOfTwoIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 / 2")
    }
    
    func test_givenListWithOneUndoneItemWithCurrency_whenPriceString_thenStringContainingPriceIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(currency: "EUR").with(price: 1.29).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "0 - 0.00 EUR / 1 - 1.29 EUR")
    }
    
    func test_givenListWithOneDoneItemWithCurrency_whenPriceString_thenStringContainingPriceIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(currency: "EUR").with(price: 1.29).insert()
        list.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 - 1.29 EUR / 1 - 1.29 EUR")
    }
    
    func test_givenListWithOneUndoneAndOneDoneItemWithSameCurrency_whenPriceString_thenStringContainingPriceIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "EUR").with(price: 1.29).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "EUR").with(price: 0.99).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 - 0.99 EUR / 2 - 2.28 EUR")
    }
    
    func test_givenListWithOneUndoneItemWithoutCurrencyAndOneDoneItemWithCurrency_whenPriceString_thenStringContainingPriceIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "EUR").with(price: 0.99).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 - 0.99 EUR / 2 - 0.99 EUR")
    }
    
    func test_givenListWithOneUndoneItemWithCurrencyAndOneDoneItemWithoutCurrency_whenPriceString_thenStringContainingPriceIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "EUR").with(price: 1.29).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 - 0.00 EUR / 2 - 1.29 EUR")
    }
    
    func test_givenListWithOneUndoneItemWithCurrencyAAndOneDoneItemWithCurrencyB_whenPriceString_thenStringContainingPricesIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "EUR").with(price: 1.29).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "USD").with(price: 0.99).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "1 - 0.00 EUR / 2 - 1.29 EUR")
    }
    
    func test_givenListWithThreeItemsTwoWithCurrencyAOneWithCurrencyB_whenPriceString_thenStringContainingPricesIsReturned() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(done: false).with(currency: "EUR").with(price: 1.29).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(done: true).with(currency: "USD").with(price: 0.99).insert()
        let item3 = ShoppingListItemBuilder.create().with(name: "foo item 3").with(done: true).with(currency: "USD").with(price: 1.99).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        list.addToItemsForList(item3)
        DBHelper.instance.saveContext()
        
        let priceString = viewModel.priceString(for: list)
        
        XCTAssertEqual(priceString, "2 - 2.98 USD / 3 - 2.98 USD")
    }
    
    // MARK: - List
    
    func test_whenList_thenFetchedResultsControllerIsCalled() {
        let _ = viewModel.list(at: IndexPath())
        XCTAssertTrue(fetchedResultsController.didCallObjectAt)
    }
    
    // MARK: - Include exclude items
    
    func test_givenEmptyListAndEmptyAggregatedList_whenIncludeItems_thenBothListsRemainEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let aggregatedList = insertAggregatedList()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        XCTAssertEqual(list.itemsForList?.count, 0)
        XCTAssertEqual(aggregatedList.itemsForList?.count, 0)
    }
    
    func test_givenEmptyListAndEmptyAggregatedList_whenExcludeItems_thenBothListsRemainEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let aggregatedList = insertAggregatedList()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: true)
        
        XCTAssertEqual(list.itemsForList?.count, 0)
        XCTAssertEqual(aggregatedList.itemsForList?.count, 0)
    }
    
    func test_givenListWithOneUndoneItemAndEmptyAggregatedList_whenIncludeItems_thenTheItemIsTransferredToTheAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        let _ = insertAggregatedList()
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(AggregatedListHelper.instance.getAggregatedList().itemsForList?.count, 1)
    }
    
    func test_givenListWithOneDoneItemAndEmptyAggregatedList_whenIncludeItems_thenTheItemIsNotTransferredToTheAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        let aggregatedList = insertAggregatedList()
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(aggregatedList.itemsForList?.count, 0)
    }
    
    func test_givenListWithOneUndoneItemAndEmptyAggregatedList_whenExcludeItems_thenTheAggregatedListRemainsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).insert()
        list.addToItemsForList(item)
        let aggregatedList = insertAggregatedList()
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: true)
        
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(aggregatedList.itemsForList?.count, 0)
    }
    
    func test_givenListWithOneDoneItemAndEmptyAggregatedList_whenExcludeItems_thenTheAggregatedListRemainsEmpty() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).insert()
        list.addToItemsForList(item)
        let aggregatedList = insertAggregatedList()
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: true)
        
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(aggregatedList.itemsForList?.count, 0)
    }
    
    func test_givenListWithOneUndoneItemAndAggregatedListWithItemOfSameNameAndSameUnit_whenIncludeItems_thenListItemIsAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(amountUnit: "kg").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 3.0)
    }
    
    func test_givenListWithOneDoneItemAndAggregatedListWithItemOfSameNameAndSameUnit_whenIncludeItems_thenListItemIsNotAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(amountUnit: "kg").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 2.0)
    }
    
    func test_givenListWithOneDoneItemAndAggregatedListWithItemOfSameNameAndSameUnit_whenExcludeItems_thenTheAggregatedListRemainsUnchanged() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(amountUnit: "kg").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: true)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 2.0)
    }
    
    func test_givenListWithOneUndoneItemAndAggregatedListWithItemOfSameNameAndSameUnit_whenExcludeItems_thenTheListItemIsRemovedFromTheAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(amountUnit: "kg").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: true)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 1.0)
    }
    
    func test_givenListWithOneUndoneItemAndAggregatedListWithItemOfSameNameAndDifferentUnit_whenIncludeItems_thenListItemIsAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(amountUnit: "l").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        let sortedItems = itemsOnAggregatedList.sorted(by: {$0.itemAmountUnit! < $1.itemAmountUnit!})
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(sortedItems.count, 2)
        XCTAssertEqual(sortedItems[0].itemName, "foo item")
        XCTAssertEqual(sortedItems[0].itemAmountUnit, "kg")
        XCTAssertEqual(sortedItems[0].itemAmountQuantity, 2.0)
        XCTAssertEqual(sortedItems[1].itemName, "foo item")
        XCTAssertEqual(sortedItems[1].itemAmountUnit, "l")
        XCTAssertEqual(sortedItems[1].itemAmountQuantity, 1.0)
    }
    
    func test_givenListWithOneDoneItemAndAggregatedListWithItemOfSameNameAndDifferentUnit_whenIncludeItems_thenListItemIsNotAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(amountUnit: "l").with(amountQuantity: 1.0).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 2.0)
    }
    
    func test_givenListWithOneUndoneItemAndAggregatedListWithItemOfSameNameAndConvertibleUnit_whenIncludeItems_thenListItemIsAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: false).with(amountUnit: "g").with(amountQuantity: 500).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 2.5)
    }
    
    func test_givenListWithOneDoneItemAndAggregatedListWithItemOfSameNameAndConvertibleUnit_whenIncludeItems_thenListItemIsNotAddedToAggregatedList() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: true).insert()
        let itemOnList = ShoppingListItemBuilder.create().with(name: "foo item").with(done: true).with(amountUnit: "g").with(amountQuantity: 500).insert()
        let itemOnAggregatedList = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").with(amountQuantity: 2.0).insert()
        list.addToItemsForList(itemOnList)
        let aggregatedList = insertAggregatedList()
        aggregatedList.addToItemsForList(itemOnAggregatedList)
        DBHelper.instance.saveContext()
        
        viewModel.includeExcludeItems(for: list, oldIncludeExclude: false)
        
        let itemsOnAggregatedList = aggregatedList.itemsForList!.allObjects as! [ShoppingListItem]
        XCTAssertEqual(list.itemsForList?.count, 1)
        XCTAssertEqual(itemsOnAggregatedList.count, 1)
        XCTAssertEqual(itemsOnAggregatedList[0].itemName, "foo item")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountUnit, "kg")
        XCTAssertEqual(itemsOnAggregatedList[0].itemAmountQuantity, 2.0)
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenAnyShoppingList_whenIsListIncludedInAggregatedList_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").with(includedInAggregatedList: false).insert()
        let _ = viewModel.isListIncludedInAggregatedList(list)
        XCTAssertEqual(dataManagerMock.didCallIsListIncludedInAggregatedList, true)
    }
    
    // MARK: - Is aggregated list
    
    func test_givenAnyShoppingList_whenIsAggregatedListByList_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.isAggregatedList(list)
        XCTAssertEqual(dataManagerMock.didCallIsAggregatedListByList, true)
    }
    
    // MARK: - Delete shopping list
    
    func test_givenAnyShoppingList_whenDeleteShoppingList_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        viewModel.deleteShoppingList(list)
        XCTAssertEqual(dataManagerMock.didCallDeleteShoppingList, true)
    }
    
    // MARK: - Delete items from aggregated list for list
    
    func test_givenShoppingList_whenDeleteItemsFromAggregatedListForList_thenTheHelperFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        viewModel.deleteItemsFromAggregatedListForList(list)
        XCTAssertEqual(aggregatedListHelperMock.didCallDeleteItemsFromAggregatedListForList, true)
    }
}

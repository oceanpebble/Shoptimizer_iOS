// SPDX-FileCopyrightText: 2025 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import CoreData
import XCTest
@testable import Shoptimizer

class TaskTableViewModelTest: ShoptimizerTestCase {
    
    private var fetchedResultsController: NSFetchedResultsControllerTaskMock!
    private var viewModel: TaskTableViewModel!
    private var notificationController: NotificationControllerMock!
    
    override func setUp() {
        Task.deleteAllTasks()
        Recipe.deleteAllRecipes()
        fetchedResultsController = NSFetchedResultsControllerTaskMock()
        notificationController = NotificationControllerMock()
        viewModel = TaskTableViewModel(fetchedResultsController: fetchedResultsController, notificationController: notificationController)
    }
    
    // MARK: - Task
    
    func test_givenIndexPath_whenTask_thenFetchedResultsControllerIsCalled() {
        let _ = viewModel.task(at: IndexPath())
        XCTAssertTrue(fetchedResultsController.didCallObjectAt)
    }
    
    // MARK: - Sections
    
    func test_whenSections_thenFetchedResultsControllerIsCalled() {
        let _ = viewModel.sections()
        XCTAssertTrue(fetchedResultsController.didCallSections)
    }
    
    // MARK: - Done for
    
    func test_givenAnUndoneTask_whenDoneFor_thenReturnFalse() {
        let task = TaskBuilder.create().with(done: false).insert()
        let done = viewModel.done(for: task)
        XCTAssertEqual(done, false)
    }
    
    func test_givenADoneTask_whenDoneFor_thenReturnTrue() {
        let task = TaskBuilder.create().with(done: true).insert()
        let done = viewModel.done(for: task)
        XCTAssertEqual(done, true)
    }
    
    func test_givenTaskWithoutDone_whenDoneFor_thenReturnFalss() {
        let task = TaskBuilder.create().with(done: nil).insert()
        let done = viewModel.done(for: task)
        XCTAssertEqual(done, false)
    }
    
    // MARK: - Update task - date completed
    
    func test_givenNilDate_whenUpdatDateCreated_thenTheTaskHasTheNewDateCreated() {
        let task = TaskBuilder.create().insert()
        viewModel.update(task: task, dateCompleted: nil)
        XCTAssertEqual(task.dateCompleted, nil)
    }
    
    func test_givenDate_whenUpdatDateCreated_thenTheTaskHasTheNewDateCreated() {
        let task = TaskBuilder.create().insert()
        let newDate = NSDate()
        viewModel.update(task: task, dateCompleted: newDate)
        XCTAssertEqual(task.dateCompleted, newDate)
    }
    
    // MARK: - Update task - done
    
    func test_givenTask_whenUpdatDone_thenTheTaskHasTheNewDone() {
        let task = TaskBuilder.create().with(done: true).insert()
        viewModel.update(task: task, done: false)
        XCTAssertEqual(task.done, false)
    }
    
    // MARK: - Delete task
    
    func test_givenTask_whenDeleteTask_thenTheTaskIsDeleted() {
        let task = TaskBuilder.create().insert()
        viewModel.deleteTask(task)
        XCTAssertEqual(Task.tasks().count, 0)
    }
    
    // MARK: - Recipe for task
    
    func test_givenTaskWithNilName_whenRecipeForTask_thenReturnNil() {
        let task = TaskBuilder.create().with(title: nil).insert()
        let recipe = viewModel.recipe(for: task)
        XCTAssertEqual(recipe, nil)
    }
    
    func test_givenNoRecipes_whenRecipeForTask_thenReturnNil() {
        let task = TaskBuilder.create().insert()
        let recipe = viewModel.recipe(for: task)
        XCTAssertEqual(recipe, nil)
    }
    
    func test_givenRecipeWithWrongName_whenRecipeForTask_thenReturnNil() {
        let task = TaskBuilder.create().with(title: "task title").insert()
        let _ = RecipeBuilder.create().with(name: "recipe name").insert()
        let fetchedRecipe = viewModel.recipe(for: task)
        XCTAssertEqual(fetchedRecipe, nil)
    }
    
    func test_givenRecipeWithMatchingName_whenRecipeForTask_thenReturnTheRecipe() {
        let task = TaskBuilder.create().with(title: "title").insert()
        let recipe = RecipeBuilder.create().with(name: "title").insert()
        let fetchedRecipe = viewModel.recipe(for: task)
        XCTAssertEqual(fetchedRecipe, recipe)
    }
    
    func test_givenMultipleRecipesOneWithMatchingName_whenRecipeForTask_thenReturnTheRecipeWithTheMatchingName() {
        let task = TaskBuilder.create().with(title: "recipe 2").insert()
        let _ = RecipeBuilder.create().with(name: "recipe 1").insert()
        let recipe2 = RecipeBuilder.create().with(name: "recipe 2").insert()
        let fetchedRecipe = viewModel.recipe(for: task)
        XCTAssertEqual(fetchedRecipe, recipe2)
    }
    
    // MARK: - Format date due
    
    func test_givenTaskWithougDateDue_whenFormatDateDue_thenReturnEmptyString() {
        let task = TaskBuilder.create().insert()
        let date = viewModel.formatDateDue(for: task)
        XCTAssertEqual(date, "")
    }
    
    func test_givenTaskWithDateDue_whenFormatDateDue_thenReturnFormattedString() {
        var dateComponents = DateComponents()
        dateComponents.year = 2025
        dateComponents.month = 01
        dateComponents.day = 17
        dateComponents.hour = 18
        dateComponents.minute = 2
        let dateDue = NSCalendar(calendarIdentifier: .gregorian)!.date(from: dateComponents)!
        let task = TaskBuilder.create().with(dateDue: dateDue as NSDate).insert()
        
        let date = viewModel.formatDateDue(for: task)

        XCTAssertEqual(date, "17.01.2025 18:02")
    }
    
    // MARK: - Unschedule notification
    
    func test_givenAnyId_whenUnscheduleNotification_thenTheNotificationControllerIsCalled() {
        viewModel.unscheduleNotification(withId: 1)
        XCTAssertEqual(notificationController.didCallUnscheduleNotification, true)
    }
    
    // MARK: - Unschedule all tasks
    
    func test_whenUnscheduleAllTasks_thenTheNotificationControllerIsCalled() {
        viewModel.unscheduleAllTasks()
        XCTAssertEqual(notificationController.didCallUnscheduleAllTasks, true)
    }
    
    // MARK: - Delete all tasks
    
    func test_givenAnyTasks_whenDeleteAllTasks_thenAllTasksAreDeleted() {
        let _ = TaskBuilder.create().insert()
        let _ = TaskBuilder.create().insert()
                
        viewModel.deleteAllTasks()
        
        XCTAssertEqual(Task.tasks().count, 0)
    }
}


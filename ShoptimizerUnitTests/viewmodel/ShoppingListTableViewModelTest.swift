// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest
@testable import Shoptimizer

class ShoppingListTableViewModelTest: ShoptimizerTestCase {
    
    private var aggregatedListHelperMock: AggregatedListHelperMock!
    private var dataManagerMock: ShoppingListTableDataManagerMock!
    private var shoppingListRepositoryMock: ShoppingListRepositoryMock!
    private var shoppingListItemRepositoryMock: ShoppingListItemRepositoryMock!
    private var fetchedResultsControllerMock: NSFetchedResultsControllerShoppingListItemMock!
    private var viewModel: ShoppingListTableViewModel!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        SuppliesItem.deleteAll()
        aggregatedListHelperMock = AggregatedListHelperMock()
        shoppingListRepositoryMock = ShoppingListRepositoryMock()
        shoppingListItemRepositoryMock = ShoppingListItemRepositoryMock()
        fetchedResultsControllerMock = NSFetchedResultsControllerShoppingListItemMock()
        dataManagerMock = ShoppingListTableDataManagerMock(aggregatedListHelper: aggregatedListHelperMock, shoppingListRepository: shoppingListRepositoryMock, shoppingListItemRepository: shoppingListItemRepositoryMock)
        viewModel = ShoppingListTableViewModel(fetchedResultsController: fetchedResultsControllerMock, aggregatedListHelper: aggregatedListHelperMock, dataManager: dataManagerMock)
    }
    
    // MARK: - Get name of
    
    func test_givenAnyShoppingList_whenGetNameOf_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.getName(of: list)
        XCTAssertEqual(dataManagerMock.didCallGetNameOf, true)
    }
    
    // MARK: - Is aggregated list
    
    func test_giveAnyShoppinglList_whenIsAggregatedList_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.isAggregatedList(list)
        XCTAssertEqual(dataManagerMock.didCallIsAggregatedListByList, true)
    }
    
    
    func test_givenAnyString_whenIsAggregatedList_thenTheDataManagerFunctionIsCalled() {
        let _ = viewModel.isAggregatedList("Aggregated List")
        XCTAssertEqual(dataManagerMock.didCallIsAggregatedListByString, true)
    }
    
    // MARK: - Item at
    
    func test_givenAnyIndexPath_whenItemAt_thenFetchedResultsControllerIsCalled() {
        let _ = viewModel.item(at: IndexPath())
        XCTAssertEqual(fetchedResultsControllerMock.didCallObjectAt, true)
    }
    
    // MARK: - List for item
    
    func test_givenAnyShoppingListItemOnAShoppingList_whenListForItem_thenTheDataManagerFunctionIsCalled() {
        let list1 = ShoppingListBuilder.create().with(name: "foo list 1").insert()
        let _ = ShoppingListBuilder.create().with(name: "foo list 2").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        list1.addToItemsForList(item)
        DBHelper.instance.saveContext()
        
        let _ = viewModel.list(for: item)
        
        XCTAssertEqual(dataManagerMock.didCallListFor, true)
    }
    
    // MARK: - Done for
    
    func test_givenAnyShoppingListItem_whenDoneFor_thenRTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(done: true).insert()
        let _ = viewModel.done(for: item)
        XCTAssertEqual(dataManagerMock.didCallDoneFor, true)
    }
    
    // MARK: - Name of
    
    func test_givenAnyShoppingListItem_whenNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = viewModel.name(of: item)
        XCTAssertEqual(dataManagerMock.didCallNameFor, true)
    }
    
    // MARK: - Shop name of
    
    func test_givenAnyShoppingListItem_whenShopNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(shopName: "foo shop").insert()
        let _ = viewModel.shopName(of: item)
        XCTAssertEqual(dataManagerMock.didCallShopNameFor, true)
    }
    
    // MARK: - Brand name of
    
    func test_givenAnyShoppingListItem_whenBrandNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(brandName: "foo brand").insert()
        let _ = viewModel.brandName(of: item)
        XCTAssertEqual(dataManagerMock.didCallBrandNameFor, true)
    }
    
    // MARK: - Amount quantity of
    
    func test_givenAnyShoppingListItem_whenAmountQuantityOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(amountQuantity: 13.37).insert()
        let _ = viewModel.amountQuantity(of: item)
        XCTAssertEqual(dataManagerMock.didCallAmountQuantityFor, true)
    }
    
    // MARK: - Amount unit of
    
    func test_givenAnyShoppingListItem_whenAmountUnitOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(amountUnit: "kg").insert()
        let _ = viewModel.amountUnit(of: item)
        XCTAssertEqual(dataManagerMock.didCallAmountUnitFor, true)
    }
    
    // MARK: - Price of
    
    func test_givenAnyShoppingListItem_whenPriceOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(price: 13.37).insert()
        let _ = viewModel.price(of: item)
        XCTAssertEqual(dataManagerMock.didCallPriceOf, true)
    }
    
    // MARK: - Currency of
    
    func test_givenAnyShoppingListItem_whenCurrencyOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(currency: "EUR").insert()
        let _ = viewModel.currency(of: item)
        XCTAssertEqual(dataManagerMock.didCallCurrencyOf, true)
    }
    
    // MARK: - Update done
    
    func test_givenAnyShoppingListItem_whenUpdateDone_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(done: true).insert()
        viewModel.update(item: item, done: false)
        XCTAssertEqual(dataManagerMock.didCallUpdateDone, true)
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenAnyShoppingList_whenIsListIncluded_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.isListIncludedInAggregatedList(list)
        XCTAssertEqual(dataManagerMock.didCallIsListIncludedInAggregatedList, true)
    }
    
    // MARK: - Add item to aggregated list
    
    func test_whenAddItemToAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        viewModel.addItemToAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallAddItemToAggregatedList)
    }
    
    // MARK: - Subtract item from aggregated list
    
    func test_whenSubtractItemFromAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        viewModel.subtractItemFromAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallSubtractItemFromAggregatedList)
    }
    
    // MARK: - Delete item from aggregated list
    
    func test_whenDeleteItemFromAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        viewModel.deleteItemFromAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallDeleteItemFromAggregatedList)
    }
    
    // MARK: - Undone items with name
    
    func test_givenAnyShoppingListItem_whenUndoneItemsNotOnAggregatedListWithName_thenTheDataManagerFunctionIsCalled() {
        let _ = insertAggregatedList()
        let _ = viewModel.undoneItemsNotOnAggregatedListWithName("", shopName: "")
        XCTAssertEqual(dataManagerMock.didCallUndoneItemsNotOnAggregatedListWithName, true)
    }
    
    // MARK: - sections
    
    func test_whenSections_thenFetchedResultsControllerIsCalled() {
        let _ = viewModel.sections()
        XCTAssertTrue(fetchedResultsControllerMock.didCallSections)
    }
    
    // MARK: - Dominant currency
    
    func test_givenAnyShoppingList_whenDominantCurrency_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.dominantCurrency(on: list, inShop: "")
        XCTAssertEqual(dataManagerMock.didCallDominantCurrency, true)
    }
    
    // MARK: - Total price of all items
    
    func test_givenAnyShoppingList_whenPriceOfAllItems_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.totalPriceOfAllItems(on: list, forCurrency: "", inShop: "")
        XCTAssertEqual(dataManagerMock.didCallTotalPriceOfAllItems, true)
    }
    
    // MARK: - Total price of done items
    
    func test_givenAnyShoppingList_whenPriceOfDoneItems_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.totalPriceOfDoneItems(on: list, forCurrency: "", inShop: "")
        XCTAssertEqual(dataManagerMock.didCallTotalPriceOfDoneItems, true)
    }
    
    // MARK: - Format price
    
    func test_givenValueZeroAsDouble_whenFormatPrice_ThenTheResultIsCorrect() {
        let output = viewModel.formatPrice(0.0)
        XCTAssertEqual(output, "0.00")
    }
    
    func test_givenValueWithThreeDecimalDigitsAsDouble_whenFormatPrice_thenTheResultHasOnlyTwoDecimalDigits() {
        let output = viewModel.formatPrice(0.111)
        XCTAssertEqual(output, "0.11")
    }
    
    func test_givenValueWithThreeDecimalDigitsAsDouble_whenFormatPrice_thenTheResultIsRoundedHalfUp() {
        let output = viewModel.formatPrice(0.985)
        XCTAssertEqual(output, "0.99")
    }
    
    func test_givenValueWithTwoDecimalDigitsAsDouble_whenFormatPrice_thenTheResultIsNotRoundedHalfUp() {
        let output = viewModel.formatPrice(0.98)
        XCTAssertEqual(output, "0.98")
    }
    
    func test_givenValueZeroAsNumber_whenFormatPrice_ThenTheResultIsCorrect() {
        let output = viewModel.formatPrice(NSNumber(value: 0.0))
        XCTAssertEqual(output, "0.00")
    }
    
    func test_givenValueWithThreeDecimalDigitsAsNumber_whenFormatPrice_thenTheResultHasOnlyTwoDecimalDigits() {
        let output = viewModel.formatPrice(NSNumber(value: 0.111))
        XCTAssertEqual(output, "0.11")
    }
    
    func test_givenValueWithThreeDecimalDigitsAsNumber_whenFormatPrice_thenTheResultIsRoundedHalfUp() {
        let output = viewModel.formatPrice(NSNumber(value: 0.985))
        XCTAssertEqual(output, "0.99")
    }
    
    func test_givenValueWithTwoDecimalDigitsAsNumber_whenFormatPrice_thenTheResultIsNotRoundedHalfUp() {
        let output = viewModel.formatPrice(NSNumber(0.98))
        XCTAssertEqual(output, "0.98")
    }
    
    // MARK: - Price string
    
    func test_givenEmptyList_whenPriceString_thenReturnShopNameDoneItemsAndTotalItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 0, numberOfDoneItems: 0)
        XCTAssertEqual(priceString, "foo shop (0 / 0)")
    }
    
    func test_givenListWithOneDoneItemWithoutDominantCurrency_whenPriceString_thenReturnShopNameDoneItemsAndTotalItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 1)
        XCTAssertEqual(priceString, "foo shop (1 / 1)")
    }
    
    func test_givenListWithOneUndoneItemWithoutDominantCurrency_whenPriceString_thenReturnShopNameDoneItemsAndTotalItems() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 0)
        XCTAssertEqual(priceString, "foo shop (0 / 1)")
    }
    
    func test_givenListWithOneDoneItemWithCurrencyAndPrice_whenPriceString_thenReturnItemCountsAndPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        list.addToItemsForList(item)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 1)
        
        XCTAssertEqual(priceString, "foo shop (1 - 13.37 EUR / 1 - 13.37 EUR)")
    }
    
    func test_givenListWithOneUndoneItemWithCurrencyAndPrice_whenPriceString_thenReturnItemCountsAndPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: false).insert()
        list.addToItemsForList(item)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 0)
        
        XCTAssertEqual(priceString, "foo shop (0 - 0.00 EUR / 1 - 13.37 EUR)")
    }
    
    func test_givenListWithOneDoneAndOneUndoneItemWithCurrencyAndPrice_whenPriceString_thenReturnItemCountsAndPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).with(done: false).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 1)
        
        XCTAssertEqual(priceString, "foo shop (1 - 13.37 EUR / 2 - 21.52 EUR)")
    }
    
    func test_givenListWithOneTwoDonItemsWithCurrencyAndPrice_whenPriceString_thenReturnItemCountsAndPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 2)
        
        XCTAssertEqual(priceString, "foo shop (2 - 21.52 EUR / 2 - 21.52 EUR)")
    }
    
    func test_givenListWithOneTwoDonItemsWithCurrencyAndPriceOnlyOneWithDominantCurrency_whenPriceString_thenReturnItemCountsAndPrices() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "USD").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 2)
        
        XCTAssertEqual(priceString, "foo shop (2 - 13.37 EUR / 2 - 13.37 EUR)")
    }
    
    func test_givenListWithOneTwoDonItemsWithCurrencyAndPriceInDifferentShops_whenPriceString_thenReturnItemCountsAndPricesOnlyOfTheCorrectShop() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop 1").with(currency: "EUR").with(price: 13.37).with(done: true).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop 2").with(currency: "EUR").with(price: 08.15).with(done: true).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop 1", objectCount: 1, numberOfDoneItems: 1)
        
        XCTAssertEqual(priceString, "foo shop 1 (1 - 13.37 EUR / 1 - 13.37 EUR)")
    }
    
    func test_givenEmptyAggregatedList_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 0, numberOfDoneItems: 0)
        XCTAssertEqual(priceString, "foo shop (0)")
    }
    
    func test_givenAggregatedListWithOneItem_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 0)
        XCTAssertEqual(priceString, "foo shop (1)")
    }
    
    func test_givenAggregatedListWithTwoItem_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 0)
        XCTAssertEqual(priceString, "foo shop (2)")
    }
    
    func test_givenAggregatedListWithOneItemOfTheDominantCurrency_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        list.addToItemsForList(item)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 1, numberOfDoneItems: 0)
        
        XCTAssertEqual(priceString, "foo shop (1 - 13.37 EUR)")
    }
    
    func test_givenAggregatedListWithTwoItemsOnlyOneOfTheDominantCurrency_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "USD").with(price: 08.15).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 0)
        
        XCTAssertEqual(priceString, "foo shop (2 - 13.37 EUR)")
    }
    
    func test_givenAggregatedListWithTwoItemsOfTheDominantCurrency_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop").with(currency: "EUR").with(price: 13.37).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop").with(currency: "EUR").with(price: 08.15).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop", objectCount: 2, numberOfDoneItems: 0)
        
        XCTAssertEqual(priceString, "foo shop (2 - 21.52 EUR)")
    }
    
    func test_givenAggregatedListWithTwoItemsOfTheDominantCurrencyOnlyOneInTheFittingShop_whenPriceString_thenReturnShopNameAndTotalItems() {
        let list = insertAggregatedList()
        let item1 = ShoppingListItemBuilder.create().with(name: "foo item 1").with(shopName: "foo shop 1").with(currency: "EUR").with(price: 13.37).insert()
        let item2 = ShoppingListItemBuilder.create().with(name: "foo item 2").with(shopName: "foo shop 2").with(currency: "EUR").with(price: 08.15).insert()
        list.addToItemsForList(item1)
        list.addToItemsForList(item2)
        
        let priceString = viewModel.priceString(for: list, inShop: "foo shop 1", objectCount: 1, numberOfDoneItems: 0)
        
        XCTAssertEqual(priceString, "foo shop 1 (1 - 13.37 EUR)")
    }
    
    // MARK: - Delete item
    
    func test_givenAnyShoppingListItem_whenDeleteItem_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        viewModel.deleteItem(item)
        XCTAssertEqual(dataManagerMock.didCallDeleteItem, true)
    }
    
    // MARK: - Convert amount quantity
    
    func test_givenValueZero_whenConvertAmountQuantity_thenTheResultIsCorrect() {
        let output = viewModel.convertAmountQuantity(NSNumber(value: 0.0))
        XCTAssertEqual(output, "0")
    }
    
    func test_givenValueWithOneTooManyDecimalDigits_whenConvertAmountQuantity_thenTrimToTheMaximumNumberOfDecimalDigits() {
        let output = viewModel.convertAmountQuantity(NSNumber(value: 0.11111111111))
        let expectedOutput = "0." + String(repeating: "1", count: Constants.MAX_FRACTION_DIGITS)
        XCTAssertEqual(output, expectedOutput)
    }
    
    // MARK: - Detail Label Text
    
    func test_givenItemWithoutAmountAndWithoutPrice_whenDetailLabelText_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "")
    }
    
    func test_givenItemWithAmountAndWithoutPrice_whenDetailLabelText_thenReturnStringWithAmount() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 5.4).with(amountUnit: "kg").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "5.4 kg")
    }
    
    func test_givenItemWithAmountWithoutQuantityAndWithoutPrice_whenDetailLabelText_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 5.4).insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "")
    }
    
    func test_givenItemWithAmountWithoutUnitAndWithoutPrice_whenDetailLabelText_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountUnit: "kg").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "")
    }
    
    func test_givenItemWithoutAmountAndWithPrice_whenDetailLabelText_thenReturnStringWithPrice() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 1.2).with(currency: "EUR").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "1.20 EUR")
    }
    
    func test_givenItemWithoutAmountAndWithPriceWithoutPrice_whenDetailLabelText_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(currency: "EUR").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "")
    }
    
    func test_givenItemWithoutAmountAndWithPriceWithoutCurrency_whenDetailLabelText_thenReturnEmptyString() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 1.2).insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "")
    }
    
    func test_givenItemWithAmountAndWithPrice_whenDetailLabelText_thenReturnStringWithAmountAndPrice() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 1.2).with(currency: "EUR").with(amountQuantity: 5.4).with(amountUnit: "kg").insert()
        let detailLabelText = viewModel.detailLabelText(for: item)
        XCTAssertEqual(detailLabelText, "5.4 kg: 1.20 EUR")
    }
    
    // MARK: - Mein label text
    
    func test_givenItemWithName_whenMainLabelText_thenReturnStringWithName() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let mainLabelText = viewModel.mainLabelText(for: item)
        XCTAssertEqual(mainLabelText, "foo item")
    }
    
    func test_givenItemWithNameAndBrand_whenMainLabelText_thenReturnStringWithNameAndBrand() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(brandName: "foo brand").insert()
        let mainLabelText = viewModel.mainLabelText(for: item)
        XCTAssertEqual(mainLabelText, "foo item (foo brand)")
    }
}

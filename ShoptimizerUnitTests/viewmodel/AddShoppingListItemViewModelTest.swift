// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
@testable import Shoptimizer

class AddShoppingListItemViewModelTest: ShoptimizerTestCase {
    
    private var aggregatedListHelperMock: AggregatedListHelperMock!
    private var dataManagerMock: AddShoppingListItemDataManagerMock!
    private var viewModel: AddShoppingListItemViewModel!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        ShoppingListItem.deleteAll()
        aggregatedListHelperMock = AggregatedListHelperMock()
        dataManagerMock = AddShoppingListItemDataManagerMock(shoppingListRepository: ShoppingListRepositoryMock(), shoppingListItemRepository: ShoppingListItemRepositoryMock())
        viewModel = AddShoppingListItemViewModel(aggregatedListHelper: aggregatedListHelperMock, dataManager: dataManagerMock)
    }
    
    // MARK: - Name of
    
    func test_givenNoShoppingListItem_whenNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = viewModel.name(of: item)
        XCTAssertEqual(dataManagerMock.didCallNameOf, true)
    }
    
    // MARK: - Shop name of
    
    func test_givenNoShoppingListItem_whenShopNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().insert()
        let _ = viewModel.shopName(of: item)
        XCTAssertEqual(dataManagerMock.didCallShopNameOf, true)
    }
    
    // MARK: - Amount unit of
    
    func test_givenShoppingListItemWithoutAmountUnit_whenAmountUnitOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = viewModel.amountUnit(of: item)
        XCTAssertEqual(dataManagerMock.didCallAmountUnitOf, true)
    }
    
    // MARK: - Currency of
    
    func test_givenShoppingListItemWithoutCurrency_whenCurrencyOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = viewModel.currency(of: item)
        XCTAssertEqual(dataManagerMock.didCallCurrencyOf, true)
    }
    
    // MARK: - Brand name of
    
    func test_givenShoppingListItemWithoutBrandName_whenBrandNameOf_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = viewModel.brandName(of: item)
        XCTAssertEqual(dataManagerMock.didCallBrandNameOf, true)
    }
    
    // MARK: - List for
    
    func test_givenNoShoppingListItem_whenListFor_thenTheDataManagerFunctionIsCalled() {
        let _ = viewModel.list(for: nil)
        XCTAssertEqual(dataManagerMock.didCallListFor, true)
    }
    
    // MARK: - Done for
    
    func test_givenNilShoppingListItem_whenDoneFor_thenTheDataManagerFunctionIsCalled() {
        let _ = viewModel.done(for: nil)
        XCTAssertEqual(dataManagerMock.didCallDoneFor, true)
    }
    
    // MARK: - Is list item available
    
    func test_givenEmptyList_whenIsListItemAvailable_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.isListItemAvailable(withName: "", andShop: "", inList: list)
        XCTAssertEqual(dataManagerMock.didCallIsListItemAvailable, true)
    }
    
    // MARK: - Is list included in aggregated list
    
    func test_givenNilList_whenIsListIncludedInAggregatedList_thenTheDataManagerFunctionIsCalled() {
        let _ = viewModel.isListIncludedInAggregatedList(nil)
        XCTAssertEqual(dataManagerMock.didCallIsListIncludedInAggregatedList, true)
    }
    
    // MARK: - Insert new shopping list item
    
    func test_givenAnyDetails_whenInsertNewShoppingListItem_thenTheDataManagerFunctionIsCalled() {
        let list = ShoppingListBuilder.create().with(name: "foo list").insert()
        let _ = viewModel.insertNewShoppingListItem(withName: "foo item", quantity: NSNumber(value: 1), unit: "kg", doneStatus: true, brand: "foo brand", forPrice: NSNumber(value: 2), atCurrency: "EUR", inShop: "foo shop", forList: list)
        XCTAssertEqual(dataManagerMock.didCallInsertNewShoppingListItem, true)
    }
    
    // MARK: - Update shopping list item
    
    func test_givenAnyDetails_whenUpdateShoppingListItem_thenTheDataManagerFunctionIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "fubar").insert()
        viewModel.updateShoppingListItem(item, itemName: "foo item", shopName: "foo shop", amountQuantity: NSNumber(value: 1), amountUnit: "kg", brandName: "foo brand", price: NSNumber(value: 2), currency: "EUR", notes: "foo notes")
        XCTAssertEqual(dataManagerMock.didCallUpdateShoppingListItem, true)
    }
    
    // MARK: - Ensure valid double
    
    func test_givenNilString_whenEnsureValidDouble_thenReturnNil() {
        let result = viewModel.ensureValidDouble(fromString: nil, allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenEmptyString_whenEnsureValidDouble_thenReturnNil() {
        let result = viewModel.ensureValidDouble(fromString: "", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenNilStringAndAllowZero_whenEnsureValidDouble_thenReturnNSNumberContainingZero() {
        let result = viewModel.ensureValidDouble(fromString: nil, allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenEmptyStringAndAllowZero_whenEnsureValidDouble_thenReturnANSNumberContainingZero() {
        let result = viewModel.ensureValidDouble(fromString: "", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenAStringContainingALetter_whenEnsureValidDouble_thenReturnNil() {
        let result = viewModel.ensureValidDouble(fromString: "a", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenAStringContainingTheNumberZeroAndNotAllowZero_whenEnsureValidDouble_thenReturnNil() {
        let result = viewModel.ensureValidDouble(fromString: "0", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenAStringContainingNumberGreaterThanZeroAndNotAllowZero_whenEnsureValidDouble_thenReturnTheNumber() {
        let result = viewModel.ensureValidDouble(fromString: "1", allowZero: false)
        XCTAssertEqual(result, NSNumber(value: 1))
    }
    
    func test_givenAStringContainingTheNumberZeroAndAllowZero_whenEnsureValidDouble_thenReturnTheNumber() {
        let result = viewModel.ensureValidDouble(fromString: "0", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenAStringContainingNumberGreaterThanZeroAndAllowZero_whenEnsureValidDouble_thenReturnTheNumber() {
        let result = viewModel.ensureValidDouble(fromString: "1", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 1))
    }
    
    func test_givenAStringContainingANumberLessThanZero_whenEnsureValidDouble_thenReturnNil() {
        let result = viewModel.ensureValidDouble(fromString: "-1", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    // MARK: - Ensure valid double
    
    func test_givenNilString_whenEnsureValidPrice_thenReturnNil() {
        let result = viewModel.ensureValidPrice(fromString: nil, allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenEmptyString_whenEnsureValidPrice_thenReturnNil() {
        let result = viewModel.ensureValidPrice(fromString: "", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenNilStringAndAllowZero_whenEnsureValidPrice_thenReturnNSNumberContainingZero() {
        let result = viewModel.ensureValidPrice(fromString: nil, allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenEmptyStringAndAllowZero_whenEnsureValidPrice_thenReturnANSNumberContainingZero() {
        let result = viewModel.ensureValidPrice(fromString: "", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenAStringContainingALetter_whenEnsureValidPrice_thenReturnNil() {
        let result = viewModel.ensureValidPrice(fromString: "a", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenAStringContainingTheNumberZeroAndNotAllowZero_whenEnsureValidPrice_thenReturnNil() {
        let result = viewModel.ensureValidPrice(fromString: "0", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    func test_givenAStringContainingNumberGreaterThanZeroAndNotAllowZero_whenEnsureValidPrice_thenReturnTheNumber() {
        let result = viewModel.ensureValidPrice(fromString: "1", allowZero: false)
        XCTAssertEqual(result, NSNumber(value: 1))
    }
    
    func test_givenAStringContainingTheNumberZeroAndAllowZero_whenEnsureValidPrice_thenReturnTheNumber() {
        let result = viewModel.ensureValidPrice(fromString: "0", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 0))
    }
    
    func test_givenAStringContainingNumberGreaterThanZeroAndAllowZero_whenEnsureValidPrice_thenReturnTheNumber() {
        let result = viewModel.ensureValidPrice(fromString: "1", allowZero: true)
        XCTAssertEqual(result, NSNumber(value: 1))
    }
    
    func test_givenAStringContainingANumberLessThanZero_whenEnsureValidPrice_thenReturnNil() {
        let result = viewModel.ensureValidPrice(fromString: "-1", allowZero: false)
        XCTAssertEqual(result, nil)
    }
    
    // MARK: - Subtract item from aggregated list
    
    func test_whenSubtractItemFromAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        viewModel.subtractItemFromAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallSubtractItemFromAggregatedList)
    }
    
    // MARK: - Add item to aggregated list
    
    func test_givenAnyDetails_whenAddItemToAggregatedList_thenAggregatedListHelperIsCalled() {
        let _ = insertAggregatedList()
        viewModel.addItemToAggregatedList("foo name", forShop: "foo shop", amountQuantity: nil, andAmountUnit: nil, andBrand: nil, forPrice: nil, atCurrency: nil)
        XCTAssertTrue(aggregatedListHelperMock.didCallAddItemToaggregatedListByDetails)
    }
    
    func test_givenAnyItem_whenAddItemToAggregatedList_thenAggregatedListHelperIsCalled() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let _ = insertAggregatedList()
        viewModel.addItemToAggregatedList(item)
        XCTAssertTrue(aggregatedListHelperMock.didCallAddItemToAggregatedList)
    }
    
    // MARK: - Quantity field text for
    
    func test_givenShoppingListItemWithoutAmountQuantity_whenQuantityFieldTextFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let quantityFieldText = viewModel.quantityFieldText(for: item)
        XCTAssertEqual(quantityFieldText, nil)
    }
    
    func test_givenShoppingListItemWithAmountQuantity_whenQuantityFieldTextFor_thenReturnQuantityFieldText() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(amountQuantity: 13.37).insert()
        let quantityFieldText = viewModel.quantityFieldText(for: item)
        XCTAssertEqual(quantityFieldText, "13.37")
    }
    
    // MARK: - Price field text for
    
    func test_givenShoppingListItemWithoutPrice_whenPriceFieldTextFor_thenReturnNil() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").insert()
        let priceFieldText = viewModel.priceFieldText(for: item)
        XCTAssertEqual(priceFieldText, nil)
    }
    
    func test_givenShoppingListItemWithPrice_whenPriceFieldTextFor_thenReturnPriceFieldText() {
        let item = ShoppingListItemBuilder.create().with(name: "foo item").with(price: 4.20).insert()
        let priceFieldText = viewModel.priceFieldText(for: item)
        XCTAssertEqual(priceFieldText, "4.20")
    }
}

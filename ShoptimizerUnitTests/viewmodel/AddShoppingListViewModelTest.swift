// SPDX-FileCopyrightText: 2024 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest
import Foundation
@testable import Shoptimizer

class AddShoppingListViewModelTest: ShoptimizerTestCase {
    
    private var viewModel: AddShoppingListViewModel!
    
    override func setUp() {
        super.setUp()
        ShoppingList.deleteAll()
        viewModel = AddShoppingListViewModel()
    }
    
    // MARK: - Trim name
    
    func test_givenNil_whenTrimName_thenReturnEmptyString() {
        let trimmedName = viewModel.trimName(nil)
        XCTAssertEqual(trimmedName, "")
    }
    
    func test_givenEmptyString_whenTrimName_thenReturnEmptyString() {
        let trimmedName = viewModel.trimName("")
        XCTAssertEqual(trimmedName, "")
    }
    
    func test_givenStringWithWhitespace_whenTrimName_thenReturnEmptyString() {
        let trimmedName = viewModel.trimName(" ")
        XCTAssertEqual(trimmedName, "")
    }
    
    func test_givenStringWithCharacters_whenTrimName_thenReturnThatString() {
        let trimmedName = viewModel.trimName("foo")
        XCTAssertEqual(trimmedName, "foo")
    }
    
    func test_givenStringWithCharactersAndWhitespace_whenTrimName_thenReturnTheStringWithoutWhitespace() {
        let trimmedName = viewModel.trimName(" foo ")
        XCTAssertEqual(trimmedName, "foo")
    }
    
    // MARK: - Name of
    
    func test_givenNil_whenNameOf_thenReturnNil() {
        let name = viewModel.name(of: nil)
        XCTAssertEqual(name, nil)
    }
    
    func test_givenShoppingListWithName_whenNameOf_thenReturnThatName() {
        let shoppingList = ShoppingListBuilder.create().with(name: "foo").insert()
        let name = viewModel.name(of: shoppingList)
        XCTAssertEqual(name, "foo")
    }
    
    // MARK: - Set name
    
    func test_givenNilList_whenSetName_thenNothingHappens() {
        XCTAssertNoThrow(viewModel.setName("foo", for: nil))
    }
    
    func test_givenShoppingListWithName_whenSetName_thenTheShoppingListHasTheNewName() {
        let shoppingList = ShoppingListBuilder.create().with(name: "foo").insert()
        viewModel.setName("bar", for: shoppingList)
        XCTAssertEqual(shoppingList.name, "bar")
    }
    
    // MARK: - Set list included in aggregated list
    
    func test_givenNilList_whenSetListIncludedInAggregatedList_thenNothingHappens() {
        XCTAssertNoThrow(viewModel.setListIncludedInAggregatedList(nil, isIncluded: false))
    }
    
    func test_givenShoppingList_whenSetListIncludedInAggregatedList_thenTheShoppingListIsIncludedInTheAggregatedList() {
        let shoppingList = ShoppingListBuilder.create().with(name: "foo").with(includedInAggregatedList: false).insert()
        viewModel.setListIncludedInAggregatedList(shoppingList, isIncluded: true)
        XCTAssertTrue(shoppingList.include_in_aggregate!.boolValue)
    }
    
    // MARK: - Exists shopping list
    
    func test_givenNameOfNonExistingShoppingList_whenExistsShoppingList_thenReturnFalse() {
        let _ = ShoppingListBuilder.create().with(name: "foo").insert()
        let result = viewModel.existsShoppingList(withName: "bar")
        XCTAssertFalse(result)
    }
    
    func test_givenNameOfExistingShoppingList_whenExistsShoppingList_thenReturnTrue() {
        let _ = ShoppingListBuilder.create().with(name: "foo").insert()
        let result = viewModel.existsShoppingList(withName: "foo")
        XCTAssertTrue(result)
    }
    
    // MARK: - Insert new shopping list
    
    func test_givenDetailsOfShoppingList_whenInsertNewShoppingList_ThenShoppingListIsInserted() {
        let insertedShoppingList = viewModel.insertNewShoppingList(withName: "foo", andType: ShoppingListType.dynamic, includeInAggregatedList: true)
        
        XCTAssertEqual(insertedShoppingList!.name, "foo")
        XCTAssertEqual(insertedShoppingList!.type, ShoppingListType.dynamic.rawValue)
        XCTAssertEqual(insertedShoppingList!.include_in_aggregate, true)
    }
}

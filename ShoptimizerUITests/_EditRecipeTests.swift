// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditRecipeTests: XCTestCase {
        
    func testChangesName() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "Recipe 2")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Recipe 2"))
    }
    
    func testShowsWarningWhenChangingNameToUsedOne() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "Recipe 2")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }
    
    func testDoesNotChangeNameToUsedOne() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "Recipe 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
    }
    
    func testShowsWarningWhenChangingNameToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotChangeNameToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
    }
    
    func testShowsWarningWhenChangingNameToWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "    ")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotChangeNameToWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe1, recipe2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
    }
    
    func testTappingCancelButtonDoesNotChangeRecipe() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(name: "Recipe 2", portions: "2")
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testChangesPortions() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "2")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "2"))
    }
    
    func testShowsWarningWhenChangingPortionsToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_PORTIONS_MISSING))
    }
    
    func testDoesNotChangePortionsToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_PORTIONS_MISSING)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testShowsWarningWhenChangingPortionsToNotANaturalNumber() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "1.1")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangePortionsToNotANaturalNumber() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "1.1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testShowsWarningWhenChangingPortionsToHaveLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "1a")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangePortionsToHaveLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testShowsWarningWhenChangingPortionsToZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "0")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangePortionsToZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "0")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testShowsWarningWhenChangingPortionsToLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "-1")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangePortionsToLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")
        app.enterRecipeDetails(portions: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
}

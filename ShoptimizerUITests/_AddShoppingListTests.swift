// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AddShoppingListTests: XCTestCase {

    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        
        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_ADD_SHOPPING_LIST))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_CANCEL))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_SAVE))
        XCTAssertTrue(app.textFields.count == 1)
        XCTAssertTrue(app.switches.count == 1)
        XCTAssertTrue(app.switches.element.isEnabled)
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_NAME))
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_INCLUDE_IN_AGGREGATE))
    }

    func testShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateListWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
    }

    func testShowsWarningWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "    ", includeInAggregatedList: true)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotCreateListWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "    ", includeInAggregatedList: true)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
    }

    func testCreatesListWithNameAndIncludeOptionTrue() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "List 1", includeInAggregatedList: true)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: true, atIndex: 1))
    }

    func testCreatesListWithNameAndIncludeOptionFalse() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "List 1", includeInAggregatedList: false)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: false, atIndex: 1))
    }

    func testShowsWarningWhenCreatingDuplicate() {
        let app = XCUIApplication()

        // Precondition:

        let list = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        app.launch(with: list)

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "List 1", includeInAggregatedList: true)
        app.tapSaveButton()

        // Expected results:

        XCTAssert(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotCreateDuplicate() {
        let app = XCUIApplication()

        // Precondition:

        let list = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        app.launch(with: list)

        // Execution:

        app.tapAddButton()
        app.enterShoppingListDetails(name: "List 1", includeInAggregatedList: true)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected results:

        XCTAssert(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: false, atIndex: 1))
    }
    
    func testTappingCancelButtonDoesNotCreateList() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.tapAddButton()
        app.enterShoppingListDetails(name: "List 1", includeInAggregatedList: true)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
    }
}

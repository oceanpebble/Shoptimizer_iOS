// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _RecipeOptionsTests: XCTestCase {

    // MARK: - Create List

    func testCreatesShoppingListWithSameNumberOfPortions() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.confirmAlert() // portions
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: "Recipe 1"))
        app.stepIntoShoppingList(name: "Recipe 1")
        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 2", amountQuantity: "0.5", amountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 3", amountQuantity: "1", amountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 4", amountQuantity: "15", amountUnit: app.UNIT_G))
    }

    func testCreatesShoppingListWithDifferentNumberOfPortions() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("4")
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: "Recipe 1"))
        app.stepIntoShoppingList(name: "Recipe 1")
        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 2", amountQuantity: "1", amountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 3", amountQuantity: "2", amountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 4", amountQuantity: "30", amountUnit: app.UNIT_G))
    }

    func testCreatesShoppingListWhenPortionsIsDecimal() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("1.5")
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: "Recipe 1"))
        app.stepIntoShoppingList(name: "Recipe 1")
        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 2", amountQuantity: "0.375", amountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 3", amountQuantity: "0.75", amountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsShoppingListItem(name: "Ingredient 4", amountQuantity: "11.25", amountUnit: app.UNIT_G))
    }

    func testDoesNotCreateListWithNoIncludedIngredients() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(includeInList: false).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWithPortionsIsLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("-1")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWithPortionsIsZero() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("0")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWithPortionsHasLetters() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("1a")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWithPortionsIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWithPortionsConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.alerts.element.textFields.element.clearAndTypeText("    ")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    func testDoesNotCreateListWhenCancellingCreation() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").with(includeInList: true).toLaunchArgumentString()
        app.launch(with: recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_CREATE_LIST].tap()
        app.dismissAlert() // portions

        // Expected Results:

        app.switchToShoppingListsTab()
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertFalse(app.existsShoppingList(name: "Recipe 1"))
    }

    // MARK: - Subtract from Supplies

    func testSubtractsFromSuppliesWithSameNumberOfPortions() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.confirmAlert() // portions
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "24.5", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "9", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "4.85", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testSubtractsFromSuppliesWithDifferentNumberOfPortions() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("4")
        app.confirmAlert() // portions
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "24", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "8", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "4.7", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testSubtractsFromSuppliesWhenNumberOfPortionsIsDecimal() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("1.5")
        app.confirmAlert() // portions
        app.confirmAlert() // success alert

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "24.625", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "9.25", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "4.8875", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenNumberOfPortionsIsLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("-1")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenNumberOfPortionsIsZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("0")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenNumberOfPortionsHasLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("1a")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenNumberOfPortionsIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenNumberOfPortionsConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.alerts.element.textFields.element.clearAndTypeText("    ")
        app.confirmAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    func testDoesNotSubtractFromSuppliesWhenCancellingSubtraction() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 10).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 3).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 150).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1.5).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, recipe, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")
        app.buttons[app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES].tap()
        app.dismissAlert() // portions

        // Expected Results:

        app.switchToSuppliesTab()
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 1", curAmountQuantity: "50", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 2", curAmountQuantity: "25", curAmountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 3", curAmountQuantity: "10", curAmountUnit: app.UNIT_CAN))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 4", curAmountQuantity: "5", curAmountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsSuppliesItem(name: "Ingredient 5", curAmountQuantity: "3", curAmountUnit: app.UNIT_PIECE))
    }

    // MARK: - Filter by Supplies

    func testFiltersRecipesWhenNumberOfPortionsIsOne() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_FILTERED))
    }

    func testFiltersRecipesWhenNumberOfPortionsIsTwo() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "2")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_FILTERED))
    }

    func testFiltersRecipesWhenNumberOfPortionsIsFive() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "5")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_FILTERED))
    }

    func testFiltersRecipesWhenNumberOfPortionsIsTen() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "10")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_FILTERED))
    }

    func testFiltersRecipesWhenNumberOfPortionsIsTwenty() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "20")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_FILTERED))
    }

    func testDoesNotFilterWhenNumberOfPortionsIsZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "0")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    func testDoesNotFilterWhenNumberOfPortionsIsLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "-1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    func testDoesNotFilterWhenNumberOfPortionsHasLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "1a")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    func testDoesNotFilterWhenNumberOfPortionsIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    func testDoesNotFilterWhenNumberOfPortionsConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.filterRecipes(portions: "   ")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    func testDoesNotFilterWhenCancellingFiltering() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Ingredient 1").with(curAmount: 50).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Ingredient 2").with(curAmount: 25).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Ingredient 3").with(curAmount: 9).with(curUnit: app.UNIT_CAN).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Ingredient 4").with(curAmount: 80).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Ingredient 5").with(curAmount: 11).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Ingredient 6").with(curAmount: 2.35).with(curUnit: app.UNIT_KG).toLaunchArgumentString()

        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").with(portions: 1).toLaunchArgumentString()
        let recipe3 = RecipeBuilder.create().with(name: "Recipe 3").with(portions: 5).toLaunchArgumentString()
        let recipe4 = RecipeBuilder.create().with(name: "Recipe 4").with(portions: 3).toLaunchArgumentString()

        let recipeIngredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 0).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient3 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()
        let recipeIngredient4 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.6).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 1").toLaunchArgumentString()

        let recipeIngredient5 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 1).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient6 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()
        let recipeIngredient7 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 2").toLaunchArgumentString()

        let recipeIngredient8 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient9 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient10 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()
        let recipeIngredient11 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.38).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 3").toLaunchArgumentString()

        let recipeIngredient12 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(amountQuantity: 0.5).with(amountUnit: app.UNIT_L).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient13 = RecipeIngredientBuilder.create().with(name: "Ingredient 3").with(amountQuantity: 1).with(amountUnit: app.UNIT_CAN).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient14 = RecipeIngredientBuilder.create().with(name: "Ingredient 4").with(amountQuantity: 15).with(amountUnit: app.UNIT_G).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient15 = RecipeIngredientBuilder.create().with(name: "Ingredient 5").with(amountQuantity: 2).with(amountUnit: app.UNIT_PIECE).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()
        let recipeIngredient16 = RecipeIngredientBuilder.create().with(name: "Ingredient 6").with(amountQuantity: 0.25).with(amountUnit: app.UNIT_KG).with(includeInSearch: true).with(recipeName: "Recipe 4").toLaunchArgumentString()

        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, recipe1, recipe2, recipe3, recipe4, recipeIngredient1, recipeIngredient2, recipeIngredient3, recipeIngredient4, recipeIngredient5, recipeIngredient6, recipeIngredient7, recipeIngredient8, recipeIngredient9, recipeIngredient10, recipeIngredient11, recipeIngredient12, recipeIngredient13, recipeIngredient14, recipeIngredient15, recipeIngredient16)

        // Execution:

        app.switchToRecipesTab()
        app.tapButton(title: app.BUTTON_TITLE_OPTIONS)
        app.tapButton(title: app.BUTTON_TITLE_FILTER_BY_SUPPLIES)
        app.dismissAlert()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 2"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 3"))
        XCTAssertTrue(app.existsRecipe(name: "Recipe 4"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
    }

    // TODO: testDoesNotFilterRecipeWhereIngredientIsNotIncludedInSearch
}

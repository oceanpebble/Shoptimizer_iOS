// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AddSuppliesItemTests: XCTestCase {
    
    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_CANCEL))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_SAVE))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_ADD_SUPPLIES_ITEM))
        XCTAssertTrue(app.textFields.count == 3)
        XCTAssertTrue(app.pickerWheels.count == 2)
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_NAME))
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_CURRENT))
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_MINIMUM))
        XCTAssertTrue(app.existsSingleButton(matching: app.BUTTON_TITLE_CATEGORIES))
    }

    func test_TappingCategoriesButton_OpensCategoriesPage() {
        let app = XCUIApplication()
        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()

        // Precondition:

        app.launch(with: suppliesItem, category1, category2, category3, category4)

        // Execution:

        app.switchToSuppliesTab()
        app.tapAddButton()
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)

        // Expected Results:

        //XCTAssertTrue(app.navigationBars.element.buttons.count == 2)
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_ADD_SUPPLIES_ITEM))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_ADD))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.cells.count == 4)
    }
    
    func testShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateItemWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateItemWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testCreatesListWithName() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
    }
    
    func testCreatesItemWithNameAndMinimumAmount() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "1.5", minAmountUnit: app.UNIT_KG)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "1.5", minAmountUnit: app.UNIT_KG))
    }

    func testCreatesItemWithNameAndCurrentAmount() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG))
    }

    func testCreatesItemWithNameAndAllAmounts() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "4", minAmountUnit: app.UNIT_PIECE, curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "4", minAmountUnit: app.UNIT_PIECE, curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG))
    }
    
    func testShowsWarningWhenCreatingDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }
    
    func testDoesNotCreateDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
    }
    
    func testCreatesItemWithMinimumAmountQuantityBeingZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "0", minAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "0", minAmountUnit: app.UNIT_PIECE))
    }
    
    func testShowsWarningWhenMinimumAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "-1", minAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateItemWhenMinimumAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "-1", minAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenMinimumAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "1a", minAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateItemWhenMinimumAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", minAmountQuantity: "1a", minAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testCreatesItemWithCurrentAmountQuantityBeingZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "0", curAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "0", curAmountUnit: app.UNIT_PIECE))
    }
    
    func testShowsWarningWhenCurrentAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "-1", curAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateItemWhenCurrentAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "-1", curAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenCurrentAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "1a", curAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateItemWhenCurrentAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1", curAmountQuantity: "1a", curAmountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testTappingCancelButtonDoesNotCreateItem() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToSuppliesTab()
        app.tapAddButton()
        app.enterSuppliesItemDetails(name: "Item 1")
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }

    func test_SelectCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4)

        // Execution:

        app.switchToSuppliesTab()
        app.tapAddButton()
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")

        // Expected Results:

        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
    }

    func test_SaveCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4)

        // Execution:

        app.switchToSuppliesTab()
        app.tapAddButton()
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")
        app.tapButton(title: "Add Supplies Item")
        app.enterSuppliesItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)

        // Expected Results:

        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
    }
}

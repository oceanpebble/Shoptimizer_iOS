// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AggregatedListTests: XCTestCase {
    // TODO: Tests for the list options -> make sure the aggregated list updated correctly (number of done/undone items, prices, etc.)
    // TODO: Tests for changing list from included to not included and vice versa
    // TODO: New Test Case: Delete item from aggregated list that has no shop name
 
    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.cells.count == 0)
        XCTAssertTrue(app.existsSingleButton(matching: "Shopping Lists"))
        XCTAssertFalse(app.existsStaticText(containing: app.CURRENCY_EUR))
    }

    func testDeleteButtonIsNotShown() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.swipeCellLeft(containing: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertFalse(app.existsSingleButton(matching: app.BUTTON_TITLE_DELETE))
    }
    
    func testOptionsArePresentedCorrectly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.openOptionsMenuForShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)
        
        // Expected results:
        
        XCTAssertTrue(app.existsSheet(withTitle: "Options for Aggregated List"))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_UPDATE_LIST))
    }

    func testEditViewCanNotBeOpened() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:
        
        app.openEditViewForShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SHOPPING_LISTS))
    }

    func testItemEditPageCanNotBeOpened() {
        let app = XCUIApplication()

        // Precondition:

        let item = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)
        app.openEditViewForShoppingListItem(name: "Item 1")

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.cells.count == 1)
    }

    func testDeleteButtonIsNotShownForItem() {
        let app = XCUIApplication()

        // Precondition:

        let item = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)
        app.swipeCellLeft(containing: "Item 1")

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.cells.count > 0)
        XCTAssertFalse(app.existsSingleButton(matching: app.BUTTON_TITLE_DELETE))
    }

    func testAggregatedListIsEmptyWhenAllIncludedShoppingListsAreDeleted() {
        let app = XCUIApplication()

        // Precondition:

        let list1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let list2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let list3 = ShoppingListBuilder.create().with(name: "List 3").with(includeInAggregatedList: false).toLaunchArgumentString()
        let item1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let item2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(listName: "List 2").toLaunchArgumentString()
        let item3 = ShoppingListItemBuilder.create().with(name: "Item 3").with(listName: "List 3").toLaunchArgumentString()
        let item4 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let item5 = ShoppingListItemBuilder.create().with(name: "Item 2").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: list1, list2, list3, item1, item2, item3, item4, item5)

        // Execution:

        app.deleteShoppingList(name: "List 1")
        app.deleteShoppingList(name: "List 2")
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected Results:

        XCTAssertTrue(app.cells.count == 0)
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditRecipeIngredientTests: XCTestCase {
    
    func testChangesName() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "Ingredient 2")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 2"))
    }

    func testShowsWarningWhenChangingNameToUsedOne() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let ingredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient1, ingredient2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "Ingredient 2")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }
    
    func testDoesNotChangeNameToUsedOne() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient1 = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        let ingredient2 = RecipeIngredientBuilder.create().with(name: "Ingredient 2").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient1, ingredient2)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "Ingredient 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1"))
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 2"))
    }
    
    func testShowsWarningWarningWhenChangingNameToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotChangeNameToEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1"))
    }
    
    func testShowsWarningWarningWhenChangingNameToWhiteSpaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "    ")
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotChangeNameToWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1"))
    }

    func testChagesAmountUnit() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)

        // Execution:

        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "2.5", amountUnit: app.UNIT_PIECE))
    }
    
    func testChangesAmountQuantity() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "3", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "3", amountUnit: app.UNIT_PIECE))
    }
    
    func testChangesAmountQuantityToZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "0", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "0", amountUnit: app.UNIT_PIECE))
    }
    
    func testShowsWarningWhenChangingAmountQuantityToLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "-1", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangeAmountQuantityToLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "-1", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "2.5", amountUnit: app.UNIT_L))
    }
    
    
    func testShowsWarningWhenChangingAmountQuantityWithLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "1a", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotChangeAmountQuantityWithLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 2.5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(amountQuantity: "1a", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected Results:
        
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "2.5", amountUnit: app.UNIT_L))
    }
    
    func testTappingCancelButtonDoesNotChangeIngredient() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.openEditViewForRecipeIngredient(name: "Ingredient 1")
        app.enterRecipeIngredientDetails(name: "Ingredient 2")
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1"))
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _RecipesOverviewTests: XCTestCase {
        
    func testDeletesRecipe() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 1).toLaunchArgumentString()
        app.launch(with: recipe)

        // Execution:

        app.switchToRecipesTab()
        app.deleteRecipe(name: "Recipe 1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testUIElementsShouldAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        app.launch(with: recipe)

        // Execution:

        app.switchToRecipesTab()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_RECIPES_UNFILTERED))
        XCTAssertTrue(app.existsSingleButton(matching: app.BUTTON_TITLE_ADD))
        XCTAssertTrue(app.existsSingleButton(matching: app.BUTTON_TITLE_OPTIONS))
        XCTAssertTrue(app.existsSingleButton(matching: app.BUTTON_TITLE_CANCEL))
        XCTAssertFalse(app.buttons[app.BUTTON_TITLE_CANCEL].isEnabled)
        XCTAssertTrue(app.searchFields.element.exists)
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "2"))
    }

    func testRecipeOptionsArePresentedCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        app.launch(with: recipe)

        // Execution:

        app.switchToRecipesTab()
        app.openOptionsMenuForRecipe(name: "Recipe 1")

        // Expected results:

        XCTAssertTrue(app.existsSheet(withTitle: "Options for Recipe 1"))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_CREATE_LIST))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_PREPARE_RECIPE))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_SCHEDULE_RECIPE))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_EDIT_RECIPE))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_SHARE_RECIPE))
    }

    func testRecipeEditViewCanBeOpened() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").with(portions: 2).toLaunchArgumentString()
        app.launch(with: recipe)

        // Execution:

        app.switchToRecipesTab()
        app.openEditViewForRecipe(name: "Recipe 1")

        // Expected results:

        XCTAssert(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_EDIT_RECIPE))
    }
    
}

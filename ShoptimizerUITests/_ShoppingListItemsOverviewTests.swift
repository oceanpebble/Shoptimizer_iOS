// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _ShoppingListItemsOverviewTests: XCTestCase {

    func testDeletesItem() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }
}

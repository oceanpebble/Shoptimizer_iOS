// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AddShoppingListItemTests: XCTestCase {
    
    func testUIElementsAppearCorrectly() {
        let shoptimizer = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        shoptimizer.launch(with: shoppingList)

        // Execution:

        shoptimizer.stepIntoShoppingList(name: "List 1")
        shoptimizer.tapAddButton()

        // Expected results:

        XCTAssertTrue(shoptimizer.navigationBar(hasIdentifier: shoptimizer.NAVBAR_TITLE_ADD_SHOPPING_LIST_ITEM))
        XCTAssertTrue(shoptimizer.navigationBar(hasButtonWithTitle: shoptimizer.BUTTON_TITLE_CANCEL))
        XCTAssertTrue(shoptimizer.navigationBar(hasButtonWithTitle: shoptimizer.BUTTON_TITLE_SAVE))
        XCTAssertTrue(shoptimizer.textFields.count == 5)
        for i in 0 ..< 5 {
            XCTAssertTrue(shoptimizer.textFields.element(boundBy: i).isEmptyOrPlaceholderValue)
        }
        XCTAssertTrue(shoptimizer.textViews.count == 1)
        XCTAssertTrue(shoptimizer.textViews.element.isEmptyOrPlaceholderValue)
        XCTAssertTrue(shoptimizer.pickerWheels.count == 2)
        XCTAssertTrue(shoptimizer.pickerWheels.element(boundBy: 0).value as? String == shoptimizer.UNIT_L)
        XCTAssertTrue(shoptimizer.pickerWheels.element(boundBy: 1).value as? String == shoptimizer.CURRENCY_EUR)
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_NAME))
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_BRAND))
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_SHOP))
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_AMOUNT))
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_PRICE))
        XCTAssertTrue(shoptimizer.existsStaticText(containing: shoptimizer.FIELD_NAME_LABEL_NOTES))
    }
    
    func testShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateItemWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "    ")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateItemWhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testCreatesItemWithName() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
    }
    
    func testCreatesItemWithNameAndShop() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 1"))
    }
    
    func testCreatesItemWithNameAndAmount() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "2.45", amountUnit: app.UNIT_L)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "2.45", amountUnit: app.UNIT_L))
    }
    
    func testCreatesItemWithNameAndBrand() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", brand: "Brand 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", brand: "Brand 1"))
    }

    func testCreatesItemWithNameAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.99", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemWithNameAndNotes() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", notes: "Foo")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        app.openEditViewForShoppingListItem(name: "Item 1")
        XCTAssertTrue(app.textViews.element.value as? String == "Foo")
    }
    
    func testShowsWarningWhenPriceIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "-1", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotCreateItemWhenPriceIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "-1", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenPriceHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "1a", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateItemWhenPriceHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "1a", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }

    func testCreatesItemWhenPriceIsZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssert(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertFalse(app.existsShoppingListItem(name: "Item 1", price: "0", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemAndRoundsDownPriceWhenPriceHasThreeDecimalDigits() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.874", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.87", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemAndRoundsUpPriceWhenPriceHasThreeDecimalDigits() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.875", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.88", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemAndAppendsZeroToPriceWhenPriceHasOneDecimalDigit() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.8", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.80", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemAndAppendsZeroesToPriceWhenPriceHasDotButNoDecimalDigits() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "1.", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.00", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemAndAppendsDotAndZeroesToPriceWhenPriceHasNoDot() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "1", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.00", currency: app.CURRENCY_EUR))
    }

    func testShowsWarningWhenAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "-1", amountUnit: app.UNIT_KG)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotCreateItemWhenAmountQuantityIsLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "-1", amountUnit: app.UNIT_KG)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testShowsWarningWhenAmountQuantityHasLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "1a", amountUnit: app.UNIT_KG)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotCreateItemWhenAmountQuantityHasLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "1a", amountUnit: app.UNIT_KG)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testCreatesItemWhenAmountQuantityIsZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "0", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssert(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertFalse(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0", amountUnit: app.UNIT_PIECE))
    }
    
    func testCreatesItemWithNameAndShopAndAmount() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)
        
        // Execution:
        
        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "1.5", amountUnit: app.UNIT_KG)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1.5", amountUnit: app.UNIT_KG, shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndShopAndBrand() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", brand: "Brand 1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", brand: "Brand 1", shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndShopAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", price: "2.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "2.99", currency: app.CURRENCY_EUR, shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndAmountAndBrand() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "2.9", amountUnit: app.UNIT_ML, brand: "Brand 1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "2.9", amountUnit: app.UNIT_ML, brand: "Brand 1"))
    }

    func testCreatesItemWithNameAndAmountAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "2.9", amountUnit: app.UNIT_ML, price: "0.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "2.9", amountUnit: app.UNIT_ML, price: "0.49", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemWithNameAndBrandAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", brand: "Brand 1", price: "0.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", brand: "Brand 1", price: "0.49", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemWithNameAndShopAndAmountAndBrand() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1", shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndShopAndAmountAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, price: "5.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, price: "5.49", currency: app.CURRENCY_EUR, shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndAmountAndBrandAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR))
    }

    func testCreatesItemWithNameAndShopAndBrandAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR, shop: "Shop 1"))
    }

    func testCreatesItemWithNameAndShopAndAmountAndBrandAndPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0.5", amountUnit: app.UNIT_PIECE, brand: "Brand 1", price: "5.49", currency: app.CURRENCY_EUR, shop: "Shop 1"))
    }

    func testShowsWarningWhenCreatingDuplicateInSameShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotCreateDuplicateInSameShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
    }

    func testDoesCreateDuplicateInDifferentShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 2"))
    }

    // MARK: - Tests that affect the Aggregated List

    // MARK: - Items on One Nonincluded List

    func testDoesNotUpdateAggregatedListWhenAddingItemToNonIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "2", amountUnit: app.UNIT_KG, brand: "Brand 1", price: "0.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    // MARK: - Items on Mixed Lists

    func testDoesNotUpdateAggregatedListWhenAddingItemToNonIncludedListThatIsAlsoOnIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(brand: "Brand 1").with(shop: "Shop 1").with(amount: 2).with(unit: app.UNIT_KG).with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(brand: "Brand 1").with(shop: "Shop 1").with(amount: 2).with(unit: app.UNIT_KG).with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", shop: "Shop 1", amountQuantity: "2", amountUnit: app.UNIT_KG, brand: "Brand 1", price: "0.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "2", amountUnit: app.UNIT_KG, brand: "Brand 1", price: "0.99", currency: app.CURRENCY_EUR, shop: "Shop 1"))
    }

    // MARK: - Items on One Included List

    func testDoesNotShowPriceOnAggregatedListWhenCreatingItemWithoutPriceOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertFalse(app.existsStaticText(containing: app.CURRENCY_EUR))
    }

    func testUpdatesPriceOnAggregatedListWhenCreatingSecondItemInSameShopOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 2", price: "0.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.existsShop(name: nil, totalItems: "2", totalPrice: "1.48", currency: app.CURRENCY_EUR))
    }

    // MARK: - Items on Two Included Lists

    func testUpdatesPriceOnAggregatedListWhenCreatingSecondItemWithSameDetailsOnSecondIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.49", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "1", totalPrice: "1.48", currency: app.CURRENCY_EUR))
    }

    func testDoesNotMergeItemsWhenCreatingSecondItemWithDifferentCurrencyOnSecondIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 1", price: "0.49", currency: app.CURRENCY_USD)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "2", totalPrice: "0.99", currency: app.CURRENCY_EUR))
    }
}

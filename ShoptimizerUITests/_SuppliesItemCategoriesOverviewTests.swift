// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _SuppliesItemCategoriesOverviewTests: XCTestCase {
        
    func test_UIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Category 2").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Category 1").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Category 1").toLaunchArgumentString()
        app.launch(with: category1, category2, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_EDIT_ITEM))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_ADD))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.searchFields.count == 1)
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsSingleButton(matching: "Cancel"))
        XCTAssertTrue(app.existsCell(containing: ["Category 1"]))
        XCTAssertTrue(app.existsCell(containing: ["Category 2"]))
    }

    func test_DeleteCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Category 2").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Category 1").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Category 1").toLaunchArgumentString()
        app.launch(with: category1, category2, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.deleteSuppliesItemCategory(name: "Category 1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 2"))
        XCTAssertFalse(app.isCategorySelected(name: "Category 2"))
        app.tapButton(title: "Edit Item")
        app.tapCancelButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 2"))
        XCTAssertFalse(app.isCategorySelected(name: "Category 2"))
    }

    func test_SearchCategories() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Fruit").toLaunchArgumentString()
        let category5 = SuppliesItemCategoryBuilder.create().with(name: "Vegatable").toLaunchArgumentString()
        let category6 = SuppliesItemCategoryBuilder.create().with(name: "Spread2").toLaunchArgumentString()
        let category7 = SuppliesItemCategoryBuilder.create().with(name: "At Home").toLaunchArgumentString()
        let category8 = SuppliesItemCategoryBuilder.create().with(name: "Work").toLaunchArgumentString()
        let category9 = SuppliesItemCategoryBuilder.create().with(name: "Holiday Home").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, category5, category6, category7, category8, category9, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.searchFields.element.tapAndTypeText("Hom")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsSuppliesItemCategory(name: "At Home"))
        XCTAssertTrue(app.existsSuppliesItemCategory(name: "Holiday Home"))
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES_FILTERED))
    }

    func test_CancelSearchTroughCancelButton() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Fruit").toLaunchArgumentString()
        let category5 = SuppliesItemCategoryBuilder.create().with(name: "Vegatable").toLaunchArgumentString()
        let category6 = SuppliesItemCategoryBuilder.create().with(name: "Spread").toLaunchArgumentString()
        let category7 = SuppliesItemCategoryBuilder.create().with(name: "At Home").toLaunchArgumentString()
        let category8 = SuppliesItemCategoryBuilder.create().with(name: "Work").toLaunchArgumentString()
        let category9 = SuppliesItemCategoryBuilder.create().with(name: "Holiday Home").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, category5, category6, category7, category8, category9, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.searchFields.element.tapAndTypeText("Hom")
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 9)
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
    }

    func test_CancelSearchTroughEmptyingSearchField() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Fruit").toLaunchArgumentString()
        let category5 = SuppliesItemCategoryBuilder.create().with(name: "Vegatable").toLaunchArgumentString()
        let category6 = SuppliesItemCategoryBuilder.create().with(name: "Spread").toLaunchArgumentString()
        let category7 = SuppliesItemCategoryBuilder.create().with(name: "At Home").toLaunchArgumentString()
        let category8 = SuppliesItemCategoryBuilder.create().with(name: "Work").toLaunchArgumentString()
        let category9 = SuppliesItemCategoryBuilder.create().with(name: "Holiday Home").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, category5, category6, category7, category8, category9, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.searchFields.element.tapAndTypeText("Hom")
        app.searchFields.element.clearAndTypeText("")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 9)
        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _SuppliesItemsOverviewTest: XCTestCase {
        
    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SUPPLIES))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_CREATE_LIST))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_ADD))
        XCTAssertTrue(app.searchFields.count == 1)
        XCTAssertTrue(app.searchFields.element.placeholderValue == "Name or Category")
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
    }

    func testDeletesItem() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.deleteSuppliesItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testHighlightsForAmountsWithSameUnits() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let suppliesItemOut = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.499).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItemLow1 = SuppliesItemBuilder.create().with(name: "Item 2").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.95).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItemLow2 = SuppliesItemBuilder.create().with(name: "Item 3").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItemStocked = SuppliesItemBuilder.create().with(name: "Item 4").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 2.0).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItemLow1, suppliesItemLow2, suppliesItemOut, suppliesItemStocked)
        
        // Execution:
        
        app.switchToSuppliesTab()
        
        // Expected results:
        
        XCTAssertTrue(app.isSuppliesItemOut(name: "Item 1"))
        XCTAssertTrue(app.isSuppliesItemLow(name: "Item 2"))
        XCTAssertTrue(app.isSuppliesItemLow(name: "Item 3"))
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 4"))
    }
    
    func testHighlightsForAmountsWithConvertibleUnits() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let suppliesItemOut = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1499).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItemLow1 = SuppliesItemBuilder.create().with(name: "Item 2").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1950).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItemLow2 = SuppliesItemBuilder.create().with(name: "Item 3").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1500).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        let suppliesItemStocked = SuppliesItemBuilder.create().with(name: "Item 4").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 2000).with(curUnit: app.UNIT_G).toLaunchArgumentString()
        app.launch(with: suppliesItemLow1, suppliesItemLow2, suppliesItemOut, suppliesItemStocked)
        
        // Execution:
        
        app.switchToSuppliesTab()
        
        // Expected results:
        
        XCTAssertTrue(app.isSuppliesItemOut(name: "Item 1"))
        XCTAssertTrue(app.isSuppliesItemLow(name: "Item 2"))
        XCTAssertTrue(app.isSuppliesItemLow(name: "Item 3"))
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 4"))
    }
    
    func testHighlightsForAmountsWithDifferentUnits() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let suppliesItemOut = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.499).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItemLow1 = SuppliesItemBuilder.create().with(name: "Item 2").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.95).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItemLow2 = SuppliesItemBuilder.create().with(name: "Item 3").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 1.5).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        let suppliesItemStocked = SuppliesItemBuilder.create().with(name: "Item 4").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).with(curAmount: 2000).with(curUnit: app.UNIT_PIECE).toLaunchArgumentString()
        app.launch(with: suppliesItemLow1, suppliesItemLow2, suppliesItemOut, suppliesItemStocked)
        
        // Execution:
        
        app.switchToSuppliesTab()
        
        // Expected results:
        
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 1"))
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 2"))
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 3"))
        XCTAssertTrue(app.isSuppliesItemStocked(name: "Item 4"))
    }

    func test_SearchItems_ByName() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let item3 = SuppliesItemBuilder.create().with(name: "Apple").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, item1, item2, item3)

        // Execution:

        app.switchToSuppliesTab()
        app.searchFields.element.tapAndTypeText("Apple")

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SUPPLIES_FILTERED))
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Apple"))
    }

    func test_SearchItems_ByCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Flour").with(category: "Spice").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Meat").with(category: "Meat").toLaunchArgumentString()
        let item3 = SuppliesItemBuilder.create().with(name: "Apple").with(category: "Spice").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, item1, item2, item3)

        // Execution:

        app.switchToSuppliesTab()
        app.searchFields.element.tapAndTypeText("Spice")

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SUPPLIES_FILTERED))
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsSuppliesItem(name: "Apple"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Flour"))
    }

    func test_CancelSearch_TroughCancelButton() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Flour").with(category: "Spice").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Meat").with(category: "Meat").toLaunchArgumentString()
        let item3 = SuppliesItemBuilder.create().with(name: "Apple").with(category: "Spice").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, item1, item2, item3)

        // Execution:

        app.switchToSuppliesTab()
        app.searchFields.element.tapAndTypeText("Spice")
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SUPPLIES))
        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsSuppliesItem(name: "Apple"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Flour"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Meat"))
    }

    func test_CancelSearch_TroughEmptyingSearchField() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Spice").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Dairy Product").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Meat").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Flour").with(category: "Spice").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Meat").with(category: "Meat").toLaunchArgumentString()
        let item3 = SuppliesItemBuilder.create().with(name: "Apple").with(category: "Spice").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, item1, item2, item3)

        // Execution:

        app.switchToSuppliesTab()
        app.searchFields.element.tapAndTypeText("Spice")
        app.searchFields.element.clearAndTypeText("")

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SUPPLIES))
        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsSuppliesItem(name: "Apple"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Flour"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Meat"))
    }
}

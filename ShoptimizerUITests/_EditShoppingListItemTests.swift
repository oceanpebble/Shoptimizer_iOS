// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditShoppingListItemTests: XCTestCase {

    func testShowsCorrectItemDataForItemWithAllData() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(brand: "Brand 1").with(amount: 1.48).with(unit: app.UNIT_PIECE).with(price: 0.99).with(currency: app.CURRENCY_EUR).with(notes: "Foo").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(brand: "Brand 2").with(amount: 0.5).with(unit: app.UNIT_KG).with(price: 1.49).with(currency: app.CURRENCY_USD).with(notes: "Bar").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(app.textFields[app.FIELD_NAME_NAME].value as? String == "Item 1")
        XCTAssertTrue(app.textFields[app.FIELD_NAME_SHOP].value as? String == "Shop 1")
        XCTAssertTrue(app.textFields[app.FIELD_NAME_BRAND].value as? String == "Brand 1")
        XCTAssertTrue(app.textFields[app.FIELD_NAME_AMOUNT_QUANTITY].value as? String == app.localizeDouble(from: "1.48"))
        XCTAssertTrue(app.textFields[app.FIELD_NAME_PRICE].value as? String == app.localizePrice(from: "0.99"))
        XCTAssertTrue(app.textViews.element.value as? String == "Foo")
        XCTAssertTrue(app.pickerWheels.element(boundBy: app.INDEX_AMOUNT_UNIT_PICKER).value as? String == "piece")
        XCTAssertTrue(app.pickerWheels.element(boundBy: app.INDEX_CURRENCY_PICKER).value as? String == "EUR")
    }

    func testShowsCorrectItemDataForItemWithNameOnly() {
        let shoptimizer = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(brand: "Brand 2").with(amount: 0.5).with(unit: shoptimizer.UNIT_KG).with(price: 1.49).with(currency: shoptimizer.CURRENCY_USD).with(notes: "Bar").with(listName: "List 1").toLaunchArgumentString()
        shoptimizer.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        shoptimizer.stepIntoShoppingList(name: "List 1")
        shoptimizer.openEditViewForShoppingListItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(shoptimizer.textFields[shoptimizer.FIELD_NAME_NAME].hasNonNilStringValue("Item 1"))
        XCTAssertTrue(shoptimizer.textFields[shoptimizer.FIELD_NAME_BRAND].isEmptyOrPlaceholderValue)
        XCTAssertTrue(shoptimizer.textFields[shoptimizer.FIELD_NAME_SHOP].isEmptyOrPlaceholderValue)
        XCTAssertTrue(shoptimizer.textFields[shoptimizer.FIELD_NAME_AMOUNT_QUANTITY].hasNonNilStringValue("0"))
        XCTAssertTrue(shoptimizer.textFields[shoptimizer.FIELD_NAME_PRICE].hasNonNilStringValue(shoptimizer.localizePrice(from: "0.00")))
        XCTAssertTrue(shoptimizer.textViews.element.isEmptyOrPlaceholderValue)
        XCTAssertTrue(shoptimizer.pickerWheels.count == 2)
        XCTAssertTrue(shoptimizer.pickerWheels.element(boundBy: 0).value as? String == shoptimizer.UNIT_L)
        XCTAssertTrue(shoptimizer.pickerWheels.element(boundBy: 1).value as? String == shoptimizer.CURRENCY_EUR)
    }

    // MARK: - Basic Tests for Items On One Nonincluded List

    func testChangesNameToUnusedOneInSameShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 1"))
    }

    func testChangesNameToUsedOnInDifferentShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 2"))
    }

    func testShowsWarningWhenChangingNameToUsedOnInSameShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotChangeNameToUsedOneInSameShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2"))
    }

    func testShowsWarningWhenChangingNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
    }

    func testShowsWarningWhenChangingNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "    ")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
    }

    func testChangesShopToOneThatDoesNotHaveTheItem() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(shop: "Shop 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 2"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 2"))
    }

    func testShowsWarningWhenChangingShopToOneThatHasTheItem() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 2").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1", shop: "Shop 1")
        app.enterShoppingListItemDetails(shop: "Shop 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotChangeShopToOneThatHasTheItem() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 2").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1", shop: "Shop 1")
        app.enterShoppingListItemDetails(shop: "Shop 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 2"))
    }

    func testChangesShopToUnusedOne() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(shop: "Shop 2")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 2"))
    }

    func testChagesAmountUnit() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountUnit: app.UNIT_L)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1.5", amountUnit: app.UNIT_L))
    }

    func testChangesAmountQuantity() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "0.12")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0.12", amountUnit: app.UNIT_KG))
    }

    func testChangesAmountQuantityToZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "0")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertFalse(app.existsShoppingListItem(name: "Item 1", amountQuantity: "0", amountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "-1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1.5", amountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "1a")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1.5", amountUnit: app.UNIT_KG))
    }

    func testChangesCurrency() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(currency: app.CURRENCY_USD)
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.99", currency: app.CURRENCY_USD))
    }

    func testChangesPrice() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "1.23")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.23", currency: app.CURRENCY_EUR))
    }

    func testChangesPriceToZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "0")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertFalse(app.existsShoppingListItem(name: "Item 1", price: "0", currency: app.CURRENCY_EUR))
    }

    func testShowsWarningWhenChangingPriceToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "-1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangePriceToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.99", currency: app.CURRENCY_EUR))
    }

    func testShowsWarningWhenChangingPriceWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "1a")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangePriceWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.99", currency: app.CURRENCY_EUR))
    }

    func testChangesNotes() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(notes: "Foo").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(notes: "Bar")
        app.tapSaveButton()
        app.openEditViewForShoppingListItem(name: "Item 1")

        // Expected Results:

        XCTAssertTrue(app.textViews.element.value as? String == "Bar")
    }

    func testChangesNotesToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(notes: "Foo").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(notes: "")
        app.tapSaveButton()
        app.openEditViewForShoppingListItem(name: "Item 1")

        // Expected Results:

        XCTAssertTrue(app.textViews.element.isEmptyOrPlaceholderValue)
    }

    func testMarksUndoneItemAsDone() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(doneStatus: false).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertTrue(app.isListItemDone(name: "Item 1"))
    }

    func testMarksDoneItemAsUndone() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertFalse(app.isListItemDone(name: "Item 1"))
    }

    // MARK: - Tests that affect the Aggregated List

    // MARK: - Items In One Included List

    func testUpdatesNameOnAggregatedListWhenChangingNameToExistingOnInDifferentShopOfItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem4 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 2").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2, shoppingListItem3, shoppingListItem4)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", shop: "Shop 2"))
    }

    func testRemovesFromAggregatedListWhenMarkingAsDoneAnItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testAddsToAggregatedListWhenMarkingAsUndoneAnItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
    }

    func testDoesNotUpdateAmountOnAggregatedListWhenChangingAmountOfDoneItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "5")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testRemovesFromAggregateListWhenDeletingUndoneItemOnSingleIncludedListWhenItemsRemainInTheShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem4 = ShoppingListItemBuilder.create().with(name: "Item 2").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2, shoppingListItem3, shoppingListItem4)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2"))
    }

    func testDoesNotUpdateAggregatedListWhenDeletingDoneItemOnSingleIncludedListWhenItemsRemainInTheShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(listName: "List 1").with(doneStatus: true).toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 2")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
    }

    func testShowsPriceOnAggregatedListWhenAddingPriceToItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(price: "1.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.99", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "1", totalPrice: "1.99", currency: app.CURRENCY_EUR))
    }

    func testUpdatesPriceOnAggregatedListWhenAddingItemInDifferentShopToSingleIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.tapAddButton()
        app.enterShoppingListItemDetails(name: "Item 2", shop: "Shop 1", price: "0.99", currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.99", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "1", totalPrice: "1.99", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", price: "0.99", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShop(name: "Shop 1", totalItems: "1", totalPrice: "0.99", currency: app.CURRENCY_EUR))
    }

    func testReducesPriceOnAggregatedListWhenMarkingAsDoneAnItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 2").with(price: 0.49).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem4 = ShoppingListItemBuilder.create().with(name: "Item 2").with(price: 0.49).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2, shoppingListItem3, shoppingListItem4)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 2")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.existsShop(name: nil, totalItems: "1", totalPrice: "1.99", currency: app.CURRENCY_EUR))
    }

    func testIncreasesPriceOnAggregatedListWhenMarkingAsUndoneAnItemOnSingleIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.99).with(currency: app.CURRENCY_EUR).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 2").with(price: 0.49).with(currency: app.CURRENCY_EUR).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 2")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.existsShop(name: nil, totalItems: "2", totalPrice: "2.48", currency: app.CURRENCY_EUR))
    }

    // MARK: - Items on Two Included Lists

    func testSplitsItemOnAggregatedListWhenChangingNameOfItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2"))
    }

    func testSplitsItemOnAggregatedListWhenChangingShopOfItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(shop: "Shop 2")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 1"))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", shop: "Shop 2"))
    }

    func testSplitsItemOnAggregatedListWhenChangingToNonConvertibleUnitOfItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1.5).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 3.5).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountUnit: app.UNIT_L)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "2", amountUnit: app.UNIT_KG))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1.5", amountUnit: app.UNIT_L))
    }

    func testMergesItemsOnAggregatedListWhenChangingToConvertibleUnitOfItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1500).with(unit: app.UNIT_ML).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem4 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1500).with(unit: app.UNIT_ML).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3, shoppingListItem4)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountUnit: app.UNIT_G)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "3.5", amountUnit: app.UNIT_KG))
    }

    func testSubtractsFromAggregatedListWhenMarkingAsDoneItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 3).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testAddsToAggregatedListWhenMarkingAsUndoneItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "3", amountUnit: app.UNIT_KG))
    }

    func testSubtractsFromAggregatedListWhenDeletingUndoneItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 3).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testDoesNotSubtractFromAggregatedListWhenDeletingDoneItemOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testMergesItemsOnAggregatedListWhenChangingCurrencyToBeEqualForItemsOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.49).with(currency: app.CURRENCY_USD).with(unit: app.UNIT_L).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        let shoppingListItem4 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.49).with(currency: app.CURRENCY_USD).with(unit: app.UNIT_L).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3, shoppingListItem4)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(currency: app.CURRENCY_EUR)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "1.48", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "1", totalPrice: "1.48", currency: app.CURRENCY_EUR))
    }

    func testSplitsItemOnAggregatedListWhenChangingCurrencyToBeDifferenForItemsOnTwoIncludedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.99).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 0.49).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(price: 1.48).with(currency: app.CURRENCY_EUR).with(unit: app.UNIT_L).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 2")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(currency: app.CURRENCY_USD)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.99", currency: app.CURRENCY_EUR))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", price: "0.49", currency: app.CURRENCY_USD))
        XCTAssertTrue(app.existsShop(name: nil, totalItems: "2", totalPrice: "0.99", currency: app.CURRENCY_EUR))
    }

    // MARK: - Items on Mixed Lists

    func testDoesNotSubtractFromAggregatedListWhenMarkingAsDoneItemOnMixedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testDoesNotAddToAggregatedListWhenMarkingAsUndoneItemOnMixedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.toggleDoneStatusOfItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testDoesNotSubtractFromAggregatedListWhenDeletingDoneItemOnMixedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: true).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    func testDoesNotSubtractFromAggregatedListWhenDeletingUndoneItemOnMixedLists() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 2).with(unit: app.UNIT_KG).with(doneStatus: false).with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: "List 2").toLaunchArgumentString()
        let shoppingListItem3 = ShoppingListItemBuilder.create().with(name: "Item 1").with(amount: 1).with(unit: app.UNIT_KG).with(listName: app.LIST_NAME_AGGREGATED_LIST).toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2, shoppingListItem1, shoppingListItem2, shoppingListItem3)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 1")
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 1", amountQuantity: "1", amountUnit: app.UNIT_KG))
    }

    // MARK: - Items on One Nonincluded List

    func testDoesNotAddToAggregatedListWhenChangingNameOfItemOnNonincludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testDoesNotAddToAggregatedListWhenChangingShopOfItemOnNonincludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(shop: "Shop 1")
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func testDoesNotAddToAggregatedListWhenChangingAmountOfItemOnNonincludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingListItem = ShoppingListItemBuilder.create().with(name: "Item 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.openEditViewForShoppingListItem(name: "Item 1")
        app.enterShoppingListItemDetails(amountQuantity: "4.5", amountUnit: app.UNIT_L)
        app.tapSaveButton()
        app.returnToShoppingLists()
        app.stepIntoShoppingList(name: app.LIST_NAME_AGGREGATED_LIST)

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }

    func test_DeletingItem_DecreasesCounterForShop() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        let shoppingListItem1 = ShoppingListItemBuilder.create().with(name: "Item 1").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        let shoppingListItem2 = ShoppingListItemBuilder.create().with(name: "Item 2").with(shop: "Shop 1").with(listName: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList, shoppingListItem1, shoppingListItem2)

        // Execution:

        app.stepIntoShoppingList(name: "List 1")
        app.deleteShoppingListItem(name: "Item 2")

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShop(name: "Shop 1", doneItems: "0", totalItems: "1"))
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditShoppingListTests: XCTestCase {
        
    func testChangesName() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")
        app.enterShoppingListDetails(name: "List 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 2"))
    }

    func testShowsWarningWhenChangingNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")
        app.enterShoppingListDetails(name: "List 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotChangeNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList1 = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        let shoppingList2 = ShoppingListBuilder.create().with(name: "List 2").toLaunchArgumentString()
        app.launch(with: shoppingList1, shoppingList2)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")
        app.enterShoppingListDetails(name: "List 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 3)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1"))
        XCTAssertTrue(app.existsShoppingList(name: "List 2"))
    }

    func testShowsWarningWhenChangingNameToEmpty() {

    }

    func testDoesNotChangeNameToEmpty() {

    }

    func testShowsWarningWhenChangingNameToWhiteSpaceOnly() {

    }

    func testDoesNotChangeNameToWhiteSpaceOnly() {

    }

    func testChangesIncludedListToNonincludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")
        app.enterShoppingListDetails(includeInAggregatedList: false)
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: false))
    }

    func testChangesNonincludedListToIncludedList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: false).toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")
        app.enterShoppingListDetails(includeInAggregatedList: true)
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: true))
    }
}

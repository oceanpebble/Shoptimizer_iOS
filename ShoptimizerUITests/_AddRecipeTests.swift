// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AddRecipeTests: XCTestCase {
    
    func testUIElementsAppearCorrectly() {
        // TODO: implement
        XCTFail()
    }
    
    func testShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateRecipeWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "  ")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateRecipeWhenNameContainsWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "  ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testTappingCancelButtonDoesNotCreateRecipe() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1")
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenPortionsIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_PORTIONS_MISSING))
    }
    
    func testDoesNotCreateRecipeWhenPortionsIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_PORTIONS_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testCreatesRecipeWithNameAndPortions() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipe(name: "Recipe 1", portions: "1"))
    }
    
    func testShowsWarningWhenPortionsIsNotANaturalNumber() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1.1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateRecipeWhenPortionsIsNotANaturalNumber() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1.1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenPortionsHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1a")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateRecipeWhenPortionsHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenPortionsIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "-1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateRecipeWhenPortionsIsLessThanZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenPortionsIsZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "0")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateRecipeWhenPortionsIsZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        app.launchWithoutArguments()
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "0")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenCreatingDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }
    
    func testDoesNotCreateDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.tapAddButton()
        app.enterRecipeDetails(name: "Recipe 1", portions: "1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func openOptionsMenuForRecipe(name: String? = nil) {
        swipeCellRight(containing: searchStringsForRecipe(name: name))
    }
    
    func stepIntoRecipe(name: String? = nil) {
        tapCell(containing: searchStringsForRecipe(name: name))
    }
    
    func openRecipeIngredientsOverview() {
        tapCell(containing: TABLE_CELL_INGREDIENTS)
    }
    
    func openEditViewForRecipe(name: String? = nil, portions: String? = nil) {
        tapCellLong(containing: searchStringsForRecipe(name: name, portions: portions))
    }

    func deleteRecipe(name: String? = nil) {
        deleteCell(containing: searchStringsForRecipe(name: name))
    }
    
    func enterRecipeDetails(name: String? = nil, portions: String? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
        if portions != nil { textFields[FIELD_NAME_PORTIONS].clearAndTypeText(portions!) }
    }
    
    func existsRecipe(name: String? = nil, portions: String? = nil) -> Bool {
        return existsCell(containing: searchStringsForRecipe(name: name, portions: portions))
    }

    func filterRecipes(portions: String?) {
        tapButton(title: BUTTON_TITLE_OPTIONS)
        tapButton(title: BUTTON_TITLE_FILTER_BY_SUPPLIES)
        let portionsAlert = alerts.element // TODO: why does app.alerts[TITLE_ALERT_PORTIONS] not work?
        if (portions != nil) { portionsAlert.textFields.element.clearAndTypeText(portions!) }
        confirmAlert()
    }

    func createShoppingListFromRecipe(name: String, portions: String?) {
        openOptionsMenuForRecipe(name: name)
        buttons[BUTTON_TITLE_CREATE_LIST].tap()
        if (portions != nil) { alerts.element.textFields.element.clearAndTypeText(portions!) }
        confirmAlert() // portions
        confirmAlert() // success alert
    }
    
    func searchStringsForRecipe(name: String? = nil, portions: String? = nil) -> [String] {
        var searchStrings = [String]()
        
        // name need not be localized
        if let name = name { searchStrings.append(name) }
        
        // portions
        if let portions = portions,
            let portionsInt = Int(portions) {
            if portionsInt == 1 {
                searchStrings.append("1 " + NSLocalizedString("portion", comment: ""))
            } else {
                searchStrings.append(portions + " " + NSLocalizedString("portions", comment: ""))
            }
        }
        
        return searchStrings
    }
    
    func returnToRecipesOverviewFromRecipe(name: String? = nil) {
        navigationBars.element.buttons["Recipe 2"].tap()
        navigationBars.element.buttons["Recipes"].tap()
    }
}

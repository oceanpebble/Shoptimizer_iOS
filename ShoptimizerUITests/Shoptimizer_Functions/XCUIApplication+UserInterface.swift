// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func tapAddButton() {
        tapButton(title: BUTTON_TITLE_ADD)
    }

    func tapSaveButton() {
        tapButton(title: BUTTON_TITLE_SAVE)
    }

    func tapCancelButton() {
        tapButton(title: BUTTON_TITLE_CANCEL)
    }
    
    func tapDeleteButton() {
        tapButton(title: BUTTON_TITLE_DELETE)
    }
    
    func returnToShoppingLists() {
        let regularButton = navigationBars.buttons[BUTTON_TITLE_SHOPPING_LISTS]
        if regularButton.exists {
            regularButton.tap()
        } else {
            navigationBars.buttons.element(boundBy: 0).tap()
        }
    }

    func switchToShoppingListsTab() {
        tabBars.buttons[TAB_TITLE_SHOPPING_LISTS].tap()
    }
    
    func switchToSuppliesTab() {
        tabBars.buttons[TAB_TITLE_SUPPLIES].tap()
    }

    func switchToRecipesTab() {
        tabBars.buttons[TAB_TITLE_RECIPES].tap()
    }

    func switchToTasksTab() {
        tabBars.buttons[TAB_TITLE_TASKS].tap()
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func openOptionsMenuForShoppingList(name: String? = nil) {
        swipeCellRight(containing: searchStringsForShoppingList(name: name))
    }

    func openEditViewForShoppingList(name: String? = nil, includeInAggregatedList: Bool? = nil) {
        tapCellLong(containing: searchStringsForShoppingList(name: name, includeInAggregatedList: includeInAggregatedList))
    }

    func stepIntoShoppingList(name: String? = nil, includeInAggregatedList: Bool? = nil) {
        tapCell(containing: searchStringsForShoppingList(name: name, includeInAggregatedList: includeInAggregatedList))
    }

    func deleteShoppingList(name: String? = nil) {
        deleteCell(containing: searchStringsForShoppingList(name: name))
    }

    func deleteAllShoppingLists() {
        while cells.element(boundBy: 0).exists {
            cells.element(boundBy: 0).swipeLeft()
            tapDeleteButton()
        }
    }

    func enterShoppingListDetails(name: String? = nil, includeInAggregatedList: Bool? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
        if includeInAggregatedList != nil {
            if switches.element.isOn && !includeInAggregatedList!
                || !switches.element.isOn &&  includeInAggregatedList! {
                switches.element.tap()
            }
        }
    }

    func existsShoppingList(name: String? = nil, includeInAggregatedList: Bool? = nil) -> Bool {
        return existsCell(containing: searchStringsForShoppingList(name: name, includeInAggregatedList: includeInAggregatedList))
    }
    
    func existsShoppingList(name: String? = nil, includeInAggregatedList: Bool? = nil, atIndex index: Int) -> Bool {
        return existsCell(containing: searchStringsForShoppingList(name: name, includeInAggregatedList: includeInAggregatedList), atIndex: index)
    }

    func transferDoneItemFromShoppingList(name: String) {
        openOptionsMenuForShoppingList(name: name)
        buttons[BUTTON_TITLE_TRANSFER_ITEMS].tap()
        confirmAlert(withTitle: TITLE_ALERT_SUCCESS)
    }
    
    fileprivate func searchStringsForShoppingList(name: String? = nil, includeInAggregatedList: Bool? = nil) -> [String] {
        var searchStrings = [String]()
        
        if let name = name { searchStrings.append(name) }
        
        if let included = includeInAggregatedList {
            if included {
                searchStrings.append("\u{25C9}")
            } else {
                searchStrings.append("\u{25CB}")
            }
        }
        
        return searchStrings
    }

    func existsShop(name: String? = nil, doneItems: String? = nil, donePrice: String? = nil, totalItems: String? = nil, totalPrice: String? = nil, currency: String? = nil) -> Bool {
        let searchStrings = searchStringsForShop(name: name, doneItems: doneItems, donePrice:donePrice, totalItems: totalItems, totalPrice: totalPrice, currency: currency)

        for i in 0 ..< tables.element.staticTexts.count {
            let currentStaticText = tables.element.staticTexts.element(boundBy: i).label
            var containsAllSearchString = true

            for searchString in searchStrings {
                if !currentStaticText.contains(searchString) {
                    containsAllSearchString = false
                    break
                }
            }

            if containsAllSearchString {
                return true
            }
        }

        return false
    }

    func searchStringsForShop(name: String? = nil, doneItems: String? = nil, donePrice: String? = nil, totalItems: String? = nil, totalPrice: String? = nil, currency: String? = nil) -> [String] {
        var searchStrings = [String]()

        // name need not be localized
        if let name = name { searchStrings.append(name) }

        if let doneItems = doneItems, let donePrice = donePrice, let totalItems = totalItems, let totalPrice = totalPrice, let currency = currency {
            searchStrings.append("\(doneItems) - \(localizePrice(from: donePrice)) \(currency) / \(totalItems) - \(localizePrice(from: totalPrice)) \(currency)")
        } else if let doneItems = doneItems, let totalItems = totalItems {
            searchStrings.append("\(doneItems) / \(totalItems)")
        } else if let totalItems = totalItems, let totalPrice = totalPrice, let currency = currency {
            searchStrings.append("\(totalItems) - \(localizePrice(from: totalPrice)) \(currency)")
        } else if let totalItems = totalItems {
            searchStrings.append("\(totalItems)")
        }

        return searchStrings
    }
}

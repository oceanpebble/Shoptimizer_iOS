// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func existsSuppliesItem(name: String? = nil, minAmountQuantity: String? = nil, minAmountUnit: String? = nil, curAmountQuantity: String? = nil, curAmountUnit: String? = nil) -> Bool {
        return existsCell(containing: searchStringsForSuppliesItem(name: name, minAmountQuantity: minAmountQuantity, minAmountUnit: minAmountUnit, curAmountQuantity: curAmountQuantity, curAmountUnit: curAmountUnit))
    }

    func stepIntoSuppliesItem(name: String? = nil) {
        tapCell(containing: searchStringsForSuppliesItem(name: name))
    }

    func openEditViewForSuppliesItem(name: String? = nil) {
        tapCell(containing: searchStringsForSuppliesItem(name: name))
    }
    
    func searchStringsForSuppliesItem(name: String? = nil, minAmountQuantity: String? = nil, minAmountUnit: String? = nil, curAmountQuantity: String? = nil, curAmountUnit: String? = nil) -> [String] {
        var searchStrings = [String]()
        
        // name need not be localized
        if let name = name { searchStrings.append(name) }
        
        // minimum amount quantity and unit must be put into one string, if possible, and be localized
        if let minQuantity = minAmountQuantity, let minUnit = minAmountUnit {
            let minQuantityLocalized = localizeDouble(from: minQuantity)
            let minUnitLocalized = UnitPickerHelper.translate(unit: minUnit)
            searchStrings.append(FIELD_NAME_LABEL_MINIMUM + " " + minQuantityLocalized + " " + minUnitLocalized)
        } else {
            if let minQuantity = minAmountQuantity {
                let minQuantityLocalized = localizeDouble(from: minQuantity)
                searchStrings.append(FIELD_NAME_LABEL_MINIMUM + " " + minQuantityLocalized + " l")
            }
            if let minUnit = minAmountUnit {
                let minUnitLocalized = UnitPickerHelper.translate(unit: minUnit)
                searchStrings.append(FIELD_NAME_LABEL_MINIMUM + " 0 " + minUnitLocalized)
            }
        }
        
        // current amount quantity and unit must be put into one string, if possible, and be localized
        if let curQuantity = curAmountQuantity, let curUnit = curAmountUnit {
            let curQuantityLocalized = localizeDouble(from: curQuantity)
            let curUnitLocalized = UnitPickerHelper.translate(unit: curUnit)
            searchStrings.append(FIELD_NAME_LABEL_CURRENT + " " + curQuantityLocalized + " " + curUnitLocalized)
        } else {
            if let curQuantity = curAmountQuantity {
                let curQuantityLocalized = localizeDouble(from: curQuantity)
                searchStrings.append(FIELD_NAME_LABEL_CURRENT + " " + curQuantityLocalized + " l")
            }
            if let curUnit = curAmountUnit {
                let curUnitLocalized = UnitPickerHelper.translate(unit: curUnit)
                searchStrings.append(FIELD_NAME_LABEL_CURRENT + " 0 " + curUnitLocalized)
            }
        }
        
        return searchStrings
    }
    
    func enterSuppliesItemDetails(name: String? = nil, minAmountQuantity: String? = nil, minAmountUnit: String? = nil, curAmountQuantity: String? = nil, curAmountUnit: String? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
        if minAmountQuantity != nil { textFields[FIELD_NAME_MINIMUM_AMOUNT_QUANTITY].clearAndTypeText(localizeDouble(from: minAmountQuantity!)) }
        if minAmountUnit != nil { pickerWheels.element(boundBy: INDEX_MINIMUM_AMOUNT_UNIT_PICKER).adjust(toPickerWheelValue: minAmountUnit!) }
        if curAmountQuantity != nil {
            if keyboards.buttons[BUTTON_TITLE_DONE].exists {
                keyboards.buttons[BUTTON_TITLE_DONE].tap()
            } else if buttons[BUTTON_TITLE_DONE].exists {
                buttons[BUTTON_TITLE_DONE].tap()
            }// to hide keyboard
            textFields[FIELD_NAME_CURRENT_AMOUNT_QUANTITY].clearAndTypeText(localizeDouble(from: curAmountQuantity!)) }
        if curAmountUnit != nil { pickerWheels.element(boundBy: INDEX_CURRENT_AMOUNT_UNIT_PICKER).adjust(toPickerWheelValue: curAmountUnit!) }
    }
    
    func isSuppliesItemStocked(name: String? = nil) -> Bool {
        let identifier = cell(containing: searchStringsForSuppliesItem(name: name)).identifier
        return identifier == AccessibilityUtil.identifierForStockedItems
    }
    
    func isSuppliesItemLow(name: String? = nil) -> Bool {
        let identifier = cell(containing: searchStringsForSuppliesItem(name: name)).identifier
        return identifier == AccessibilityUtil.identifierForLowItems
    }
    
    func isSuppliesItemOut(name: String? = nil) -> Bool {
        let identifier = cell(containing: searchStringsForSuppliesItem(name: name)).identifier
        return identifier == AccessibilityUtil.identifierForOutItems
    }

    func isCategorySelected(name: String? = nil) -> Bool {
        let identifier = cell(containing: searchStringsForSuppliesItem(name: name)).identifier
        return identifier == AccessibilityUtil.identifierForSelectedCategory
    }
    
    func deleteSuppliesItem(name: String? = nil) {
        deleteCell(containing: searchStringsForSuppliesItem(name: name))
    }

    func createListFromSupplies() {
        tapButton(title: BUTTON_TITLE_CREATE_LIST)
    }
}

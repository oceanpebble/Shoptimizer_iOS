// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func openEditViewForRecipeIngredient(name: String? = nil, amountQuantity : String? = nil, amountUnit: String? = nil) {
        tapCell(containing: searchStringsForRecipeIngredient(name: name, amountQuantity: amountQuantity, amountUnit: amountUnit))
    }
    
    func enterRecipeIngredientDetails(name: String? = nil, amountQuantity : String? = nil, amountUnit: String? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
        if amountQuantity != nil { textFields[FIELD_NAME_QUANTITY].clearAndTypeText(localizeDouble(from: amountQuantity!)) }
        if amountUnit != nil { pickerWheels.element.adjust(toPickerWheelValue: amountUnit!) }
    }
    
    func existsRecipeIngredient(name: String? = nil, amountQuantity: String? = nil, amountUnit: String? = nil) -> Bool {
        return existsCell(containing: searchStringsForRecipeIngredient(name: name, amountQuantity: amountQuantity, amountUnit: amountUnit))
    }
    
    func searchStringsForRecipeIngredient(name: String? = nil, amountQuantity: String? = nil, amountUnit: String? = nil) -> [String] {
        var searchStrings = [String]()
        
        // name need not be localized
        if let name = name { searchStrings.append(name) }
        
        // amount quantity and unit must be put into one string, if possible, and be localized
        if let amountQuantity = amountQuantity, let amountUnit = amountUnit {
            let quantityLocalized = localizeDouble(from: amountQuantity)
            let unitLocalized = UnitPickerHelper.translate(unit: amountUnit)
            searchStrings.append(quantityLocalized + " " + unitLocalized)
        } else {
            if let amountQuantity = amountQuantity {
                let quantityLocalized = localizeDouble(from: amountQuantity)
                searchStrings.append(quantityLocalized + " l")
            }
            if let amountUnit = amountUnit {
                let unitLocalized = UnitPickerHelper.translate(unit: amountUnit)
                searchStrings.append(" 0 " + unitLocalized)
            }
        }
        return searchStrings
    }
    
    func deleteRecipeIngredient(name: String? = nil) {
        deleteCell(containing: searchStringsForRecipeIngredient(name: name))
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func toggleDoneStatusOfItem(name: String) {
        swipeCellRight(containing: searchStringsForShoppingListItem(name: name))
    }

    func markShoppingListItemAsDoneOnAggregatedList(name: String) {
        swipeCellRight(containing: searchStringsForShoppingListItem(name: name))
    }
    
    func isListItemDone(name: String) -> Bool {
        let identifier = cell(containing: searchStringsForShoppingListItem(name: name)).identifier
        return identifier == AccessibilityUtil.identifierForDoneItems
    }

    func deleteShoppingListItem(name: String? = nil) {
        deleteCell(containing: searchStringsForShoppingListItem(name: name))
    }
    
    func openEditViewForShoppingListItem(name: String, shop: String? = nil) {
        let searchStrings = searchStringsForShoppingListItem(name: name)

        if let shop = shop {
            sleep(1) // This is somehow needed for XCUITest to get its internals organized. Without the sleep, this method will fail.
            let indices = indicesOfShopsAndItem(name: name, shop: shop)

            if indices.indexOfGivenItem >= 0 {
                tables.element.staticTexts.element(boundBy: indices.indexOfGivenItem).tap()
            }
        } else {
            tapCell(containing: searchStrings)
        }
    }
    
    func existsShoppingListItem(name: String? = nil, amountQuantity: String? = nil, amountUnit: String? = nil, brand: String? = nil, price: String? = nil, currency: String? = nil, doneStatus: Bool? = nil, shop: String? = nil) -> Bool {
        var correctDoneStatus = true
        if let doneStatus = doneStatus, let name = name {
            let actualDoneStatus = isListItemDone(name: name)
            correctDoneStatus = actualDoneStatus == doneStatus
        }
        
        var correctIndexOfItem = true
        if let shop = shop {
            // index of list item in app.tables.element.otherElements must be greater than index of shop name and less then remaining shops
            let indices = indicesOfShopsAndItem(name: name, amountQuantity: amountQuantity, amountUnit: amountUnit, brand: brand, price: price, currency: currency, shop: shop)

            // the item was found in the right place if it is placed between the given shop and the next shop
            correctIndexOfItem = indices.indexOfGivenShop < indices.indexOfGivenItem && indices.indexOfNextShop > indices.indexOfGivenItem
        }
        
        return existsCell(containing: searchStringsForShoppingListItem(name: name, quantity: amountQuantity, unit: amountUnit, brand: brand, price: price, currency: currency))
            && correctDoneStatus
            && correctIndexOfItem
    }
    
    func searchStringsForShoppingListItem(name: String? = nil, quantity: String? = nil, unit: String? = nil, brand: String? = nil, price: String? = nil, currency: String? = nil) -> [String] {
        var searchStrings = [String]()
        
        // name need not be localized
        if let name = name { searchStrings.append(name) }
        
        // quantity and unit must be put into one string, if possible, and be localized
        if let quantity = quantity, let unit = unit {
            let quantityLocalized = localizeDouble(from: quantity)
            let unitLocalized = UnitPickerHelper.translate(unit: unit)
            searchStrings.append(quantityLocalized + " " + unitLocalized)
        } else {
            if let quantity = quantity {
                let quantityLocalized = localizeDouble(from: quantity)
                searchStrings.append(quantityLocalized)
            }
            if let unit = unit {
                let unitLocalized = UnitPickerHelper.translate(unit: unit)
                searchStrings.append(unitLocalized)
            }
        }
        
        // brand need not be localized
        if let brand = brand { searchStrings.append(brand) }
        
        // price and currency must be put into one string, if possible, and be localized
        if let price = price, let currency = currency {
            let priceLocalized = localizePrice(from: price)
            searchStrings.append(priceLocalized + " " + currency)
        } else {
            if let price = price {
                let priceLocalized = localizeDouble(from: price)
                searchStrings.append(priceLocalized)
            }
            if let currency = currency {
                searchStrings.append(currency)
            }
        }
        
        return searchStrings
    }
    
    func enterShoppingListItemDetails(name: String? = nil, shop: String? = nil,  amountQuantity: String? = nil, amountUnit: String? = nil, brand: String? = nil, price: String? = nil, currency: String? = nil, notes: String? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
        if shop != nil { textFields[FIELD_NAME_SHOP].clearAndTypeText(shop!) }
        if amountQuantity != nil {
            if keyboards.buttons[BUTTON_TITLE_DONE].exists {
                keyboards.buttons[BUTTON_TITLE_DONE].tap()
            } else if buttons[BUTTON_TITLE_DONE].exists {
                buttons[BUTTON_TITLE_DONE].tap()
            }// to hide keyboard
            textFields[FIELD_NAME_AMOUNT_QUANTITY].clearAndTypeText(localizeDouble(from: amountQuantity!)) }
        if amountUnit != nil { pickerWheels.element(boundBy: INDEX_AMOUNT_UNIT_PICKER).adjust(toPickerWheelValue: amountUnit!) }
        if brand != nil { textFields[FIELD_NAME_BRAND].clearAndTypeText(brand!) }
        if price != nil {
            if keyboards.element.buttons[BUTTON_TITLE_DONE].exists {
                keyboards.element.buttons[BUTTON_TITLE_DONE].tap()
            } else if buttons[BUTTON_TITLE_DONE].exists {
                buttons[BUTTON_TITLE_DONE].tap()
            } // to hide keyboard
            textFields[FIELD_NAME_PRICE].clearAndTypeText(localizeDouble(from: price!)) }
        if currency != nil { pickerWheels.element(boundBy: INDEX_CURRENCY_PICKER).adjust(toPickerWheelValue: currency!) }
        if notes != nil {
            if keyboards.element.buttons[BUTTON_TITLE_DONE].exists {
                keyboards.element.buttons[BUTTON_TITLE_DONE].tap()
            } else if buttons[BUTTON_TITLE_DONE].exists {
                buttons[BUTTON_TITLE_DONE].tap()
            } // to hide keyboard
            textViews.element.clearAndTypeText(notes!) }
    }

    // MARK: - Utility Functions

    private func indicesOfShopsAndItem(name: String? = nil, amountQuantity: String? = nil, amountUnit: String? = nil, brand: String? = nil, price: String? = nil, currency: String? = nil, shop: String) -> (indexOfGivenShop: Int, indexOfNextShop: Int, indexOfGivenItem: Int) {
        var indexOfGivenShop = -1
        var indexOfNextShop = Int.max
        var indexOfGivenItem = -1
        var foundGivenShop = false
        let searchStrings = searchStringsForShoppingListItem(name: name, quantity: amountQuantity, unit: amountUnit, brand: brand, price: price, currency: currency)

        for i in 0 ..< tables.element.staticTexts.count {
            let currentStaticText = tables.element.staticTexts.element(boundBy: i).label

            // if the next shop was found, the search is cancelled
            if foundGivenShop && currentStaticText.matches(regex: REGEX_SHOP_NAMES) {
                indexOfNextShop = i
                foundGivenShop = false
                break
            }

            // test if the given shop name was found
            if currentStaticText.contains(shop) {
                indexOfGivenShop = i
                foundGivenShop = true
                continue
            }

            // if the given shop name was found, search for the search terms of the item
            // an item might be given in two static texts, both contained in the same table cell
            if foundGivenShop {
                var textToSearch = currentStaticText
                let nextStaticText = tables.element.staticTexts.element(boundBy: i+1)
                if nextStaticText.exists {
                    textToSearch += " \(nextStaticText.label)"
                }

                var itemContainsAllSearchStrings = true
                for searchString in searchStrings {
                    if !textToSearch.contains(searchString) {
                        itemContainsAllSearchStrings = false
                        break
                    }
                }

                if itemContainsAllSearchStrings {
                    indexOfGivenItem = i
                }
            }
        }

        return (indexOfGivenShop, indexOfNextShop, indexOfGivenItem)
    }
}

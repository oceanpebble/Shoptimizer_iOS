// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    var LIST_NAME_AGGREGATED_LIST : String { return NSLocalizedString("Aggregated List", comment: "") }
    var LIST_PREFIX_REFILL_SUPPLIES : String { return NSLocalizedString("Refill supplies", comment: "") }
    
    // MARK: - Button Titles
    
    var BUTTON_TITLE_ADD : String { return NSLocalizedString("Add", comment: "") }
    var BUTTON_TITLE_DELETE : String { return NSLocalizedString("Delete", comment: "") }
    var BUTTON_TITLE_SAVE : String { return NSLocalizedString("Save", comment: "") }
    var BUTTON_TITLE_OK : String { return NSLocalizedString("OK", comment: "") }
    var BUTTON_TITLE_CANCEL : String { return NSLocalizedString("Cancel", comment: "") }
    var BUTTON_TITLE_CREATE_LIST : String { return NSLocalizedString("Create List", comment: "") }
    var BUTTON_TITLE_OPTIONS : String { return NSLocalizedString("Options", comment: "") }
    var BUTTON_TITLE_DONE : String { return NSLocalizedString("Done", comment: "") }
    var BUTTON_TITLE_CATEGORIES : String { return NSLocalizedString("Categories", comment: "") }
    var BUTTON_TITLE_BACK : String { return NSLocalizedString("Back", comment: "") }
    var BUTTON_TITLE_ADD_SUPPLIES_ITEM: String { return NSLocalizedString("Add Supplies Item", comment: "") }
    
    // MARK: - Button Titles - Options For Shopping Lists
    
    var BUTTON_TITLE_SHOPPING_LISTS : String { return NSLocalizedString("Shopping Lists", comment: "") }
    var BUTTON_TITLE_TRANSFER_ITEMS : String { return NSLocalizedString("Transfer done items to supplies", comment: "") }
    var BUTTON_TITLE_TRANSFER_AND_DELETE_ITEMS : String { return NSLocalizedString("Transfer and delete done items", comment: "") }
    var BUTTON_TITLE_UPDATE_LIST : String { return NSLocalizedString("Update", comment: "") }
    var BUTTON_TITLE_DELETE_DONE_ITEMS : String { return NSLocalizedString("Delete done items", comment: "") }
    var BUTTON_TITLE_CLEAR_LIST : String { return NSLocalizedString("Clear list", comment: "") }
    var BUTTON_TITLE_UNCHECK_ITEMS : String { return NSLocalizedString("Uncheck all items", comment: "") }
    var BUTTON_TITLE_EDIT_LIST : String { return NSLocalizedString("Edit list", comment: "") }
    var BUTTON_TITLE_EDIT_ITEM : String { return NSLocalizedString("Edit Item", comment: "") }
    var BUTON_TITLE_SHARE_LIST : String { return NSLocalizedString("Share list", comment: "") }

    // MARK: - Button Titles - Options For Recipes

    var BUTTON_TITLE_SUBTRACT_FROM_SUPPLIES : String { return NSLocalizedString("Subtract from Supplies", comment: "") }
    var BUTTON_TITLE_FILTER_BY_SUPPLIES : String { return NSLocalizedString("Filter by Supplies", comment: "") }
    var BUTTON_TITLE_PREPARE_RECIPE : String { return NSLocalizedString("Prepare Recipe", comment: "") }
    var BUTTON_TITLE_SCHEDULE_RECIPE : String { return NSLocalizedString("Schedule Recipe", comment: "") }
    var BUTTON_TITLE_EDIT_RECIPE : String { return NSLocalizedString("Edit Recipe", comment: "") }
    var BUTTON_TITLE_SHARE_RECIPE : String { return NSLocalizedString("Share Recipe", comment: "") }

    // MARK: - Navigation Bar Titles
    
    var NAVBAR_TITLE_ADD_SHOPPING_LIST : String { return NSLocalizedString("Add List", comment: "") }
    var NAVBAR_TITLE_EDIT_SHOPPING_LIST : String { return NSLocalizedString("Edit List", comment: "") }
    var NAVBAR_TITLE_SHOPPING_LISTS: String { return NSLocalizedString("Shopping Lists", comment: "") }
    var NAVBAR_TITLE_ADD_SHOPPING_LIST_ITEM : String { return NSLocalizedString("Add Item", comment: "") }
    var NAVBAR_TITLE_SUPPLIES: String { return NSLocalizedString("Supplies", comment: "") }
    var NAVBAR_TITLE_SUPPLIES_FILTERED: String { return NSLocalizedString("Supplies (filtered)", comment: "") }
    var NAVBAR_TITLE_ADD_SUPPLIES_ITEM: String { return NSLocalizedString("Add Supplies Item", comment: "") }
    var NAVBAR_TITLE_EDIT_SUPPLIES_ITEM: String { return NSLocalizedString("Edit Supplies Item", comment: "") }
    var NAVBAR_TITLE_RECIPES_FILTERED: String { return NSLocalizedString("Recipes (filtered)", comment: "") }
    var NAVBAR_TITLE_RECIPES_UNFILTERED: String { return NSLocalizedString("Recipes", comment: "") }
    var NAVBAR_TITLE_EDIT_RECIPE : String { return NSLocalizedString("Edit Recipe", comment: "") }
    var NAVBAR_TITLE_INGREDIENTS : String { return NSLocalizedString("Ingredients", comment: "") }
    var NAVBAR_TITLE_CATEGORIES : String { return NSLocalizedString("Categories", comment: "") }
    var NAVBAR_TITLE_ADD_CATEGORY : String { return NSLocalizedString("Add Category", comment: "") }
    var NAVBAR_TITLE_EDIT_CATEGORY : String { return NSLocalizedString("Edit Category", comment: "") }
    var NAVBAR_TITLE_CATEGORIES_FILTERED: String { return NSLocalizedString("Categories (filtered)", comment: "") }
    
    // MARK: - Field Name Labels
    
    var FIELD_NAME_LABEL_NAME : String { return NSLocalizedString("Name", comment: "") }
    var FIELD_NAME_LABEL_INCLUDE_IN_AGGREGATE : String { return NSLocalizedString("Include in Aggregated List", comment: "") }
    var FIELD_NAME_LABEL_MINIMUM : String { return NSLocalizedString("Minimum:", comment: "") }
    var FIELD_NAME_LABEL_CURRENT : String { return NSLocalizedString("Current:", comment: "") }
    var FIELD_NAME_LABEL_BRAND : String { return NSLocalizedString("Brand", comment: "") }
    var FIELD_NAME_LABEL_SHOP : String { return NSLocalizedString("Shop", comment: "") }
    var FIELD_NAME_LABEL_AMOUNT : String { return NSLocalizedString("Amount", comment: "") }
    var FIELD_NAME_LABEL_PRICE : String { return NSLocalizedString("Price", comment: "") }
    var FIELD_NAME_LABEL_NOTES : String { return NSLocalizedString("Notes", comment: "") }
    
    // MARK: - Field Name
    
    var FIELD_NAME_NAME : String { return NSLocalizedString("Name", comment: "") }
    var FIELD_NAME_MINIMUM_AMOUNT_QUANTITY : String { return NSLocalizedString("Minimum Quantity", comment: "") }
    var FIELD_NAME_CURRENT_AMOUNT_QUANTITY : String { return NSLocalizedString("Current Quantity", comment: "") }
    var FIELD_NAME_AMOUNT_QUANTITY : String { return NSLocalizedString("Amount Quantity", comment: "") }
    var FIELD_NAME_QUANTITY : String { return NSLocalizedString("Quantity", comment: "") }
    var FIELD_NAME_PORTIONS : String { return NSLocalizedString("Portions", comment: "") }
    var FIELD_NAME_SHOP : String { return NSLocalizedString("Shop", comment: "") }
    var FIELD_NAME_BRAND : String { return NSLocalizedString("Brand", comment: "") }
    var FIELD_NAME_PRICE : String { return NSLocalizedString("Price", comment: "") }
    var FIELD_NAME_NOTES : String { return NSLocalizedString("Notes", comment: "") }
    
    // MARK: - Tab Titles
    
    var TAB_TITLE_SUPPLIES : String { return NSLocalizedString("Supplies", comment: "") }
    var TAB_TITLE_RECIPES : String { return NSLocalizedString("Recipes", comment: "") }
    var TAB_TITLE_SHOPPING_LISTS : String { return NSLocalizedString("Lists", comment: "") }
    var TAB_TITLE_TASKS : String { return NSLocalizedString("Tasks", comment: "") }
    
    // MARK: - Table Cell Titles
    
    var TABLE_CELL_INGREDIENTS : String { return NSLocalizedString("Ingredients", comment: "") }
    
    // MARK: - Alert Title
    
    var TITLE_ALERT_NAME_MISSING : String { return NSLocalizedString("No Name", comment: "") }
    var TITLE_ALERT_DUPLICATE : String { return NSLocalizedString("Duplicate", comment: "") }
    var TITLE_ALERT_SUCCESS : String { return NSLocalizedString("Success", comment: "") }
    var TITLE_ALERT_ERROR : String { return NSLocalizedString("Error", comment: "") }
    var TITLE_ALERT_INVALID_DATA : String { return NSLocalizedString("Invalid data", comment: "") }
    var TITLE_ALERT_PORTIONS_MISSING : String { return NSLocalizedString("No Portions", comment: "") }
    
    // MARK: - UI Element Indices
    
    var INDEX_MINIMUM_AMOUNT_UNIT_PICKER : Int { return 0 }
    var INDEX_CURRENT_AMOUNT_UNIT_PICKER : Int { return 1 }
    var INDEX_AMOUNT_UNIT_PICKER : Int { return 0 }
    var INDEX_CURRENCY_PICKER : Int { return 1 }
    
    // MARK: - Units
    
    var UNIT_KG : String { return "kg" }
    var UNIT_G : String { return "g" }
    var UNIT_PIECE : String { return NSLocalizedString("piece", comment: "") }
    var UNIT_L : String { return "l" }
    var UNIT_ML : String { return "ml" }
    var UNIT_CAN : String { return NSLocalizedString("can", comment: "") }
    var UNIT_PACKAGE : String { return NSLocalizedString("package", comment: "") }
    
    // MARK: - Currencies
    
    var CURRENCY_EUR : String { return "EUR" }
    var CURRENCY_USD : String { return "USD" }

    // MARK: - Regular Expressions

    var REGEX_STRING_SHOP_NAMES : String { return ".+\\s(.+/.+)" }
    var REGEX_SHOP_NAMES : NSRegularExpression { return try! NSRegularExpression(pattern: REGEX_STRING_SHOP_NAMES, options: [.caseInsensitive]) }
}

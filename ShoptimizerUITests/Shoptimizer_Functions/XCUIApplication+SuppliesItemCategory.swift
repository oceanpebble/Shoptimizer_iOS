// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func stepIntoSuppliesItemCategory(name: String? = nil) {
        tapCell(containing: searchStringsForSuppliesItemCategory(name: name))
    }

    func searchStringsForSuppliesItemCategory(name: String? = nil) -> [String] {
        var searchStrings = [String]()
        if let name = name { searchStrings.append(name) }
        return searchStrings
    }

    func enterSuppliesItemCategoryDetails(name: String? = nil) {
        if name != nil { textFields[FIELD_NAME_NAME].clearAndTypeText(name!) }
    }

    func deleteSuppliesItemCategory(name: String? = nil) {
        if name != nil {
            swipeCellLeft(containing: searchStringsForSuppliesItemCategory(name: name))
            tapDeleteButton()
        }
    }

    func existsSuppliesItemCategory(name: String? = nil) -> Bool {
        return existsCell(containing: searchStringsForSuppliesItemCategory(name: name))
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditSuppliesItemTests: XCTestCase {
        
    func testChangesName() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "Item 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 2"))
    }

    func testShowsWarningWhenChangingNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Item 2").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "Item 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotChangeNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Item 2").toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "Item 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 2"))
    }

    func testShowsWarningWhenChangingNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
    }

    func testShowsWarningWhenChangingNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1"))
    }

    func testChangesMinimumAmountUnit() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountUnit: app.UNIT_L)
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "1.5", minAmountUnit: app.UNIT_L))
    }

    func testChangesMinimumAmountQuantity() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "0.2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "0.2", minAmountUnit: app.UNIT_KG))
    }

    func testChangesMinimumAmountQuantityToZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "0")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "0", minAmountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingMinimumAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "-1")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeMinimumAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "1.5", minAmountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingMinimumAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "1a")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeMinimumAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1.5).with(minUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(minAmountQuantity: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", minAmountQuantity: "1.5", minAmountUnit: app.UNIT_KG))
    }

    func testChangesCurrentAmountUnit() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountUnit: app.UNIT_L)
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "1.5", curAmountUnit: app.UNIT_L))
    }

    func testChangesCurrentAmountQuantity() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "0.2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "0.2", curAmountUnit: app.UNIT_KG))
    }

    func testChangesCurrentAmountQuantityToZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "0")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "0", curAmountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingCurrentAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "-1")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeCurrentAmountQuantityToLessThanZero() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "-1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG))
    }

    func testShowsWarningWhenChangingCurrentAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "1a")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }

    func testDoesNotChangeCurrentAmountQuantityWithLetters() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 1").with(curAmount: 1.5).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.openEditViewForSuppliesItem(name: "Item 1")
        app.enterSuppliesItemDetails(curAmountQuantity: "1a")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Item 1", curAmountQuantity: "1.5", curAmountUnit: app.UNIT_KG))
    }

    func test_DeselectCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")

        // Expected Results:

        XCTAssertFalse(app.isCategorySelected(name: "Tools"))
    }

    func test_RemoveCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)

        // Expected Results:

        XCTAssertFalse(app.isCategorySelected(name: "Tools"))
    }

    func test_SelectSecondCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Stuff")

        // Expected Results:

        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
        XCTAssertTrue(app.isCategorySelected(name: "Stuff"))
    }

    func test_AddSecondCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Stuff")
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)

        // Expected Results:

        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
        XCTAssertTrue(app.isCategorySelected(name: "Stuff"))
    }

    func test_SelectSameCategoryToDifferentItem() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")

        // Expected Results:

        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
    }

    func test_AddSameCategoryToDifferentItem() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()

        // Expected Results:

        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
    }
    
    func test_DeselectCategoryFromItem_WhenCategoryIsGivenToTwoItems() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")

        // Expected Results:

        XCTAssertFalse(app.isCategorySelected(name: "Tools"))
    }

    func test_RemoveCategoryFromItem_WhenCategoryIsGivenToTwoItems() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Dairy").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Tools").toLaunchArgumentString()
        let category3 = SuppliesItemCategoryBuilder.create().with(name: "Flour").toLaunchArgumentString()
        let category4 = SuppliesItemCategoryBuilder.create().with(name: "Stuff").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Tools").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Tools").toLaunchArgumentString()
        app.launch(with: category1, category2, category3, category4, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.swipeCellRight(containing: "Tools")
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()

        // Expected Results:

        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.isCategorySelected(name: "Tools"))
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertFalse(app.isCategorySelected(name: "Tools"))
    }
}

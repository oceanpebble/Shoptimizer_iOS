// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class RecipeIngredientBuilder {
    var name = ""
    var amountQuantity = 0.0
    var amountUnit = ""
    var recipeName = ""
    var includeInList = false
    var includeInSearch = false

    private init() {
        
    }
    
    static func create() -> RecipeIngredientBuilder {
        return RecipeIngredientBuilder()
    }
    
    func with(name: String) -> RecipeIngredientBuilder {
        self.name = name
        return self
    }
    
    func with(amountQuantity: Double) -> RecipeIngredientBuilder {
        self.amountQuantity = amountQuantity
        return self
    }
    
    func with(amountUnit: String) -> RecipeIngredientBuilder {
        self.amountUnit = amountUnit
        return self
    }
    
    func with(recipeName: String) -> RecipeIngredientBuilder {
        self.recipeName = recipeName
        return self
    }

    func with(includeInList: Bool) -> RecipeIngredientBuilder {
        self.includeInList = includeInList
        return self
    }

    func with(includeInSearch: Bool) -> RecipeIngredientBuilder {
        self.includeInSearch = includeInSearch
        return self
    }
    
    func toLaunchArgumentString() -> String {
        return "CreateRecipeIngredient/\(name)/\(amountQuantity)/\(amountUnit)/\(recipeName)/\(includeInList)/\(includeInSearch)"
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class SuppliesItemCategoryBuilder {
    private var name = ""

    private init() {

    }

    static func create() -> SuppliesItemCategoryBuilder {
        return SuppliesItemCategoryBuilder()
    }

    func with(name: String) -> SuppliesItemCategoryBuilder {
        self.name = name
        return self
    }

    func toLaunchArgumentString() -> String {
        return "CreateSuppliesItemCategory/\(name)"
    }
}

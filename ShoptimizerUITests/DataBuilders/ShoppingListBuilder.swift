// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class ShoppingListBuilder {
    private var name = ""
    private var includeInAggregatedList = false
    
    private init() {
        
    }

    static func create() -> ShoppingListBuilder {
        return ShoppingListBuilder()
    }

    func with(name: String) -> ShoppingListBuilder {
        self.name = name
        return self
    }

    func with(includeInAggregatedList: Bool) -> ShoppingListBuilder {
        self.includeInAggregatedList = includeInAggregatedList
        return self
    }

    func toLaunchArgumentString() -> String {
        return "CreateShoppingList/\(name)/\(includeInAggregatedList)"
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class SuppliesItemBuilder {
    private var name = ""
    private var minAmount = 0.0
    private var minUnit = ""
    private var curAmount = 0.0
    private var curUnit = ""
    private var category = ""
    
    private init() {
        
    }

    static func create() -> SuppliesItemBuilder {
        return SuppliesItemBuilder()
    }

    func with(name: String) -> SuppliesItemBuilder {
        self.name = name
        return self
    }

    func with(minAmount: Double) -> SuppliesItemBuilder {
        self.minAmount = minAmount
        return self
    }

    func with(minUnit: String) -> SuppliesItemBuilder {
        self.minUnit = minUnit
        return self
    }

    func with(curAmount: Double) -> SuppliesItemBuilder {
        self.curAmount = curAmount
        return self
    }

    func with(curUnit: String) -> SuppliesItemBuilder {
        self.curUnit = curUnit
        return self
    }

    func with(category: String) -> SuppliesItemBuilder {
        self.category = category
        return self
    }

    func toLaunchArgumentString() -> String {
        return "CreateSuppliesItem/\(name)/\(String(minAmount))/\(minUnit)/\(String(curAmount))/\(curUnit)/\(category)"
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class RecipeBuilder {
    private var name = ""
    private var portions : UInt = 0
    
    static func create() -> RecipeBuilder {
        return RecipeBuilder()
    }
    
    func with(name: String) -> RecipeBuilder {
        self.name = name
        return self
    }
    
    func with(portions: UInt) -> RecipeBuilder {
        self.portions = portions
        return self
    }
    
    func toLaunchArgumentString() -> String {
        return "CreateRecipe/\(name)/\(String(portions))"
    }
}

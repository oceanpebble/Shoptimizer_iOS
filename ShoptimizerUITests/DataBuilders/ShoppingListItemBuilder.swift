// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation

class ShoppingListItemBuilder {
    private var name = ""
    private var brand = ""
    private var shop = ""
    private var amount = 0.0
    private var unit = ""
    private var price = 0.0
    private var currency = ""
    private var notes = ""
    private var doneStatus = false
    private var listName = ""
    
    private init() {
        
    }

    static func create() -> ShoppingListItemBuilder {
        return ShoppingListItemBuilder()
    }

    func with(name: String) -> ShoppingListItemBuilder {
        self.name = name
        return self
    }

    func with(brand: String) -> ShoppingListItemBuilder {
        self.brand = brand
        return self
    }

    func with(shop: String) -> ShoppingListItemBuilder {
        self.shop = shop
        return self
    }

    func with(amount: Double) -> ShoppingListItemBuilder {
        self.amount = amount
        return self
    }

    func with(unit: String) -> ShoppingListItemBuilder {
        self.unit = unit
        return self
    }

    func with(price: Double) -> ShoppingListItemBuilder {
        self.price = price
        return self
    }

    func with(currency: String) -> ShoppingListItemBuilder {
        self.currency = currency
        return self
    }

    func with(notes: String) -> ShoppingListItemBuilder {
        self.notes = notes
        return self
    }

    func with(doneStatus: Bool) -> ShoppingListItemBuilder {
        self.doneStatus = doneStatus
        return self
    }

    func with(listName: String) -> ShoppingListItemBuilder {
        self.listName = listName
        return self
    }

    func toLaunchArgumentString() -> String {
        return "CreateShoppingListItem/\(name)/\(brand)/\(shop)/\(String(amount))/\(unit)/\(String(price))/\(currency)/\(doneStatus)/\(listName)/\(notes)"
    }
}

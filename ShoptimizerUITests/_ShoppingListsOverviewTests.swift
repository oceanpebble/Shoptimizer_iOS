// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _ShoppingListsOverviewTests: XCTestCase {

    func testDeletesList() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.deleteShoppingList(name: "List 1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
    }

    func testUIElementsShouldAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let list1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        let list2 = ShoppingListBuilder.create().with(name: "List 2").with(includeInAggregatedList: true).toLaunchArgumentString()
        let list3 = ShoppingListBuilder.create().with(name: "List 3").with(includeInAggregatedList: false).toLaunchArgumentString()
        app.launch(with: list1, list2, list3)

        // Execution:

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_SHOPPING_LISTS))
        XCTAssertTrue(app.existsSingleButton(matching: app.BUTTON_TITLE_ADD))
        XCTAssertTrue(app.cells.count == 4)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST, atIndex: 0))
        XCTAssertTrue(app.existsShoppingList(name: "List 1", includeInAggregatedList: true, atIndex: 1))
        XCTAssertTrue(app.existsShoppingList(name: "List 2", includeInAggregatedList: true, atIndex: 2))
        XCTAssertTrue(app.existsShoppingList(name: "List 3", includeInAggregatedList: false, atIndex: 3))
    }

    func testShoppingListOptionsArePresentedCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let list1 = ShoppingListBuilder.create().with(name: "List 1").with(includeInAggregatedList: true).toLaunchArgumentString()
        app.launch(with: list1)

        // Execution:

        app.openOptionsMenuForShoppingList(name: "List 1")

        // Expected results:

        XCTAssertTrue(app.existsSheet(withTitle: "Options for list List 1"))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_TRANSFER_ITEMS))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_TRANSFER_AND_DELETE_ITEMS))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_DELETE_DONE_ITEMS))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_UNCHECK_ITEMS))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_CLEAR_LIST))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTTON_TITLE_EDIT_LIST))
        XCTAssertTrue(app.existsSheetButton(withTitle: app.BUTON_TITLE_SHARE_LIST))
    }

    func testShoppingListEditViewCanBeOpened() {
        let app = XCUIApplication()

        // Precondition:

        let shoppingList = ShoppingListBuilder.create().with(name: "List 1").toLaunchArgumentString()
        app.launch(with: shoppingList)

        // Execution:

        app.openEditViewForShoppingList(name: "List 1")

        // Expected results:

        XCTAssert(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_EDIT_SHOPPING_LIST))
    }
}

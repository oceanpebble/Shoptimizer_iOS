// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _EditSupplitesItemCategoryTest: XCTestCase {
    
    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_EDIT_CATEGORY))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_CANCEL))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_SAVE))
        XCTAssertTrue(app.textFields.count == 1)
        XCTAssertTrue(app.textFields.element.value as? String == "Category 1")
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_NAME))
    }

    func test_ChangesName() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Category 1").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Category 1").toLaunchArgumentString()
        app.launch(with: category, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemCategoryDetails(name: "Category 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.isCategorySelected(name: "Category 2"))
        app.tapButton(title: "Edit Item")
        app.tapSaveButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.isCategorySelected(name: "Category 2"))
    }

    func testShowsWarningWhenChangingNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Category 2").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category1, category2, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemCategoryDetails(name: "Category 2")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotChangeNameToUsedOne() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Category 2").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category1, category2, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemCategoryDetails(name: "Category 2")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 1"))
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 2"))
    }

    func testShowsWarningWhenChangingNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemCategoryDetails(name: "")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemCategoryDetails(name: "")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 1"))
    }

    func testShowsWarningWhenChangingNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func testDoesNotChangeNameToWhiteSpaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.stepIntoSuppliesItemCategory(name: "Category 1")
        app.enterSuppliesItemDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 1"))
    }

    func test_DeleteCategory() {
        let app = XCUIApplication()

        // Precondition:

        let category1 = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let category2 = SuppliesItemCategoryBuilder.create().with(name: "Category 2").toLaunchArgumentString()
        let item1 = SuppliesItemBuilder.create().with(name: "Item 1").with(category: "Category 1").toLaunchArgumentString()
        let item2 = SuppliesItemBuilder.create().with(name: "Item 2").with(category: "Category 1").toLaunchArgumentString()
        app.launch(with: category1, category2, item1, item2)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.deleteSuppliesItemCategory(name: "Category 1")

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 2"))
        XCTAssertFalse(app.isCategorySelected(name: "Category 2"))
        app.tapButton(title: "Edit Item")
        app.tapCancelButton()
        app.stepIntoSuppliesItem(name: "Item 2")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsSuppliesItem(name: "Category 2"))
        XCTAssertFalse(app.isCategorySelected(name: "Category 2"))
    }
}

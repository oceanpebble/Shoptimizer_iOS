// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _SuppliesItemsOptionsTests: XCTestCase {

    func testShowsSuccessAlertWhenCreatingList() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem = SuppliesItemBuilder.create().with(name: "Item 2").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 0.99).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        app.launch(with: suppliesItem)

        // Execution:

        app.switchToSuppliesTab()
        app.createListFromSupplies()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_SUCCESS))
    }

    func testCreatesList() {
        let app = XCUIApplication()

        // Precondition:

        let suppliesItem1 = SuppliesItemBuilder.create().with(name: "Item 1").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 1.01).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem2 = SuppliesItemBuilder.create().with(name: "Item 2").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 0.99).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem3 = SuppliesItemBuilder.create().with(name: "Item 3").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 1000).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem4 = SuppliesItemBuilder.create().with(name: "Item 4").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 999).with(curUnit: app.UNIT_ML).toLaunchArgumentString()
        let suppliesItem5 = SuppliesItemBuilder.create().with(name: "Item 5").with(minAmount: 1).with(minUnit: app.UNIT_L).with(curAmount: 0.8).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        let suppliesItem6 = SuppliesItemBuilder.create().with(name: "Item 6").with(minAmount: 0).with(minUnit: app.UNIT_L).with(curAmount: 0).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem7 = SuppliesItemBuilder.create().with(name: "Item 7").with(minAmount: 1).with(minUnit: app.UNIT_KG).with(curAmount: 0).with(curUnit: app.UNIT_L).toLaunchArgumentString()
        let suppliesItem8 = SuppliesItemBuilder.create().with(name: "Item 8").with(minAmount: 0).with(minUnit: app.UNIT_L).with(curAmount: 0.8).with(curUnit: app.UNIT_KG).toLaunchArgumentString()
        app.launch(with: suppliesItem1, suppliesItem2, suppliesItem3, suppliesItem4, suppliesItem5, suppliesItem6, suppliesItem7, suppliesItem8)

        // Execution:

        app.switchToSuppliesTab()
        app.createListFromSupplies()
        app.confirmAlert(withTitle: app.TITLE_ALERT_SUCCESS)
        app.switchToShoppingListsTab()

        // Expected results:

        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_PREFIX_REFILL_SUPPLIES))
        app.stepIntoShoppingList(name: app.LIST_PREFIX_REFILL_SUPPLIES)
        XCTAssertTrue(app.cells.count == 2)
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 2", amountQuantity: "0.01", amountUnit: app.UNIT_L))
        XCTAssertTrue(app.existsShoppingListItem(name: "Item 4", amountQuantity: "0.001", amountUnit: app.UNIT_L))
    }

    func testDoesNotShowSuccessAlertWhenCreatingList() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.switchToSuppliesTab()
        app.createListFromSupplies()

        // Expected results:

        XCTAssertFalse(app.existsAlert(withTitle: app.TITLE_ALERT_SUCCESS))
    }

    func testDoesNotCreateListWhenNoSuppliesItemsAvailable() {
        let app = XCUIApplication()

        // Precondition:

        app.launchWithoutArguments()

        // Execution:

        app.switchToSuppliesTab()
        app.createListFromSupplies()
        app.switchToShoppingListsTab()

        // Expected results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsShoppingList(name: app.LIST_NAME_AGGREGATED_LIST))
    } 
}

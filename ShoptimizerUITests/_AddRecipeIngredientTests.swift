// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _AddRecipeIngredientTests: XCTestCase {
    
    func testUIElementsAppearCorrectly() {
        // TODO: implement
        XCTFail()
    }
    
    func testShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateIngredientWhenNameIsEmpty() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_INGREDIENTS))
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "    ")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }
    
    func testDoesNotCreateIngredientWhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testCreatesIngredientWithName() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1"))
    }
    
    func testCreatesIngredientWithNameAndAmount() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "2.5", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "2.5", amountUnit: app.UNIT_G))
    }
    
    func testCreatesIngredientWhenAmountQuantityIsZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "0", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "0", amountUnit: app.UNIT_G))
    }
    
    func testShowsWarningWhenAmountQuantityIsLessThenZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "-1", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateIngredientWhenAmountQuantityIsLessThenZero() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "-1", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "1a", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_INVALID_DATA))
    }
    
    func testDoesNotCreateIngredientWhenAmountQuantityHasLetters() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "1a", amountUnit: app.UNIT_G)
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_INVALID_DATA)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
    
    func testShowsWarningWhenCreatingDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1")
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }
    
    func testDoesNotCreateDuplicate() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
    }
    
    func testDoesCreateDuplicateInDifferentRecipe() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe1 = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let recipe2 = RecipeBuilder.create().with(name: "Recipe 2").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(amountQuantity: 5).with(amountUnit: app.UNIT_L).with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe1, recipe2, ingredient)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 2")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1", amountQuantity: "3", amountUnit: app.UNIT_PIECE)
        app.tapSaveButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "3", amountUnit: app.UNIT_PIECE))
        app.returnToRecipesOverviewFromRecipe(name: "Recipe 2")
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        XCTAssertTrue(app.existsRecipeIngredient(name: "Ingredient 1", amountQuantity: "5", amountUnit: app.UNIT_L))
    }
    
    func testTappingCancelButtonDoesNotCreateIngredient() {
        let app = XCUIApplication()
        
        // Precondition:
        
        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe)
        
        // Execution:
        
        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.tapAddButton()
        app.enterRecipeIngredientDetails(name: "Ingredient 1")
        app.tapCancelButton()
        
        // Expected results:
        
        XCTAssertTrue(app.cells.count == 0)
    }
}

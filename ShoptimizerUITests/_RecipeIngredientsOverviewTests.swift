// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import XCTest

class _RecipeIngredientsOverviewTests: XCTestCase {

    func testDeletesIngredient() {
        let app = XCUIApplication()

        // Precondition:

        let recipe = RecipeBuilder.create().with(name: "Recipe 1").toLaunchArgumentString()
        let ingredient = RecipeIngredientBuilder.create().with(name: "Ingredient 1").with(recipeName: "Recipe 1").toLaunchArgumentString()
        app.launch(with: recipe, ingredient)

        // Execution:

        app.switchToRecipesTab()
        app.stepIntoRecipe(name: "Recipe 1")
        app.openRecipeIngredientsOverview()
        app.deleteRecipeIngredient(name: "Ingredient 1")

        // Expected results:

        XCTAssertTrue(app.cells.count == 0)
    }
}

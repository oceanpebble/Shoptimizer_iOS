// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func existsSheet(withTitle title: String) -> Bool {
        return sheets[title].waitForExistence(timeout: 5.0)
    }
    
    func existsSheetButton(withTitle title: String) -> Bool {
        return buttons[title].waitForExistence(timeout: 5.0)
    }
}

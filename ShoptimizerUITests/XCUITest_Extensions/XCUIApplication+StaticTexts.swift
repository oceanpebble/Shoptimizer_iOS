// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func existsStaticText(containing labels: String...) -> Bool {
        return staticText(containing: labels).exists
    }
    
    func staticText(containing labels: [String]) -> XCUIElement {
        return filterStaticTexts(containing: labels).element
    }
    
    private func filterStaticTexts(containing labels: [String]) -> XCUIElementQuery {
        var texts = self.staticTexts
        
        for label in labels {
            texts = texts.containing(NSPredicate(format: "label CONTAINS %@", label))
        }
        return texts
    }
}

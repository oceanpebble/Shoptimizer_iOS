// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func localizeDouble(from value: String) -> String {
        var returnValue = value
        
        if let separator = Locale.current.decimalSeparator,
            separator == "," {
            returnValue = value.replacingOccurrences(of: ".", with: ",")
        }
        
        let format = NumberFormatter()
        format.numberStyle = .decimal
        format.maximumFractionDigits = Constants.MAX_FRACTION_DIGITS
        
        guard let number = format.number(from: returnValue) else { return returnValue }
        
        return format.string(from: number)!
    }
    
    func localizePrice(from value: String) -> String {
        var returnValue = value
        
        if let separator = Locale.current.decimalSeparator,
            separator == "," {
            returnValue = value.replacingOccurrences(of: ".", with: ",")
        }
        
        let format = NumberFormatter()
        format.numberStyle = .decimal
        format.minimumFractionDigits = 2
        format.maximumFractionDigits = 2
        
        guard let number = format.number(from: returnValue) else { return returnValue }
        
        return format.string(from: number)!
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIElement {

    var isOn : Bool {
        return value as? String == "1"
    }
}

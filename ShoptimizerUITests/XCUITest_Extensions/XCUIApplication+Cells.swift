// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func deleteCell(containing labels: [String]) {
        let _ = cell(containing: labels).waitForExistence(timeout: 5.0)
        cell(containing: labels).swipeLeft()
        tapDeleteButton()
    }
    
    func existsCell(containing labels: [String]) -> Bool {
        return cell(containing: labels).exists
    }
    
    func existsCell(containing labels: [String], atIndex index: Int) -> Bool {
        return testExistsCell(containing: labels, atIndex: index)
    }
    
    func cell(containing labels: [String]) -> XCUIElement {
        return findCells(containing: labels).element
    }
    
    func findCells(containing labels: [String]) -> XCUIElementQuery {
        var filteredCells = self.cells
        
        for label in labels {
            filteredCells = filteredCells.containing(NSPredicate(format: "label CONTAINS %@", label))
        }
        
        return filteredCells
    }
    
    func tapCell(containing labels: String...) {
        tapCell(containing: labels)
    }
    
    func tapCell(containing labels: [String]) {
        cell(containing: labels).tap()
    }
    
    func tapCellLong(containing labels: String...) {
        tapCellLong(containing: labels)
    }
    
    func tapCellLong(containing labels: [String]) {
        cell(containing: labels).press(forDuration: TimeInterval(floatLiteral: 0.6))
    }
    
    func swipeCellRight(containing labels: String...) {
        swipeCellRight(containing: labels)
    }
    
    func swipeCellRight(containing labels: [String]) {
        cell(containing: labels).swipeRight()
    }
    
    func swipeCellLeft(containing labels: String...) {
        swipeCellLeft(containing: labels)
    }
    
    func swipeCellLeft(containing labels: [String]) {
        cell(containing: labels).swipeLeft()
    }
    
    func testExistsCell(containing labels: [String], atIndex index: Int) -> Bool {
        let cell = cells.element(boundBy: index)
        
        for label in labels {
            let text = cell.staticTexts.containing(NSPredicate(format: "label CONTAINS %@", label)).element
            if !text.exists {
                return false;
            }
        }
        
        return true
    }
}

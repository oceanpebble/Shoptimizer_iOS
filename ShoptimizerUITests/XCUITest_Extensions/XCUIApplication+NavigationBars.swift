// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func navigationBar(hasIdentifier identifier: String) -> Bool {
        return navigationBars.element.identifier == identifier
    }

    func navigationBar(hasButtonWithTitle title: String) -> Bool {
        return navigationBars.element.buttons[title].exists
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    final func launchWithoutArguments() {
        launch(with: "")
    }

    final func launch(with arguments: String ...) {
        launchArguments = arguments
        launchArguments += ["ClearAll"]
        launch()
    }
}

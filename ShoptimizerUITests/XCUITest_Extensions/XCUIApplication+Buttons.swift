// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {
    
    func existsSingleButton(matching title: String) -> Bool {
        return buttons[title].exists
    }

    func tapButton(title: String) {
        buttons[title].tap()
    }
}

// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIApplication {

    func existsAlert(withTitle title: String) -> Bool {
        return alerts[title].exists
    }

    func confirmAlert() {
        alerts.element.buttons[BUTTON_TITLE_OK].tap()
    }

    func confirmAlert(withTitle title: String) {
        alerts[title].buttons[BUTTON_TITLE_OK].tap()
    }
    
    func dismissAlert() {
        let _ = alerts.element.buttons[BUTTON_TITLE_CANCEL].waitForExistence(timeout: 5.0)
        alerts.element.buttons[BUTTON_TITLE_CANCEL].tap()
    }
}

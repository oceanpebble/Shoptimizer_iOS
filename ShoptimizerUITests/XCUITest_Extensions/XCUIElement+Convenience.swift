// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIElement {
    func clearAndTypeText(_ text: String) {
        tap()
        if let stringValue = self.value as? String {
            tap()
            let characterCount: Int = stringValue.count
            for _ in 0..<characterCount {
                typeText(XCUIKeyboardKey.delete.rawValue)
            }
        }
        typeText(text)
    }

    func tapAndTypeText(_ text: String) {
        tap()
        typeText(text)
    }
}

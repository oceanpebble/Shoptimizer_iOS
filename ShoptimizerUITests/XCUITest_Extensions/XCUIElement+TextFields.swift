// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

extension XCUIElement {

    var isEmptyOrPlaceholderValue: Bool {
        return value == nil
        || value as? String == ""
        || value as? String == placeholderValue
    }

    func hasNonNilStringValue(_ stringValue: String) -> Bool {
        return value != nil && value as? String == stringValue
    }
}

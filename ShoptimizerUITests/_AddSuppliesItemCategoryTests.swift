// SPDX-FileCopyrightText: 2023 The Shoptimizer authors
//
// SPDX-License-Identifier: MIT

import Foundation
import XCTest

class _AddSuppliesItemCategoryTests : XCTestCase {

    func testUIElementsAppearCorrectly() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()

        // Expected Results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_ADD_CATEGORY))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_CANCEL))
        XCTAssertTrue(app.navigationBar(hasButtonWithTitle: app.BUTTON_TITLE_SAVE))
        XCTAssertTrue(app.textFields.count == 1)
        XCTAssertTrue(app.textFields.element.placeholderValue == "Name")
        XCTAssertTrue(app.existsStaticText(containing: app.FIELD_NAME_LABEL_NAME))
    }

    func test_ShowsWarningWhenNameIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func test_DoesNotCreateIngredient_WhenNameIsEmpty() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.cells.count == 0)
    }

    func test_ShowsWarning_WhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "    ")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_NAME_MISSING))
    }

    func test_DoesNotCreateIngredient_WhenNameConsistsOfWhitespaceOnly() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "    ")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_NAME_MISSING)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.cells.count == 0)
    }

    func test_CreateCategory_WithName() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "Category 1")
        app.tapSaveButton()

        // Expected Results:

        XCTAssertTrue(app.cells.count == 1)
        XCTAssertTrue(app.existsCell(containing: ["Category 1"]))
    }

    func testShowsWarningWhenCreatingDuplicate() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "Category 1")
        app.tapSaveButton()

        // Expected results:

        XCTAssertTrue(app.existsAlert(withTitle: app.TITLE_ALERT_DUPLICATE))
    }

    func testDoesNotCreateDuplicate() {
        let app = XCUIApplication()

        // Precondition:

        let category = SuppliesItemCategoryBuilder.create().with(name: "Category 1").toLaunchArgumentString()
        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: category, item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "Category 1")
        app.tapSaveButton()
        app.confirmAlert(withTitle: app.TITLE_ALERT_DUPLICATE)
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.cells.count == 1)
    }

    func test_TappingCancelButtonDoesNotCreateIngredient() {
        let app = XCUIApplication()

        // Precondition:

        let item = SuppliesItemBuilder.create().with(name: "Item 1").toLaunchArgumentString()
        app.launch(with: item)

        // Execution:

        app.switchToSuppliesTab()
        app.stepIntoSuppliesItem(name: "Item 1")
        app.tapButton(title: app.BUTTON_TITLE_CATEGORIES)
        app.tapAddButton()
        app.enterSuppliesItemCategoryDetails(name: "Category 1")
        app.tapCancelButton()

        // Expected results:

        XCTAssertTrue(app.navigationBar(hasIdentifier: app.NAVBAR_TITLE_CATEGORIES))
        XCTAssertTrue(app.cells.count == 0)
    }
}
